Template de Solicitação Encaminhada

```
Título do Evento:
[Notificar Requisitante] - Solicitação Encaminhada para Análise

Assunto:
Processo de Solicitação de Auxílio Funeral - {Processo.Codigo} | Solicitação Encaminhada com Sucesso

Corpo da Mensagem:
Prezado(a),

Sua Solicitação do processo de "Solicitação de Auxílio Funeral" de código {Processo.Codigo} <b>foi encaminhada com sucesso</b> para análise.

Informações da solicitação:
Nome: {Formulario.nomeCooperado}
Matrícula: {Formulario.matricula}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```

---

Template de Regularizar Pendências

```
Título do Evento:
[Notificar Requisitante] - Regularizar Pendência(s)

Assunto:
Processo Solicitação de Auxílio Funeral - {Processo.Codigo} | Regularizar Pendência(s)

Corpo da Mensagem:
Prezado(a),

Sua solicitação referente ao processo "Solicitação de Auxílio Funeral" de código {Processo.Codigo} <b>foi solicitado mais informações</b>. Acesse a ferramenta INTEGRA para dar continuidade na sua solicitação.

Informações da solicitação:
Nome: {Formulario.nomeCooperado}
Matrícula: {Formulario.matricula}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```

---

Template de Novas Informações Encaminhadas com Sucesso

```
Título do Evento:
[Notificar Requisitante] - Novas Informações Encaminhadas com Sucesso

Assunto:
Processo Solicitação de Auxílio Funeral - {Processo.Codigo} | Novas Informações Encaminhadas com Sucesso

Corpo da Mensagem:
Prezado(a),

Sua solicitação referente ao processo "Solicitação de Auxílio Funeral" de código {Processo.Codigo}, que foi solicitado mais informações, <b>foi encaminhado com sucesso</b> para análise.

Informações da solicitação:
Nome: {Formulario.nomeCooperado}
Matrícula: {Formulario.matricula}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```

---

Template de Solicitação Rejeitada

```
Título do Evento:
[Notificar Requisitante] - Regularização Cancelada

Assunto:
Processo Solicitação de Auxílio Funeral - {Processo.Codigo} | Solicitação Cancelada

Corpo da Mensagem:
Prezado(a),

<b>E-mail para conhecimento.</b>
Você selecionou a opção de cancelamento da sua solicitação referente ao processo auxílio funeral de código {Processo.Codigo}.

Informações da solicitação:
Nome: {Formulario.nomeCooperado}
Matrícula: {Formulario.matricula}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```

---

Template de Solicitação Atendida

```
Título do Evento:
[Notificar Requisitante] - Solicitação Atendida

Assunto:
Processo de Solicitação de Auxílio Funeral - {Processo.Codigo} | Solicitação Atendida

Corpo da Mensagem:
Prezado(a),

Sua solicitação do processo de "Solicitação de Auxílio Funeral" de código {Processo.Codigo} <b>foi atendida</b>.

Informações da solicitação:
Nome: {Formulario.nomeCooperado}
Matrícula: {Formulario.matricula}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```
