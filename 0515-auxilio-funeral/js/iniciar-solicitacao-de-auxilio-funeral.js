const URL_TOKEN =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJg-CqsL3NypcCvWuQu01@2-7ejKkCN5V0IBK52p0D6LEWgV3g7RcWg6-TyDXTbVyzg__';
const URL_TOKEN_TST =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJvrH1QaVJrlzgWlabL@t4hFszHI6vLzXaLS9jlRj75U@ToRxL15Hh24treqU3l6log__';
const URL_CADASTRO_PF = cpf =>
  `https://servicos.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-fisica/v1/pessoa-fisica/${cpf}`;

let dados = {};

$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();

  ajustarFormulario();
  ajustarLayoutTabelaMultivalorada();

  definirCamposObrigatorios();
  desativarInputs();
  esconderSections();

  renderizarEventosPorClick();
  renderizarEventosPorMudanca();
});

const ajustarFormulario = () => {
  moment.locale('pt');

  // configurações default do formulário
  // $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');
  // $('#customBtn_Solicitação\\ Encaminhada\\ para\\ o\\ Diretor').removeAttr(
  //   'onclick'
  // );

  $('.input-group-append').show();
  $('.requisitante-herdeiro').show();
  $('.requerente').hide();

  $('#customBtn_Solicitação\\ Encaminhada\\ para\\ o\\ Diretor').hide();
  $('#customBtn_Solicitação\\ Encaminhada').hide();
};

const definirCamposObrigatorios = () => {
  $('inp:dataFalecimento').addClass('required');
  $('inp:banco').addClass('required');
  $('inp:agencia').addClass('required');
  $('inp:conta').addClass('required');
  $('inp:cpfcnpj').addClass('required');
  $('inp:nomeBancario').addClass('required');
  $('inp:nomeHerdeiroMeeiro').addClass('required');
  $('inp:requisitante').addClass('required');
};

const desativarInputs = () => {
  $('inp:nomeCooperado').attr('readonly', true);
  $('inp:matricula').attr('readonly', true);
  $('inp:posto').attr('readonly', true);
  $('inp:dataAssociacaoCooperado').attr('readonly', true);
};

const esconderSections = () => {
  $('.informacoes-do-cooperado').hide();
  $('.informacoes-dos-dados-bancario').hide();
  $('.informacoes-do-associado').hide();
  $('.informacoes-do-associado').hide();
};

const mostrarSections = () => {
  $('.informacoes-do-cooperado').show();
  $('.informacoes-dos-dados-bancario').show();
  $('.informacoes-do-associado').show();
  $('.informacoes-do-associado').show();
};

const renderizarEventosPorClick = () => {
  const infosCooperado = $('.buscar-informacoes-da-solicitacao');
  const btnAddNewRow = $('#BtnInsertNewRow');

  const btnSo = $('#customBtn_Solicitação\\ Encaminhada');
  const btnEx = $('#customBtn_Solicitação\\ Encaminhada\\ para\\ o\\ Diretor');

  infosCooperado.click(() => validarBuscaInformacoes());
  btnAddNewRow.click(() => ordernarSwitches());

  btnSo.click(() => {
    $('inp:passaPeloDiretor').val('Não');

    validarEnvioSolicitacao(btnSo);
  });

  btnEx.click(() => {
    $('inp:passaPeloDiretor').val('Sim');

    validarEnvioSolicitacao(btnEx);
  });
};

const renderizarEventosPorMudanca = () => {
  const input = $('inp:dataFalecimento');

  input.change(() => {
    if (!$('inp:dataFalecimento').val().length) return;

    const dataInputFormatada = `${input.val().split('/')[2]}-${
      input.val().split('/')[1]
    }-${input.val().split('/')[0]}`;

    const dataMesAnteriorFormatada = moment()
      .subtract(1, 'months')
      .format('YYYY-MM-DD');

    moment(dataInputFormatada).isBetween(
      moment(dataMesAnteriorFormatada),
      moment()
    )
      ? mostrarBotaoEncaminharDiretor()
      : esconderBotaoEncaminharDiretor();
  });

  input.trigger('change');
};

const mostrarBotaoEncaminharDiretor = () => {
  $('#customBtn_Solicitação\\ Encaminhada\\ para\\ o\\ Diretor').hide();
  $('#customBtn_Solicitação\\ Encaminhada').show();
};

const esconderBotaoEncaminharDiretor = () => {
  $('#customBtn_Solicitação\\ Encaminhada\\ para\\ o\\ Diretor').show();
  $('#customBtn_Solicitação\\ Encaminhada').hide();
};

const renderizarMudancaSwitch = elementoMudado => {
  const requisitanteMudado = pegarInputParaAlteracao(elementoMudado);

  desabilitarSwitches();
  $(elementoMudado).prop('checked', true);

  alterarInputBaseadoNoSwitch(requisitanteMudado);
};

const atribuirValorParaListaDeHerdeiros = () => {
  dados.listaDeHerdeiros = _.map(
    _.filter($('inp:nomeHerdeiroMeeiro'), elemento => elemento.value !== ''),
    elemento => elemento.value
  );

  $('inp:listaDeHerdeiros').val(dados.listaDeHerdeiros.join(', '));
};

const atribuirValorHerdeiroRequisitante = () => {
  const indexDoRequisitante = _.map(
    $('inp:requisitante'),
    e => e.value
  ).indexOf('Sim');

  $('inp:herdeiroRequisitanteDoAuxilio').val(
    $('inp:listaDeHerdeiros')
      .val()
      .split(',')[indexDoRequisitante]
  );

  $('.custom-control.custom-switch').remove();
};

const pegarInputParaAlteracao = elemento => {
  return $(elemento)
    .parent()
    .parent()
    .siblings()[2];
};

const alterarInputBaseadoNoSwitch = requisitanteMudado => {
  $('inp:requisitante').val('Não');

  $(requisitanteMudado)
    .find('input')
    .val('Sim');
};

const ajustarLayoutTabelaMultivalorada = () => {
  const btnAddRow = $('#BtnInsertNewRow');
  const btnDeleteRow = $('#coluna-table-auxilio-funeral button.btn-delete-mv');

  $(btnAddRow)
    .addClass('btn-primary btn-sm')
    .text('')
    .append(`<i class="fas fa-plus"></i>`);
  $(btnDeleteRow)
    .addClass('btn-danger btn-sm')
    .text('')
    .append(`<i class="fas fa-trash-alt"></i>`);
};

const ordernarSwitches = () => {
  const buttons = $('.custom-control-input');
  const labels = $('.custom-control-label');

  _.each(buttons, (button, index) => {
    $(button).attr('id', `custom-switch-${index}`);
    $(labels[index]).attr('for', `custom-switch-${index}`);

    $(button).prop('checked', false);
  });
};

const desabilitarSwitches = () => {
  const buttons = $('.custom-control-input');

  _.each(buttons, button => $(button).prop('checked', false));
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const buscarToken = async () => {
  await axios
    .get(URL_TOKEN)
    .then(success => {
      if (success.data.success[0].fields.access_token) {
        dados.token = `Bearer ${success.data.success[0].fields.access_token}`;
      } else {
        toastr.error('Erro de comunicaçao com o serviço!');
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      console.log(error);
    });
};

const validarBuscaInformacoes = async () => {
  toggleSpinner();

  dados.cpf = $('inp:cpf').val();

  if (!dados.token && !dados.cpf) {
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );

    toggleSpinner();

    return;
  }

  await buscarToken();
  await buscarDadosPeloCpf(dados.cpf);
};

const buscarDadosPeloCpf = async identificador => {
  axios
    .get(URL_CADASTRO_PF(identificador), {
      headers: {
        Authorization: dados.token,
        cooperativa: '0515',
      },
    })
    .then(res => {
      dados.nomeCooperado = R.path(['data', 'nome'], res);
      dados.matricula = R.path(['data', 'associacao', 'posto'], res);
      dados.posto = R.path(['data', 'associacao', 'posto'], res);
      dados.dataAssociacaoCooperado = moment(
        R.path(['data', 'associacao', 'dataAssociacaoCooperado'], res)
      ).format('DD/MM/YYYY');

      preencherFormulario();
      mostrarSections();
      toggleSpinner();
    })
    .catch(error => {
      toggleSpinner();
      console.log('error: ', error);

      toastr.error('Ocorreu um erro ao trazer as informações!', 'Atenção');
    });
};

const preencherFormulario = () => {
  $('inp:nomeCooperado').val(dados.nomeCooperado);
  $('inp:matricula').val(dados.matricula);
  $('inp:posto').val(dados.posto);
  $('inp:dataAssociacaoCooperado').val(dados.dataAssociacaoCooperado);

  $('inp:postoRequisitante').val(dados.posto);
  $('inp:dataSolicitacao').val(moment().format('DD [de] MMMM [de] YYYY'));
};

const validarInput = elemento => {
  !elemento.value.length
    ? $(elemento)
        .addClass('is-invalid')
        .removeClass('is-valid')
    : $(elemento)
        .addClass('is-valid')
        .removeClass('is-invalid');
};

const validarEnvioSolicitacao = btnEncaminharSolicitacao => {
  atribuirValorParaListaDeHerdeiros();

  const inputsObrigatorios = $('.form-integra .required');

  inputsObrigatorios.change(() => {
    _.each(inputsObrigatorios, elemento => validarInput(elemento));
  });

  inputsObrigatorios.trigger('change');

  if (!!$('.is-invalid').length) {
    toastr.error('Campos ou documentos obrigatórios!', 'Atenção');
  } else {
    atribuirValorHerdeiroRequisitante();

    $('inp:passaPeloDiretor').val() === 'Não'
      ? doAction('Solicitação Encaminhada', false, false)
      : doAction('Solicitação Encaminhada para o Diretor', false, false);

    $($(document.getElementsByClassName('cryo-confirm-dialog padded'))[0])
      .find('button.btn.btn-success')
      .click();
  }
};
