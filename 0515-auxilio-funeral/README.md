```
Nome do Processo - PR_0515 - Auxílio Funeral

Template = 0515 - Template Auxílio Funeral

Fluxograma
  Colaboradores [Unicred Florianópolis]
    - Iniciar Solicitação de Auxílio Funeral
      - Gerará o template Autorização para Saque de Auxílio Funeral

  Colaborador Requisitante
    - Anexar Documentos Obrigatórios para Solicitação de Auxílio Funeral
      - Documentos obrigatórios:
        Certidão de Óbito do Cooperado;
        Documento Identificação do Requisitante do Auxílio;
        Autorização para Saque de Auxílio Funeral Assinado;

  Unidade Administrativa [Unicred Florianópolis]
    - Aguardar Validação do Conselho de Administração para Solicitação de Auxílio Funeral
      - Funcionalidade Associação

  Financeiro [Unicred Central SC/PR]
    - Realizar Pagamento de Auxílio Funeral
      - Funcionalidade Associação
      - Funcionalidade Procedure Diferente Áreas

Formulário
  Informações da Solicitação
    CPF Cooperado [pesquisar]
    Valor Aprovado

  Informações do Cooperado
    Nome
    Matrícula
    Posto
    Data Associação
    Data Falecimento
      Até 30 dias após o falecimento
        Já se passaram os 30 dias da data do auxílio falecimento

  Informação(ões) do(s) Herdeiro(s)/Meeiro(a)
    Nome (Possibilidade de adicionar mais de um)
    Indicativo de quem está requisitando o auxílio

  Informações dos Dados Bancários
    Banco
    Agência
    Conta
    CPF/CNPJ
    Nome

  Informações Auxíliares
    Nome Requisitante
    Cooperativa Requisitante
    Área Requisitante
    Posto Requisitante
    Lista de Herdeiros
    Heideiro Requisitante do Auxílio
    Data Solicitação

  Campos Funcionalidade Associação ?
```

<!--
// 29983215934
// 04820886991
// 15506720963
// 00195413920
// 34367837904
// 09617469987
// 01829661949
// 41519469934
// 29074550959
-->

```
data falecimento
  - 30 antes ao de hoje, se não ele não pode requerer
funções padrões em todos as atividades
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();
data assinatura template em ingles
alterar campos para somente numero
  agencia
  conta
  cpf/cnpj
regularizar pendencias
  deixar visivel

aguardar validação conselho
  auxilio funeral aprovado

realizar pagamento de auxilio funeral
  botao pagamento realizado


validar os 30 dias auxilio funeral
  se sim segue o baile
  se nao botar a mais possibilidade de encaminhar pro diretor



o que liberar segundo
  ressarcimento 0515
  alteração de limite
    backoffice

  auxilio funeral
    recolher assinatura template - botar documentos obrigatorios
      Certidão de óbito do cooperado;
      Identificação do solicitante do auxílio;
      Autorização de saque do auxílio assinada por todos os sucessores legais com firmas reconhecida em cartório

    colocar valor na atividade do comite - Aguardar Validação do Conselho de Administração para Solicitação de Auxílio Funeral
      executor atividade (gabriela e juliana) - UA [Unicred Florianópolis]

    configurar financeiro
      0507 - financeiro centralizado - essa area e as que estao abaixo dela (3 areas, procedure)
      Financeiro [UCSC/PR]
        0507 - Colaborador(a) Financeiro
        (0507 - Financeiro)

        0507 - Colaborador(a) Gestão de Recursos
        (0507 - Gestão de Recursos)

        0507 - Supervisor(a) Financeiro
        (0507 - Financeiro Centralizado)

      Gabriela Gonçalves Da Silva e Juliana Niehues de Medeiros

      Gabriela Gonçalves Da Silva
        0515 - 90 - Operacional Financeiro	0515 - Colaborador(a) Operacional Financeiro
        0515 - 90 - Operacional Financeiro	0515_Colaborador(a) de contabilidade_docs. Contábeis
        0515 - 90 - Operacional Financeiro	0515_Colaborador(a) de contabilidade_envio docs ASS

      Juliana Niehues de Medeiros
        0515 - 90 - Operacional Financeiro	0515 - Colaborador(a) Operacional Financeiro
	      0515 - 90 - Operacional Financeiro	0515_Colaborador(a) de contabilidade_envio docs ASS

    apagar os testes antes de publicar

    0507 - Colaborador(a) Financeiro
    (0507 - Financeiro)

    0507 - Colaborador(a) Gestão de Recursos
    (0507 - Gestão de Recursos)

    0507 - Supervisor(a) Financeiro
    (0507 - Financeiro Centralizado)

    0515 - 90 - Operacional Financeiro	0515 - Colaborador(a) Operacional Financeiro

    0515 - Diretor(a) do Administrativo
    (0515 - 90 - Administrativo)
```
