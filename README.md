Começando a escrever o README (contribua sempre que for preciso!)
--------------------------------------------------------------------------------
Template de Solicitação Encaminhada

```
Título do Evento:
[Notificar Requisitante] - Solicitação Encaminhada para Análise

Assunto:
Processo de Solicitação de Assinatura Digital - {Processo.Codigo} | Solicitação Encaminhada com Sucesso

Corpo da Mensagem:
Prezado(a),

Sua Solicitação do processo de "Solicitação de Assinatura Digital" de código {Processo.Codigo} <b>foi encaminhada com sucesso</b> para análise.

Informações da solicitação:
Nome/Razão Social: {Formulario.nomeRazaoSocial}
Matrícula: {Formulario.matricula}
Contas: {Formulario.contas}
Tipo de Solicitação: {Formulario.tipoDeSolicitacao}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```
--------------------------------------------------------------------------------
Template de Regularizar Pendências

```
Título do Evento:
[Notificar Requisitante] - Regularizar Pendência(s)

Assunto:
Processo Solicitação de Assinatura Digital - {Processo.Codigo} | Regularizar Pendência(s)

Corpo da Mensagem:
Prezado(a),

Sua solicitação referente ao processo "Solicitação de Assinatura Digital" de código {Processo.Codigo} <b>foi solicitado mais informações</b>. Acesse a ferramenta INTEGRA para dar continuidade na sua solicitação.

Informações da solicitação:
Nome/Razão Social: {Formulario.nomeRazaoSocial}
Matrícula: {Formulario.matricula}
Contas: {Formulario.contas}
Tipo de Solicitação: {Formulario.tipoDeSolicitacao}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```
--------------------------------------------------------------------------------
Template de Novas Informações Encaminhadas com Sucesso

```
Título do Evento:
[Notificar Requisitante] - Novas Informações Encaminhadas com Sucesso

Assunto:
Processo Solicitação de Assinatura Digital - {Processo.Codigo} | Novas Informações Encaminhadas com Sucesso

Corpo da Mensagem:
Prezado(a),

Sua solicitação referente ao processo "Solicitação de Assinatura Digital" de código {Processo.Codigo}, que foi solicitado mais informações, <b>foi encaminhado com sucesso</b> para análise.

Informações da solicitação:
Nome/Razão Social: {Formulario.nomeRazaoSocial}
Matrícula: {Formulario.matricula}
Contas: {Formulario.contas}
Tipo de Solicitação: {Formulario.tipoDeSolicitacao}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```
--------------------------------------------------------------------------------
Template de Solicitação Atendida

```
Título do Evento:
[Notificar Requisitante] - Solicitação Atendida

Assunto:
Processo de Solicitação de Assinatura Digital - {Processo.Codigo} | Solicitação Atendida

Corpo da Mensagem:
Prezado(a),

Sua solicitação do processo de "Solicitação de Assinatura Digital" de código {Processo.Codigo} <b>foi atendida</b>.

Informações da solicitação:
Nome/Razão Social: {Formulario.nomeRazaoSocial}
Matrícula: {Formulario.matricula}
Contas: {Formulario.contas}
Tipo de Solicitação: {Formulario.tipoDeSolicitacao}

Link de acompanhamento:
https://bpm.e-unicred.com.br{Processo.LinkRelatorio}

Qualquer dúvida referente à ferramenta Integra, favor contatar a área de processos da Unicred Central SC/PR por meio dos contatos abaixo:
E-mail: processos.0507@unicred.com.br
Telefone: (48) 3221-5600
```
