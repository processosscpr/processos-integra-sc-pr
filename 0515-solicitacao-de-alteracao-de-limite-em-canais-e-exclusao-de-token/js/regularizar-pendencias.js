$(document).ready(function() {
  $(document.getElementById('inf-token')).hide();
  $('#divStatisticsTable').hide();
  $(document.getElementById('inf-alteracao-limite')).hide();
  $(document.getElementById('informacoes-auxiliares')).hide();
  exibirBotoes();

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
    document
      .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
      .setAttribute('style', 'height: 70px!important;');
  } else {
    adaptarMobile();
  }

  consultaContasCooperado(document.querySelector("[xname='inpconta']").value);
  setTimeout(function() {
    marcarUsuariosSelecionados();
  }, 2000);

  adaptarFormulario(document.querySelector("[xname='inpconta']").value);

  var imagemPesquisar =
    '<div id="botaoPesquisar" align="center"><i class="icon-search icon-black"></i></div>';
  document.querySelector(
    "[data-search-and-fill-for='inpconta']"
  ).innerHTML = imagemPesquisar;

  $(document.querySelector("[data-search-and-fill-for='inpconta']")).click(
    function() {
      setTimeout(function() {
        if (document.querySelector("[xname='inpconta']").value != '') {
          consultaContasCooperado(
            document.querySelector("[xname='inpconta']").value
          );

          adaptarFormularioAposConsultarContas();
        }
      }, 100);
    }
  );

  $(document.getElementById('limpar')).click(function() {
    limparDadosPesquisados();
  });

  if (document.getElementById('inptipoDeSolicitacao-0').checked == true) {
    $(document.getElementById('inf-alteracao-limite')).show();
  } else {
    $(document.getElementById('inf-alteracao-limite')).hide();
  }

  if (document.getElementById('inptipoDeSolicitacao-1').checked == true) {
    $(document.getElementById('inf-token')).show();
    document
      .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
      .setAttribute('required', 'S');
  } else {
    $(document.getElementById('inf-token')).hide();
  }

  document.getElementById('inptipoDeSolicitacao-0').onchange = function() {
    exibirBotoes();
    if (document.getElementById('inptipoDeSolicitacao-0').checked == true) {
      $(document.getElementById('inf-alteracao-limite')).show();
    } else {
      $(document.getElementById('inf-alteracao-limite')).hide();
    }
  };

  document.getElementById('inptipoDeSolicitacao-1').onchange = function() {
    if (document.getElementById('inptipoDeSolicitacao-1').checked == true) {
      $(document.getElementById('inf-token')).show();
      document
        .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
        .setAttribute('required', 'S');
    } else {
      $(document.getElementById('inf-token')).hide();
    }
  };

  $(
    document.getElementById('customBtn_Novas Informações\\ Encaminhadas')
  ).click(function() {
    verificaMaiorValor();
    aplicarUsuariosSelecionados();
    verificarDataSolicitacao();
    verificarCidade();
  });

  toggleNumeroProtocolo($('inp:possuiProtocoloDaSolicitacao').val());

  $('inp:possuiProtocoloDaSolicitacao').change(elemento =>
    toggleNumeroProtocolo(elemento.target.value)
  );
});

const toggleNumeroProtocolo = possuiProtocolo => {
  possuiProtocolo === 'Sim'
    ? obrigaNumeroProtocolo()
    : naoObrigaNumeroProtocolo();
};

const obrigaNumeroProtocolo = () => {
  $('inp:numeroProtocolo')[0].setAttribute('required', 'S');
  $('.numero-protocolo').show();
  $('#customBtn_Solicitação\\ Encaminhada').show();
  $('#customBtn_Template\\ Gerado').hide();
};

const naoObrigaNumeroProtocolo = () => {
  $('inp:numeroProtocolo').removeAttr('required');
  $('.numero-protocolo').hide();
  $('#customBtn_Solicitação\\ Encaminhada').hide();
  $('#customBtn_Template\\ Gerado').show();
};

function exibirBotoes() {
  if (document.getElementById('inptipoDeSolicitacao-0').checked) {
    $(
      document.getElementById(
        'customBtn_Novas Informações Encaminhadas Gerado Novamente'
      )
    ).show();
  } else if (
    document.getElementById('inptipoDeSolicitacao-0').checked == false
  ) {
    $(
      document.getElementById('customBtn_Novas Informações Encaminhadas')
    ).hide();
  }
}

function aplicarUsuariosSelecionados() {
  var listaUsuariosSelecionados = $(
    document.querySelector('#usuarios-gerados')
  ).find('input');
  var usuariosGerados = '';
  for (var i = 0; i < listaUsuariosSelecionados.length; i++) {
    if (listaUsuariosSelecionados[i].checked) {
      usuariosGerados += listaUsuariosSelecionados[i].value;
    }
  }
  $('inp:usuarioSelecionado').val(usuariosGerados);
}

function adaptarFormularioAposConsultarContas() {
  $(document.querySelector("[data-search-and-fill-for='inpconta']")).hide();
  $(document.querySelector('#limpar')).show();
}

function verificaMaiorValor() {
  var campoComparado = 0;
  var listaTabelaValorMaximo = document.querySelectorAll(
    '#tabela-valor-maximo'
  );
  var listaTabelaValorDiario = document.querySelectorAll(
    '#tabela-valor-diario'
  );

  //FOR PARA VALOR MAXIMO
  for (var i = 0; i < listaTabelaValorMaximo.length; i++) {
    var elementoPai = $(listaTabelaValorMaximo[i]).find('#tr-valor-maximo');
    var elementoFilhoCarregado = $(elementoPai).find(
      "[xname='inpvalorMaximoPorTransacao']"
    );
    var valorComPossivelPonto = ajustarValorComPonto(
      elementoFilhoCarregado[0].value
    );
    var valorFilhoCarregado = valorComPossivelPonto.replace(',', '.');

    var maiorValor = new Number(valorFilhoCarregado);
    if (campoComparado < maiorValor) {
      campoComparado = valorFilhoCarregado;
    }
  }

  //FOR PARA VALOR MINIMO
  for (var i = 0; i < listaTabelaValorMaximo.length; i++) {
    var elementoPai2 = $(listaTabelaValorMaximo[i]).find('#tr-valor-minimo');
    var elementoFilhoCarregado2 = $(elementoPai2).find(
      "[xname='inpvalorMinimoPorTransacao']"
    );
    var valorComPossivelPonto2 = ajustarValorComPonto(
      elementoFilhoCarregado2[0].value
    );
    var valorFilhoCarregado2 = valorComPossivelPonto2.replace(',', '.');

    var maiorValor2 = new Number(valorFilhoCarregado2);
    if (campoComparado < maiorValor2) {
      campoComparado = valorFilhoCarregado2;
    }
  }

  //FOR PARA VALOR DIARIO
  for (var i = 0; i < listaTabelaValorDiario.length; i++) {
    var elementoPai3 = $(listaTabelaValorDiario[i]).find('#tr-limite-diario');
    var elementoFilhoCarregado3 = $(elementoPai3).find(
      "[xname='inplimiteDiario']"
    );
    var valorComPossivelPonto3 = ajustarValorComPonto(
      elementoFilhoCarregado3[0].value
    );
    var valorFilhoCarregado3 = valorComPossivelPonto3.replace(',', '.');

    var maiorValor3 = new Number(valorFilhoCarregado3);
    if (campoComparado < maiorValor3) {
      campoComparado = valorFilhoCarregado3;
    }
  }

  document.querySelector("[xname='inpcampoComparado']").value = campoComparado;
}

function limparDadosPesquisados() {
  document.querySelector("[xname='inpconta']").value = '';

  $(document.querySelector('#limpar')).hide();
  $(document.querySelector("[data-search-and-fill-for='inpconta']")).show();

  document.querySelector("[xid='divnomeDoCooperado']").textContent = '';
  document.querySelector("[xid='divtipoDePessoa']").textContent = '';
  $('inp:tipoDeSolicitacao').prop('checked', false);

  $('inp:dispositivoDeSeguranca').prop('checked', false);
  $('inp:dispositivoDeSeguranca2').prop('checked', false);

  $('inp:tipoDeTransacao').val('');
  $('inp:valorMaximoPorTransacao').val('');
  $('inp:valorMinimoPorTransacao').val('');
  $('inp:limiteDiario').val('');
  $('inp:justificativaDaExclusaoDoToken').val('');

  for (
    var index =
      $(document.getElementById('usuarios-gerados')).find('label').length - 1;
    index >= 0;
    index--
  ) {
    $(document.getElementById('usuarios-gerados'))
      .find('label')
      [index].remove();
  }
}

function adaptarFormulario(conta) {
  var imagemPesquisar =
    '<div align="center"><img src="http://www.unicredcentralsc.com.br/CENTRAL/img-search.png" height="5" width="15"></div>';
  document.querySelector(
    "[data-search-and-fill-for='inpconta']"
  ).innerHTML = imagemPesquisar;

  document.querySelector('#conta').innerHTML +=
    "<button type='button' class='btn btn-default' id='limpar' style='margin-bottom:5px;margin-top:5px'> Limpar</button>";

  $(document.querySelector("[data-search-and-fill-for='inpconta']")).hide();

  document.querySelector("[xname='inpconta']").value = conta;

  $('#templateAssinado').hide();
  $('.template').hide();
  $('#usuarioSelecionado').hide();
}

function consultaContasCooperado(contaCooperado) {
  var c =
    'qw0Xk6xWKL563BI8VvBqJuuDhh-aNf5SCX6mSaXYPeyBdEpznIm1ir@kcVJK5HCsP-UUiauDYv4K5n1728WSVQ__';
  var qs = '';
  var xname = 'inp' + 'conta';
  var xvalue =
    $('form').find("[xname='" + xname + "']").length == 1
      ? $('form')
          .find("[xname='" + xname + "']")
          .val()
      : $field
          .parents('tr')
          .find("[xname='" + xname + "']")
          .val();
  if (xvalue == null) {
    xvalue = '';
  }
  qs += xname + '=' + xvalue;
  var query = contaCooperado; //filtro do JSON

  var jqxhr = $.ajax({
    type: 'post',
    dataType: 'json',
    url: '../api/json/datasource/get/1.0/' + c,
    data: (qs != '' ? '&' + qs : '') + '&f=' + query,
  }).done(function(data) {
    var inputHTML = '';
    if (data.success != null) {
      $.each(data.success, function(i, item) {
        inputHTML +=
          "<label for='inpusuarioGerados-" +
          i +
          "' class='radio'><input type='radio' name='inp12567' xname='inpusuarioGerados' id='inpusuarioGerados-" +
          i +
          "' label='Usuário(a):' required='S' value='" +
          item.cod +
          "'><font color='SteelBlue'>" +
          item.cod +
          '</font></label>';
      });
      var inputFinal = '<b>Usuário(s) do Cooperado: </b>  ' + inputHTML;

      document.getElementById('usuarios-gerados').innerHTML = inputFinal;
    }
  });
}

function ajustarValorComPonto(valor) {
  var auxiliarParaRemoverPontos;
  if (valor.length > 6) {
    auxiliarParaRemoverPontos = valor;
    for (var i = 0; i < valor.split('.').length - 1; i++) {
      auxiliarParaRemoverPontos = auxiliarParaRemoverPontos.replace('.', '');
    }
    return auxiliarParaRemoverPontos;
  } else {
    return valor;
  }
}

function marcarUsuariosSelecionados() {
  var listaUsuarios = $(document.querySelector('#usuarios-gerados')).find(
    'input'
  );
  var campoUsuarioGerado = document.querySelector(
    '[xname="inpusuarioSelecionado"]'
  );
  for (var i = 0; i < listaUsuarios.length; i++) {
    if (listaUsuarios[i].value == campoUsuarioGerado.value) {
      listaUsuarios[i].checked = true;
    }
  }
}

function aplicarUsuariosSelecionados() {
  var listaUsuariosSelecionados = $(
    document.querySelector('#usuarios-gerados')
  ).find('input');
  var usuariosGerados = '';
  for (var i = 0; i < listaUsuariosSelecionados.length; i++) {
    if (listaUsuariosSelecionados[i].checked) {
      usuariosGerados += listaUsuariosSelecionados[i].value;
    }
  }
  $('inp:usuarioSelecionado').val(usuariosGerados);
  $('inp:usuarioGerados').val(usuariosGerados);
}

function verificarDataSolicitacao() {
  var data = new Date();
  $('inp:dia').val(data.getDate());
  $('inp:ano').val(data.getFullYear());

  if (data.getMonth() == 0) {
    $('inp:mes').val('janeiro');
  } else if (data.getMonth() == 1) {
    $('inp:mes').val('fevereiro');
  } else if (data.getMonth() == 2) {
    $('inp:mes').val('março');
  } else if (data.getMonth() == 3) {
    $('inp:mes').val('abril');
  } else if (data.getMonth() == 4) {
    $('inp:mes').val('maio');
  } else if (data.getMonth() == 5) {
    $('inp:mes').val('junho');
  } else if (data.getMonth() == 6) {
    $('inp:mes').val('julho');
  } else if (data.getMonth() == 7) {
    $('inp:mes').val('agosto');
  } else if (data.getMonth() == 8) {
    $('inp:mes').val('setembro');
  } else if (data.getMonth() == 9) {
    $('inp:mes').val('outubro');
  } else if (data.getMonth() == 10) {
    $('inp:mes').val('novembro');
  } else if (data.getMonth() == 10) {
    $('inp:mes').val('dezembro');
  }
}

function verificarCidade() {
  if ($('inp:agencia').val() == 'Ag. Tenente Silveira') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. São José') {
    $('inp:cidade').val('São José');
  } else if ($('inp:agencia').val() == 'Ag. Beira Mar') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Madre Benvenuta') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Palhoça') {
    $('inp:cidade').val('Palhoça');
  } else if ($('inp:agencia').val() == 'Ag. Baía Sul') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Garopaba') {
    $('inp:cidade').val('Garopaba');
  } else if ($('inp:agencia').val() == 'Ag. Estreito') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Gama Deca') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Terra Firme') {
    $('inp:cidade').val('São José');
  } else if ($('inp:agencia').val() == 'Ag. Ingleses') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Sul Da Ilha') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Biguacu') {
    $('inp:cidade').val('Biguaçu');
  } else {
    $('inp:cidade').val('Florianópolis');
  }
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();
  document
    .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
    .setAttribute('style', 'height: 40px!important;');
  document
    .querySelector("[xname='inpusuarioGerados']")
    .setAttribute('style', '');
  document
    .querySelector("[xname='inpconta']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inptipoDeTransacao']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpvalorMinimoPorTransacao']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpvalorMaximoPorTransacao']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inplimiteDiario']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
}
