$(document).ready(function() {
  $('inp:numeroProtocolo').val().length > 0
    ? $('.numero-protocolo').show()
    : $('.numero-protocolo').hide();

  $(document.getElementById('inf-token')).hide();
  $('#usuarios-gerados').hide();
  $('#template').hide();
  $('#templateAssinado').attr('colspan', '2');
  $(document.getElementById('inf-alteracao-limite')).hide();
  $(document.getElementById('informacoes-auxiliares')).hide();

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
    document
      .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
      .setAttribute('style', 'height: 70px!important;');
  } else {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }

  if (
    document.querySelector("[xname='inptipoDeSolicitacao']").value ==
    'Alteração de Limite'
  ) {
    $(document.getElementById('inf-alteracao-limite')).show();
    $('#templateAssinado').show();
  } else if (
    document.querySelector("[xname='inptipoDeSolicitacao']").value ==
    'Exclusão de Token'
  ) {
    $(document.getElementById('inf-token')).show();
    $('#templateAssinado').hide();
  } else if (
    document.querySelector("[xname='inptipoDeSolicitacao']").value ==
    'Alteração de Limite, Exclusão de Token'
  ) {
    $(document.getElementById('inf-alteracao-limite')).show();
    $(document.getElementById('inf-token')).show();
    $('#templateAssinado').show();
  }
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}
