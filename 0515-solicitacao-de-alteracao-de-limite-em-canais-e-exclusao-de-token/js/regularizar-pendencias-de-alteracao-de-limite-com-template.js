$(document).ready(function() {
  ajustarFormulario();

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
    document
      .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
      .setAttribute('style', 'height: 70px!important;');
  } else {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }

  if (
    document.querySelector("[xname='inptipoDeSolicitacao']").value ==
    'Alteração de Limite'
  ) {
    $(document.getElementById('inf-alteracao-limite')).show();
  } else if (
    document.querySelector("[xname='inptipoDeSolicitacao']").value ==
    'Exclusão de Token'
  ) {
    $(document.getElementById('inf-token')).show();
  } else if (
    document.querySelector("[xname='inptipoDeSolicitacao']").value ==
    'Alteração de Limite, Exclusão de Token'
  ) {
    $(document.getElementById('inf-alteracao-limite')).show();
    $(document.getElementById('inf-token')).show();
  }
});

function ajustarFormulario() {
  $('#usuarios-gerados').hide();
  $('#divStatisticsTable').hide();
  $(document.getElementById('inf-token')).hide();
  $(document.getElementById('inf-alteracao-limite')).hide();
  $(document.getElementById('informacoes-auxiliares')).hide();
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}
