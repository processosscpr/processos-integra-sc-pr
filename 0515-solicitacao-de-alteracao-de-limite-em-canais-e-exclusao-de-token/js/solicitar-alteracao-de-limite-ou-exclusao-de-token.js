$(document).ready(function() {
  verificarPosicaoIniciadora();
  ajustarFormulario();
  $(document.getElementById('inf-token')).hide();
  $(document.getElementById('inf-alteracao-limite')).hide();
  $(document.getElementById('informacoes-auxiliares')).hide();
  $(document.querySelector('#divStatisticsTable')).hide();
  $(document.querySelector('#btnCancel')).hide();
  $('#template').hide();
  $('#usuarioSelecionado').hide();
  document.querySelector("[xname='inpexclusaoDeToken']").value = 'Não';
  var imagemPesquisar =
    '<div id="botaoPesquisar" align="center"><i class="icon-search icon-black"></i></div>';
  document.querySelector(
    "[data-search-and-fill-for='inpconta']"
  ).innerHTML = imagemPesquisar;

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
    document
      .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
      .setAttribute('style', 'height: 70px!important;');
  } else {
    adaptarMobile();
  }

  $(document.getElementById('limpar')).click(function() {
    limparDadosPesquisados();
  });

  $(document.querySelector("[data-search-and-fill-for='inpconta']")).click(
    function() {
      setTimeout(function() {
        if (document.querySelector("[xname='inpconta']").value != '') {
          consultaContasCooperado(
            document.querySelector("[xname='inpconta']").value
          );
          adaptarFormularioAposConsultarConta();
        }
      }, 100);
    }
  );

  $(document.getElementById('customBtn_Solicitação Encaminhada')).click(
    function() {
      verificaMaiorValor();
      verificarDataSolicitacao();
      verificarCidade();
    }
  );

  document.getElementById('inptipoDeSolicitacao-0').onchange = function() {
    exibirBotoes();
    if (document.getElementById('inptipoDeSolicitacao-0').checked == true) {
      $(document.getElementById('inf-alteracao-limite')).show();
      document
        .querySelector("[xname='inptipoDeTransacao']")
        .setAttribute('required', 'S');
      document
        .querySelector("[xname='inplimiteDiario']")
        .setAttribute('required', 'S');
      document
        .querySelector("[xname='inpvalorMaximoPorTransacao']")
        .setAttribute('required', 'S');
      document
        .querySelector("[xname='inpvalorMinimoPorTransacao']")
        .setAttribute('required', 'S');
    } else {
      $(document.getElementById('inf-alteracao-limite')).hide();
      document
        .querySelector("[xname='inptipoDeTransacao']")
        .setAttribute('required', 'N');
      document
        .querySelector("[xname='inplimiteDiario']")
        .setAttribute('required', 'N');
      document
        .querySelector("[xname='inpvalorMaximoPorTransacao']")
        .setAttribute('required', 'N');
      document
        .querySelector("[xname='inpvalorMinimoPorTransacao']")
        .setAttribute('required', 'N');
    }
  };

  document.getElementById('inptipoDeSolicitacao-1').onchange = function() {
    exibirBotoes();
    if (document.getElementById('inptipoDeSolicitacao-1').checked) {
      $(document.getElementById('inf-token')).show();
      document
        .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
        .setAttribute('required', 'S');
      document.querySelector("[xname='inpexclusaoDeToken']").value = 'Sim';
    } else {
      $(document.getElementById('inf-token')).hide();
      document
        .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
        .setAttribute('required', 'N');
      document.querySelector("[xname='inpexclusaoDeToken']").value = 'Não';
    }
  };

  $('.numero-protocolo').hide();

  $('inp:possuiProtocoloDaSolicitacao').change(elemento =>
    toggleNumeroProtocolo(elemento.target.value)
  );
});

const atribuirUsuarioSelecionado = () => {
  $('inp:usuarioSelecionado').val($('inp:usuarioGerados').val());
};

const toggleNumeroProtocolo = possuiProtocolo => {
  possuiProtocolo === 'Sim'
    ? obrigaNumeroProtocolo()
    : naoObrigaNumeroProtocolo();
};

const obrigaNumeroProtocolo = () => {
  $('inp:numeroProtocolo')[0].setAttribute('required', 'S');
  $('.numero-protocolo').show();
};

const naoObrigaNumeroProtocolo = () => {
  $('inp:numeroProtocolo').removeAttr('required');
  $('.numero-protocolo').hide();
};

function ajustarFormulario() {
  document.querySelector('#conta').innerHTML +=
    "<button type='button' class='btn btn-default' id='limpar' style='margin-bottom:5px;margin-top:5px'> Limpar</button>";
  $(document.querySelector('#limpar')).hide();
}

function adaptarFormularioAposConsultarConta() {
  $(document.querySelector("[data-search-and-fill-for='inpconta']")).hide();
  $(document.querySelector('#limpar')).show();
}

function limparDadosPesquisados() {
  document.querySelector("[xname='inpconta']").value = '';

  $(document.querySelector('#limpar')).hide();
  $(document.querySelector("[data-search-and-fill-for='inpconta']")).show();

  document.querySelector("[xid='divnomeDoCooperado']").textContent = '';
  document.querySelector("[xid='divtipoDePessoa']").textContent = '';

  for (
    var index =
      $(document.getElementById('usuarios-gerados')).find('label').length - 1;
    index >= 0;
    index--
  ) {
    $(document.getElementById('usuarios-gerados'))
      .find('label')
      [index].remove();
  }
}

function consultaContasCooperado(contaCooperado) {
  var c =
    'qw0Xk6xWKL563BI8VvBqJuuDhh-aNf5SCX6mSaXYPeyBdEpznIm1ir@kcVJK5HCsP-UUiauDYv4K5n1728WSVQ__';
  var qs = '';
  var xname = 'inp' + 'conta';
  var xvalue =
    $('form').find("[xname='" + xname + "']").length == 1
      ? $('form')
          .find("[xname='" + xname + "']")
          .val()
      : $field
          .parents('tr')
          .find("[xname='" + xname + "']")
          .val();
  if (xvalue == null) {
    xvalue = '';
  }
  qs += xname + '=' + xvalue;
  var query = contaCooperado; //filtro do JSON

  var jqxhr = $.ajax({
    type: 'post',
    dataType: 'json',
    url: '../api/json/datasource/get/1.0/' + c,
    data: (qs != '' ? '&' + qs : '') + '&f=' + query,
  }).done(function(data) {
    var inputHTML = '';
    if (data.success != null) {
      $.each(data.success, function(i, item) {
        inputHTML +=
          "<label for='inpusuarioGerados-" +
          i +
          "' class='radio'><input type='radio' name='inp12567' xname='inpusuarioGerados' id='inpusuarioGerados-" +
          i +
          "' label='Usuário(a):' required='S' onclick='atribuirUsuarioSelecionado()' value='" +
          item.cod +
          "'><font color='SteelBlue'>" +
          item.cod +
          '</font></label>';
      });
      var inputFinal = '<b>Usuário(s) do Cooperado: </b>  ' + inputHTML;

      document.getElementById('usuarios-gerados').innerHTML = inputFinal;
    }
  });
}

function verificaMaiorValor() {
  var campoComparado = 0;
  var listaTabelaValorMaximo = document.querySelectorAll(
    '#tabela-valor-maximo'
  );
  var listaTabelaValorDiario = document.querySelectorAll(
    '#tabela-valor-diario'
  );

  //FOR PARA VALOR MAXIMO
  for (var i = 0; i < listaTabelaValorMaximo.length; i++) {
    var elementoPai = $(listaTabelaValorMaximo[i]).find('#tr-valor-maximo');
    var elementoFilhoCarregado = $(elementoPai).find(
      "[xname='inpvalorMaximoPorTransacao']"
    );
    var valorComPossivelPonto = ajustarValorComPonto(
      elementoFilhoCarregado[0].value
    );
    var valorFilhoCarregado = valorComPossivelPonto.replace(',', '.');

    var maiorValor = new Number(valorFilhoCarregado);
    if (campoComparado < maiorValor) {
      campoComparado = valorFilhoCarregado;
    }
  }

  //FOR PARA VALOR MINIMO
  for (var i = 0; i < listaTabelaValorMaximo.length; i++) {
    var elementoPai2 = $(listaTabelaValorMaximo[i]).find('#tr-valor-minimo');
    var elementoFilhoCarregado2 = $(elementoPai2).find(
      "[xname='inpvalorMinimoPorTransacao']"
    );
    var valorComPossivelPonto2 = ajustarValorComPonto(
      elementoFilhoCarregado2[0].value
    );
    var valorFilhoCarregado2 = valorComPossivelPonto2.replace(',', '.');

    var maiorValor2 = new Number(valorFilhoCarregado2);
    if (campoComparado < maiorValor2) {
      campoComparado = valorFilhoCarregado2;
    }
  }

  //FOR PARA VALOR DIARIO
  for (var i = 0; i < listaTabelaValorDiario.length; i++) {
    var elementoPai3 = $(listaTabelaValorDiario[i]).find('#tr-limite-diario');
    var elementoFilhoCarregado3 = $(elementoPai3).find(
      "[xname='inplimiteDiario']"
    );
    var valorComPossivelPonto3 = ajustarValorComPonto(
      elementoFilhoCarregado3[0].value
    );
    var valorFilhoCarregado3 = valorComPossivelPonto3.replace(',', '.');

    var maiorValor3 = new Number(valorFilhoCarregado3);
    if (campoComparado < maiorValor3) {
      campoComparado = valorFilhoCarregado3;
    }
  }

  document.querySelector("[xname='inpcampoComparado']").value = campoComparado;
}

function ajustarValorComPonto(valor) {
  var auxiliarParaRemoverPontos;
  if (valor.length > 6) {
    auxiliarParaRemoverPontos = valor;
    for (var i = 0; i < valor.split('.').length - 1; i++) {
      auxiliarParaRemoverPontos = auxiliarParaRemoverPontos.replace('.', '');
    }
    return auxiliarParaRemoverPontos;
  } else {
    return valor;
  }
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();

  document
    .querySelector("[xname='inpjustificativaDaExclusaoDoToken']")
    .setAttribute('style', 'height: 40px!important;');

  document
    .querySelector("[xname='inpusuarioGerados']")
    .setAttribute('style', '');
  document
    .querySelector("[xname='inpconta']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');

  document
    .querySelector("[xname='inptipoDeTransacao']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpvalorMinimoPorTransacao']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpvalorMaximoPorTransacao']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inplimiteDiario']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
}

function verificarDataSolicitacao() {
  var data = new Date();
  $('inp:dia').val(data.getDate());
  $('inp:ano').val(data.getFullYear());

  if (data.getMonth() == 0) {
    $('inp:mes').val('janeiro');
  } else if (data.getMonth() == 1) {
    $('inp:mes').val('fevereiro');
  } else if (data.getMonth() == 2) {
    $('inp:mes').val('março');
  } else if (data.getMonth() == 3) {
    $('inp:mes').val('abril');
  } else if (data.getMonth() == 4) {
    $('inp:mes').val('maio');
  } else if (data.getMonth() == 5) {
    $('inp:mes').val('junho');
  } else if (data.getMonth() == 6) {
    $('inp:mes').val('julho');
  } else if (data.getMonth() == 7) {
    $('inp:mes').val('agosto');
  } else if (data.getMonth() == 8) {
    $('inp:mes').val('setembro');
  } else if (data.getMonth() == 9) {
    $('inp:mes').val('outubro');
  } else if (data.getMonth() == 10) {
    $('inp:mes').val('novembro');
  } else if (data.getMonth() == 10) {
    $('inp:mes').val('dezembro');
  }
}

function verificarCidade() {
  if ($('inp:agencia').val() == 'Ag. Tenente Silveira') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. São José') {
    $('inp:cidade').val('São José');
  } else if ($('inp:agencia').val() == 'Ag. Beira Mar') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Madre Benvenuta') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Palhoça') {
    $('inp:cidade').val('Palhoça');
  } else if ($('inp:agencia').val() == 'Ag. Baía Sul') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Garopaba') {
    $('inp:cidade').val('Garopaba');
  } else if ($('inp:agencia').val() == 'Ag. Estreito') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Gama Deca') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Terra Firme') {
    $('inp:cidade').val('São José');
  } else if ($('inp:agencia').val() == 'Ag. Ingleses') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Sul Da Ilha') {
    $('inp:cidade').val('Florianópolis');
  } else if ($('inp:agencia').val() == 'Ag. Biguacu') {
    $('inp:cidade').val('Biguaçu');
  } else {
    $('inp:cidade').val('Florianópolis');
  }
}

function verificarPosicaoIniciadora() {
  if ($('select[name="inpCodPositionArea"]').val() != undefined) {
    var arrayAuxiliar = new Array();
    for (
      var i = 0;
      i < document.querySelector("[name='inpCodPositionArea']").options.length;
      i++
    ) {
      arrayAuxiliar.push(
        document.querySelector("[name='inpCodPositionArea']").options[i]
          .innerText
      );
    }
    arrayAuxiliar.shift();
    for (var i = arrayAuxiliar.length - 1; i >= 0; i--) {
      if (arrayAuxiliar[i].substring(4, 5) == '_') {
        arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]), 1);
      }
    }
    $('select[name="inpCodPositionArea"]').val(
      $("option:contains('" + arrayAuxiliar[0] + "')").val()
    );
    $(
      $('select[name="inpCodPositionArea"]')
        .parent()
        .parent()
    ).hide();
  }
}

function exibirBotoes() {
  if (document.getElementById('inptipoDeSolicitacao-0').checked) {
    $(document.getElementById('customBtn_Solicitação Encaminhada')).show();
  } else if (!document.getElementById('inptipoDeSolicitacao-0').checked) {
    $(document.getElementById('customBtn_Solicitação Encaminhada')).hide();
  }
}
