```
0515-solicitacao-de-alteracao-de-limite-em-canais-e-exclusao-de-token

https://bpm.e-unicred.com.br/attachments/templates/PR0515-alteracaoDeLimite-dispensaToken.docx

0507 - Template para Exclusão de Token

<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <CreatePdf01 xmlns="http://www.cryo.com.br/">
      <Token>{Requisitante.Token}</Token>
      <CodFlow>{Processo.CodigoModelo}</CodFlow>
      <CodFlowExecute>{Processo.Codigo}</CodFlowExecute>
      <XmlProcess>{Processo.Xml}</XmlProcess>
      <Template>/attachments/templates/PR0515-alteracaoDeLimite-dispensaToken.docx</Template>
      <GeneratedFilename>templateAlteracaoLimite{Processo.Codigo}</GeneratedFilename>
      <ShouldShowInfo>false</ShouldShowInfo>
      <ShouldSign>false</ShouldSign>
      <ShouldTrackQrcode>false</ShouldTrackQrcode>
    </CreatePdf01>
  </soap:Body>
</soap:Envelope>

Olá! Segue detalhes do que temos que desenvolver nessa semana:

PROCESSO PR_0515 - Solicitação de Alteração de Limite em Canais e Exclusão de Token
Formulário: Formulário do(a) PR_0515 - Solicitação de Alteração de Limite v.3

Criar nova versão para as alterações descritas abaixo:

AS-IS:
Todos os colaboradores da Unicred Florianópolis possuem acesso para solicitar o processo, que no final é encaminhado para a UA da cooperativa realizar o procedimento solicitado.

TO-BE:
Todos os colaboradores da Unicred Florianópolis possuem acesso para solicitar o processo, que no final é encaminhado para a área de BackOffice da UCSC/PR realizar o procedimento solicitado.

- Acrescentar novo template para quando solicitado exclusão de token. Segue anexo!
- No formulário, o requisitante selecionará se possui protocolo da solicitação. Caso sim, abre um campo para digitar o número desse protocolo e não obrigará gerar os templates. Caso não tenha protocolo, deverá gerar o(s) template(s) e anexar os documentos assinados.
- Criar um novo campo arquivo para receber esse outro template, pois a pessoa pode selecionar os dois itens (alteração de limite ou exclusão de token)
- Fazer controle nesses campos para saber quando foram selecionados, para trabalhar no teu fluxograma

PROCESSO PR_0515 - Solicitação de Análise de Contratos
Criar nova versão para as alterações descritas abaixo:
Liberar para que os colaboradores da Unicred Florianópolis possam solicitar o processo em questão. Deve-se passar pelas devidas validações, conforme Unicred Central SC/PR. Após OK do Jurídico, deve-se criar uma nova atividade (apenas Unicred Florianópolis. Demais requisições continuam o caminho atual) para resgatar assinatura no contrato. Feito isso, é encaminhado para a atividade atual Iniciar Procedimento de Arquivamento, já selecionada a opção Assinaturas Coletadas.
```
