$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();
  setAssociationFunctionality();
  ajustarFormulario();
  spinnerLoaderOnAnalyse();

  const tipoSolicitacao = $('inp:tipo').val();
  const numeroEmprestimo = $('inp:numero').val();

  tipoSolicitacao === 'Borderô de Desconto'
    ? buscarGarantiaCheque(numeroEmprestimo)
    : buscarGarantiasAvalista(numeroEmprestimo);

  $('#customBtn_Contrato\\ Assinado\\ Digitalmente').click(elemento => {
    verificarObrigatoriedadesParaSubmeterAtividade();
  });
});

function ajustarFormulario() {
  // esconder elementos do formulário
  $('#coluna-input-arquivamento').hide();
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();
  $('#section-informacoes-verificacao').hide();

  // configurações default do formulário
  $('#customBtn_Contrato\\ Assinado\\ Digitalmente').removeAttr('onclick');
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  apresentarInformacoesGeradasAoUsuario();
};

const apresentarInformacoesGeradasAoUsuario = () => {
  ajustarCampoChecklist();

  const tipoSolicitacao = $('inp:tipo').val();
  const assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipoSolicitacao) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();
      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();

  captureModule.capture.addDoc('Contrato (Obrigatório)');
  captureModule.capture.addDoc('Prestamista');
  captureModule.capture.addDoc('DPS Assinada');

  $('inp:possuiExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-eighthRow').hide()
    : $('#div-informacoes-emprestimo-eighthRow').show();

  assinaturaDigital === 'Sim'
    ? $('#div-informacoes-emprestimo-seventhRow').show()
    : $('#div-informacoes-emprestimo-seventhRow').hide();

  $('#section-informacoes-garantias').show();
  $('#section-informacoes-verificacao').show();

  toastr.success(`Informações do(a) ${tipoSolicitacao} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const verificarObrigatoriedadesParaSubmeterAtividade = () => {
  _.forEach(
    $('div#invalid-feedback-envio-requisicao'),
    invalidFeedbackRequest => {
      $(invalidFeedbackRequest).remove();
    }
  );

  let docsPendentes = $('#customizedUpload span.badge-secondary').map(
    (index, element) =>
      $(element)
        .text()
        .match(/Obrigat/gi)
  );

  if (docsPendentes.length > 0) {
    $('#customizedUpload').append(
      `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Documento Contrato Deve ser Anexado!</div>`
    );
    toastr.error('Há Documento Obrigatório a Ser Anexado. Verifique!');
    $('#invalid-feedback-envio-requisicao').show();
  } else {
    doAction('Contrato Assinado Digitalmente', false, false);
  }
};
