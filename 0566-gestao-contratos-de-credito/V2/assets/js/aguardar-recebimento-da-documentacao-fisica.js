$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();
  ajustarFormulario();
  spinnerLoaderOnAnalyse();

  const tipoSolicitacao = $('inp:tipo').val();
  const numeroEmprestimo = $('inp:numero').val();

  tipoSolicitacao === 'Borderô de Desconto'
    ? buscarGarantiaCheque(numeroEmprestimo)
    : buscarGarantiasAvalista(numeroEmprestimo);
});

function ajustarFormulario() {
  const acervo = $('inp:acervo').val();
  if (acervo === 'Não') {
    $('#coluna-input-arquivamento').hide();
  }

  // esconder elementos do formulário
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();
  $('#section-informacoes-verificacao').hide();
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  apresentarInformacoesGeradasAoUsuario();
};

const apresentarInformacoesGeradasAoUsuario = () => {
  ajustarCampoChecklist();

  const tipoSolicitacao = $('inp:tipo').val();
  const assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipoSolicitacao) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();
      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  $('inp:possuiExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-eighthRow').hide()
    : $('#div-informacoes-emprestimo-eighthRow').show();

  assinaturaDigital === 'Sim'
    ? $('#div-informacoes-emprestimo-seventhRow').show()
    : $('#div-informacoes-emprestimo-seventhRow').hide();

  $('#section-informacoes-garantias').show();
  $('#section-informacoes-verificacao').show();

  toastr.success(`Informações do(a) ${tipoSolicitacao} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};
