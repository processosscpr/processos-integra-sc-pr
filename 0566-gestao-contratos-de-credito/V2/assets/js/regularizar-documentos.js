$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();
  ajustarFormulario();
  spinnerLoaderOnAnalyse();

  const tipoSolicitacao = $('inp:tipo').val();
  const numeroEmprestimo = $('inp:numero').val();

  tipoSolicitacao === 'Borderô de Desconto'
    ? buscarGarantiaCheque(numeroEmprestimo)
    : buscarGarantiasAvalista(numeroEmprestimo);

  // adiciona painel atrás dos critérios de uma aba de formalização
  $('#tab1 p').click(e => {
    $('#tab1')
      .children()
      .children()
      .each(function(k, v) {
        if ($(v).find('[checkviewer] p').length === 0) return;
        $(v)
          .find('[checkviewer]')
          .addClass('conferencia-card');
      });
  });

  const btnFormalization = $('#btnsFormalization span')[1];
  $(btnFormalization)
    .addClass('tooltip-message')
    .append(`<div class="top"><p>Regularize aqui</p><i></i></div>`);

  $(btnFormalization).click(e => {
    $(e.currentTarget)
      .find('.top')
      .text('');
  });

  $(document).on('click', '#adicionarOutrosDocumentos', elemento => {
    disponibilizarOutrosDocumentos();
  });

  $('#inpDsReasonInputReason').focusout(element => {
    $('inp:tipoRetorno').val();
  });
});

function ajustarFormulario() {
  analisysModule.regularization.load();

  const conferente = $('inp:usuarioAssociado').val();

  const elementoListaAbaFormalizacao = $('#docContent ul.nav.nav-tabs');
  $(elementoListaAbaFormalizacao).append(
    '<button type="button" id="adicionarOutrosDocumentos" style="margin-right: 0.6rem" class="btn btn-small btn-info right"><i class="fas fa-file-import"></i> Importar Outros Documentos</button>'
  );

  // esconder elementos do INTEGRA
  $($('#btnsFormalization').children()[2]).hide();
  $($('#ContainerAttach tr')[0]).hide();
  $('#annex button.btn.btn-primary.btn-small').hide();

  // esconder elementos do formulário
  $('#coluna-input-arquivamento').hide();
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();
  $('#section-informacoes-verificacao').hide();

  $('#tab1').prepend(
    '<div class="alert alert-info" style="margin-right: 5px; margin-left: 5px"><small>Para regularizar os documentos reprovados clique no ícone: <span class="badge badge-secondary" style="padding: 2px 4px 2px 4px;"><i class="icon-white icon-upload"></i></span></small></div>'
  );

  $('inp:tipoRetorno').val(
    `RETORNO REPROVAÇÃO [Último Usuário: ${conferente}]`
  );
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  apresentarInformacoesGeradasAoUsuario();
};

const apresentarInformacoesGeradasAoUsuario = () => {
  ajustarCampoChecklist();

  const tipoSolicitacao = $('inp:tipo').val();
  const assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipoSolicitacao) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();
      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  $('inp:possuiExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-eighthRow').hide()
    : $('#div-informacoes-emprestimo-eighthRow').show();

  assinaturaDigital === 'Sim'
    ? $('#div-informacoes-emprestimo-seventhRow').show()
    : $('#div-informacoes-emprestimo-seventhRow').hide();

  $('#section-informacoes-garantias').show();
  $('#section-informacoes-verificacao').show();

  toastr.success(`Informações do(a) ${tipoSolicitacao} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const disponibilizarOutrosDocumentos = async () => {
  $('#adicionarOutrosDocumentos').attr('disabled', true);

  const token = $('#inpToken').val();
  const tipoSolicitacao = $('inp:tipo').val();

  const elementoBadgeSecondaryListaDocumentos = $(
    '#customizedUpload span.badge-secondary'
  );
  let documentosReprovados = [];
  await _.forEach(elementoBadgeSecondaryListaDocumentos, badgeDocumento =>
    documentosReprovados.push(
      $(badgeDocumento)
        .text()
        .trim()
    )
  );

  const requestListOfDocuments = {
    url: url_regra_negocio,
    method: 'POST',
    headers: { Authorization: token, 'content-type': 'application/json' },
    data: [
      {
        DsFieldName: 'tipo',
        DsValue: tipoSolicitacao,
      },
    ],
  };

  try {
    let { data } = await axios(requestListOfDocuments);
    const documentos = JSON.parse(data[0].DsReturn);

    await $.each(documentos, (key, value) => {
      documentosReprovados.indexOf(value.Document) == -1
        ? captureModule.capture.addDoc(value.Document)
        : null;
    });

    $($('#ContainerAttach tr')[0]).show();
    $('html, body').animate(
      {
        scrollTop: $('#ContainerAttach').offset().top,
      },
      2000
    );
  } catch (error) {
    spinnerLoaderOffAnalyse();
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    console.log(error);
  }
};
