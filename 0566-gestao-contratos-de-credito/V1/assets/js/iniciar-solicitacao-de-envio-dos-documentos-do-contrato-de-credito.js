$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();
  toggleSpinner();
  desativarInputsPopuladosPorAPI();

  $('inp:tipo').change(async elemento => {
    await limparInformcoesFormulario();
    $('#coluna-numero-emprestimo').show();
    $('#procurar-informacoes-emprestimo').show();
  });

  $('inp:numero').change(elemento => {
    limparInformcoesFormulario();
  });

  $('#procurar-informacoes-emprestimo').click(async elemento => {
    await limparInformcoesFormulario();
    validarSubmissaoEmprestimo();
  });

  $(document).on('change', '#customSwitchExcecao', e => {
    verificarNecessidadePreenchimentoExcecao(e.target.checked);
  });

  $('#customBtn_Solicitação\\ Encaminhada').click(elemento => {
    verificarObrigatoriedadesParaSubmeterSolicitacao();
  });
});

// prettier-ignore
const linhas_demaisLinhas = [11000,10002,10003,10015,10004,10009,10012,10013,10017,10093,10019,10020,10021,10022,10023,10029,10036,10048,10052,10056,11707,11711,11715,858,865,866,869,870,10014,212,854,855,11753,11709,566,10058,11702,11727,11803,11768,11788];
const linhas_veiculosLeves = [10037, 10038];
const linhas_cheque = [10018, 10087, 10059];
const linhas_cota = [10028, 10095];

// prettier-ignore
const linhas_aval = [10071,859,11046,843,878,879,881,882,11709,10091,11718,11713,10058,10083,10081,10068,10079,10078,10075,10074,10073,10070,10080,10063,10062,10061,10060,10059,10052,10090,10001,11717,10056,10048,10038,10037,10087,10065,11729,11728,11768,11727,11702,11803,10082,10104,10012,10036,10029,10023,10022,11707,11711,11715,10013,10009,11000,10004,10003,10002,10015,870,869,10021,10020,10019,10093,10017,866,858,865,10014,212,854,855,11753,566,10028,10095,11788];
// prettier-ignore
const linhas_demaisGarantias = [10037,10059,10038,10048,10056,11717,10001,10090,10088,10052,10060,10061,10062,10063,10080,10070,10071,10073,10074,10075,10078,10079,10068,10081,10083,10058,882,881,879,878,11046,859,11702,11727,11768,11728,11729,10065,10087,566,11713,11718,10091,11803,10082,10104,11709,10012,10002,10003,10015,10004,11000,10009,10013,10017,10093,10018,10019,10020,10021,10022,10023,10029,10036,11711,11715,858,865,866,869,870];
// prettier-ignore
const linhas_demaisGarantiasPJ = [10037,10059,10038,10048,10056,11717,10001,10090,10088,10052,10060,10061,10062,10063,10080,10070,10071,10073,10074,10075,10078,10079,10068,10081,10083,10058,10058,882,881,882,881,879,878,11046,859,11702,11727,11768,11728,11729,10065,10087,566,11713,11718,10091,11803,10082,10104,11709];

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#ContainerAttach').hide();
  $('#divStatisticsTable').hide();
  $('#customBtn_Solicitação\\ Encaminhada').hide();
  $('#btnCancel').hide();

  // configurações default do formulário
  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');

  // esconder elementos do formulário
  $('#coluna-numero-emprestimo').hide();
  $('#section-informacoes-emprestimo').hide();
  $('#section-informacoes-arquivamento').hide();
  $('#section-informacoes-verificacao').hide();
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const desativarInputsPopuladosPorAPI = () => {
  let inputInfosEmprestimo = $('#section-informacoes-emprestimo').find('input');
  inputInfosEmprestimo.attr('readOnly', true);
};

const limparInformcoesFormulario = async () => {
  $('#section-informacoes-emprestimo').hide();
  $('#section-informacoes-garantias').hide();
  $('#section-informacoes-verificacao').hide();
  $('#ContainerAttach').hide();

  let inputInfosEmprestimo = $('#section-informacoes-emprestimo').find('input');
  await _.forEach(inputInfosEmprestimo, input => {
    $(input).val('');
  });

  $('#div-informacoes-emprestimo-sixthRow')
    .find('#inputSwitchExcecao')
    .remove();
  $('inp:possuiExcecao').val('');
  $('inp:dadosDaExcecao').val('');

  await _.forEach($('#div-informacoes-garantias').children(), elemento => {
    $(elemento)
      .find('div.col-xs-12')
      .remove(),
      $(elemento)
        .find('h6.text-gray-dark.border-bottom.border-gray.pb-2')
        .remove();
  });

  informacoesEmprestimo = [];
  garantias = [];

  $('#switchExcecao').remove();
  $('#coluna-checklist label.checkbox').remove();
};

const validarSubmissaoEmprestimo = () => {
  toggleSpinner();
  let numeroEmprestimo = $('inp:numero').val();

  if (!numeroEmprestimo) {
    toggleSpinner();
    toastr.error('Preencha um dado para que possamos iniciar a busca!');
    return;
  }

  verificarQualRequisicaoRealizar();
};

const verificarQualRequisicaoRealizar = () => {
  let tipo = $('inp:tipo').val();

  switch (tipo) {
    case 'Proposta de Crédito':
      buscarinformacoesEmprestimo(url_proposta_credito);
      break;
    case 'Termo Aditivo':
      buscarinformacoesEmprestimo(url_aditivo);
      break;
    case 'Renegociação':
      buscarinformacoesEmprestimo(url_renegociacao);
      break;
    case 'Borderô de Desconto':
      buscarinformacoesEmprestimo(url_titulos);
      break;
  }
};

const buscarinformacoesEmprestimo = async url => {
  let numeroEmprestimo = $('inp:numero').val();

  await axios
    .post(url, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      if (response.data && response.data.success && response.data.success[0]) {
        informacoesEmprestimo = response.data.success[0];
        buscarGarantiasAvalista(numeroEmprestimo);
      } else {
        toggleSpinner();
        toastr.error(
          'Não encontramos esta informação. Ajuste e tente novamente!'
        );
      }
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  gerarCheckDeVerificacaoDeAcordoComLinhaEGarantias();
};

const gerarCheckDeVerificacaoDeAcordoComLinhaEGarantias = async () => {
  let countAtual = 0;
  $('#coluna-checklist div input').remove();

  if (
    linhas_demaisLinhas.indexOf(
      parseInt(informacoesEmprestimo.fields.LinhaCredito)
    ) != -1
  ) {
    await checkboxPadrao();
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          '1º parcela com prazo máx. de 30/45 dias contados da liberação do crédito',
          4
        )
      )
    );
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Votação do gestor (Documento e exigências atendidas)',
          5
        )
      )
    );
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Formulário de TED preenchido', 6))
    );
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Simulação de taxas', 7))
    );
  } else if (
    linhas_veiculosLeves.indexOf(
      parseInt(informacoesEmprestimo.fields.LinhaCredito)
    ) != -1
  ) {
    await checkboxPadrao();
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          '1º parcela com prazo máx. de 30/45 dias contados da liberação do crédito',
          4
        )
      )
    );
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Votação do gestor', 5))
    );
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Formulário de TED preenchido', 6))
    );
  } else if (
    linhas_cheque.indexOf(
      parseInt(informacoesEmprestimo.fields.LinhaCredito)
    ) != -1
  ) {
    await checkboxPadrao();
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Votação do gestor (Documento e exigências atendidas)',
          4
        )
      )
    );
  } else if (
    linhas_cota.indexOf(
      parseInt(informacoesEmprestimo.fields.LinhaCredito) != -1
    )
  ) {
    await checkboxPadrao();
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          '1º parcela com prazo máx. de 30/45 dias contados da liberação do crédito',
          4
        )
      )
    );
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Votação do gestor (Documento e exigências atendidas)',
          5
        )
      )
    );
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Extrato Cota Capital', 6))
    );
  }

  countAtual = $('#coluna-checklist label.checkbox').length;
  if (
    linhas_aval.indexOf(parseInt(informacoesEmprestimo.fields.LinhaCredito)) !=
      -1 &&
    _.findIndex(garantias, { tipo: 'Avalista' }) != -1
  ) {
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Avalista PF: Cadastro atualizado (Renda/Endereço/SERASA/BACEN) e assinatura conforme cartão autógrafo',
          countAtual
        )
      )
    );
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Avalista PJ: Cadastro atualizado (Faturamento/Endereço/Balanço/SERASA/BACEN/CONTRATO SOCIAL(Verificar cláusula de ADM) e assinatura conforme cartão autógrafo',
          countAtual + 1
        )
      )
    );
  }

  countAtual = $('#coluna-checklist label.checkbox').length;
  if (
    linhas_demaisGarantias.indexOf(
      parseInt(informacoesEmprestimo.fields.LinhaCredito)
    ) != -1 &&
    _.findIndex(garantias, { tipo: 'Veículo' }) != -1
  ) {
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Apólice de seguro (não permitido seguro de associação)',
          countAtual
        )
      )
    );
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Comprovante da FIPE', countAtual + 1))
    );
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Comprovante do gravame (cadastrado e anexado no contrato pela U.A)',
          countAtual + 2
        )
      )
    );
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Veículos novos: cópia da nota fiscal',
          countAtual + 3
        )
      )
    );
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Veículos usados: DUT preenchido e assinado pelo vendedor e comprador e reconhecido em cartório',
          countAtual + 4
        )
      )
    );
  }

  countAtual = $('#coluna-checklist label.checkbox').length;
  if (
    linhas_demaisGarantias.indexOf(
      parseInt(informacoesEmprestimo.fields.LinhaCredito)
    ) != -1 &&
    _.findIndex(garantias, { tipo: 'Imóvel' }) != -1
  ) {
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Laudo AGL', countAtual))
    );
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Garantia cadastrada no SAU', countAtual + 1))
    );
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'MatrÍcula emitida pelo cartório com alienação registrada',
          countAtual + 2
        )
      )
    );
  }

  countAtual = $('#coluna-checklist label.checkbox').length;
  if (
    linhas_demaisGarantiasPJ.indexOf(
      parseInt(informacoesEmprestimo.fields.LinhaCredito)
    ) != -1 &&
    _.findIndex(garantias, { tipo: 'Imóvel' }) != -1
  ) {
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Apólice de seguro', countAtual))
    );
  }

  countAtual = $('#coluna-checklist label.checkbox').length;
  if (
    linhas_demaisGarantias.indexOf(
      parseInt(informacoesEmprestimo.fields.LinhaCredito)
    ) != -1 &&
    _.findIndex(garantias, { tipo: 'Aplicação Financeira' }) != -1
  ) {
    $('#coluna-checklist div').append(
      $(checkboxLiberacao('Aplicação travada no SAU', countAtual))
    );
    $('#coluna-checklist div').append(
      $(
        checkboxLiberacao(
          'Em caso de aplicação PF em contratos PJ é permitido em nome do sócio majoritario',
          countAtual + 1
        )
      )
    );
  }

  preencherDadosDoEmprestimoNosInputs();
};

const checkboxPadrao = () => {
  $('#coluna-checklist div').append($(checkboxLiberacao('CET assinado', 0)));
  $('#coluna-checklist div').append(
    $(checkboxLiberacao('Contrato assinado e rubricado', 1))
  );
  $('#coluna-checklist div').append(
    $(
      checkboxLiberacao(
        'Assinaturas conferidas e carimbadas conforme cartão autógrafo (conferência por funcionário com curso de grafoscopia)',
        2
      )
    )
  );
  $('#coluna-checklist div').append(
    $(
      checkboxLiberacao(
        'Seguro prestamista conforme regra da seguradora (imagem via INTEGRA - original anexa contrato)',
        3
      )
    )
  );
};

const checkboxLiberacao = (text, count) => `
  <label for="inpchecklistParaSolicitarLiberacao-${count}" class="checkbox" style="display: block;">
    <input type="checkbox" name="inp26865" xname="inpchecklistParaSolicitarLiberacao" id="inpchecklistParaSolicitarLiberacao-${count}" label="Checklist para Solicitar Liberação" required="S" value="${text}" onchange="controlValueChange(this);">
    ${text}
  </label>
`;

const preencherDadosDoEmprestimoNosInputs = async () => {
  let tipo = $('inp:tipo').val();
  let valorEmMoeda = await transformarEmFormatoMoeda(
    informacoesEmprestimo.fields.Valor
  );

  $('inp:conta').val(informacoesEmprestimo.txt);
  $('inp:associado').val(informacoesEmprestimo.fields.Associado);
  $('inp:linhaDeCredito').val(
    informacoesEmprestimo.fields.DescricaoLinhaCredito
  );
  $('inp:valor').val(valorEmMoeda);
  $('inp:finalidade').val(informacoesEmprestimo.fields.Finalidade);
  $('inp:dataSolicitacao').val(informacoesEmprestimo.fields.DataSolicitacao);
  $('inp:parcelas').val(informacoesEmprestimo.fields.Parcelas);
  $('inp:seguroPrestamista').val(informacoesEmprestimo.fields.Prestamista);
  $('inp:tipoVencimento').val(informacoesEmprestimo.fields.TipoVencimento);
  $('inp:diaVencimento').val(informacoesEmprestimo.fields.DiaVencimento);
  $('inp:carenciaPrimParc').val(informacoesEmprestimo.fields.Carencia);
  $('inp:jurosNaCarencia').val(informacoesEmprestimo.fields.JurosNaCarencia);
  $('inp:financiarIof').val(informacoesEmprestimo.fields.FinanciarIOF);

  switch (tipo) {
    case 'Renegociação':
      $('inp:numeroContratoAnteriorMae').val(
        informacoesEmprestimo.fields.PropostaAnterior
      );
      break;
    case 'Borderô de Desconto':
      let valorTituloEmMoeda = await transformarEmFormatoMoeda(
        informacoesEmprestimo.fields.ValorTitulo
      );

      $('inp:numeroContratoAnteriorMae').val(
        informacoesEmprestimo.fields.PropostaPai
      );
      $('inp:valorTitulo').val(valorTituloEmMoeda);
      $('inp:dataVencimentoTitulo').val(
        informacoesEmprestimo.fields.DataVencimentoTitulo
      );
      $('inp:totalParcelasTitulo').val(
        informacoesEmprestimo.fields.ParcelasTitulo
      );
      break;
  }
  $('inp:possuiExcecao').val('Não');

  if (_.findIndex(garantias, { tipo: 'Imóvel' }) != -1) {
    $('inp:garantiaImóvelVinculada').val('Sim');
  } else {
    $('inp:garantiaImóvelVinculada').val('Não');
  }

  apresentarInformacoesGeradasAoSolicitante();
};

const apresentarInformacoesGeradasAoSolicitante = () => {
  let tipo = $('inp:tipo').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipo) {
    case 'Proposta de Crédito':
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      break;
  }

  $('#div-informacoes-emprestimo-sixthRow').prepend($(inputSwitchExcecao()));
  $('#coluna-input-excecao').hide();
  $('#div-informacoes-emprestimo-seventhRow').hide();

  $('#section-informacoes-garantias').show();
  $('#section-informacoes-verificacao').show();

  $('#annex').hide();
  $('#ContainerAttach').show();
  $('#customBtn_Solicitação\\ Encaminhada').show();

  toggleSpinner();
  toastr.success(`Informações do(a) ${tipo} geradas com sucesso!`);
};

const verificarNecessidadePreenchimentoExcecao = async check => {
  switch (check) {
    case true:
      $('inp:possuiExcecao').val('Sim');
      $('inp:dadosDaExcecao').val('');
      $('inp:dadosDaExcecao')[0].setAttribute('required', 'S');
      $('#div-informacoes-emprestimo-seventhRow').show();
      $($('#coluna-textarea-excecao').children()[0]).append(
        `<div id="invalid-feedback-excecao" class="invalid-feedback">Retiramos a obrigatoriedade de todos os documentos. Sua solicitação passará por validação da Coordenadoria Operacional e/ou Diretoria da Cooperativa!.</div>`
      );
      $('#invalid-feedback-excecao').show();
      _.forEach(
        $('div#invalid-feedback-envio-requisicao'),
        invalidFeedbackRequest => {
          $(invalidFeedbackRequest).remove();
        }
      );
      break;
    case false:
      $('inp:possuiExcecao').val('Não');
      $('inp:dadosDaExcecao').val('');
      $('inp:dadosDaExcecao')[0].setAttribute('required', 'N');
      $('#div-informacoes-emprestimo-seventhRow').hide();
      $('#invalid-feedback-excecao').remove();
      break;
  }
};

const verificarObrigatoriedadesParaSubmeterSolicitacao = () => {
  _.forEach(
    $('div#invalid-feedback-envio-requisicao'),
    invalidFeedbackRequest => {
      $(invalidFeedbackRequest).remove();
    }
  );
  let possuiExcecao = $('inp:possuiExcecao').val();
  let tipo = $('inp:tipo').val();
  let docsPendentes = $('#customizedUpload span.badge-secondary').map(
    (index, element) =>
      $(element)
        .text()
        .match(/Obrigat/gi)
  );

  if (possuiExcecao === 'Não') {
    switch (tipo) {
      case 'Proposta de Crédito':
      case 'Renegociação':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Documento Contrato e Parecer Devem ser Anexados!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          $('#switchExcecao').remove();
          doAction('Solicitação Encaminhada', false, false);
        }
        break;
      case 'Borderô de Desconto':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          $('#switchExcecao').remove();
          doAction('Solicitação Encaminhada', false, false);
        }
        break;
      case 'Termo Aditivo':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          $('#switchExcecao').remove();
          doAction('Solicitação Encaminhada', false, false);
        }
        break;
    }
  } else if (possuiExcecao === 'Sim') {
    $('#switchExcecao').remove();
    doAction('Solicitação Encaminhada', false, false);
  }
};
