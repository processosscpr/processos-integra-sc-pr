$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();
  spinnerLoaderOnAnalyse();
  buscarGarantiasAvalista($('inp:numero').val());

  $(
    '#customBtn_Encaminhar\\ Pendências\\ para\\ Finalização\\ da\\ Solicitação'
  ).click(elemento => {
    verificarObrigatoriedadesParaSubmeterAtividade();
  });
});

function ajustarFormulario() {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // esconder elementos do formulário
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();
  $('#section-informacoes-verificacao').hide();

  // configurações default do formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');
  $(
    '#customBtn_Encaminhar\\ Pendências\\ para\\ Finalização\\ da\\ Solicitação'
  ).removeAttr('onclick');
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  buscarDocumentosObrigatoriosParaContinuarSolicitacao();
};

const buscarDocumentosObrigatoriosParaContinuarSolicitacao = async () => {
  let documentosJaImportados = [];
  _.forEach($('#tblFile td.docType'), tipoDocumentoImportado =>
    documentosJaImportados.push($(tipoDocumentoImportado).text())
  );

  //Adicionar Estrutura Formalization no Form
  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();
  //Buscar Documentos e Tratar Obrigatoriedade
  let token = $('#inpToken').val(),
    tipo = $('inp:tipo').val();
  const requestListOfDocuments = {
    url: url_regra_negocio,
    method: 'POST',
    headers: { Authorization: token, 'content-type': 'application/json' },
    data: [
      {
        DsFieldName: 'tipo',
        DsValue: tipo,
      },
    ],
  };

  try {
    let { data } = await axios(requestListOfDocuments);
    const documentos = JSON.parse(data[0].DsReturn);

    await $.each(documentos, (key, value) => {
      documentosJaImportados.indexOf(value.Document) == -1
        ? captureModule.capture.addDoc(value.Document)
        : null;
    });
    apresentarInformacoesGeradasAoSolicitanteParaFinalizarRequisicaoComExcecao();
  } catch (error) {
    spinnerLoaderOffAnalyse();
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    console.log(error);
  }
};

const apresentarInformacoesGeradasAoSolicitanteParaFinalizarRequisicaoComExcecao = () => {
  ajustarCampoChecklist();

  let tipo = $('inp:tipo').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipo) {
    case 'Proposta de Crédito':
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      break;
  }

  $('inp:possuiExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-seventhRow').hide()
    : $('#div-informacoes-emprestimo-seventhRow').show();

  $('#section-informacoes-garantias').show();
  $('#section-informacoes-verificacao').show();

  toastr.success(`Informações do(a) ${tipo} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const verificarObrigatoriedadesParaSubmeterAtividade = () => {
  let tipo = $('inp:tipo').val();
  let docsPendentes = $('#customizedUpload span.badge-secondary').map(
    (index, element) =>
      $(element)
        .text()
        .match(/Obrigat/gi)
  );

  switch (tipo) {
    case 'Proposta de Crédito':
    case 'Renegociação':
      if (docsPendentes.length > 0) {
        $('#customizedUpload').append(
          `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Documento Contrato e Parecer Devem ser Anexados!</div>`
        );
        toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
        $('#invalid-feedback-envio-requisicao').show();
      } else {
        doAction(
          'Encaminhar Pendências para Finalização da Solicitação',
          false,
          false
        );
      }
      break;
    case 'Borderô de Desconto':
      if (docsPendentes.length > 0) {
        $('#customizedUpload').append(
          `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`
        );
        toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
        $('#invalid-feedback-envio-requisicao').show();
      } else {
        doAction(
          'Encaminhar Pendências para Finalização da Solicitação',
          false,
          false
        );
      }
      break;
    case 'Termo Aditivo':
      if (docsPendentes.length > 0) {
        $('#customizedUpload').append(
          `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`
        );
        toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
        $('#invalid-feedback-envio-requisicao').show();
      } else {
        doAction(
          'Encaminhar Pendências para Finalização da Solicitação',
          false,
          false
        );
      }
      break;
  }
};
