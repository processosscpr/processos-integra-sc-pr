$(document).ready(function() {});

/*--------------------------------------------------------*/
//FONTE DE DADOS: 0507 - Gestão Contratos - Contratos 0515
const url_proposta_credito = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJms4uA2UGWoLz1HkelVw7ebyVVOR-tAGX3yk3IMbhxUUJdx7SAt87-Ajrgo578nJMQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Renegociações 0515
const url_renegociacao = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJhDRuxXZnP6S3CklKdVOeOaRFYBGFyWbdDs4IjqjcauHiPXZs5Cx2MULW-GsZnEBCg__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Títulos 0515
const url_titulos = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJlyNN0W1dDaRU6YTPkrwMZn9HSf3Qjiq9wjqH3BWQo87mEbA03P7GtkPRCqNuRvqIQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Aditivo 0515
const url_aditivo = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJm6VcRxyiNmh8Qtv6ue@VWiqYYDZHbH@RtnCDY7c46k37fdw57a3ABFLCblih@S8YQ__`;

//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Aval 0515
const url_garantia_aval = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJr@XM2qvv0ihL2quV60ATjRwfyYBr6FDNZLXKo13Pwitv1tNrItVInTBT42GcSWGpQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Cheque 0515
const url_garantia_cheque = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJs9ix1z4sKnIsZ5Sc9b7ufp3tp1S0wksvbdkGb089nizjS8hwhJmPeNUb-aRcptqqQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Cartão 0515
const url_garantia_cartao = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJkPQP64T4mskDbZJYaDecgObjb2FbfcTvJ6YD80bywGPxqmgVMczGcDnm@dwzy8I3Q__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Capital 0515
const url_garantia_capital = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJnB3NUD75TZlyaTGKNP8olpe-anbfx44xmWYyvLStuP2EQdRerpvnKti5vHUG-g5iw__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Veículo 0515
const url_garantia_veiculos = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJo3ggUYJ7VD-eT0f-gy8FnDgGbYVdErIhAPBfR0z-weYt5Gv5Zujt5qHZlU@stfEmQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Imóvel 0515
const url_garantia_imovel = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJk7P1TlagZsiwgRGf-jKFUq3MJLD4Qp5JZiekqxeRLC3krTPXMA3HGBYZtSsYJTnZg__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Aplicação 0515
const url_garantia_aplicacao_financeira = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJs9TRar1IIbjl4sftgEprNKznVjgb2nEahWQmUiE5Z26iAuI4AzpJV7g1Qiy8sPBeA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Outras 0515
const url_garantia_outras = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJhHgf2T35LQmk-8WOWOn5GSJbmT7JqN7oO-9GZQceIjMa@siR7vtSHTj5YgT8pT@vw__`;
//REGRA DE NEGÓCIO: 0507 - Listagem e Obrigatoriedade de Documentos - Gestão Contratos de Crédito - Demais Cooperativas
const url_regra_negocio = `https://bpm.e-unicred.com.br/api/1.0/businessrules/92/evaluate`;
/*--------------------------------------------------------*/

/*--------------------------------------------------------*/
let informacoesEmprestimo = [];
let garantias = [];
/*--------------------------------------------------------*/

//Função dinâmica utilizada em todas as atividades
const adicionarRequisitanteBadge = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile =
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1;

  if (!isMobile) {
    $(
      `
        <span style='margin-left: 0.4em' class='label label-secondary'>
          <strong> Requisitante: ${$('inp:nomeRequisitante').val()}</strong>
        </span>
      `
    ).insertAfter('.title span.label.label-info.flow-title');
  }
};

const spinnerLoaderOnAnalyse = () =>
  $(
    '#section-informacoes-emprestimo h5.text-dark.border-bottom.border-gray.pb-2'
  ).append(
    ` <span id="spinner-loader-analyse" class="spinner-border spinner-border-sm" role="status"></span>`
  );
const spinnerLoaderOffAnalyse = () => $('#spinner-loader-analyse').remove();

//Função dinâmica utilizada em todas as atividades
const buscarGarantiasAvalista = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_aval, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Avalista',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaCheque(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCheque = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_cheque, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Cheque',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaCartao(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCartao = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_cartao, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Cartão',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaCapital(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCapital = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_capital, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Capital',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaVeiculo(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaVeiculo = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_veiculos, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Veículo',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaImovel(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaImovel = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_imovel, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Imóvel',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaAplicacao(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaAplicacao = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_aplicacao_financeira, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Aplicação Financeira',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaOutras(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaOutras = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_outras, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Outras',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      montarEstruturaSectionGarantias();
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const montarEstruturaSectionGarantias = async () => {
  //AVALISTA
  if (_.findIndex(garantias, { tipo: 'Avalista' }) != -1) {
    $('#div-informacoes-garantias-avalistas').prepend(
      $(sectionCollapse('far fa-id-badge', 'Avalistas'))
    );
    $('#div-informacoes-garantias-resumo-avalistas').append(
      $(
        cardResume(
          _.countBy(garantias, { tipo: 'Avalista' }).true,
          'Avalista(s)'
        )
      )
    );
  }
  //CHEQUE
  if (_.findIndex(garantias, { tipo: 'Cheque' }) != -1) {
    let totalChequeBruto = [],
      totalChequeLiquido = [];
    $('#div-informacoes-garantias-cheques').prepend(
      $(sectionCollapse('fas fa-money-check-alt', 'Cheques'))
    );
    $('#div-informacoes-garantias-resumo-cheques').append(
      $(cardResume(_.countBy(garantias, { tipo: 'Cheque' }).true, 'Cheque(s)'))
    );
    //Iniciar Cálculo Somatório Cheque
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Cheque'
        ? (totalChequeBruto.push(parseFloat(garantia.dados.ValorBruto)),
          totalChequeLiquido.push(parseFloat(garantia.dados.ValorLiquido)))
        : null;
    });
    $('#div-informacoes-garantias-resumo-cheques').append(
      $(
        cardResume(
          'Total Bruto:',
          transformarEmFormatoMoeda(_.sum(totalChequeBruto, Number).toFixed(2))
        )
      )
    );
    $('#div-informacoes-garantias-resumo-cheques').append(
      $(
        cardResume(
          'Total Líquido:',
          transformarEmFormatoMoeda(
            _.sum(totalChequeLiquido, Number).toFixed(2)
          )
        )
      )
    );
  }
  //CARTÃO
  if (_.findIndex(garantias, { tipo: 'Cartão' }) != -1) {
    let totalCartaoCredito = [],
      totalCartaoAntecipar = [],
      totalCartaoLiquido = [];
    $('#div-informacoes-garantias-cartoes').prepend(
      $(sectionCollapse('far fa-credit-card', 'Créditos de Cartões'))
    );
    $('#div-informacoes-garantias-resumo-cartoes').append(
      $(
        cardResume(
          _.countBy(garantias, { tipo: 'Cartão' }).true,
          'Crédito(s) de Cartão(ões)'
        )
      )
    );
    //Iniciar Cálculo Somatório Cartão
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Cartão'
        ? (totalCartaoCredito.push(parseFloat(garantia.dados.ValorCredito)),
          totalCartaoAntecipar.push(
            parseFloat(garantia.dados.ValorAntecipacao)
          ),
          totalCartaoLiquido.push(parseFloat(garantia.dados.ValorLiquido)))
        : null;
    });
    $('#div-informacoes-garantias-resumo-cartoes').append(
      $(
        cardResume(
          'Total Crédito:',
          transformarEmFormatoMoeda(
            _.sum(totalCartaoCredito, Number).toFixed(2)
          )
        )
      )
    );
    $('#div-informacoes-garantias-resumo-cartoes').append(
      $(
        cardResume(
          'Total Antecipar:',
          transformarEmFormatoMoeda(
            _.sum(totalCartaoAntecipar, Number).toFixed(2)
          )
        )
      )
    );
    $('#div-informacoes-garantias-resumo-cartoes').append(
      $(
        cardResume(
          'Total Líquido:',
          transformarEmFormatoMoeda(
            _.sum(totalCartaoLiquido, Number).toFixed(2)
          )
        )
      )
    );
  }
  //CAPITAL
  if (_.findIndex(garantias, { tipo: 'Capital' }) != -1) {
    $('#div-informacoes-garantias-cotas').prepend(
      $(sectionCollapse('fas fa-coins', 'Cota Capital'))
    );
    $('#div-informacoes-garantias-resumo-cotas').append(
      $(
        cardResume(
          _.countBy(garantias, { tipo: 'Capital' }).true,
          'Cota Capital'
        )
      )
    );
  }
  //VEÍCULO
  if (_.findIndex(garantias, { tipo: 'Veículo' }) != -1) {
    let totalValorVeiculo = [];
    $('#div-informacoes-garantias-veiculos').prepend(
      $(sectionCollapse('fas fa-car', 'Veículos'))
    );
    $('#div-informacoes-garantias-resumo-veiculos').append(
      $(
        cardResume(_.countBy(garantias, { tipo: 'Veículo' }).true, 'Veículo(s)')
      )
    );
    //Iniciar Cálculo Somatório Veículo
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Veículo'
        ? totalValorVeiculo.push(parseFloat(garantia.dados.ValorVeiculo))
        : null;
    });
    $('#div-informacoes-garantias-resumo-veiculos').append(
      $(
        cardResume(
          'Total:',
          transformarEmFormatoMoeda(_.sum(totalValorVeiculo, Number).toFixed(2))
        )
      )
    );
  }
  //IMÓVEL
  if (_.findIndex(garantias, { tipo: 'Imóvel' }) != -1) {
    let totalValorImovel = [];
    $('#div-informacoes-garantias-imoveis').prepend(
      $(sectionCollapse('fas fa-home', 'Imóveis'))
    );
    $('#div-informacoes-garantias-resumo-imoveis').append(
      $(cardResume(_.countBy(garantias, { tipo: 'Imóvel' }).true, 'Imóvel(is)'))
    );
    //Iniciar Cálculo Somatório Imóvel
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Imóvel'
        ? totalValorImovel.push(parseFloat(garantia.dados.ValorImovel))
        : null;
    });
    $('#div-informacoes-garantias-resumo-imoveis').append(
      $(
        cardResume(
          'Total:',
          transformarEmFormatoMoeda(_.sum(totalValorImovel, Number).toFixed(2))
        )
      )
    );
  }
  //APLICAÇÃO FINANCEIRA
  if (_.findIndex(garantias, { tipo: 'Aplicação Financeira' }) != -1) {
    let totalValorAplicacaoFinanceira = [];
    $('#div-informacoes-garantias-aplicacoes').prepend(
      $(sectionCollapse('fas fa-hand-holding-usd', 'Aplicações Financeiras'))
    );
    $('#div-informacoes-garantias-resumo-aplicacoes').append(
      $(
        cardResume(
          _.countBy(garantias, { tipo: 'Aplicação Financeira' }).true,
          'Aplicação(ões)'
        )
      )
    );
    //Iniciar Cálculo Somatório Aplicação Fincanceira
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Aplicação Financeira'
        ? totalValorAplicacaoFinanceira.push(
            parseFloat(garantia.dados.ValorGarantia)
          )
        : null;
    });
    $('#div-informacoes-garantias-resumo-aplicacoes').append(
      $(
        cardResume(
          'Total:',
          transformarEmFormatoMoeda(
            _.sum(totalValorAplicacaoFinanceira, Number).toFixed(2)
          )
        )
      )
    );
  }
  //OUTRAS
  if (_.findIndex(garantias, { tipo: 'Outras' }) != -1) {
    let totalValorOutras = [];
    $('#div-informacoes-garantias-outras').prepend(
      $(sectionCollapse('fas fa-box-open', 'Outras'))
    );
    $('#div-informacoes-garantias-resumo-outras').append(
      $(
        cardResume(
          'Quantidade Outras:',
          _.countBy(garantias, { tipo: 'Outras' }).true
        )
      )
    );
    //Iniciar Cálculo Somatório Outras
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Outras'
        ? totalValorOutras.push(parseFloat(garantia.dados.ValorGarantia))
        : null;
    });
    $('#div-informacoes-garantias-resumo-outras').append(
      $(
        cardResume(
          'Total:',
          transformarEmFormatoMoeda(_.sum(totalValorOutras, Number).toFixed(2))
        )
      )
    );
  }
  montarEstruturaCardsDeGarantias();
};

//Função dinâmica utilizada em todas as atividades
const montarEstruturaCardsDeGarantias = async () => {
  await _.forEach(garantias, garantia => {
    switch (garantia.tipo) {
      case 'Avalista':
        $('#div-informacoes-garantias-cards-avalistas').append(
          $(cardGarantiaAvalista(garantia.dados))
        );
        break;
      case 'Cheque':
        $('#div-informacoes-garantias-cards-cheques').append(
          $(cardGarantiaCheque(garantia.dados))
        );
        break;
      case 'Cartão':
        $('#div-informacoes-garantias-cards-cartoes').append(
          $(cardGarantiaCartao(garantia.dados))
        );
        break;
      case 'Capital':
        $('#div-informacoes-garantias-cards-cotas').append(
          $(cardGarantiaCota(garantia.dados))
        );
        break;
      case 'Veículo':
        $('#div-informacoes-garantias-cards-veiculos').append(
          $(cardGarantiaVeiculo(garantia.dados))
        );
        break;
      case 'Imóvel':
        $('#div-informacoes-garantias-cards-imoveis').append(
          $(cardGarantiaImovel(garantia.dados))
        );
        break;
      case 'Aplicação Financeira':
        $('#div-informacoes-garantias-cards-aplicacoes').append(
          $(cardGarantiaAplicacao(garantia.dados))
        );
        break;
      case 'Outras':
        $('#div-informacoes-garantias-cards-outras').append(
          $(cardGarantiaOutras(garantia.dados))
        );
        break;
    }
  });
  submeterProximaValidacaoAposRetornoDasGarantias();
};

/*--------------------------------------------------------*/
//COMPONENTES - CARDS
const cardResume = (label, value) => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card text-white bg-primary">
            <div class="card-body">
                <strong class="text-gray-dark">${label} ${value}</strong>
            </div>
        </div>
    </div>
`;

const cardGarantiaAvalista = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${
                  dados.Pessoa
                }</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(
                  dados.IdentificacaoPessoal
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaCheque = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Banco:</b> ${dados.Banco} <b>Ag:</b> ${
  dados.Agencia
} <b>Conta:</b> ${dados.Conta}</p>
                <p class="card-text"><b>Número Cheque:</b> ${dados.Cheque}</p>
                <p class="card-text"><b>Data Vencimento:</b> ${
                  dados.DataVencimentoCheque
                }</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Bruto:</b> ${transformarEmFormatoMoeda(
                  dados.ValorBruto
                )}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Líquido:</b> ${transformarEmFormatoMoeda(
                  dados.ValorLiquido
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaCartao = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Adquirente:</b> ${
                  dados.Adquirente
                } <b>Antecip:</b> ${dados.PercentualAntecipacao}%</p>
                <p class="card-text"><b>Data Crédito:</b> ${
                  dados.DataCredito
                }</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Crédito:</b> ${transformarEmFormatoMoeda(
                  dados.ValorCredito
                )}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Antecipar:</b> ${transformarEmFormatoMoeda(
                  dados.ValorAntecipacao
                )}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Líquido:</b> ${transformarEmFormatoMoeda(
                  dados.ValorLiquido
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaCota = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorCapital
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaVeiculo = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${
                  dados.Pessoa
                }</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(
                  dados.IdentificacaoPessoal
                )}</p>
                <p class="card-text"><span><i class="fas fa-car-side"></i></span> ${
                  dados.Veiculo
                } - ${dados.Marca} | ${dados.Placa}</p>
                <p class="card-text"><b>Renavam:</b> ${
                  dados.Renavam
                } <b>Chassi:</b> ${dados.Chassi}</p>
                <p class="card-text"><span><i class="far fa-calendar-alt"></i></span> <b>Fabr:</b> ${
                  dados.AnoFabricacao
                } <span><i class="far fa-calendar-alt"></i> <b>Modelo:</b> ${
  dados.AnoModelo
}</span></p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorVeiculo
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaImovel = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${
                  dados.Pessoa
                }</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(
                  dados.IdentificacaoPessoal
                )}</p>
                <p class="card-text"><span><i class="fas fa-map-marker-alt"></i></span> [${
                  dados.TipoImovel
                }] ${dados.DsEndereco}, ${dados.DsNumero} - ${
  dados.DsComplemento
}, ${dados.DsBairro}, ${dados.DsCidade} - ${dados.DsUF} - ${dados.DsCEP}</p>
                <p class="card-text"><b>Matrícula:</b> ${
                  dados.MatriculaImovel
                } <b>Registro:</b> ${dados.RegistroImovel}</p>
                <p class="card-text"><b>Descrição:</b> ${dados.DsImovel}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorImovel
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaAplicacao = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Conta:</b> ${
                  dados.Conta
                } <b>Produto:</b> ${dados.Produto}</p>
                <p class="card-text"><b>Título:</b> ${dados.Titulo}</p>
                <p class="card-text"><span><i class="far fa-calendar-alt"></i></span> <b>Aplicação:</b> ${
                  dados.DataAplicacao
                } <span><i class="far fa-calendar-alt"></i> <b>Vencimento:</b> ${
  dados.DataVencimento
}</span></p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorGarantia
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaOutras = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Descrição Garantia:</b> ${
                  dados.DsGarantia
                }</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorGarantia
                )}</p>
            </div>
        </div>
    </div>
`;

//COMPONENTES - SECTION COLLAPSE
const sectionCollapse = (classe, tipo) => `
    <h6 class="text-gray-dark border-bottom border-gray pb-2"><span><i class="${classe}"></i></span> ${tipo}<a class="text-dark float-right" onclick="collapseGarantia(this)" target="waiting-collapse-integra"><i class="fas fa-chevron-down"></i></a></h6>
`;

//COMPONENTES - ALERT
const alert = text => `
    <div class="col-xs-12 col-md-12" style="margin-top: 10px; margin-bottom: 10px">
        <div class="alert alert-info" role="alert">
            ${text}
        </div>
    </div>
`;

//COMPONENTES - SWITCH
const inputSwitchExcecao = () => `
    <div id="switchExcecao" class="col-xs-12 col-md-2">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="customSwitchExcecao">
            <label class="custom-control-label font-weight-bold pt-1" for="customSwitchExcecao">Possui Exceção</label>
        </div>
    </div>
`;
/*--------------------------------------------------------*/

//Função utilizada em todas as atividades
const transformarEmFormatoMoeda = valor => {
  return parseFloat(valor).toLocaleString('pt-br', {
    minimumFractionDigits: 2,
  });
};

//Função utilizada em todas as atividades
const mascaraIdentificador = valor => {
  if (valor.length <= 11) {
    return valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4');
  } else {
    return valor.replace(
      /(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,
      '$1.$2.$3/$4-$5'
    );
  }
};

//Função utilizada em todas as atividades
const collapseGarantia = elemento => {
  $(elemento).attr('target') === 'waiting-collapse-integra'
    ? ($(
        $(
          $(elemento)
            .parent()
            .parent()
        ).find('div.row')[0]
      ).show(),
      $(
        $(
          $(elemento)
            .parent()
            .parent()
        ).find('div.row')[1]
      ).show(),
      $(elemento).attr('target', 'in-collapse-integra'),
      $(elemento)
        .find('i')
        .removeClass('fa-chevron-down')
        .addClass('fa-chevron-up'))
    : ($(
        $(
          $(elemento)
            .parent()
            .parent()
        ).find('div.row')[0]
      ).hide(),
      $(
        $(
          $(elemento)
            .parent()
            .parent()
        ).find('div.row')[1]
      ).hide(),
      $(elemento).attr('target', 'waiting-collapse-integra'),
      $(elemento)
        .find('i')
        .removeClass('fa-chevron-up')
        .addClass('fa-chevron-down'));
};
