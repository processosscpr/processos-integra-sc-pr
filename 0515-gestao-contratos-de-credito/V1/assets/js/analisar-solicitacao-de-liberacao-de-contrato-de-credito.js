$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();
  spinnerLoaderOnAnalyse();
  buscarGarantiasAvalista($('inp:numero').val());

  // adiciona painel atrás dos critérios de uma aba de formalização
  $('#tab1 p').click(e => {
    $('#tab1')
      .children()
      .children()
      .each(function(k, v) {
        if ($(v).find('[checkviewer] p').length === 0) return;
        $(v)
          .find('[checkviewer]')
          .addClass('conferencia-card');
      });
  });

  $('.tab-content').click(elemento => {
    validarSeTodosOsDocumentosObrigatoriosForamRecebidosParaSubmeterSolicitacao();
  });
});

function ajustarFormulario() {
  analisysModule.analisys.load();

  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');
  $($('#btnsFormalization').children()[2]).hide();

  // esconder elementos do formulário
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();

  // configurações default do formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  apresentarInformacoesGeradasAoUsuario();
};

const apresentarInformacoesGeradasAoUsuario = () => {
  let tipo = $('inp:tipo').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipo) {
    case 'Proposta de Crédito':
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      break;
  }

  $('inp:possuiExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-seventhRow').hide()
    : $('#div-informacoes-emprestimo-seventhRow').show();

  $('#section-informacoes-garantias').show();
  toastr.success(`Informações do(a) ${tipo} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const validarSeTodosOsDocumentosObrigatoriosForamRecebidosParaSubmeterSolicitacao = () => {
  let documentosJaImportados = [];
  _.forEach($('#tblFile td.docType'), tipoDocumentoImportado =>
    documentosJaImportados.push($(tipoDocumentoImportado).text())
  );

  if (
    documentosJaImportados.length == 1 &&
    documentosJaImportados.indexOf('Pedido de Exceção') != -1
  ) {
    $('#btnApprove').hide();
  }
};
