$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();
  ajustarFormulario();
  spinnerLoaderOnAnalyse();

  const tipoSolicitacao = $('inp:tipo').val();
  const numeroEmprestimo = $('inp:numero').val();

  tipoSolicitacao === 'Borderô de Desconto'
    ? buscarGarantiaCheque(numeroEmprestimo)
    : buscarGarantiasAvalista(numeroEmprestimo);

  $(
    '#customBtn_Encaminhar\\ Pendências\\ para\\ Finalização\\ da\\ Solicitação'
  ).click(elemento => {
    verificarObrigatoriedadesParaSubmeterAtividade();
  });
});

function ajustarFormulario() {
  $('#ContainerAttach .box-header').append(`
    <div id="aviso-nao-unificacao-anexo">
      <div class="col">
        <div class="alert alert-info my-2">
          Somente é possível um arquivo por tipo de documento. Caso possua vários arquivos
          separados, deve-se, por enquanto, agrupá-los pelo scanner da impressora.
        </div>
      </div>
    </div>
  `);

  // esconder elementos do formulário
  $('#coluna-input-arquivamento').hide();
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();

  // configurações default do formulário
  $(
    '#customBtn_Encaminhar\\ Pendências\\ para\\ Finalização\\ da\\ Solicitação'
  ).removeAttr('onclick');
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  buscarDocumentosObrigatoriosParaContinuarSolicitacao();
};

const buscarDocumentosObrigatoriosParaContinuarSolicitacao = async () => {
  const token = $('#inpToken').val();
  const tipoSolicitacao = $('inp:tipo').val();

  //Adicionar Estrutura Formalization no Form
  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();
  //Buscar Documentos e Tratar Obrigatoriedade
  const requestListOfDocuments = {
    url: url_regra_negocio,
    method: 'POST',
    headers: { Authorization: token, 'content-type': 'application/json' },
    data: [
      {
        DsFieldName: 'tipo',
        DsValue: tipoSolicitacao,
      },
    ],
  };

  try {
    let { data } = await axios(requestListOfDocuments);
    const documentos = JSON.parse(data[0].DsReturn);

    await $.each(documentos, (key, value) => {
      captureModule.capture.addDoc(value.Document);
    });
    apresentarInformacoesGeradasAoSolicitanteParaFinalizarRequisicaoComExcecao();
  } catch (error) {
    spinnerLoaderOffAnalyse();
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    console.log(error);
  }
};

const apresentarInformacoesGeradasAoSolicitanteParaFinalizarRequisicaoComExcecao = () => {
  const tipoSolicitacao = $('inp:tipo').val();
  const assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipoSolicitacao) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();
      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  $('inp:possuiExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-eighthRow').hide()
    : $('#div-informacoes-emprestimo-eighthRow').show();

  assinaturaDigital === 'Sim'
    ? $('#div-informacoes-emprestimo-seventhRow').show()
    : $('#div-informacoes-emprestimo-seventhRow').hide();

  $('#section-informacoes-garantias').show();
  toastr.success(`Informações do(a) ${tipoSolicitacao} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const verificarObrigatoriedadesParaSubmeterAtividade = async () => {
  const tipoSolicitacao = $('inp:tipo').val();

  const documentosObrigatoriosNaoAnexados = await buscarDocumentosObrigatoriosNaoAnexados();

  switch (tipoSolicitacao) {
    case 'Proposta de Crédito':
    case 'Renegociação':
      if (documentosObrigatoriosNaoAnexados.length > 0) {
        $('#customizedUpload').append(
          `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Documento Contrato Deve ser Anexado!</div>`
        );
        toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
        $('#invalid-feedback-envio-requisicao').show();
      } else {
        doAction(
          'Encaminhar Pendências para Finalização da Solicitação',
          false,
          false
        );
      }
      break;
    case 'Borderô de Desconto':
      if (documentosObrigatoriosNaoAnexados.length > 0) {
        $('#customizedUpload').append(
          `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`
        );
        toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
        $('#invalid-feedback-envio-requisicao').show();
      } else {
        doAction(
          'Encaminhar Pendências para Finalização da Solicitação',
          false,
          false
        );
      }
      break;
    case 'Termo Aditivo':
      if (documentosObrigatoriosNaoAnexados.length > 0) {
        $('#customizedUpload').append(
          `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`
        );
        toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
        $('#invalid-feedback-envio-requisicao').show();
      } else {
        doAction(
          'Encaminhar Pendências para Finalização da Solicitação',
          false,
          false
        );
      }
      break;
  }
};

const buscarDocumentosObrigatoriosNaoAnexados = async () => {
  const elementoBadgeSecondaryListaDocumentos = $(
    '#customizedUpload span.badge-secondary'
  );
  let listaDocumentosParaAnexar = [];
  await _.forEach(elementoBadgeSecondaryListaDocumentos, badgeDocumento =>
    listaDocumentosParaAnexar.push(
      $(badgeDocumento)
        .text()
        .trim()
    )
  );

  const elementoTabelaDocumentosJaAnexados = $('#tblFile td.docType');
  let documentosJaAnexados = [];
  await _.forEach(elementoTabelaDocumentosJaAnexados, documento =>
    documentosJaAnexados.push(
      $(documento)
        .text()
        .trim()
    )
  );

  let documentosAindaNaoAnexados = [];
  await $.each(listaDocumentosParaAnexar, (key, value) => {
    documentosJaAnexados.indexOf(value) == -1
      ? documentosAindaNaoAnexados.push(value)
      : null;
  });

  let documentosObrigatoriosNaoAnexados = [];
  await _.forEach(documentosAindaNaoAnexados, documento => {
    documento.match(/Obrigat/gi)
      ? documentosObrigatoriosNaoAnexados.push(documento)
      : null;
  });

  return documentosObrigatoriosNaoAnexados;
};
