$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();
  setAssociationFunctionality();
  ajustarFormulario();
  spinnerLoaderOnAnalyse();

  const tipoSolicitacao = $('inp:tipo').val();
  const numeroEmprestimo = $('inp:numero').val();

  tipoSolicitacao === 'Borderô de Desconto'
    ? buscarGarantiaCheque(numeroEmprestimo)
    : buscarGarantiasAvalista(numeroEmprestimo);

  // adiciona painel atrás dos critérios de uma aba de formalização
  $('#tab1 p').click(e => {
    $('#tab1')
      .children()
      .children()
      .each(function(k, v) {
        if ($(v).find('[checkviewer] p').length === 0) return;
        $(v)
          .find('[checkviewer]')
          .addClass('conferencia-card');
      });
  });

  $('.tab-content').click(elemento => {
    validarSeTodosOsDocumentosObrigatoriosForamRecebidosParaSubmeterSolicitacao();
  });
});

function ajustarFormulario() {
  analisysModule.analisys.load();

  // esconder elementos do INTEGRA
  $($('#btnsFormalization').children()[2]).hide();

  // esconder elementos do formulário
  $('#coluna-input-arquivamento').hide();
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  apresentarInformacoesGeradasAoUsuario();
};

const apresentarInformacoesGeradasAoUsuario = () => {
  const tipoSolicitacao = $('inp:tipo').val();
  const assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipoSolicitacao) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();
      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  $('inp:possuiExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-eighthRow').hide()
    : $('#div-informacoes-emprestimo-eighthRow').show();

  assinaturaDigital === 'Sim'
    ? $('#div-informacoes-emprestimo-seventhRow').show()
    : $('#div-informacoes-emprestimo-seventhRow').hide();

  $('#section-informacoes-garantias').show();
  toastr.success(`Informações do(a) ${tipoSolicitacao} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const validarSeTodosOsDocumentosObrigatoriosForamRecebidosParaSubmeterSolicitacao = () => {
  let documentosJaImportados = [];
  _.forEach($('#tblFile td.docType'), tipoDocumentoImportado =>
    documentosJaImportados.push($(tipoDocumentoImportado).text())
  );

  if (
    documentosJaImportados.length == 1 &&
    documentosJaImportados.indexOf('Pedido de Exceção') != -1
  ) {
    $('#btnApprove').hide();
  }
};
