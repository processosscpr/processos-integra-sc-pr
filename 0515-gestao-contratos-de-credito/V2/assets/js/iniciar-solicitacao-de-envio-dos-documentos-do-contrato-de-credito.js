$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();
  toggleSpinner();
  desativarInputsPopuladosPorAPI();

  $('inp:tipo').change(async elemento => {
    await limparInformcoesFormulario();
    $('#coluna-numero-emprestimo').show();
    $('#procurar-informacoes-emprestimo').show();
  });

  $('inp:numero').change(elemento => {
    limparInformcoesFormulario();
  });

  $('#procurar-informacoes-emprestimo').click(async elemento => {
    await limparInformcoesFormulario();
    validarSubmissaoEmprestimo();
  });

  $(document).on('change', '#customSwitchAssinaturaDigital', e => {
    alterarValorInputAssinaturaDigital(e.target.checked);
  });

  $(document).on('change', '#customSwitchExcecao', e => {
    verificarNecessidadePreenchimentoExcecao(e.target.checked);
  });

  $('#customBtn_Solicitação\\ Encaminhada').click(elemento => {
    verificarObrigatoriedadesParaSubmeterSolicitacao();
  });
});

// prettier-ignore
const linhas_assinaturaDigital = [10003,10038,10040,10042,10044,10046,10074,10075,10077,10104,11049,11820,11727,11729,10017,10013,10011,10028,10095,10088,10087];

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#ContainerAttach').hide();
  $('#divStatisticsTable').hide();
  $('#customBtn_Solicitação\\ Encaminhada').hide();
  $('#btnCancel').hide();

  // configurações default do formulário
  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');

  // esconder elementos do formulário
  $('#coluna-numero-emprestimo').hide();
  $('#section-informacoes-emprestimo').hide();
  $('#section-informacoes-arquivamento').hide();
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const desativarInputsPopuladosPorAPI = () => {
  let inputInfosEmprestimo = $('#section-informacoes-emprestimo').find('input');
  inputInfosEmprestimo.attr('readOnly', true);
};

const limparInformcoesFormulario = async () => {
  $('#section-informacoes-emprestimo').hide();
  $('#section-informacoes-garantias').hide();
  $('#ContainerAttach').hide();

  let inputInfosEmprestimo = $('#section-informacoes-emprestimo').find('input');
  await _.forEach(inputInfosEmprestimo, input => {
    $(input).val('');
  });

  $('inp:contratoSeraAssinadoDigitalmente').val('');
  $('inp:possuiExcecao').val('');
  $('inp:dadosDaExcecao').val('');

  await _.forEach($('#div-informacoes-garantias').children(), elemento => {
    $(elemento)
      .find('div.col-xs-12')
      .remove(),
      $(elemento)
        .find('h6.text-gray-dark.border-bottom.border-gray.pb-2')
        .remove();
  });

  informacoesEmprestimo = [];
  garantias = [];

  $('#switchAssinaturaDigital').remove();
  $('#switchExcecao').remove();
};

const validarSubmissaoEmprestimo = () => {
  toggleSpinner();
  let numeroEmprestimo = $('inp:numero').val();

  if (!numeroEmprestimo) {
    toggleSpinner();
    toastr.error('Preencha um dado para que possamos iniciar a busca!');
    return;
  }

  verificarQualRequisicaoRealizar();
};

const verificarQualRequisicaoRealizar = () => {
  let tipo = $('inp:tipo').val();

  switch (tipo) {
    case 'Proposta de Crédito':
      buscarinformacoesEmprestimo(url_proposta_credito);
      break;
    case 'Termo Aditivo':
      buscarinformacoesEmprestimo(url_aditivo);
      break;
    case 'Renegociação':
      buscarinformacoesEmprestimo(url_renegociacao);
      break;
    case 'Borderô de Desconto':
      buscarinformacoesEmprestimo(url_titulos);
      break;
  }
};

const buscarinformacoesEmprestimo = async url => {
  let numeroEmprestimo = $('inp:numero').val();

  await axios
    .post(url, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      if (response.data && response.data.success && response.data.success[0]) {
        informacoesEmprestimo = response.data.success[0];
        buscarGarantiasAvalista(numeroEmprestimo);
      } else {
        toggleSpinner();
        toastr.error(
          'Não encontramos esta informação. Ajuste e tente novamente!'
        );
      }
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  preencherDadosDoEmprestimoNosInputs();
};

const preencherDadosDoEmprestimoNosInputs = async () => {
  let tipo = $('inp:tipo').val();
  let valorEmMoeda = await transformarEmFormatoMoeda(
    informacoesEmprestimo.fields.Valor
  );

  $('inp:conta').val(informacoesEmprestimo.txt);
  $('inp:associado').val(informacoesEmprestimo.fields.Associado);
  $('inp:linhaDeCredito').val(
    informacoesEmprestimo.fields.DescricaoLinhaCredito
  );
  $('inp:valor').val(valorEmMoeda);
  $('inp:finalidade').val(informacoesEmprestimo.fields.Finalidade);
  $('inp:dataSolicitacao').val(informacoesEmprestimo.fields.DataSolicitacao);
  $('inp:parcelas').val(informacoesEmprestimo.fields.Parcelas);
  $('inp:seguroPrestamista').val(informacoesEmprestimo.fields.Prestamista);
  $('inp:tipoVencimento').val(informacoesEmprestimo.fields.TipoVencimento);
  $('inp:diaVencimento').val(informacoesEmprestimo.fields.DiaVencimento);
  $('inp:carenciaPrimParc').val(informacoesEmprestimo.fields.Carencia);
  $('inp:jurosNaCarencia').val(informacoesEmprestimo.fields.JurosNaCarencia);
  $('inp:financiarIof').val(informacoesEmprestimo.fields.FinanciarIOF);

  switch (tipo) {
    case 'Proposta de Crédito':
      $('inp:contratoSeraAssinadoDigitalmente').val('Não');
      break;
    case 'Renegociação':
      $('inp:numeroContratoAnteriorMae').val(
        informacoesEmprestimo.fields.PropostaAnterior
      );
      break;
    case 'Borderô de Desconto':
      let valorTituloEmMoeda = await transformarEmFormatoMoeda(
        informacoesEmprestimo.fields.ValorTitulo
      );

      $('inp:numeroContratoAnteriorMae').val(
        informacoesEmprestimo.fields.PropostaPai
      );
      $('inp:valorTitulo').val(valorTituloEmMoeda);
      $('inp:dataVencimentoTitulo').val(
        informacoesEmprestimo.fields.DataVencimentoTitulo
      );
      $('inp:totalParcelasTitulo').val(
        informacoesEmprestimo.fields.ParcelasTitulo
      );
      break;
  }
  $('inp:possuiExcecao').val('Não');

  if (_.findIndex(garantias, { tipo: 'Imóvel' }) != -1) {
    $('inp:garantiaImovelVinculada').val('Sim');
  } else {
    $('inp:garantiaImovelVinculada').val('Não');
  }

  apresentarInformacoesGeradasAoSolicitante();
};

const apresentarInformacoesGeradasAoSolicitante = () => {
  let tipo = $('inp:tipo').val();
  let postoRequisitante = $('inp:postoRequisitante').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  $('#div-informacoes-emprestimo-sixthRow').prepend($(inputSwitchExcecao()));

  switch (tipo) {
    case 'Proposta de Crédito':
      if (
        postoRequisitante == '00' ||
        postoRequisitante == '03' ||
        postoRequisitante == '90'
      ) {
        $('#div-informacoes-emprestimo-sixthRow').prepend(
          $(inputSwitchAssinaturaDigital())
        );
      }
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      break;
  }

  $('#coluna-input-assinado-digitalmente').hide();
  $('#coluna-input-excecao').hide();
  $('#div-informacoes-emprestimo-seventhRow').hide();

  $('#section-informacoes-garantias').show();

  $('#annex').hide();
  $('#ContainerAttach').show();
  $('#customBtn_Solicitação\\ Encaminhada').show();

  toggleSpinner();
  toastr.success(`Informações do(a) ${tipo} geradas com sucesso!`);
};

const alterarValorInputAssinaturaDigital = async check => {
  await _.forEach(
    $('div#invalid-feedback-envio-requisicao'),
    invalidFeedbackRequest => {
      $(invalidFeedbackRequest).remove();
    }
  );

  switch (check) {
    case true:
      if (
        linhas_assinaturaDigital.indexOf(
          parseInt(informacoesEmprestimo.fields.LinhaCredito)
        ) == -1
      ) {
        toastr.error(
          'Essa linha de crédito ainda não está parametrizada para o processo de assinatura digital. Valide com a sua Unidade Administrativa!'
        );
        $('#customSwitchAssinaturaDigital').attr('checked', false);
        return;
      }

      if (
        $('span.badge-success')
          .text()
          .match(/Contrato/gi)
      ) {
        toastr.error(
          'Primeiro remova o contrato anexado, pois será gerado automaticamente ao cooperado na ferramenta DocuSign!'
        );
        $('#customSwitchAssinaturaDigital').attr('checked', false);
        return;
      }

      if (
        $('span.badge-success')
          .text()
          .match(/Prestamista/gi)
      ) {
        toastr.error(
          'Primeiro remova o prestamista anexado, pois será gerado automaticamente ao cooperado na ferramenta DocuSign!'
        );
        $('#customSwitchAssinaturaDigital').attr('checked', false);
        return;
      }

      await _.forEach($('#customizedUpload span.badge'), docList => {
        $(docList)
          .text()
          .match(/Contrato/gi) ||
        $(docList)
          .text()
          .match(/Prestamista/gi)
          ? captureModule.capture.removeDoc($(docList).attr('cod'))
          : null;
      });
      $('inp:contratoSeraAssinadoDigitalmente').val('Sim');
      toastr.info(
        'Atividade será encaminhada para a área BackOffice gerar o contrato e/ou prestamista para assinatura digital no DocuSign'
      );
      break;

    case false:
      $('inp:contratoSeraAssinadoDigitalmente').val('Não');

      const lengthDocs = $('span.badge:not(.badge-important)').length;
      const ultimoCodDocs = $(
        $('span.badge:not(.badge-important)')[lengthDocs - 1]
      ).attr('cod');

      captureModule.capture.addDoc('Contrato (Obrigatório)');
      captureModule.capture.addDoc('Prestamista');

      const lengthDocsAfterInsert = $('span.badge:not(.badge-important)')
        .length;
      $($('span.badge:not(.badge-important)')[lengthDocsAfterInsert - 2]).attr(
        'cod',
        parseInt(ultimoCodDocs) + 1
      );
      $($('span.badge:not(.badge-important)')[lengthDocsAfterInsert - 1]).attr(
        'cod',
        parseInt(ultimoCodDocs) + 2
      );

      break;
  }
};

const verificarNecessidadePreenchimentoExcecao = async check => {
  switch (check) {
    case true:
      $('inp:possuiExcecao').val('Sim');
      $('inp:dadosDaExcecao').val('');
      $('inp:dadosDaExcecao')[0].setAttribute('required', 'S');
      $('#div-informacoes-emprestimo-seventhRow').show();
      $($('#coluna-textarea-excecao').children()[0]).append(
        `<div id="invalid-feedback-excecao" class="invalid-feedback">Retiramos a obrigatoriedade de todos os documentos. Sua solicitação passará por validação da Diretoria da Cooperativa! Disponibilizamos a possibilidade de anexar o Pedido de Exceção.</div>`
      );
      $('#invalid-feedback-excecao').show();
      _.forEach(
        $('div#invalid-feedback-envio-requisicao'),
        invalidFeedbackRequest => {
          $(invalidFeedbackRequest).remove();
        }
      );
      captureModule.capture.addDoc('Pedido de Exceção');
      break;
    case false:
      $('inp:possuiExcecao').val('Não');
      $('inp:dadosDaExcecao').val('');
      $('inp:dadosDaExcecao')[0].setAttribute('required', 'N');
      $('#div-informacoes-emprestimo-seventhRow').hide();
      $('#invalid-feedback-excecao').remove();
      await _.forEach($('#customizedUpload span.badge'), docList => {
        $(docList)
          .text()
          .match(/Exce/gi)
          ? captureModule.capture.removeDoc($(docList).attr('cod'))
          : null;
      });
      break;
  }
};

const verificarObrigatoriedadesParaSubmeterSolicitacao = () => {
  _.forEach(
    $('div#invalid-feedback-envio-requisicao'),
    invalidFeedbackRequest => {
      $(invalidFeedbackRequest).remove();
    }
  );
  let possuiExcecao = $('inp:possuiExcecao').val();
  let tipo = $('inp:tipo').val();
  let assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();
  let docsPendentes = $('#customizedUpload span.badge-secondary').map(
    (index, element) =>
      $(element)
        .text()
        .match(/Obrigat/gi)
  );

  if (possuiExcecao === 'Não') {
    switch (tipo) {
      case 'Proposta de Crédito':
        if (assinaturaDigital === 'Sim') {
          $('#switchAssinaturaDigital').remove();
          $('#switchExcecao').remove();
          doAction('Solicitação Encaminhada', false, false);
          return;
        }
      case 'Renegociação':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Documento Contrato Deve ser Anexado!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          $('#switchAssinaturaDigital').remove();
          $('#switchExcecao').remove();
          doAction('Solicitação Encaminhada', false, false);
        }
        break;
      case 'Borderô de Desconto':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          $('#switchAssinaturaDigital').remove();
          $('#switchExcecao').remove();
          doAction('Solicitação Encaminhada', false, false);
        }
        break;
      case 'Termo Aditivo':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          $('#switchAssinaturaDigital').remove();
          $('#switchExcecao').remove();
          doAction('Solicitação Encaminhada', false, false);
        }
        break;
    }
  } else if (possuiExcecao === 'Sim') {
    $('#switchAssinaturaDigital').remove();
    $('#switchExcecao').remove();
    doAction('Solicitação Encaminhada', false, false);
  }
};
