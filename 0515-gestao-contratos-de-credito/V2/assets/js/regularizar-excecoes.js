$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();
  spinnerLoaderOnAnalyse();
  buscarGarantiasAvalista($('inp:numero').val());

  $(document).on('change', '#customSwitchExcecao', e => {
    verificarNecessidadePreenchimentoExcecao(e.target.checked);
  });

  $('#customBtn_Novas\\ Informações\\ Encaminhadas').click(elemento => {
    verificarObrigatoriedadesParaSubmeterAtividade();
  });

  $('#inpDsReasonInputReason').focusout(element => {
    $('#switchExcecao').remove();
  });
});

function ajustarFormulario() {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // esconder elementos do formulário
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();

  // configurações default do formulário
  $('inp:possuiExcecao').attr('readOnly', true);
  $('#customBtn_Novas\\ Informações\\ Encaminhadas').removeAttr('onclick');
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  buscarDocumentosObrigatoriosParaContinuarSolicitacao();
};

const buscarDocumentosObrigatoriosParaContinuarSolicitacao = async () => {
  let documentosJaImportados = [];
  _.forEach($('#tblFile td.docType'), tipoDocumentoImportado =>
    documentosJaImportados.push($(tipoDocumentoImportado).text())
  );

  //Adicionar Estrutura Formalization no Form
  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();
  //Buscar Documentos e Tratar Obrigatoriedade
  let token = $('#inpToken').val(),
    tipo = $('inp:tipo').val();
  const requestListOfDocuments = {
    url: url_regra_negocio,
    method: 'POST',
    headers: { Authorization: token, 'content-type': 'application/json' },
    data: [
      {
        DsFieldName: 'tipo',
        DsValue: tipo,
      },
    ],
  };

  try {
    let { data } = await axios(requestListOfDocuments);
    const documentos = JSON.parse(data[0].DsReturn);

    await $.each(documentos, (key, value) => {
      documentosJaImportados.indexOf(value.Document) == -1
        ? captureModule.capture.addDoc(value.Document)
        : null;
    });
    captureModule.capture.addDoc('Pedido de Exceção');
    apresentarInformacoesGeradasAoSolicitanteParaVerificarRegularizacaoDeExcecao();
  } catch (error) {
    spinnerLoaderOffAnalyse();
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    console.log(error);
  }
};

const apresentarInformacoesGeradasAoSolicitanteParaVerificarRegularizacaoDeExcecao = async () => {
  let tipo = $('inp:tipo').val();
  let assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipo) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();

      if (assinaturaDigital === 'Sim') {
        await _.forEach($('#customizedUpload span.badge'), docList => {
          $(docList)
            .text()
            .match(/Contrato/gi)
            ? captureModule.capture.removeDoc($(docList).attr('cod'))
            : null;
        });
      }

      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  $('#div-informacoes-emprestimo-sixthRow').append($(inputSwitchExcecao()));
  $('#coluna-input-excecao').hide();
  $('#customSwitchExcecao').prop('checked', true);

  $($('#coluna-textarea-excecao').children()[0]).append(
    `<div id="invalid-feedback-excecao" class="invalid-feedback">Como a soliictação está selecionada como exceção, os documentos não são obrigatórios, porém, a requisição necessita de aprovação da Diretoria.</div>`
  );
  $('#invalid-feedback-excecao').show();

  $('#section-informacoes-garantias').show();
  toastr.success(`Informações do(a) ${tipo} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const verificarNecessidadePreenchimentoExcecao = async check => {
  switch (check) {
    case true:
      $('inp:possuiExcecao').val('Sim');
      $('inp:dadosDaExcecao').val('');
      $('inp:dadosDaExcecao')[0].setAttribute('required', 'S');
      $('#div-informacoes-emprestimo-seventhRow').show();
      $($('#coluna-textarea-excecao').children()[0]).append(
        `<div id="invalid-feedback-excecao" class="invalid-feedback">Retiramos a obrigatoriedade de todos os documentos. Sua solicitação passará por validação da Diretoria da Cooperativa! Disponibilizamos a possibilidade de anexar o Pedido de Exceção.</div>`
      );
      $('#invalid-feedback-excecao').show();
      _.forEach(
        $('div#invalid-feedback-envio-requisicao'),
        invalidFeedbackRequest => {
          $(invalidFeedbackRequest).remove();
        }
      );
      captureModule.capture.addDoc('Pedido de Exceção');
      break;
    case false:
      $('inp:possuiExcecao').val('Não');
      $('inp:dadosDaExcecao').val('');
      $('inp:dadosDaExcecao')[0].setAttribute('required', 'N');
      $('#div-informacoes-emprestimo-seventhRow').hide();
      $('#invalid-feedback-excecao').remove();
      await _.forEach($('#customizedUpload span.badge'), docList => {
        $(docList)
          .text()
          .match(/Exce/gi)
          ? captureModule.capture.removeDoc($(docList).attr('cod'))
          : null;
      });
      break;
  }
};

const verificarObrigatoriedadesParaSubmeterAtividade = () => {
  _.forEach(
    $('div#invalid-feedback-envio-requisicao'),
    invalidFeedbackRequest => {
      $(invalidFeedbackRequest).remove();
    }
  );
  let possuiExcecao = $('inp:possuiExcecao').val();
  let tipo = $('inp:tipo').val();
  let docsPendentes = $('#customizedUpload span.badge-secondary').map(
    (index, element) =>
      $(element)
        .text()
        .match(/Obrigat/gi)
  );

  if (possuiExcecao === 'Não') {
    switch (tipo) {
      case 'Proposta de Crédito':
      case 'Renegociação':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Documento Contrato Deve ser Anexado!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          doAction('Novas Informações Encaminhadas', false, true);
        }
        break;
      case 'Borderô de Desconto':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          doAction('Novas Informações Encaminhadas', false, true);
        }
        break;
      case 'Termo Aditivo':
        if (docsPendentes.length > 0) {
          $('#customizedUpload').append(
            `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`
          );
          toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
          $('#invalid-feedback-envio-requisicao').show();
        } else {
          doAction('Novas Informações Encaminhadas', false, true);
        }
        break;
    }
  } else if (possuiExcecao === 'Sim') {
    doAction('Novas Informações Encaminhadas', false, true);
  }
};
