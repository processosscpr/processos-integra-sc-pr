$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();
  spinnerLoaderOnAnalyse();
  buscarGarantiasAvalista($('inp:numero').val());

  // adiciona painel atrás dos critérios de uma aba de formalização
  $('#tab1 p').click(e => {
    $('#tab1')
      .children()
      .children()
      .each(function(k, v) {
        if ($(v).find('[checkviewer] p').length === 0) return;
        $(v)
          .find('[checkviewer]')
          .addClass('conferencia-card');
      });
  });
});

function ajustarFormulario() {
  analisysModule.regularization.load();

  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');
  $($('#btnsFormalization').children()[2]).hide();
  $('#annex button.btn.btn-primary.btn-small').hide();

  // esconder elementos do formulário
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();

  $('#tab1').prepend(
    '<div class="alert alert-info" style="margin-right: 5px; margin-left: 5px"><small>Para regularizar os documentos reprovados clique no ícone: <span class="badge badge-secondary" style="padding: 2px 4px 2px 4px;"><i class="icon-white icon-upload"></i></span></small></div>'
  );
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  apresentarInformacoesGeradasAoUsuario();
};

const apresentarInformacoesGeradasAoUsuario = () => {
  let tipo = $('inp:tipo').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipo) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();
      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  $('inp:possuiExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-seventhRow').hide()
    : $('#div-informacoes-emprestimo-seventhRow').show();

  $('#section-informacoes-garantias').show();
  toastr.success(`Informações do(a) ${tipo} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};
