$(document).ready(function() {
  adaptarFormulario();

  if (isMobile()) {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }
});

function adaptarCargo() {
  if (
    document.querySelector("[xname='inpcodigoNivel']").value == 'Supervisor' ||
    document.querySelector("[xname='inpcodigoNivel']").value == 'Coordenador' ||
    document.querySelector("[xname='inpcodigoNivel']").value == 'Gerente'
  ) {
    document.querySelector("[xname='inpportalDoColaborador']").show();
    document
      .querySelector("[xname='inpportalDoColaborador']")
      .setAttribute('required', 'S');
  } else {
    document.querySelector("[xname='inpportalDoColaborador']").hide();
    document
      .querySelector("[xname='inpportalDoColaborador']")
      .setAttribute('required', 'N');
  }
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarFormulario() {
  $('#informacoes-solicitacao').attr('class', 'box box-open-and-close');

  $('#tipo-solicitacao').attr('colspan', '2');

  $('#notificacao-gestor').hide();
  $('#informacoes-sistemas').hide();
  $('#informacoes-rescisao').hide();
  $('#detalhamento-sistemas').hide();
  $('#detalhamento-materiais').hide();
  $('#informacoes-acesso').hide();
  $('#informacoes-auxiliares').hide();
  $('#checklist-beneficios').hide();
}
