$(document).ready(function() {
  adaptarFormulario();

  if (!isMobile()) {
    $('#RequesterInfo').hide();
  } else {
    adaptarMobile();
  }
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();

  document
    .querySelector("[xname='inpnomeContratadoa']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpcpfContratadoa']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
}

function adaptarFormulario() {
  $('#materiais-escolhidos').hide();
  $('#informacoes-aso').hide();
  $('#informacoes-rescisao').hide();
  $('#informacoes-acesso').hide();
  $('#informacoes-auxiliares').hide();
  $('#divStatisticsTable').hide();
  $('#checklist-beneficios').hide();
  $('#informacoes-cargo').hide();
  $('#portalColaborador').hide();
  $('#informacoes-sistemas').hide();
}
