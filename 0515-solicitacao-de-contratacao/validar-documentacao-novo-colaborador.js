$(document).ready(function() {
  cryo_TaskClaimOwnershipWrapper();

  var AgrupadorFormulario = document.querySelector('#ContainerForm');
  var FilhoAgrupadorFormulario = $(AgrupadorFormulario).children();
  var DivFormulario = $(FilhoAgrupadorFormulario[0]).children();
  DivFormulario[0].innerHTML =
    'Formulário<button type="button" id="btnDesassociar" class="btn btn-primary;btn-group dropup right" style="background-color: rgb(0, 109, 204);color:white">Desassociar</button>';

  var clickButtonDesassociar = document.querySelector('#btnDesassociar');
  clickButtonDesassociar.addEventListener('click', function() {
    cryo_TaskUnclaimOwnershipWrapper();
    alert(
      'Alterações salvas!\nEsta atividade será reencaminhada para a fila de atendimento de sua área. \n\nVocê será redirecionado para a página inicial da Ferramenta Integra. \n\n*Desconsiderar próxima mensagem, ao clicar em OK!'
    );
    save();
  });

  adaptarFormulario();

  if (isMobile()) {
    adaptarMobile();
  }
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();

  document
    .querySelector("[xname='inpaso']")
    .setAttribute(
      'style',
      'height: 20px!important;width:50%!important;background-color:white'
    );
  document.querySelector("[xname='inpaso']").setAttribute('readonly', 'true');
}

function adaptarFormulario() {
  $('#informacoes-solicitacao').attr('class', 'box box-open-and-close');

  $('#informacoes-sistemas').hide();
  $('#informacoes-rescisao').hide();
  $('#detalhamento-sistemas').hide();
  $('#detalhamento-materiais').hide();
  $('#informacoes-acesso').hide();
  $('#informacoes-auxiliares').hide();
  $('#checklist-beneficios').hide();
  $('#portalColaborador').hide();
  $('#horarioTrabalho').attr('colSpan', '2');

  cryo_alert(
    'Documentos que possivelmente deverão ser anexados, favor verificar! \n\n- <b>Para Casado, União Estável ou Divorciado:</b> Certidão de casamento, união estável ou averbação de separação. \n- <b>Para filho menor ou igual a 14 anos:</b> Certidão de nascimento de filho ou doc de equiparado. \n- <b>Para sexo masculino:</b> Certificado de reservista. \n- <b>Para filho maior de 7 anos:</b> Comprovante de frequência escolar de filho ou equiparado. \n- <b> Para filho menor de 7 anos: </b>Comprovante de vacinação de filho ou equiparado. \n- <b>Para deficientes físicos:</b> Laudo médico. \n- <b>Para estado civil igual cadado ou união estável:</b> RG e CPF do cônjuge ou companheiro(a).'
  );
}
