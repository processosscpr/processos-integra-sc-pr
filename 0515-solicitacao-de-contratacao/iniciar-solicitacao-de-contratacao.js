$(document).ready(function() {
  verificarPosicaoIniciadora();
  adaptarFormulario();

  if (!isMobile()) {
    $('#RequesterInfo').hide();
  } else {
    adaptarMobile();
  }

  $('inp:acessoSistemas').change(apresentarBlocoSistemas);
  $('inp:necessitaMaterialDeExpediente').change(apresentarBlocoMateriais);
  $('inp:materiaisDeExpediente').change(replicarMateriaisSelecionados);
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();

  document
    .querySelector("[xname='inptipoSolicitacao']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpnotificarGestorParaConhecimentoDaSolicitacao']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpnomeContratadoa']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpcpfContratadoa']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute(
      'style',
      'height: 20px!important;width:50%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpnivel']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpcargo']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpfaixaSalarial']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpcentroDeCusto']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inphorarioDeTrabalho']")
    .setAttribute('style', 'width:85%!important');
}

function adaptarFormulario() {
  $('#informacoes-solicitacao').attr('class', 'box box-open-and-close');
  $('#informacoes-sistemas').attr('class', 'box box-open-and-close');

  $('#materiais').attr('colspan', '2');

  $('#informacoes-aso').hide();
  $('#informacoes-rescisao').hide();
  $('#informacoes-cargo').hide();
  $('#detalhamento-sistemas').hide();
  $('#detalhamento-materiais').hide();
  $('#materiais-escolhidos').hide();
  $('#informacoes-acesso').hide();
  $('#informacoes-auxiliares').hide();
  $('#divStatisticsTable').hide();
  $('#btnCancel').hide();
  $('#checklist-beneficios').hide();
  $('#portalColaborador').hide();
}

function apresentarBlocoSistemas() {
  if (document.getElementById('inpacessoSistemas-1').checked) {
    $('#detalhamento-sistemas').show();
    document
      .querySelector("[xname='inpsistemasEquipamentos1']")
      .setAttribute('required', 'S');
    document
      .querySelector("[xname='inpsistemasEquipamentos2']")
      .setAttribute('required', 'S');
  } else if (document.getElementById('inpacessoSistemas-0').checked) {
    $('#detalhamento-sistemas').hide();
    document
      .querySelector("[xname='inpsistemasEquipamentos1']")
      .setAttribute('required', 'N');
    document
      .querySelector("[xname='inpsistemasEquipamentos2']")
      .setAttribute('required', 'N');
    for (
      var index =
        $(document.getElementById('detalhamento-sistemas')).find('input')
          .length - 1;
      index >= 0;
      index--
    ) {
      $(document.getElementById('detalhamento-sistemas')).find('input')[
        index
      ].checked = false;
    }
  }
}

function apresentarBlocoMateriais() {
  if (document.getElementById('inpnecessitaMaterialDeExpediente-1').checked) {
    $('#detalhamento-materiais').show();
    document
      .querySelector("[xname='inpmateriaisDeExpediente']")
      .setAttribute('required', 'S');
  } else if (
    document.getElementById('inpnecessitaMaterialDeExpediente-0').checked
  ) {
    $('#detalhamento-materiais').hide();
    document
      .querySelector("[xname='inpmateriaisDeExpediente']")
      .setAttribute('required', 'N');
    for (
      var index =
        $(document.getElementById('detalhamento-materiais')).find('input')
          .length - 1;
      index >= 0;
      index--
    ) {
      $(document.getElementById('detalhamento-materiais')).find('input')[
        index
      ].checked = false;
    }
  }
}

function verificarPosicaoIniciadora() {
  if ($('select[name="inpCodPositionArea"]').val() != undefined) {
    var arrayAuxiliar = new Array();
    for (
      var i = 0;
      i < document.querySelector("[name='inpCodPositionArea']").options.length;
      i++
    ) {
      arrayAuxiliar.push(
        document.querySelector("[name='inpCodPositionArea']").options[i]
          .innerText
      );
    }
    arrayAuxiliar.shift();
    for (var i = arrayAuxiliar.length - 1; i >= 0; i--) {
      if (arrayAuxiliar[i].substring(4, 5) == '_') {
        arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]), 1);
      }
    }
    $('select[name="inpCodPositionArea"]').val(
      $("option:contains('" + arrayAuxiliar[0] + "')").val()
    );
    $(
      $('select[name="inpCodPositionArea"]')
        .parent()
        .parent()
    ).hide();
  }
}

function replicarMateriaisSelecionados() {
  var listaMateriaisSelecionadas = $(document.querySelector('#materiais')).find(
    'input'
  );
  var materiaisGerados = '';
  for (var i = 0; i < listaMateriaisSelecionadas.length; i++) {
    if (listaMateriaisSelecionadas[i].checked) {
      if (materiaisGerados == '') {
        materiaisGerados += listaMateriaisSelecionadas[i].value;
      } else {
        materiaisGerados += ', ' + listaMateriaisSelecionadas[i].value;
      }
    }
  }
  document.querySelector(
    "[xid='divmateriaisSelecionados']"
  ).textContent = materiaisGerados;
  document.querySelector(
    "[xname='inpmateriaisSelecionados']"
  ).value = materiaisGerados;
}
