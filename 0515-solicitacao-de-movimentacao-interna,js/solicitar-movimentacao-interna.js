/**
  PR_0515 - Solicitação de Movimentação Interna

  Versão do Processo: 3
  Versão do Formulário: 2
  Escopo:
    Quando houver alteração de centro de custo, encaminharemos atividade ao DP para realizar tais procedimentos.

    Fluxo deve ser iniciado pela Fanny ou Marina, indicando a notificação para um gestor. Após, será encaminhado para a ASS realizar a movimentação, TI e para o DP da 0507 realizarem os devidos procedimentos, quando as condições para assumirem atividades desse processo forem verdadeiras (TUDO PARALELO).
 */

$(document).ready(function() {
  $(document.querySelector('#divStatisticsTable')).hide();
  $(document.querySelector('#btnCancel')).hide();
  $(document.querySelector('#tblAjustePerfilGestao')).hide();
  $(document.querySelector('#seguroDitevg')).hide();

  if (!isMobile()) {
    adaptarWeb();
  } else {
    adaptarMobile();
  }

  $('inp:nomeColaborador').change(function() {
    document.getElementById('inptipoDeMovimentacao-0').checked = false;
    document.getElementById('inptipoDeMovimentacao-1').checked = false;
    document.getElementById('inptipoDeMovimentacao-2').checked = false;
    tipoMovimentacao();
    colaboradorResponsavel();
  });

  $('inp:tipoDeMovimentacao').change(function() {
    tipoMovimentacao();
    colaboradorResponsavel();
  });

  $('inp:dataDaMovimentacao')
    .datepicker()
    .on('changeDate', function(ev) {
      validarDataMovimentacao();
      preencherDataAjusteFerramenta();
    });

  document.getElementById('inptipoDeMovimentacao-0').onclick = function() {
    exibirMensagemAlteracaoSalario();
  };

  $(document.getElementById('customBtn_Solicitação Encaminhada')).click(
    function() {
      preencherDataAjusteFerramenta();
    }
  );

  $('inp:salario').change(function() {
    verificarSalario();
  });

  tipoMovimentacao();
  verificarPosicaoIniciadora();
  colaboradorResponsavel();
});

function exibirMensagemAlteracaoSalario() {
  var alteracaoCargo = document.getElementById('inptipoDeMovimentacao-0')
    .checked;
  if (alteracaoCargo == true) {
    $(document.querySelector('#tblAlteracaoCargo')).show();
    cryo_confirm(
      'Esta mudança de cargo impacta na alteração da faixa salarial?',
      function() {
        document.getElementById('inptipoDeMovimentacao-2').checked = true;
        $(document.querySelector('#tblAlteracaoSalario')).show();
        document
          .querySelector("[xname='inpsalario']")
          .setAttribute('required', 'S');

        var alteracaoSalario = document.getElementById(
          'inptipoDeMovimentacao-2'
        ).checked;
        obrigaPreencher("[xname='inpsalario']", alteracaoSalario);
        document.querySelector(
          "[xname='inpalteracaoDeSalario']"
        ).value = alteracaoSalario ? 'Sim' : 'Não';
        document.querySelector(
          "[xid='divalteracaoDeSalario']"
        ).value = alteracaoSalario ? 'Sim' : 'Não';
        document.querySelector(
          "[xid='divalteracaoDeSalario']"
        ).textContent = alteracaoSalario ? 'Sim' : 'Não';
      }
    );
  } else {
    document.getElementById('inptipoDeMovimentacao-2').checked = false;
    document
      .querySelector("[xname='inpsalario']")
      .setAttribute('required', 'N');
  }
}

function preencherDataAjusteFerramenta() {
  var dataAjuste = null;
  var ajustarFerramentas = '';

  if (
    document.getElementById('inptipoDeMovimentacao-0').checked ||
    document.getElementById('inptipoDeMovimentacao-1').checked
  ) {
    dataAjuste = diminuirDia(
      retornaDataXname("[xname='inpdataDaMovimentacao']"),
      7
    );
    document.querySelector(
      "[xid='divdataAjusteFerramentas']"
    ).textContent = retornaDataFormatada(dataAjuste);
    document.querySelector(
      "[xid='divdataAjusteFerramentas']"
    ).value = retornaDataFormatada(dataAjuste);
    document.querySelector(
      "[xname='inpdataAjusteFerramentas']"
    ).value = retornaDataFormatada(dataAjuste);
  }

  if (dataAjuste == null) {
    ajustarFerramentas = 'Não';
  } else {
    ajustarFerramentas = dataAjuste < new Date() ? 'Sim' : 'Não';
  }

  document.querySelector(
    "[xid='divajustarFerramentas']"
  ).textContent = ajustarFerramentas;
  document.querySelector(
    "[xid='divajustarFerramentas']"
  ).value = ajustarFerramentas;
  document.querySelector(
    "[xname='inpajustarFerramentas']"
  ).value = ajustarFerramentas;
}

//subtrair apenas dias úteis de uma data
function diminuirDia(data, qtdDia) {
  var i = 0;
  while (i < qtdDia) {
    data.setDate(data.getDate() - 1);
    if (data.getDay() > 0 && data.getDay() < 6) {
      i++;
    }
  }
  return data;
}

//retorna data formatada dd/mm/yyyy
function retornaDataFormatada(data) {
  var dataToString = '';
  if (data != null) {
    if (data.getDate() < 10) {
      dataToString = '0' + data.getDate();
    } else {
      dataToString = data.getDate();
    }
    if (data.getMonth() < 9) {
      dataToString = dataToString + '/0' + (data.getMonth() + 1);
    } else {
      dataToString = dataToString + '/' + (data.getMonth() + 1);
    }
    dataToString = dataToString + '/' + data.getFullYear();
  }
  return dataToString;
}

function colaboradorResponsavel() {
  var alteracaoCargo = document.getElementById('inptipoDeMovimentacao-0')
    .checked;
  var alteracaoCentroCusto = document.getElementById('inptipoDeMovimentacao-1')
    .checked;
  var tipoArea = document.querySelector("[xid='divtipoArea']").textContent;
  if (
    (alteracaoCargo || alteracaoCentroCusto) &&
    (tipoArea == 'Coordenadoria' ||
      tipoArea == 'Diretoria' ||
      tipoArea == 'Gerência' ||
      tipoArea == 'Gerência de Agência' ||
      tipoArea == 'Superintendência' ||
      tipoArea == 'Supervisão Administrativo Financeiro' ||
      tipoArea == 'Supervisão de Área' ||
      tipoArea == 'Empresa')
  ) {
    obrigaPreencher("[xname='inpcolaboradorResponsavelPorAtividade']", true);
    $(document.querySelector('#tblColaboradorResponsavel')).show();
  } else {
    $(document.querySelector('#tblColaboradorResponsavel')).hide();
    obrigaPreencher("[xname='inpcolaboradorResponsavelPorAtividade']", false);
    document.querySelector(
      "[xname='inpcolaboradorResponsavelPorAtividade']"
    ).selectedIndex = 0;
  }
}

function filtrarAgenciaCooperativaSolicitante() {
  var agencias = document.querySelector("[xname='inpnovoCentroDeCusto']")
    .options;
  var cooperativaSolicitante = document.querySelector(
    "[xid='divcooperativaRequisitante']"
  ).textContent;
  for (var i = 1; i < agencias.length; i++) {
    if (!agencias[i].dataset.others.includes(cooperativaSolicitante)) {
      agencias[i].remove();
      i--;
    }
  }
}

function consultarAgencias() {
  //Fonte de Dados: 0507 - Agências Unicred SC/PR
  var c =
    'qw0Xk6xWKL563BI8VvBqJqhlHAWTAwb5Mw5ScMKgqOoJezfcFRMj8u8YG2ECR79@LLfWg6AM84vncbn7Q8zsGg__';

  var jqxhr = $.ajax({
    type: 'post',
    dataType: 'json',
    url: '../api/json/datasource/get/1.0/' + c,
    data: '',
  }).done(function(data) {
    if (data.success != null) {
      if (data.cache) {
        $(document.querySelector("[xname='inpnovoCentroDeCusto']")).attr(
          'data-from-cache',
          true
        );
      }
      if (data.runtime) {
        $(document.querySelector("[xname='inpnovoCentroDeCusto']")).attr(
          'data-runtime',
          data.runtime
        );
      }
      if (data.bytes) {
        $(document.querySelector("[xname='inpnovoCentroDeCusto']")).attr(
          'data-bytes',
          data.bytes
        );
      }

      $(document.querySelector("[xname='inpnovoCentroDeCusto']"))
        .find('option')
        .remove();
      $(document.querySelector("[xname='inpnovoCentroDeCusto']")).append(
        '<option value="">Selecione</option>'
      );
      $.each(data.success, function(i, item) {
        if (
          document.querySelector("[xid='divcooperativaRequisitante']")
            .textContent == item.fields.cooperativaRequisitante
        ) {
          $(document.querySelector("[xname='inpnovoCentroDeCusto']")).append(
            "<option value='" +
              item.cod +
              "'" +
              (item.fields != null && item.fields != ''
                ? "data-others='" + JSON.stringify(item.fields) + "'"
                : '') +
              '>' +
              item.txt +
              '</option>'
          );
        }
      });
    }
  });
}

function validarDataMovimentacao() {
  var dataMovimentacao = retornaDataXname("[xname='inpdataDaMovimentacao']");
  if (dataMovimentacao != null) {
    var dataAtual = new Date();
    var limpaCampo = false;
    if (dataMovimentacao <= dataAtual) {
      cryo_alert('Data de Movimentação deve ser maior que Data Atual.');
      document.querySelector("[xname='inpdataDaMovimentacao']").value = '';
      document.querySelector("[xname='inpdataDaMovimentacao']").textContent =
        '';
    }
  }
}

//retorna data do campo do formulário
function retornaDataXname(xname) {
  var data = document.querySelector(xname).value;
  if (data != '') {
    var dataSplit = data.split('/');
    var dataCompleta = new Date(dataSplit[2], dataSplit[1] - 1, dataSplit[0]);
    return dataCompleta;
  } else {
    return null;
  }
}

function tipoMovimentacao() {
  var alteracaoCargo = document.getElementById('inptipoDeMovimentacao-0')
    .checked;
  var alteracaoCentroCusto = document.getElementById('inptipoDeMovimentacao-1')
    .checked;
  var alteracaoSalario = document.getElementById('inptipoDeMovimentacao-2')
    .checked;

  if (alteracaoCargo) {
    $(document.querySelector('#tblAlteracaoCargo')).show();
    document
      .querySelector("[xname='inpnivelCargo']")
      .setAttribute('required', 'S');
    document.querySelector("[xname='inpcargo']").setAttribute('required', 'S');
  } else {
    $(document.querySelector('#tblAlteracaoCargo')).hide();
    document.querySelector("[xname='inpnivelCargo']").selectedIndex = 0;
    document.querySelector("[xname='inpcargo']").selectedIndex = 0;
    document
      .querySelector("[xname='inpnivelCargo']")
      .setAttribute('required', 'N');
    document.querySelector("[xname='inpcargo']").setAttribute('required', 'N');
  }
  obrigaPreencher("[xname='inpnivelCargo']", alteracaoCargo);
  obrigaPreencher("[xname='inpcargo']", alteracaoCargo);
  document.querySelector("[xname='inpalteracaoDeCargo']").value = alteracaoCargo
    ? 'Sim'
    : 'Não';
  document.querySelector("[xid='divalteracaoDeCargo']").value = alteracaoCargo
    ? 'Sim'
    : 'Não';
  document.querySelector(
    "[xid='divalteracaoDeCargo']"
  ).textContent = alteracaoCargo ? 'Sim' : 'Não';

  if (alteracaoCentroCusto) {
    $(document.querySelector('#tblAlteracaoCentroCusto')).show();
    document
      .querySelector("[xname='inpcentroDeCusto']")
      .setAttribute('required', 'S');
  } else {
    $(document.querySelector('#tblAlteracaoCentroCusto')).hide();
    document.querySelector("[xname='inpcentroDeCusto']").value = '';
    document
      .querySelector("[xname='inpcentroDeCusto']")
      .setAttribute('required', 'N');
  }
  obrigaPreencher("[xname='inpcentroDeCusto']", alteracaoCentroCusto);
  document.querySelector(
    "[xname='inpalteracaoDeCentroDeCusto']"
  ).value = alteracaoCentroCusto ? 'Sim' : 'Não';
  document.querySelector(
    "[xid='divalteracaoDeCentroDeCusto']"
  ).value = alteracaoCentroCusto ? 'Sim' : 'Não';
  document.querySelector(
    "[xid='divalteracaoDeCentroDeCusto']"
  ).textContent = alteracaoCentroCusto ? 'Sim' : 'Não';

  if (alteracaoSalario) {
    $(document.querySelector('#tblAlteracaoSalario')).show();
    document
      .querySelector("[xname='inpsalario']")
      .setAttribute('required', 'S');
    $(document.querySelector('#tblAlteracaoCargo')).show();
    document
      .querySelector("[xname='inpnivelCargo']")
      .setAttribute('required', 'S');
    document.querySelector("[xname='inpcargo']").setAttribute('required', 'S');
  } else {
    $(document.querySelector('#tblAlteracaoSalario')).hide();
    document.querySelector("[xname='inpsalario']").selectedIndex = 0;
    document
      .querySelector("[xname='inpsalario']")
      .setAttribute('required', 'N');
  }
  obrigaPreencher("[xname='inpsalario']", alteracaoSalario);
  document.querySelector(
    "[xname='inpalteracaoDeSalario']"
  ).value = alteracaoSalario ? 'Sim' : 'Não';
  document.querySelector(
    "[xid='divalteracaoDeSalario']"
  ).value = alteracaoSalario ? 'Sim' : 'Não';
  document.querySelector(
    "[xid='divalteracaoDeSalario']"
  ).textContent = alteracaoSalario ? 'Sim' : 'Não';
}

//Obriga ou Não o preenchimento do campo
function obrigaPreencher(xname, obrigatorio) {
  document
    .querySelector(xname)
    .setAttribute('required', obrigatorio ? 'S' : 'N');
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarWeb() {
  document
    .querySelector("[xname='inpnomeColaborador']")
    .setAttribute('style', 'width:70%!important');
  document
    .querySelector("[xname='inpcargo']")
    .setAttribute('style', 'width:80%!important');
  document
    .querySelector("[xname='inpcentroDeCusto']")
    .setAttribute('style', 'width:70%!important');
  document
    .querySelector("[xname='inpjustificativaMovimentacao']")
    .setAttribute('style', 'width:65%!important');
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();
  document
    .querySelector("[xname='inpnomeColaborador']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpdataDaMovimentacao']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataDaMovimentacao']")
    .setAttribute('readyonly', 'true');
  document
    .querySelector("[xname='inpjustificativaMovimentacao']")
    .setAttribute('style', 'height: 40px!important;width:85%!important');
  document
    .querySelector("[xname='inpcargo']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpcentroDeCusto']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpsalario']")
    .setAttribute('style', 'width:85%!important');
}

function verificarPosicaoIniciadora() {
  if ($('select[name="inpCodPositionArea"]').val() != undefined) {
    var arrayAuxiliar = new Array();
    for (
      var i = 0;
      i < document.querySelector("[name='inpCodPositionArea']").options.length;
      i++
    ) {
      arrayAuxiliar.push(
        document.querySelector("[name='inpCodPositionArea']").options[i]
          .innerText
      );
    }
    arrayAuxiliar.shift();
    for (var i = arrayAuxiliar.length - 1; i >= 0; i--) {
      if (arrayAuxiliar[i].substring(4, 5) == '_') {
        arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]), 1);
      }
    }
    $('select[name="inpCodPositionArea"]').val(
      $("option:contains('" + arrayAuxiliar[0] + "')").val()
    );
    $(
      $('select[name="inpCodPositionArea"]')
        .parent()
        .parent()
    ).hide();
  }
}

function verificarSalario() {
  var salario = document.querySelector("[xname='inpsalario']").value;
  var salarioSubstring = salario.substring(12, 22);
  var salarioSemPonto = salarioSubstring.replace('.', '');
  var salarioSemVirgula = salarioSemPonto.replace(',', '.');
  var salarioFinal = parseFloat(salarioSemVirgula);

  var salarioComparado = 8561.42;

  if (salarioFinal > salarioComparado) {
    $(document.querySelector('#seguroDitevg')).show();
    document
      .querySelector("[xname='inpseguroDitevg']")
      .setAttribute('required', 'S');
  } else {
    $(document.querySelector('#seguroDitevg')).hide();
    document
      .querySelector("[xname='inpseguroDitevg']")
      .setAttribute('required', 'N');
    if (document.querySelector("[xid='divseguroDitevg']").textContent !== '') {
      $(document.getElementsByClassName('btn btn-mini btn-danger')).click();
    }
  }
}

//Exibe campo
function exibirCampo(idCampo) {
  $(document.getElementById(idCampo)).show();
}

//Oculta campo
function ocultarCampo(idCampo) {
  $(document.getElementById(idCampo)).hide();
}
