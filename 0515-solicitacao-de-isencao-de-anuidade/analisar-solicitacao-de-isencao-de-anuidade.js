$(document).ready(function() {
  ajustarFormulario();
  adicionarMobileSpan();
  esconderSections();

  adicionarSectionsDaIsencao();
  adicionarMensagensAuxiliares();

  mostrarOuEsconderSecaoPeloRadio();
  definirCamposObrigatorios();

  renderizarEventosPorMudancaNoRange();
  renderizarEventosPorClick();

  $($('div.well')[0]).hide();
});

const renderizarEventosPorClick = () => {
  const btnSolicitacao = $('#customBtn_Isenção\\ Aprovada');

  btnSolicitacao.click(() => validarFormulario());
};

const renderizarEventosPorMudancaNoRange = () => {
  $('.rangePercentualAprovadoInput').change(e => {
    $('inp:percentualAprovado').val(e.target.value);

    $('.rangePercentualAprovadoOutput')[0].innerText = `${e.target.value} %`;
  });
};

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $('#div-msg-attach-documents').hide();
  $('#RequesterInfo').hide();
  $('#btnCancel').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // funções de manipulação / ajustes em formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');

  // configurações default do formulário
  $('#customBtn_Isenção\\ Aprovada').removeAttr('onclick');
};

const adicionarMobileSpan = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile =
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1;

  if (!isMobile) {
    $(
      `
        <span style='margin-left: 0.4em' class='label label-secondary'>
          <strong> Requisitante:${$('inp:nomeRequisitante').val()}</strong>
        </span>
      `
    ).insertAfter('.title span.label.label-info.flow-title');
  }
};

const esconderSections = () => {
  $('.span-porcentagem-aprovado').hide();
  $('.div-percentual-aprovado').hide();
};

const construirElementoInputRange = identificador => {
  return `
    <input
      type="range"
      class="custom-range range${identificador}Input"
      min="0"
      max="100"
      step="10"
      value="0"
    />
    <h5>
      <span
        class="badge badge-pill badge-success range${identificador}Output"
      >
        0 %
      </span>
    </h5>
  `;
};

const adicionarSectionsDaIsencao = () => {
  $('inp:percentualAprovado').val(0);

  const elementoAprovado = construirElementoInputRange('PercentualAprovado');

  $('.porcentagem-aprovado').append(elementoAprovado);
};

const adicionarMensagensAuxiliares = () => {
  mensagemIsencao =
    'Utilize a barra abaixo para selecionar a Isenção que deseja solicitar!';

  tippy($('.div-percentual-aprovado')[0], {
    content: mensagemIsencao,
    theme: 'google',
  });
};

const mostrarOuEsconderSecaoPeloRadio = () => {
  $('inp:aprovarValorDeIsencaoSolicitado').on('change', function() {
    const valor = retornarValorDoInputRadioPeloXname(
      'inp:aprovarValorDeIsencaoSolicitado'
    );

    valor === 'Não'
      ? $('.div-percentual-aprovado').show()
      : $('.div-percentual-aprovado').hide();
  });
};

const retornarValorDoInputRadioPeloXname = xname => {
  const inputChecked = $(
    $(xname)
      .parent()
      .parent()[0]
  ).find('input:checked');

  return inputChecked.length ? inputChecked[0].value : null;
};

const validarInputRadio = group => {
  console.log('group: ', group);

  $(group).find('input:checked').length
    ? $(group)
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $(group)
        .addClass('is-invalid')
        .removeClass('is-valid');
};

const definirCamposObrigatorios = () => {
  // input radio
  $(
    $('inp:aprovarValorDeIsencaoSolicitado')
      .parent()
      .parent()[0]
  ).addClass('required-radio');
};

const validarFormulario = () => {
  validarInputRadio($('.form-integra .required-radio'));

  const valorAprovarIsencaoSolicitado = retornarValorDoInputRadioPeloXname(
    'inp:aprovarValorDeIsencaoSolicitado'
  );

  if (valorAprovarIsencaoSolicitado === 'Sim') {
    $('inp:percentualAprovado').val(null);
  }

  if (!$('.is-invalid').length) {
    doAction('Isenção Aprovada', false, false);

    $($(document.getElementsByClassName('cryo-confirm-dialog padded'))[0])
      .find('button.btn.btn-success')
      .click();
  } else {
    toastr.error('Campos obrigatórios, favor verificar!');

    return false;
  }
};
