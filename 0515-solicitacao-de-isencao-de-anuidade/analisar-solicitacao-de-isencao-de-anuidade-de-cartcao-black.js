$(document).ready(function() {
  ajustarFormulario();
  adicionarMobileSpan();

  validarCamposVazios();

  renderizarEventosPorClick();

  $($('div.well')[0]).hide();
});

const renderizarEventosPorClick = () => {
  const btnSolicitacao = $('#customBtn_Solicitação\\ Aprovada');

  btnSolicitacao.click(() => validarFormulario());
};

const validarFormulario = () => {
  doAction('Solicitação Aprovada', true, false);

  $($(document.getElementsByClassName('cryo-confirm-dialog padded'))[0])
    .find('button.btn.btn-success')
    .click();
};

const validarCamposVazios = () => {
  if (!$('inp:percentualAprovado').val().length)
    $('inp:percentualAprovado')
      .parent()
      .parent()
      .parent()
      .hide();
};

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $('#div-msg-attach-documents').hide();
  $('#RequesterInfo').hide();
  $('#btnCancel').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // funções de manipulação / ajustes em formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');

  // // configurações default do formulário
  $('#customBtn_Solicitação\\ Aprovada').removeAttr('onclick');
};

const adicionarMobileSpan = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile =
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1;

  if (!isMobile) {
    $(
      `
        <span style='margin-left: 0.4em' class='label label-secondary'>
          <strong> Requisitante:${$('inp:nomeRequisitante').val()}</strong>
        </span>
      `
    ).insertAfter('.title span.label.label-info.flow-title');
  }
};
