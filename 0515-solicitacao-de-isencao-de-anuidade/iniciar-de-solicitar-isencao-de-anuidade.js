/*--------------------------------------------------------*/
const URL_TOKEN =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJg-CqsL3NypcCvWuQu01@2-7ejKkCN5V0IBK52p0D6LEWgV3g7RcWg6-TyDXTbVyzg__';
const URL_FONTE_AGENCIA =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJjjHsuLLfiKk1oHY8ar9PpJ09GhN-jx7ZuOJ2Yyc1brA7178QSps-IItsHERsWcG@A__';
const URL_FONTE_CONTAS =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJpgXLzsoAViVPtyj1iakvw6xtuZhpZIz1RBPeKrZez@YnzSUEanQSEZhHn4uD5MWyQ__';
const URL_FONTE_SCORE =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJrruZStJA@TmQ78a00F7VCiDGLZkVnYFy3dXjIID7fUrbZ-R7d7jLENsIKNeJzWAtA__';
const URL_CONTA = numeroConta =>
  `https://servicos.e-unicred.com.br/conta-corrente-us/conta-corrente/conta-corrente/v1/contas-correntes/${numeroConta}`;
const URL_CADASTRO_PF = cpf =>
  `https://servicos.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-fisica/v1/pessoa-fisica/${cpf}`;
const URL_CADASTRO_PJ = cnpj =>
  `https://servicos.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-juridica/v1/pessoa-juridica/${cnpj}`;
/*--------------------------------------------------------*/

let token, cooperativa, agencia, tipoPessoa, identificador;

$(document).ready(function() {
  ajustarFormulario();
  adicionarMobileSpan();

  desativarInputs();
  esconderSections();
  definirCamposObrigatorios();

  adicionarMensagensAuxiliares();
  adicionarSectionsDaIsencao();

  renderizarEventosPorMudancaNoRadio();
  renderizarEventosPorMudancaNoRange();
  renderizarEventosPorClick();

  $($('div.well')[0]).hide();
});

const transformarEmFormatoMoeda = valor => {
  return parseFloat(valor).toLocaleString('pt-br', {
    minimumFractionDigits: 2,
  });
};

const desativarInputs = () => {
  $('inp:nomeRazaoSocial').attr('readonly', true);
  $('inp:matricula').attr('readonly', true);
  $('inp:tipoDePessoa').attr('readonly', true);
  $('inp:agencia').attr('readonly', true);
  $('inp:renda').attr('readonly', true);
  $('inp:profissao').attr('readonly', true);
  $('inp:dataDeAssociacao').attr('readonly', true);
  $('inp:contaCartaoSipag').attr('readonly', true);
  $('inp:scoreSerasa').attr('readonly', true);
  $('inp:faixaScore').attr('readonly', true);
  $('inp:percentualDaIsencao').attr('readonly', true);
};

const definirCamposObrigatorios = () => {
  $('inp:contaDoCooperado').addClass('required');
  $('inp:modalidadeDoCartao').addClass('required');

  $(
    $('inp:cartaoAdicional')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:existemParcelasPagas')
      .parent()
      .parent()[0]
  ).addClass('required-radio');
};

const esconderSections = () => {
  $('.informacoes-do-cooperado').hide();
  $('.informacoes-do-cartao').hide();
  $('.informacoes-da-isencao').hide();
  $('.conta-cartao-sipag').hide();
  $('.score-serasa-section').hide();
  $('.span-porcentagem-isencao').hide();
};

const mostrarSections = () => {
  $('.informacoes-do-cooperado').show();
  $('.informacoes-do-cartao').show();
  $('.informacoes-da-isencao').show();
  $('.score-serasa-section').show();
};

const adicionarMensagensAuxiliares = () => {
  mensagemIsencao =
    'Utilize a(s) barra(s) abaixo para selecionar a Isenção que deseja solicitar!';

  tippy($('.informacoes-da-isencao')[0], {
    content: mensagemIsencao,
    theme: 'google',
  });
};

const renderizarEventosPorMudancaNoRadio = () => {
  $('.funkyradio.div-contas-cartao').change('input[type="radio"]', () => {
    const inputRadio = $('.funkyradio.div-contas-cartao input:checked')[0]
      .labels[0].innerText;

    $('inp:contaCartaoSipag').val(inputRadio);
  });
};

const renderizarEventosPorClick = () => {
  const infosCooperado = $('.procurar-informacoes-do-cooperado');
  const btnSolicitacao = $('#customBtn_Solicitação\\ Encaminhada');

  infosCooperado.click(() => validarBuscaCooperado());
  btnSolicitacao.click(() => validarFormulario());
};

const renderizarEventosPorMudancaNoRange = () => {
  $('.rangePercentualDaIsencaoInput').change(e => {
    $('inp:percentualDaIsencao').val(e.target.value);

    $('.rangePercentualDaIsencaoOutput')[0].innerText = `${e.target.value} %`;
  });
};

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $('#div-msg-attach-documents').hide();
  $('#RequesterInfo').hide();
  $('#btnCancel').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // funções de manipulação / ajustes em formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');

  // configurações default do formulário
  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');

  $('.input-group-append').show();
};

const adicionarMobileSpan = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile =
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1;

  if (!isMobile) {
    $(
      `
        <span style='margin-left: 0.4em' class='label label-secondary'>
          <strong> Requisitante:${$('inp:nomeRequisitante').val()}</strong>
        </span>
      `
    ).insertAfter('.title span.label.label-info.flow-title');
  }
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const formatarData = data => {
  const dataSplit = data.split('-');
  return `${dataSplit[2]}/${dataSplit[1]}/${dataSplit[0]}`;
};

const buscarToken = async () => {
  await axios
    .get(URL_TOKEN)
    .then(success => {
      if (success.data.success[0].fields.access_token) {
        token = `Bearer ${success.data.success[0].fields.access_token}`;
      } else {
        toastr.error('Erro de comunicaçao com o serviço!');
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      console.log(error);
    });
};

const limparFormulario = () => {
  esconderSections();

  if (!!$('.funkyradio.div-contas-cartao').length)
    $('.funkyradio.div-contas-cartao')[0].innerHTML = '';

  $('inp:nomeRazaoSocial').val('');
  $('inp:matricula').val('');
  $('inp:tipoDePessoa').val('');
  $('inp:agencia').val('');
  $('inp:renda').val('');
  $('inp:profissao').val('');
  $('inp:dataDeAssociacao').val('');
  $('inp:contasCartaoSipag').val('');

  $('inp:cartaoAdicional').prop('checked', false);
  $('inp:existemParcelasPagas').prop('checked', false);
  $('inp:modalidadeDoCartao').val('');

  $('.rangePercentualDaIsencaoInput').val('0');
  $('.rangePercentualDaIsencaoOutput')[0].innerText = '0 %';
};

const buscarDadosPeloCpf = identificador => {
  axios
    .get(URL_CADASTRO_PF(identificador), {
      headers: {
        Authorization: token,
        cooperativa: cooperativa,
      },
    })
    .then(res => {
      $('inp:nomeRazaoSocial').val(res.data.nome);
      $('inp:matricula').val(res.data.associacao.matricula);
      $('inp:tipoDePessoa').val(tipoPessoa);
      $('inp:agencia').val(agencia);
      $('inp:renda').val(
        transformarEmFormatoMoeda(res.data.dadosRenda.rendaTotal)
      );
      $('inp:profissao').val(res.data.dadosProfissionais.profissao);
      $('inp:dataDeAssociacao').val(
        moment(res.data.associacao.dataAssociacao).format('DD/MM/YYYY')
      );
    })
    .catch(error => console.log('error: ', error));
};

const buscarDadosPeloCnpj = identificador => {
  axios
    .get(URL_CADASTRO_PJ(identificador), {
      headers: {
        Authorization: token,
        cooperativa: cooperativa,
      },
    })
    .then(res => {
      $('inp:nomeRazaoSocial').val(res.data.nome);
      $('inp:matricula').val(res.data.associacao.matricula);
      $('inp:tipoDePessoa').val(tipoPessoa);
      $('inp:agencia').val(agencia);
      $('inp:renda').val(
        transformarEmFormatoMoeda(
          res.data.dadosFaturamento.faturamentoMedio.valor
        )
      );
      $('inp:profissao').val(
        res.data.cnae.atividadeEconomicaPrincipal.descricao
      );
      $('inp:dataDeAssociacao').val(
        moment(res.data.associacao.dataAssociacao).format('DD/MM/YYYY')
      );
    })
    .catch(error => console.log('error: ', error));
};

const verificarTipoDePessoa = identificador => {
  if (identificador.length <= 11) {
    buscarDadosPeloCpf(identificador);
    tipoPessoa = 'Pessoa Física';
  } else {
    buscarDadosPeloCnpj(identificador);
    tipoPessoa = 'Pessoa Jurídica';
  }
};

const validarEnvioSolicitacao = () => {
  console.log('validando formulário!');
};

const validarBuscaCooperado = () => {
  numeroConta = $('inp:contaDoCooperado').val();

  if (!numeroConta) {
    toastr.error('Campo obrigatório!', 'Erro!');

    return;
  }

  bucarInformacoesCooperado(numeroConta);
};

const bucarInformacoesCooperado = async numeroConta => {
  limparFormulario();
  toggleSpinner();

  await buscarToken();
  cooperativa = '0515'; //$('inp:cooperativaRequisitante').val();

  if (!token || !cooperativa) {
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    return;
  }

  await buscarDadosPelaConta(numeroConta, token, cooperativa);
  await trazerAgenciaPelaConta(numeroConta);
  await trazerContasDoCartao(numeroConta);
  await trazerInformacoesSerasaPeloCpfCnpj(identificador);

  toggleSpinner();
};

const validarCheckboxContas = () => {
  $('.funkyradio.div-contas-cartao input:checked').length
    ? $('.funkyradio.div-contas-cartao label')
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $('.funkyradio.div-contas-cartao label')
        .addClass('is-invalid')
        .removeClass('is-valid');
};

const validarInput = elemento => {
  !elemento.value.length
    ? $(elemento)
        .addClass('is-invalid')
        .removeClass('is-valid')
    : $(elemento)
        .addClass('is-valid')
        .removeClass('is-invalid');
};

const validarInputRadio = group => {
  $(group).find('input:checked').length
    ? $(group)
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $(group)
        .addClass('is-invalid')
        .removeClass('is-valid');
};

const validarFormulario = () => {
  validarCheckboxContas();

  const inputsObrigatorios = $('.form-integra .required');
  const inputsRadioObrigatorios = $('.form-integra .required-radio input');
  const divsRadioObrigatorios = $('.form-integra .required-radio');

  inputsRadioObrigatorios.change(() => {
    _.each(divsRadioObrigatorios, elemento => validarInputRadio(elemento));
  });

  inputsObrigatorios.change(() => {
    _.each(inputsObrigatorios, elemento => validarInput(elemento));
  });

  inputsRadioObrigatorios.trigger('change');
  inputsObrigatorios.trigger('change');

  if (!$('.is-invalid').length) {
    $('.porcentagem-isencao').remove();
    $('.span-porcentagem-isencao').show();

    doAction('Solicitação Encaminhada', false, false);

    $($(document.getElementsByClassName('cryo-confirm-dialog padded'))[0])
      .find('button.btn.btn-success')
      .click();
  } else {
    toastr.error('Campos obrigatórios, favor verificar!');
  }
};

/* ************************************************* */
const construirElementoInputRadioSelect = (texto, index, identificador) => {
  return `
      <div class="col-xs-12 col-md-2">
        <div class="form-group">
          <div class="funkyradio-success">
            <input
              class="input-${identificador}"
              type="radio"
              name="radio-${identificador}"
              id="radio-${identificador}-${index}"
            />
            <label
              class="label-group-${identificador}"
              for="radio-${identificador}-${index}"
            >
              ${texto}
            </label>
          </div>
        </div>
      </div>
    `;
};

const construirSectionContas = contas => {
  _.each(contas, (conta, index) => {
    const elemento = construirElementoInputRadioSelect(conta, index, 'conta');

    $('.funkyradio.div-contas-cartao').append(elemento);
  });
};
/* ************************************************* */
const construirElementoInputRange = identificador => {
  return `
    <input
      type="range"
      class="custom-range range${identificador}Input"
      min="0"
      max="100"
      step="10"
      value="0"
    />
    <h5>
      <span
        class="badge badge-pill badge-success range${identificador}Output"
      >
        0 %
      </span>
    </h5>
  `;
};

const adicionarSectionsDaIsencao = () => {
  $('inp:percentualDaIsencao').val(0);

  const elementIsencao = construirElementoInputRange('PercentualDaIsencao');

  $('.porcentagem-isencao').append(elementIsencao);
};
/* ************************************************* */

const trazerContasDoCartao = async numeroConta => {
  await axios
    .post(URL_FONTE_CONTAS, { inpconta: numeroConta })
    .then(res => {
      console.log('contas: ', res.data.success);

      construirSectionContas(_.map(res.data.success, conta => conta.txt));
      if (res.data.success.length === 1) $('.input-conta')[0].click();
      // if (success.data.success.length) {
      //   agencia = success.data.success[0].txt;
      // } else {
      //   toastr.error('Erro de comunicaçao com o serviço!');

      //   return;
      // }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      console.log(error);
      return;
    });
};

const trazerAgenciaPelaConta = async numeroConta => {
  await axios
    .post(URL_FONTE_AGENCIA, { inpconta: numeroConta })
    .then(success => {
      if (success.data.success.length) {
        agencia = success.data.success[0].txt;
      } else {
        toastr.error('Erro de comunicaçao com o serviço!');

        return;
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      console.log(error);
      return;
    });
};

const buscarDadosPelaConta = async (numeroConta, token, cooperativa) => {
  await axios
    .get(URL_CONTA(numeroConta), {
      headers: {
        Authorization: token,
        cooperativa: cooperativa,
      },
    })
    .then(response => {
      if (response.data.situacao === 'ENCERRADA') {
        toastr.error('Conta pesquisada está encerrada!', 'Erro');

        return;
      }

      identificador = response.data.cpfCnpjPrimeiroTitular;
      verificarTipoDePessoa(identificador);
    })
    .catch(error => {
      console.log('error: ', error);

      toastr.error('Falha na comunicação!', 'Erro');
    });
};

const trazerInformacoesSerasaPeloCpfCnpj = async identificador => {
  await axios
    .post(URL_FONTE_SCORE, { inpcpfCnpj: identificador })
    .then(res => {
      console.log('success: ', res.data.success[0].fields);

      $('inp:scoreSerasa').val(res.data.success[0].fields.scoreSerasa);
      $('inp:faixaScore').val(res.data.success[0].fields.faixaScore);

      toastr.success('Informações carregadas com sucesso!', 'Cooperado');
      mostrarSections();
    })
    .catch(error => console.log('error: ', error));
};
