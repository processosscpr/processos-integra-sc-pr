const URL_DATA_ADMISSAO = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJgycpA-hFmYxQ96BS96AyyzZ8ZNyzQMMBkBZnBl-2wcVf-UXyDxmiNlE@8vMTvjYGw__`;
const URL_CPF = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJpHqND6TSnv9qpoaZumMHsYrvcsJ7gU83IBgnqeQEu1UwXzRY8YlTiC8o-4DDh2CqQ__`;
const URL_AUXILIO_VESTIMENTA = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJj9fT4A60yNDBl71qpbKudXZyfpMOZ1HiUmHzlaPHtUqpWf4CPolra1DJDhZU9HAOg__`;

let dataAdmissao, codigoUsuario, solicitacaoVestimentaValida;

const anoVigente = moment().year();
const anoPassado = moment().year() - 1;
const anoSguinte = moment().year() + 1;

const priPeriodoInicio = `${anoVigente}-01-01`;
const priPeriodoNovoFim = `${anoVigente}-03-10`;
const priPeriodoAntigoFim = `${anoVigente}-05-10`;

const segPeriodoInicio = `${anoVigente}-07-01`;
const segPeriodoNovoFim = `${anoVigente}-09-10`;
const segPeriodoAntigoFim = `${anoVigente}-11-10`;

let dataAtual = moment();
let dtGlobal = moment('2019-11-10');

const valorRessaricmentoUniforme = 800;

$(document).ready(function() {
  verificarPosicaoIniciadora();

  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');

  if (
    document.querySelector("[xname='inpareaRequisitante']").value ==
    '0515 - 90 - Assessoria Executiva'
  ) {
    $(document.querySelector('#tbltipo-solicitacao')).show();
    $(document.querySelector('#especificar-consolheiropresidente'))
      .children()
      .hide();
    document
      .querySelector("[xname='inptipoSolicitacao']")
      .setAttribute('required', 'S');
    document
      .querySelector("[xname='inpconselheiropresidente']")
      .setAttribute('required', 'N');
  } else {
    $(document.querySelector('#tbltipo-solicitacao')).hide();
    document
      .querySelector("[xname='inptipoSolicitacao']")
      .setAttribute('required', 'N');
    document
      .querySelector("[xname='inpconselheiropresidente']")
      .setAttribute('required', 'N');
  }

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
    $('inp:motivo').attr('style', 'width:80%');
    $('inp:conselheiropresidente').attr('style', 'width: 80%');
  } else {
    adaptarMobile();
  }

  document
    .querySelector('#informacoes-despesas')
    .setAttribute('class', 'box box-open-and-close');
  $(document.querySelector('#informacoes-auxiliares')).hide();
  $(document.querySelector('#informacoes-financeiras')).hide();
  $(document.querySelector('#divStatisticsTable')).hide();
  $(document.querySelector('#btnCancel')).hide();

  document.querySelector('#valor-despesa').children[0].textContent =
    'Valor Despesa:';

  $('inp:tipoDespesa').change(AjustarNome);
  $('inp:tipoDespesa').change(function() {
    verificarTipoDeDespesa(this);
  });

  $('inp:tipoSolicitacao').on('change', function() {
    var solicitacaoConselheirosPresidentes = document.getElementById(
      'inptipoSolicitacao-1'
    ).checked;
    if (solicitacaoConselheirosPresidentes) {
      $(document.querySelector('#especificar-consolheiropresidente'))
        .children()
        .show();
      document
        .querySelector("[xname='inpconselheiropresidente']")
        .setAttribute('required', 'S');
      document.querySelector("[xname='inptipoCadastrado']").value = 'Diretoria';
      document.querySelector("[xid='divtipoCadastrado']").textContent =
        'Diretoria';
    } else {
      document.querySelector("[xname='inpconselheiropresidente']").value = '';
      $(document.querySelector('#especificar-consolheiropresidente'))
        .children()
        .hide();
      document
        .querySelector("[xname='inpconselheiropresidente']")
        .setAttribute('required', 'N');
      document.querySelector("[xname='inptipoCadastrado']").value =
        'Colaborador(a)';
      document.querySelector("[xid='divtipoCadastrado']").textContent =
        'Colaborador(a)';
    }
  });

  $('table button:first').click(function() {
    var descobrirQntTabelas = document.querySelectorAll('#valor-despesa');
    var tamanhoTabela = descobrirQntTabelas.length - 1;
    descobrirQntTabelas[tamanhoTabela].children[0].textContent =
      'Valor Despesa';

    $('inp:tipoDespesa').change(AjustarNome);
    $('inp:tipoDespesa').change(function() {
      verificarTipoDeDespesa(this);
    });

    $('inp:valorDespesa').blur(function(event) {
      verificarValorDespesaPeloTipo(event);
    });
  });

  $('#customBtn_Solicitação\\ Encaminhada').click(() => {
    var listaTabelaValores = document.querySelectorAll(
      '#tabela-principal-despesas'
    );
    var calculoKmRodado = TipoCadastrado();
    var totalDespesas = CalcularTotal(listaTabelaValores, calculoKmRodado);

    $('inp:totalDaDespesa').val(totalDespesas);
    $('inp:totalDaDespesa').siblings()[0].innerText = totalDespesas;

    verificarUniformeMasculino();
    VerificarCertificacao(listaTabelaValores);

    doAction('Solicitação Encaminhada', true, false);
  });

  codigoUsuario = $('inp:codigoUsuario').val();

  trazerCpfPeloCodigoDoUsuario(codigoUsuario);

  $('inp:valorDespesa').blur(function(event) {
    verificarValorDespesaPeloTipo(event);
  });
});

const trazerCpfPeloCodigoDoUsuario = async codigoUsuario => {
  $('#customBtn_Solicitação\\ Encaminhada').attr('disabled', true);
  $('#multivalorada').hide();

  await axios
    .post(URL_CPF, { inpcodigoUsuario: codigoUsuario })
    .then(res => {
      if (res.data.success.length) {
        trazerDataAdmissaoPeloCpf(res.data.success[0].fields.cpf);
      } else {
        toastr.error('Erro de comunicaçao com o serviço!');
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');
    });
};

const trazerDataAdmissaoPeloCpf = async cpf => {
  await axios
    .post(URL_DATA_ADMISSAO, { inpcpf: cpf })
    .then(res => {
      if (res.data.success.length) {
        let ano, mes, dia;

        ano = res.data.success[0].fields.dataAdmissao.split('/')[2];
        mes = res.data.success[0].fields.dataAdmissao.split('/')[1];
        dia = res.data.success[0].fields.dataAdmissao.split('/')[0];

        dataAdmissao = `${ano}-${mes}-${dia}`;

        $('#customBtn_Solicitação\\ Encaminhada').attr('disabled', false);
        $('#multivalorada').show();
      } else {
        toastr.error('Erro de comunicaçao com o serviço!');
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');
    });
};

const validarSolicitacaoAuxilioVestimenta = async elemento => {
  dtAdmissao = moment(dataAdmissao);

  if (isFirstSemester(dataAtual)) {
    dtAdmissao < moment(priPeriodoInicio)
      ? regraColaboradorAntigo(
          moment(priPeriodoInicio),
          moment(priPeriodoAntigoFim),
          elemento
        )
      : regraColaboradorNovo(
          moment(priPeriodoInicio),
          moment(priPeriodoNovoFim),
          elemento
        );
  } else if (isSecondSemester(dataAtual)) {
    dtAdmissao < moment(segPeriodoInicio)
      ? regraColaboradorAntigo(
          moment(segPeriodoInicio),
          moment(segPeriodoAntigoFim),
          elemento
        )
      : regraColaboradorNovo(
          moment(segPeriodoInicio),
          moment(segPeriodoNovoFim),
          elemento
        );
  }
};

const regraColaboradorNovo = async (
  dataInicioPeriodo,
  dataFimPeriodo,
  elemento
) => {
  await validarSolicitacaoAuxilioVestimentaExistente(dataInicioPeriodo);

  if (!solicitacaoVestimentaValida) {
    toastr.error(
      'Já houve uma solicitação de uniforme/auxílio vestimenta para seu usuário nesse período. É permitido apenas uma solicitação por período!'
    );

    elemento.value = '';
  } else if (dtAdmissao > dataFimPeriodo) {
    if (isFirstSemesterSemester(dataAtual)) {
      if (
        !dtGlobal.isBetween(dataInicioPeriodo, dataFimPeriodo) &&
        !dataAtual.isBetween(dtGlobal, priPeriodoAntigoFim)
      ) {
        toastr.error(
          'Ainda não está liberado para realizar solicitações/auxílio vestimenta!'
        );

        elemento.value = '';
      }
    } else {
      if (
        !dtGlobal.isBetween(dataInicioPeriodo, dataFimPeriodo) &&
        !dataAtual.isBetween(dtGlobal, segPeriodoAntigoFim)
      ) {
        toastr.error(
          'Ainda não está liberado para realizar solicitações/auxílio vestimenta!'
        );

        elemento.value = '';
      }
    }
  } else if (
    !dtGlobal.isBetween(dataInicioPeriodo, dataFimPeriodo) &&
    !dataAtual.isBetween(dtGlobal, dataFimPeriodo)
  ) {
    toastr.error(
      'Ainda não está liberado para realizar solicitações/auxílio vestimenta!'
    );

    elemento.value = '';
  }
};

const regraColaboradorAntigo = async (
  dataInicioPeriodo,
  dataFimPeriodo,
  elemento
) => {
  await validarSolicitacaoAuxilioVestimentaExistente(dataInicioPeriodo);

  if (!solicitacaoVestimentaValida) {
    toastr.error(
      'Já houve uma solicitação de uniforme/auxílio vestimenta para seu usuário nesse período. É permitido apenas uma solicitação por período!'
    );

    elemento.value = '';
  } else if (
    !dtGlobal.isBetween(dataInicioPeriodo, dataFimPeriodo) &&
    !dataAtual.isBetween(dtGlobal, dataFimPeriodo)
  ) {
    toastr.error(
      'Ainda não está liberado para realizar solicitações/auxílio vestimenta!'
    );

    elemento.value = '';
  }
};

const isFirstSemester = semesterDate => {
  return semesterDate.isBetween(
    moment(`${anoPassado}-12-31`),
    moment(`${anoVigente}-07-01`)
  );
};

const isSecondSemester = semesterDate => {
  return semesterDate.isBetween(
    moment(`${anoVigente}-06-30`),
    moment(`${anoSguinte}-01-01`)
  );
};

const validarSolicitacaoAuxilioVestimentaExistente = async data => {
  const requisitante = $('inp:nomeRequisitante').val(); // "Alecsander Marques"
  const processo = $('inp:processo').val(); // "PR_0515 - Ressarcimento de Despesas"

  await axios
    .post(URL_AUXILIO_VESTIMENTA, {
      inpprocesso: processo,
      inpnomeRequisitante: requisitante,
      inpdataComparacaoFluxos: data,
    })
    .then(res => {
      solicitacaoVestimentaValida = !!!res.data.success.length;
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      solicitacaoVestimentaValida = false;
    });
};

const formatarNumero = number => {
  return parseFloat(number.replace(/\./g, '').replace(',', '.'));
};

const verificarValorDespesaPeloTipo = event => {
  const valorDespesa = formatarNumero(event.target.value);

  const tipoDespesa = $(
    $($(event.target)[0])
      .parent()
      .parent()
      .parent()[0].children[1].children[1].children[0]
  ).val();

  if (
    tipoDespesa === 'Uniforme/Auxílio Vestimenta' &&
    valorDespesa > valorRessaricmentoUniforme
  ) {
    toastr.error('Está maior que o permitido!', 'Atenção');
    event.target.value = '';
  }
};

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();

  document
    .querySelector("[xname='inptipoDespesa']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpmotivo']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpdataDespesa']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataDespesa']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpvalorDespesa']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');

  document
    .querySelector("[xname='inpconselheiropresidente']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
}

const verificarTipoDeDespesa = itemMudado => {
  const seletor = $('inp:tipoDespesa');
  const despesas = _.map(seletor, e => $(e).val());

  if (despesas.filter(x => x === 'Uniforme/Auxílio Vestimenta').length > 1) {
    itemMudado.value = '';

    toastr.error(
      'Não permitido selecionar mais de um <b>"Uniforme/Auxílio Vestimenta"</b>',
      'Atenção!'
    );
  }

  if (itemMudado.value === 'Uniforme/Auxílio Vestimenta') {
    validarSolicitacaoAuxilioVestimenta(itemMudado);
  }
};

function AjustarNome() {
  var tipoDespesaSelecionada = $(this)
    .closest('tr')
    .find('inp:tipoDespesa')
    .val();

  if (tipoDespesaSelecionada == 'Km Rodado') {
    var elementoTabelaDesteCampo = $(this).closest('tr');
    var campoValorCorrespondente = $(elementoTabelaDesteCampo).find(
      '#valor-despesa'
    );
    var filhosDestaEstrutura = $(campoValorCorrespondente.children());
    filhosDestaEstrutura[0].textContent = 'Quantidade KM Rodado:';
  } else {
    var elementoTabelaDesteCampo = $(this).closest('tr');
    var campoValorCorrespondente = $(elementoTabelaDesteCampo).find(
      '#valor-despesa'
    );
    var filhosDestaEstrutura = $(campoValorCorrespondente.children());
    filhosDestaEstrutura[0].textContent = 'Valor Despesa:';
  }
}

function TipoCadastrado() {
  var tipoCadastrado = document.querySelector("[xname='inptipoCadastrado']")
    .value;
  var calculoKmRodado;
  if (tipoCadastrado == 'Diretoria') {
    calculoKmRodado = document.querySelector(
      "[xname='inpvalorCalculoKmrodadoDiretoria']"
    ).value;
    return calculoKmRodado;
  } else {
    calculoKmRodado = document.querySelector(
      "[xname='inpvalorCalculoKmrodadoColaborador']"
    ).value;
    return calculoKmRodado;
  }
}

function CalcularTotal(listaTabelaValores, calculoKmRodado) {
  var totalDespesas = 0;
  for (var i = 0; i < listaTabelaValores.length; i++) {
    var elementoTipoDespesa = $(listaTabelaValores[i]).find(
      "[xname='inptipoDespesa']"
    );
    if (elementoTipoDespesa[0].value == 'Km Rodado') {
      var valorDespesaPreenchida = ResgatarTipoDespesa(listaTabelaValores, i);
      var valorDespesaPreenchidaSemVirgula = valorDespesaPreenchida.replace(
        ',',
        '.'
      );
      var valorKmRodadoFinal =
        valorDespesaPreenchidaSemVirgula * calculoKmRodado;
      var valorKmRodadoFinalFixed = valorKmRodadoFinal.toFixed(2);
      totalDespesas = (
        parseFloat(valorKmRodadoFinalFixed) + parseFloat(totalDespesas)
      ).toFixed(2);
    } else {
      var valorDespesaPreenchida = ResgatarTipoDespesa(listaTabelaValores, i);
      var valorDespesaPreenchidaSemVirgula = valorDespesaPreenchida.replace(
        ',',
        '.'
      );
      totalDespesas = (
        parseFloat(valorDespesaPreenchidaSemVirgula) + parseFloat(totalDespesas)
      ).toFixed(2);
    }
  }
  return totalDespesas;
}

function ResgatarTipoDespesa(listaTabelaValores, i) {
  var auxiliarParaRemoverPontos;
  var elementoPaiValorDespesa = $(listaTabelaValores[i]).find('#valor-despesa');
  var elementoFilho = $(elementoPaiValorDespesa).find(
    "[xname='inpvalorDespesa']"
  );
  var valorFilho = elementoFilho[0].value;

  if (valorFilho.length > 6) {
    auxiliarParaRemoverPontos = valorFilho;
    for (var i = 0; i < valorFilho.split('.').length - 1; i++) {
      auxiliarParaRemoverPontos = auxiliarParaRemoverPontos.replace('.', '');
    }
    return auxiliarParaRemoverPontos;
  } else {
    return valorFilho;
  }
}

const verificarUniformeMasculino = () => {
  const seletorDespesa = $('inp:tipoDespesa');
  const seletorUniforme = $('inp:possuiSomenteUniformeMasculino');
  const despesas = _.map(seletorDespesa, e => $(e).val());
  const divUniforme = $("[xid='divpossuiSomenteUniformeMasculino']")[0];

  !!despesas.filter(x => x === 'Uniforme/Auxílio Vestimenta').length &&
  despesas.length === 1
    ? atribuirMensagemUniforme(
        'Possui Somente Uniforme/Auxílio Vestimenta',
        seletorUniforme,
        divUniforme
      )
    : atribuirMensagemUniforme(
        'Não Possui Somente Uniforme/Auxílio Vestimenta',
        seletorUniforme,
        divUniforme
      );
};

const atribuirMensagemUniforme = (mensagem, input, div) => {
  input.val(mensagem);
  div.textContent = mensagem;
};

function VerificarCertificacao(listaTabelaValores) {
  var array = new Array();
  for (var i = 0; i < listaTabelaValores.length; i++) {
    var elementoTipoDespesa = $(listaTabelaValores[i]).find(
      "[xname='inptipoDespesa']"
    );
    array.push(elementoTipoDespesa[0].value);
  }
  if (array.indexOf('Certificação') == -1) {
    document.querySelector("[xid='divpossuiCertificacao']").textContent =
      'Não Possui Certificação';
    document.querySelector("[xname='inppossuiCertificacao']").value =
      'Não Possui Certificação';
  } else {
    document.querySelector("[xid='divpossuiCertificacao']").textContent =
      'Possui Certificação';
    document.querySelector("[xname='inppossuiCertificacao']").value =
      'Possui Certificação';
  }
}

function verificarPosicaoIniciadora() {
  if ($('select[name="inpCodPositionArea"]').val() != undefined) {
    var arrayAuxiliar = new Array();
    for (
      var i = 0;
      i < document.querySelector("[name='inpCodPositionArea']").options.length;
      i++
    ) {
      arrayAuxiliar.push(
        document.querySelector("[name='inpCodPositionArea']").options[i]
          .innerText
      );
    }
    arrayAuxiliar.shift();
    for (var i = arrayAuxiliar.length - 1; i >= 0; i--) {
      if (arrayAuxiliar[i].substring(4, 5) == '_') {
        arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]), 1);
      }
    }
    $('select[name="inpCodPositionArea"]').val(
      $("option:contains('" + arrayAuxiliar[0] + "')").val()
    );
    $(
      $('select[name="inpCodPositionArea"]')
        .parent()
        .parent()
    ).hide();
  }
}
