$(document).ready(function() {
  if (
    document.querySelector("[xname='inpareaRequisitante']").value ==
    '0515 - 90 - Assessoria Executiva'
  ) {
    $(document.querySelector('#tbltipo-solicitacao')).show();
    $(document.querySelector('#especificar-consolheiropresidente'))
      .children()
      .hide();
  } else {
    $(document.querySelector('#tbltipo-solicitacao')).hide();
  }

  if (
    document.querySelector("[xname='inptipoSolicitacao']").value ==
    'Solicitação para conselheiro/presidente'
  ) {
    $(document.querySelector('#especificar-consolheiropresidente'))
      .children()
      .show();
  } else {
    $(document.querySelector('#especificar-consolheiropresidente'))
      .children()
      .hide();
  }

  if (isMobile()) {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }

  document
    .querySelector('#informacoes-despesas')
    .setAttribute('class', 'box box-open-and-close');
  document
    .querySelector('#informacoes-financeiras')
    .setAttribute('class', 'box box-open-and-close');
  $(document.querySelector('#informacoes-auxiliares')).hide();

  var listaTabelaValores = document.querySelectorAll(
    '#tabela-principal-despesas'
  );
  AjustarNome(listaTabelaValores);
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function AjustarNome(listaTabelaValores) {
  for (var i = 0; i < listaTabelaValores.length; i++) {
    var elementoTipoDespesa = $(listaTabelaValores[i]).find(
      "[xname='inptipoDespesa']"
    );
    if (elementoTipoDespesa[0].value == 'Km Rodado') {
      var elementoPaiValorDespesa = $(listaTabelaValores[i]).find(
        '#valor-despesa'
      );
      var elementoFilho = $(elementoPaiValorDespesa.children());
      elementoFilho[0].textContent = 'Quantidade Km Rodado:';
      AdicionarKMRodadoHTML(listaTabelaValores, i);
    } else {
      var elementoPaiValorDespesa = $(listaTabelaValores[i]).find(
        '#valor-despesa'
      );
      var elementoFilho = $(elementoPaiValorDespesa.children());
      elementoFilho[0].textContent = 'Valor Despesa:';
    }
  }
}

function AdicionarKMRodadoHTML(listaTabelaValores, i) {
  var calculoKmRodado = TipoCadastrado();
  var elementoPaiValorDespesa = $(listaTabelaValores[i]).find('#valor-despesa');
  var elementoFilho = $(elementoPaiValorDespesa.children());
  var elemento = $(elementoFilho.children());
  var elementoResgatado = ResgatarTipoDespesa(elemento[1].value);
  var resultadoKmRodado = (
    parseFloat(elementoResgatado.replace(',', '.')) *
    parseFloat(calculoKmRodado)
  )
    .toFixed(2)
    .replace('.', ',');
  elemento[0].textContent =
    elementoResgatado.replace(',', '.') + 'km ' + ' = ' + resultadoKmRodado;
}

function TipoCadastrado() {
  var tipoCadastrado = document.querySelector("[xname='inptipoCadastrado']")
    .value;
  var calculoKmRodado;
  if (tipoCadastrado == 'Diretoria') {
    calculoKmRodado = document.querySelector(
      "[xname='inpvalorCalculoKmrodadoDiretoria']"
    ).value;
    return calculoKmRodado;
  } else {
    calculoKmRodado = document.querySelector(
      "[xname='inpvalorCalculoKmrodadoColaborador']"
    ).value;
    return calculoKmRodado;
  }
}

function ResgatarTipoDespesa(valor) {
  var auxiliarParaRemoverPontos;

  if (valor.length > 6) {
    auxiliarParaRemoverPontos = valor;
    for (var i = 0; i < valor.split('.').length - 1; i++) {
      auxiliarParaRemoverPontos = auxiliarParaRemoverPontos.replace('.', '');
    }
    return auxiliarParaRemoverPontos;
  } else {
    return valor;
  }
}
