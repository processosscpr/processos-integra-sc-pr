SELECT instance.CodFlowExecute CodAttributeCustomValue, instance.CodFlowExecute DsAttributeValueName, 
(
    SELECT field_aux.DsFormFieldValue
    FROM WFFLOW_EXECUTE instance_aux
    LEFT OUTER JOIN WFFLOW flow_aux ON (flow_aux.CodFlow = instance_aux.CodFlow)
    LEFT OUTER JOIN WFFLOW_FORM_FIELD_LOG field_aux ON (field_aux.CodFlowExecute = instance_aux.CodFlowExecute)
    LEFT OUTER JOIN WFFORM_FIELD form_aux ON (form_aux.CodForm = flow_aux.CodForm) AND form_aux.CodField = field_aux.CodField
    WHERE instance_aux.CodFlowExecute = instance.CodFlowExecute
    AND field_aux.CodOrder = field.CodOrder
    AND form_aux.DsFieldName = 'valorDespesa'

) Valor, instance.DsFlowResult Resultado_Instância,
CASE
    WHEN instance.StFlowActive = 'S' THEN 'Em Andamento'
    WHEN instance.StFlowActive = 'N' THEN 'Finalizado'
    ELSE 'Não Encontrado' END AS Status,
instance.DtStartDate Data_Inicio
FROM WFFLOW_EXECUTE instance
LEFT OUTER JOIN WFFLOW flow ON (flow.CodFlow = instance.CodFlow)
LEFT OUTER JOIN WFVWFLOW_EXECUTE view_instance ON (instance.CodFlowExecute = view_instance.CodFlowExecute)
LEFT OUTER JOIN WUSER requester ON (requester.CodUser = view_instance.CodUserOwner)
LEFT OUTER JOIN WFFLOW_FORM_FIELD_LOG field ON (field.CodFlowExecute = instance.CodFlowExecute)
LEFT OUTER JOIN WFFORM_FIELD form ON (form.CodForm = flow.CodForm) AND form.CodField = field.CodField
WHERE flow.DsFlowName = '{Formulario.processo}'
AND requester.DsName = '{Formulario.nomeRequisitante}'
AND form.DsFieldName = 'tipoDespesa' AND field.DsFormFieldValue = 'Uniforme/Auxílio Vestimenta'
AND instance.DtStartDate >= '{Formulario.dataComparacaoFluxos}'
