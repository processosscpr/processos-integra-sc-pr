$(document).ready(function() {
  ajustarFormulario();

  $('#customBtn_De\\ Acordo\\ com\\ os\\ Status\\ Selecionados').click(
    elemento => {
      verificarAlteracoesTabelaChecklistDocumentosParaDispensa();
    }
  );

  $('#customBtn_Deve\\ Anexar\\ os\\ Documentos\\ Dispensados').click(
    elemento => {
      verificarAlteracoesTabelaChecklistDocumentosParaNaoDispensa();
    }
  );
});

function ajustarFormulario() {
  let nomeRequisitante = $('inp:nomeRequisitante').val();

  //hide elements from INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  //show elements from the from
  if (!isMobile()) {
    $(
      `<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${nomeRequisitante}</strong></span>`
    ).insertAfter('.title span.label.label-info.flow-title');
  }

  //funções de manipulação / ajustes em formulário
  $('#customBtn_De\\ Acordo\\ com\\ os\\ Status\\ Selecionados').removeAttr(
    'onclick'
  );
  $('#customBtn_Deve\\ Anexar\\ os\\ Documentos\\ Dispensados').removeAttr(
    'onclick'
  );

  _.forEach(
    $('#section-informacoes-checklist-documentos table').find('tbody tr'),
    row => {
      $(row)
        .find('div[xtype="FILE"] a[data-colorbox="true"]')
        .text('Arquivo');
    }
  );

  $('#section-informacoes-checklist-documentos table')
    .find('thead tr td')
    .remove();

  _.forEach(
    $('#section-informacoes-checklist-documentos table').find('tbody tr'),
    row => {
      $($(row).children()[0]).remove(),
        $(row)
          .find('select[xname="inpstatus"]')
          .val() === 'Dispensar Documento'
          ? $(row)
              .find('select[xname="inpstatus"]')
              .addClass('is-invalid')
          : $(row)
              .find('select[xname="inpstatus"]')
              .attr('readOnly', true);
    }
  );

  $(
    `<div class="alert alert-warning" role="alert">Caso não concorde com a dispensa do documento, ajuste nos status e clique no botão Deve Anexar os Documentos Dispensados! Caso concorde, apenas clique no botão De Acordo com os Status Selecionados!</div>`
  ).insertBefore('div.table-responsive-sm');
}

const verificarStatusSelecionado = async elemento => {
  $(elemento).val() !== 'Dispensar Documento'
    ? $(elemento)
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $(elemento)
        .addClass('is-invalid')
        .removeClass('is-valid');
};

const verificarAlteracoesTabelaChecklistDocumentosParaDispensa = async () => {
  let quantidadeDispensa = 0;

  await _.forEach(
    $('#section-informacoes-checklist-documentos table').find('tbody tr'),
    row => {
      $(row)
        .find('select[xname="inpstatus"]')
        .val() === 'Dispensar Documento'
        ? quantidadeDispensa++
        : null;
    }
  );

  quantidadeDispensa >= 1
    ? doAction('De Acordo com os Status Selecionados', true, false)
    : toastr.error(
        'Para esta opção, deve conter na tabela pelo menos um dos documentos indicados como Dispensar Documento!'
      );
};

const verificarAlteracoesTabelaChecklistDocumentosParaNaoDispensa = async () => {
  let quantidadeAjusteTabela = 0;

  await _.forEach(
    $('#section-informacoes-checklist-documentos table').find('tbody tr'),
    row => {
      $(row)
        .find('select[xname="inpstatus"]')
        .val() === 'Inserir Documento' &&
      $(row)
        .find('select[xname="inpstatus"]')
        .attr('readOnly') === undefined
        ? quantidadeAjusteTabela++
        : null;
    }
  );

  quantidadeAjusteTabela >= 1
    ? doAction('Deve Anexar os Documentos Dispensados', false, true)
    : toastr.error(
        'Não houve alteração na tabela. Primeiro altere o status para "Inserir Documento" em um dos documentos selecionados como dispensado!'
      );
};
