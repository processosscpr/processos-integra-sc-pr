$(document).ready(function() {});

/*--------------------------------------------------------*/
//FONTE DE DADOS: 0507 - Check Crédito - Proposta 0515
const url_proposta_0515 = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJsUkJ-dhj44SoG-CJkzFVjs9sc7CqIDm9DCZ08@VuHYSCcghyJ9oDIcoY8Y0D8Ur1g__`;
//FONTE DE DADOS: 0507 - Check Crédito - Proposta 0544
const url_proposta_0544 = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJhsf2DQG1J3nMWFwAE12yLtUhqgjI8nrkz8qP183Ee@D5XS0-00LnO4Wg1ppLQTXpA__`;
//FONTE DE DADOS: 0507 - Check Crédito - Proposta 0566
const url_proposta_0566 = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJv0139DDv4EHSMlMIYmxXadtmvU7xS37y1e1@Nxli3lM8mY5-lWKVtZy0ioRNRXeOw__`;
//FONTE DE DADOS: 0507 - Check Crédito - Proposta 0582
const url_proposta_0582 = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJsc99nrkdlBnsUEOKq23@gHJPj-8q8h5dUtrLnHFFPJQ3OhfEEeSfNBgcYGfauFGgA__`;
//FONTE DE DADOS: 0507 - Check Crédito - Proposta 0590
const url_proposta_0590 = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJjXe96B3tWiDwH3t@5I1oHYdeJ7MLJ499fxHLyjUQNB7XWDfUlngHWvRbDOYjT2qLA__`;
//FONTE DE DADOS: 0507 - Check Crédito - Proposta 9091
const url_proposta_9091 = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJkvJ7UCGpz4XsydcyTQPEduGXyrA5-wo9fpIN9ZV5G4ezqTfsCW0IOp8-tztMu7Sqg__`;
/*--------------------------------------------------------*/

const spinnerLoaderOn = () => $('#spinner-loader').show();
const spinnerLoaderOff = () => $('#spinner-loader').hide();

const isMobile = () => {
  let userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
};

//Função utilizada em atividades do requisitante, no qual poderá alterar as informações solicitadas
const limparInformcoesFormulario = async () => {
  let rowsOfTheTableToClean = $(
    '#section-informacoes-checklist-documentos table'
  ).find('tbody tr');
  $('#section-informacoes-proposta').hide();
  $('#section-informacoes-checklist-documentos').hide();

  informacoesProposta = [];

  let inputInfosProposta = $('#section-informacoes-proposta').find('input');
  await _.forEach(inputInfosProposta, input => {
    $(input).val('');
  });

  $('inp:tipo').val('');
  $('inp:nome').val('');
  $('inp:documento').val('');

  for (let index = rowsOfTheTableToClean.length - 1; index > 0; index--) {
    $($(rowsOfTheTableToClean)[index])
      .find('button.btn-danger')
      .click();
  }
};

//Função utilizada em atividades do requisitante, no qual poderá alterar as informações solicitadas
const verificarCooperativaDestinoParaBuscarInformacoesProposta = () => {
  feedbackInput('remove', 'propostaDeCredito', false);
  spinnerLoaderOn();
  let cooperativaDestino = $('inp:cooperativaDestino').val();

  switch (cooperativaDestino) {
    case '0515 – Unicred Florianópolis':
      buscarinformacoesProposta(url_proposta_0515);
      break;
    case '0544 – Unicred Desbravadora Sul':
      buscarinformacoesProposta(url_proposta_0544);
      break;
    case '0566 – Unicred Sul Catarinense':
      buscarinformacoesProposta(url_proposta_0566);
      break;
    case '0582 – Unicred União':
      buscarinformacoesProposta(url_proposta_0582);
      break;
    case '0590 – Unicred Vale Europeu':
      buscarinformacoesProposta(url_proposta_0590);
      break;
    case '9091 – Unicred Coomarca':
      buscarinformacoesProposta(url_proposta_9091);
      break;
  }
};

//Função utilizada em atividades do requisitante, no qual poderá alterar as informações solicitadas
const buscarinformacoesProposta = async url => {
  let propostaDeCredito = $('inp:propostaDeCredito').val();

  if (!propostaDeCredito) {
    spinnerLoaderOff();
    feedbackInput(
      'add',
      'propostaDeCredito',
      true,
      'invalid',
      'Preencha um dado para que possamos iniciar a busca!'
    );
    return;
  }

  try {
    const { data } = await axios.post(url, {
      inppropostaDeCredito: propostaDeCredito,
    });
    if (data && data.success && data.success[0]) {
      informacoesProposta = data.success[0];
      inserirInformacoesDaProposta();
    } else {
      spinnerLoaderOff();
      feedbackInput(
        'add',
        'propostaDeCredito',
        true,
        'invalid',
        'Não encontramos esta informação. Ajuste e tente novamente!'
      );
    }
  } catch (error) {
    spinnerLoaderOff();
    feedbackInput(
      'add',
      'propostaDeCredito',
      true,
      'invalid',
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    console.log(error);
  }
};

//Função utilizada em atividades do requisitante, no qual poderá alterar as informações solicitadas
const inserirInformacoesDaProposta = async () => {
  let valorEmMoeda = await transformarEmFormatoMoeda(
    informacoesProposta.fields.ValorSolicitado
  );

  $('inp:conta').val(informacoesProposta.txt);
  $('inp:associado').val(informacoesProposta.fields.Associado);
  $('inp:linhaDeCredito').val(informacoesProposta.fields.DescricaoLinhaCredito);
  $('inp:dataSolicitacao').val(informacoesProposta.fields.DataSolicitacao);
  $('inp:valorSolicitado').val(valorEmMoeda);

  apresentarInformacoesGeradas();
};

const apresentarInformacoesGeradas = () => {
  $('#section-informacoes-proposta').show();
  $('#section-informacoes-checklist-documentos').show();

  spinnerLoaderOff();
};

//Função utilizada em atividades do requisitante, no qual poderá alterar as informações solicitadas
//STATUS: remove ou add | IDENTIFICADOR: identificador do input | CLASSE: invalid ou valid | FEEDBACK: mensagem de feedback | EMPTYINPUT: true ou false
const feedbackInput = (status, identificador, emptyInput, classe, feedback) => {
  status === 'remove'
    ? $(`inp:${identificador}`)
        .removeClass('is-invalid')
        .removeClass('is-valid')
    : classe === 'valid'
    ? $(`inp:${identificador}`).addClass('is-valid')
    : $(`inp:${identificador}`).addClass('is-invalid');
  status === 'remove'
    ? $($(`inp:${identificador}`).parent())
        .find('#feedback-input')
        .remove()
    : classe === 'valid'
    ? $($(`inp:${identificador}`).parent()).append(
        `<div class="valid-feedback" id="feedback-input">${feedback}</div>`
      )
    : $($(`inp:${identificador}`).parent()).append(
        `<div class="invalid-feedback" id="feedback-input">${feedback}</div>`
      );
  $('#feedback-input').show();
  emptyInput ? $(`inp:${identificador}`).val('') : null;
};

//Função utilizada em atividades do requisitante, no qual poderá alterar as informações solicitadas
const transformarEmFormatoMoeda = valor => {
  return parseFloat(valor).toLocaleString('pt-br', {
    minimumFractionDigits: 2,
  });
};

//Funções abaixo fazem parte da estrutura de associação em uma atividade
//As atividades apenas chamam, quando precisam dessa estrutura
const adicionarEstruturaClaimUnclaimTask = () => {
  const taskTitle = $('div.title h1.huge.task-title').text();
  const associacao = $('inp:associacao').val();

  if (associacao === 'Usuário Associado') {
    $('#ContainerForm h2').html(
      'Formulário<button id="claim-unclaim-task" type="button" class="btn btn-small btn-danger right"><span><i class="fas fa-check"></i></span> Desassociar Atividade</button>'
    );
    $('div.title h1.huge.task-title').html(
      `${taskTitle} <span id="icon-claim-task"><i class="fas fa-hand-point-up" style="color: #dc3545;"></i></span>`
    );

    liberarAcoesClaimTask();
  } else {
    $('#ContainerForm h2').html(
      'Formulário<button id="claim-unclaim-task" type="button" class="btn btn-small btn-info right"><span><i class="fas fa-check"></i></span> Assumir Atividade</button>'
    );

    bloquearAcoesClaimTask();
  }
};

const claimUnclaimTask = async () => {
  const taskTitle = $('div.title h1.huge.task-title').text();
  const associacao = $('inp:associacao').val();
  const dataAtual = moment(new Date()).format('DD/MM/YYYY HH:mm');

  if (associacao === 'Usuário Associado') {
    await salvarDadosFormularioViaAPI('', '');
    cryo_TaskUnclaimOwnershipWrapper();

    $('#claim-unclaim-task')
      .addClass('btn-info')
      .removeClass('btn-danger')
      .html('<span><i class="fas fa-check"></i></span> Assumir Atividade');
    $('#icon-claim-task').remove();
    toastr.info('Você desassociou a atividade de seu usuário!');

    bloquearAcoesClaimTask();
  } else {
    await salvarDadosFormularioViaAPI('Usuário Associado', dataAtual);
    cryo_TaskClaimOwnershipWrapper();

    $('#claim-unclaim-task')
      .addClass('btn-danger')
      .removeClass('btn-info')
      .html('<span><i class="fas fa-check"></i></span> Desassociar Atividade');
    $('div.title h1.huge.task-title').html(
      `${taskTitle} <span id="icon-claim-task"><i class="fas fa-hand-point-up" style="color: #dc3545;"></i></span>`
    );
    toastr.warning(
      'Você se associou a atividade! Liberamos as demais funcionalidades para que possas executar a atividade!'
    );

    liberarAcoesClaimTask();
  }
};

const bloquearAcoesClaimTask = () => {
  $('.form-integra select').attr('readOnly', true);
  $('.form-integra input[type="text"]').attr('readOnly', true);
  $('.form-integra textarea').attr('readOnly', true);
  $('.form-integra button#btnUploadarquivo').attr('disabled', true);
  $('a.btn.btn-mini.btn-danger[title="Excluir"]').attr('disabled', true);
  $('a.btn.btn-mini.btn-danger[title="Excluir"]').removeAttr('onclick');

  $('tr#BoxComent.insertcoment').hide();
  $('div#ContainerAttach div#annex button.btn.btn-primary.btn-small').hide();
  $('div#buttons button').hide();
  $('#aActions').hide();
};

const liberarAcoesClaimTask = () => {
  $('.form-integra select').attr('readOnly', false);
  $('.form-integra input[type="text"]').attr('readOnly', false);
  $('.form-integra textarea').attr('readOnly', false);
  $('.form-integra button#btnUploadarquivo').attr('disabled', false);
  $('a.btn.btn-mini.btn-danger[title="Excluir"]').attr('disabled', false);
  $('a.btn.btn-mini.btn-danger[title="Excluir"]').attr(
    'onclick',
    'javascript:delFileFormField("arquivo", this)'
  );

  $('tr#BoxComent.insertcoment').show();
  $('div#ContainerAttach div#annex button.btn.btn-primary.btn-small').show();
  $('div#buttons button').show();
  $('#aActions').show();
};

const salvarDadosFormularioViaAPI = async (associacao, data) => {
  const token = $('#inpToken').val();
  const instancia = $('#inpCodFlowExecute').val();

  const dataValues = [
    {
      name: 'associacao',
      value: associacao,
    },
    {
      name: 'dataAssociacao',
      value: data,
    },
  ];

  const headerConfig = {
    headers: {
      'content-type': 'application/json',
      authorization: 'Bearer ' + token,
    },
  };

  await axios
    .patch(
      `https://bpm.e-unicred.com.br/api/1.0/instances/${instancia}/formvalues`,
      dataValues,
      headerConfig
    )
    .then(response => {
      $('inp:associacao').val(associacao);
      $('inp:dataAssociacao').val(data);
    })
    .catch(error => {
      toastr.error('Ocorreu um erro! Contate o suporte.');
      console.log(error);
    });
};

const verificarClaimUnclaimTask = () => {
  const associacao = $('inp:associacao').val();

  if (associacao === 'Usuário Associado') {
    window.alert('Você ainda está associado na atividade!');
  }
};
