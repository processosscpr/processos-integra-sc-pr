$(document).ready(function() {
  ajustarFormulario();
});

function ajustarFormulario() {
  let nomeRequisitante = $('inp:nomeRequisitante').val();

  //hide elements from INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  //show elements from the from
  if (!isMobile()) {
    $(
      `<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${nomeRequisitante}</strong></span>`
    ).insertAfter('.title span.label.label-info.flow-title');
  }

  _.forEach(
    $('#section-informacoes-checklist-documentos table').find('tbody tr'),
    row => {
      $(row)
        .find('div[xtype="FILE"] a[data-colorbox="true"]')
        .text('Arquivo');
    }
  );
}
