$(document).ready(function() {
  ajustarFormulario();
});

function ajustarFormulario() {
  let nomeRequisitante = $('inp:nomeRequisitante').val();

  //hide elements from INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  $('#customBtn_Encaminhado\\ Documentos\\ da\\ Lista\\ Solicitada').hide();
  $('#customBtn_Encaminhado\\ para\\ Validação\\ da\\ Diretoria').hide();

  //show elements from the from
  if (!isMobile()) {
    $(
      `<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${nomeRequisitante}</strong></span>`
    ).insertAfter('.title span.label.label-info.flow-title');
  }

  //funções de manipulação / ajustes em formulário
  $('#section-informacoes-checklist-documentos table')
    .find('thead tr td')
    .remove();

  _.forEach(
    $('#section-informacoes-checklist-documentos table').find('tbody tr'),
    row => {
      $($(row).children()[0]).remove();
      $(row)
        .find('input[xtype="FILE"]')[0]
        .setAttribute('required', 'N'),
        $(row)
          .find('button#btnUploadarquivo')
          .addClass('btn-info btn-sm')
          .text('')
          .append(`<i class="fas fa-paperclip"></i>`)
          .hide();
    }
  );
}

const verificarStatusSelecionado = async elemento => {
  const valueElemento = $(elemento).val();
  let quantidadeDispensaDocumentos = 0;

  if (valueElemento === 'Inserir Documento') {
    $(
      $(elemento)
        .parent()
        .parent()
    )
      .find('button#btnUploadarquivo')
      .show();
    $(
      $(elemento)
        .parent()
        .parent()
    )
      .find('input[xtype="FILE"]')[0]
      .setAttribute('required', 'S');
  } else {
    $(
      $(elemento)
        .parent()
        .parent()
    )
      .find('button#btnUploadarquivo')
      .hide();
    $(
      $(elemento)
        .parent()
        .parent()
    )
      .find('input[xtype="FILE"]')[0]
      .setAttribute('required', 'N');
  }

  await _.forEach($('inp:status'), input => {
    $(input).val() === 'Dispensar Documento'
      ? quantidadeDispensaDocumentos++
      : null;
  });

  if (quantidadeDispensaDocumentos >= 1) {
    $('#customBtn_Encaminhado\\ Documentos\\ da\\ Lista\\ Solicitada').hide();
    $('#customBtn_Encaminhado\\ para\\ Validação\\ da\\ Diretoria').show();
  } else {
    $('#customBtn_Encaminhado\\ Documentos\\ da\\ Lista\\ Solicitada').show();
    $('#customBtn_Encaminhado\\ para\\ Validação\\ da\\ Diretoria').hide();
  }
};
