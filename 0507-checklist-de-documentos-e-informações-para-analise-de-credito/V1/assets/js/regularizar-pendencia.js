$(document).ready(function() {
  ajustarFormulario();

  $('inp:cooperativaDestino').change(async elemento => {
    await limparInformcoesFormulario();
    elemento.target.value === '0515 – Unicred Florianópolis'
      ? ($('#coluna-colaborador').hide(),
        $('inp:colaborador')[0].setAttribute('required', 'N'))
      : ($('#coluna-colaborador').show(),
        $('inp:colaborador')[0].setAttribute('required', 'S'));
    $('#div-search-proposta').show();
  });

  $('inp:propostaDeCredito').change(elemento => {
    limparInformcoesFormulario();
  });

  $('#search-proposta').click(async e => {
    await limparInformcoesFormulario();
    verificarCooperativaDestinoParaBuscarInformacoesProposta();
  });

  $('#BtnInsertNewRow').click(() => {
    let quantidadeRows = $(
      '#section-informacoes-checklist-documentos table'
    ).find('tbody tr').length;
    let rowToDuplicateValues = $(
      '#section-informacoes-checklist-documentos table'
    ).find('tbody tr')[quantidadeRows - 2];
    $('#section-informacoes-checklist-documentos table')
      .find('tbody tr #checkTipo')
      .last()
      .find('select')
      .val(
        $(rowToDuplicateValues)
          .find('#checkTipo select')
          .val()
      );
    $('#section-informacoes-checklist-documentos table')
      .find('tbody tr #checkNome')
      .last()
      .find('input')
      .val(
        $(rowToDuplicateValues)
          .find('#checkNome input')
          .val()
      );
  });
});

function ajustarFormulario() {
  $('#spinner-loader').hide();
  let nomeRequisitante = $('inp:nomeRequisitante').val();
  let inputInfosProposta = $('#section-informacoes-proposta').find('input');
  let optionsCooperativaDestino = $('inp:cooperativaDestino').find('option');

  //hide elements from INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  //hide elements from the form
  _.forEach(optionsCooperativaDestino, option => {
    $(option).val() === '0507 – Unicred Central SC/PR'
      ? $(option).hide()
      : $(option).show();
  });
  //hide elements from the table
  $($('#section-informacoes-checklist-documentos table').find('th')[3]).hide();
  $($('#section-informacoes-checklist-documentos table').find('th')[4]).hide();

  _.forEach(
    $('#section-informacoes-checklist-documentos table').find('tbody tr'),
    row => {
      $($(row).children()[4]).hide();
    }
  );
  _.forEach(
    $('#section-informacoes-checklist-documentos table').find('tbody tr'),
    row => {
      $($(row).children()[5]).hide();
    }
  );

  //show elements from the from
  if (!isMobile()) {
    $(
      `<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${nomeRequisitante}</strong></span>`
    ).insertAfter('.title span.label.label-info.flow-title');
  }
  $('#search-proposta').attr('style', 'display: block');

  //funções de manipulação / ajustes em formulário
  if ($('inp:cooperativaDestino').val() === '0515 – Unicred Florianópolis') {
    $('#coluna-colaborador').hide();
    $('inp:colaborador')[0].setAttribute('required', 'N');
  }

  inputInfosProposta.attr('readOnly', true);
  _.forEach(inputInfosProposta, input => {
    input.setAttribute('required', 'N');
  });

  $('#BtnInsertNewRow')
    .addClass('btn-primary btn-sm')
    .text('')
    .append(`<i class="fas fa-plus"></i>`);
  $(
    '#section-informacoes-checklist-documentos table tbody button.btn-delete-mv'
  )
    .addClass('btn-danger btn-sm')
    .text('')
    .append(`<i class="fas fa-trash-alt"></i>`);
}

/*--------------------------------------------------------*/
let informacoesProposta = [];
/*--------------------------------------------------------*/
