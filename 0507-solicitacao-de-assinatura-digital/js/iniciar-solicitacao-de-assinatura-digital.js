const URL_TOKEN =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJg-CqsL3NypcCvWuQu01@2-7ejKkCN5V0IBK52p0D6LEWgV3g7RcWg6-TyDXTbVyzg__';
const URL_TOKEN_TST =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJvrH1QaVJrlzgWlabL@t4hFszHI6vLzXaLS9jlRj75U@ToRxL15Hh24treqU3l6log__';
const URL_CADASTRO_PF = cpf =>
  `https://servicos.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-fisica/v1/pessoa-fisica/${cpf}`;
const URL_CADASTRO_PJ = cnpj =>
  `https://servicos.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-juridica/v1/pessoa-juridica/${cnpj}`;
const URL_CONTATO = cpf =>
  `https://servicos.e-unicred.com.br/pessoa-us/cadastro/pessoa/v1/fisica/${cpf}/contato`;

let dadosCooperado = {};

$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();

  ajustarLayoutTabelaMultivalorada();
  ajustarFormulario();
  inserirMensagemAuxiliar();

  definirCamposObrigatorios();

  desativarInputs();
  esconderSection();

  renderizarEventosPorClick();

  realizarRegraComCoperativa();
});

const ajustarLayoutTabelaMultivalorada = () => {
  const btnAddRow = $('#BtnInsertNewRow');
  const btnDeleteRow = $(
    '#coluna-table-assinatura-digital button.btn-delete-mv'
  );

  $(btnAddRow)
    .addClass('btn-primary btn-sm')
    .text('')
    .append(`<i class="fas fa-plus"></i>`);
  $(btnDeleteRow)
    .addClass('btn-danger btn-sm')
    .text('')
    .append(`<i class="fas fa-trash-alt"></i>`);
};

const ajustarFormulario = () => {
  // configurações default do formulário
  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');

  $('.input-group-append').show();
};

const inserirMensagemAuxiliar = () => {
  $(`
    <div class="alert alert-error my-2">
      <strong>
        As informações preenchidas e os anexos disponibilizados são de responsabilidade do requisitante da solicitação. Não haverá análise do que for disponibilizado na instância, apenas será importado no DocuSign para resgate de assinatura digital do cooperado.
      </strong>
    </div>
  `).insertAfter('.card');
};

const desativarInputs = () => {
  $('inp:nomeRazaoSocial').attr('readonly', true);
  $('inp:tipoDePessoa').attr('readonly', true);
  $('inp:profissaoRamo').attr('readonly', true);
  $('inp:posto').attr('readonly', true);
  $('inp:matricula').attr('readonly', true);
  $('inp:celular').attr('readonly', true);
};

const definirCamposObrigatorios = () => {
  $('inp:tipoDeSolicitacao').addClass('required');
  $('inp:cpfCnpj').addClass('required');
  $('inp:nomeDeQuemReceberaOArquivo').addClass('required');
  $('inp:emailDeQuemReceberaOArquivo').addClass('required');
};

const mostrarSections = () => {
  $('.label-div-contas').show();
  $('.div-contas').show();
  $('.informacoes-do-associado').show();
  $('.informacoes-do-pedido').show();
};

const esconderSection = () => {
  $('.informacoes-do-associado').hide();
  $('.informacoes-do-pedido').hide();
  $('.div-contas-seleciodas').hide();
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const limparFormulario = () => {
  $('.informacoes-do-associado').hide();
  $('.informacoes-do-pedido').hide();

  if (!!$('.div-contas').length) $('.div-contas')[0].innerHTML = '';

  $('.label-div-contas').hide();

  $('inp:matricula').val('');
  $('inp:posto').val('');
  $('inp:nomeRazaoSocial').val('');
  $('inp:tipoDePessoa').val('');
  $('inp:profissaoRamo').val('');
  $('inp:detalhesDaSolicitacao').val('');
  $('inp:tipoDeSolicitacao').val('');
  $('inp:celular').val('');

  $('inp:nomeDeQuemReceberaOArquivo').val('');
  $('inp:emailDeQuemReceberaOArquivo').val('');
};

const renderizarEventosPorClick = () => {
  const infosCooperado = $('.procurar-informacoes-do-cooperado');
  const btnSolicitacao = $('#customBtn_Solicitação\\ Encaminhada');

  infosCooperado.click(() => validarBuscaCooperado());
  btnSolicitacao.click(() => validarEnvioSolicitacao());
};

const realizarRegraComCoperativa = () => {
  const cooperativaRequisitante = $('inp:cooperativaRequisitante').val();

  if (cooperativaRequisitante !== '0507') {
    $('inp:cooperativa').val(cooperativaRequisitante);
    $('inp:cooperativa').attr('readonly', true);
  }
};

const validarBuscaCooperado = () => {
  limparFormulario();

  dadosCooperado.cpfCnpj = $('inp:cpfCnpj').val();
  dadosCooperado.cooperativa = $('inp:cooperativa').val();

  if (!dadosCooperado.cpfCnpj || !dadosCooperado.cooperativa) {
    toastr.error('Cooperativa e CPF / CNPJ são campos obrigatórios!', 'Erro!');

    return;
  }

  bucarInformacoesCooperado();
};

const bucarInformacoesCooperado = async () => {
  toggleSpinner();

  await buscarToken();

  if (!dadosCooperado.token || !dadosCooperado.cooperativa) {
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    return;
  }

  verificarTipoDePessoa(dadosCooperado.cpfCnpj);

  toggleSpinner();
};

const buscarToken = async () => {
  await axios
    .get(URL_TOKEN)
    .then(success => {
      if (success.data.success[0].fields.access_token) {
        dadosCooperado.token = `Bearer ${success.data.success[0].fields.access_token}`;
      } else {
        toastr.error('Erro de comunicaçao com o serviço!');
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      console.log(error);
    });
};

const verificarTipoDePessoa = identificador => {
  if (identificador.length <= 11) {
    buscarDadosPeloCpf(identificador);
    dadosCooperado.tipoDePessoa = 'Pessoa Física';
  } else {
    buscarDadosPeloCnpj(identificador);
    dadosCooperado.tipoDePessoa = 'Pessoa Jurídica';
  }
};

const buscarDadosPeloCpf = async identificador => {
  axios
    .get(URL_CADASTRO_PF(identificador), {
      headers: {
        Authorization: dadosCooperado.token,
        cooperativa: dadosCooperado.cooperativa,
      },
    })
    .then(res => {
      dadosCooperado.posto = res.data.associacao.posto;
      dadosCooperado.matricula = res.data.associacao.matricula;
      dadosCooperado.razaoSocial = res.data.nome;
      dadosCooperado.profissaoRamo = res.data.dadosProfissionais.profissao;
      dadosCooperado.contas = res.data.contas;

      buscarDadosDaConta(dadosCooperado.cpfCnpj);
    })
    .catch(() =>
      toastr.error('Ocorreu um erro ao trazer as informações!', 'Atenção')
    );
};

const buscarDadosPeloCnpj = identificador => {
  axios
    .get(URL_CADASTRO_PJ(identificador), {
      headers: {
        Authorization: dadosCooperado.token,
        cooperativa: dadosCooperado.cooperativa,
      },
    })
    .then(res => {
      dadosCooperado.razaoSocial = res.data.nome;
      dadosCooperado.profissaoRamo =
        res.data.cnae.atividadeEconomicaPrincipal.descricao;
      dadosCooperado.contas = res.data.contas;
      dadosCooperado.matricula = res.data.associacao.matricula;
      dadosCooperado.posto = res.data.associacao.posto;

      dadosCooperado.contato = res.data.contatos.telefones.find(
        x => x.tipo === 'CELULAR'
      );

      preencherFormulario(dadosCooperado);
      construirSectionContas(dadosCooperado.contas);
    })
    .catch(() =>
      toastr.error('Ocorreu um erro ao trazer as informações!', 'Atenção')
    );
};

const buscarDadosDaConta = async identificador => {
  await axios
    .get(URL_CONTATO(identificador), {
      headers: {
        Authorization: dadosCooperado.token,
        cooperativa: dadosCooperado.cooperativa,
      },
    })
    .then(res => {
      dadosCooperado.contato = res.data.telefones.find(
        x => x.podeEnviarSMS === true
      );

      toastr.success('Informações carregadas com sucesso!', 'Sucesso');
    })
    .catch(() =>
      toastr.error('Ocorreu um erro ao trazer as informações!', 'Atenção')
    );

  preencherFormulario(dadosCooperado);
  construirSectionContas(dadosCooperado.contas);
};

const preencherFormulario = async () => {
  $('inp:cooperativa').val(dadosCooperado.cooperativa);
  $('inp:posto').val(dadosCooperado.posto);
  $('inp:matricula').val(dadosCooperado.matricula);
  $('inp:nomeRazaoSocial').val(dadosCooperado.razaoSocial);
  $('inp:cpfCnpj').val(dadosCooperado.cpfCnpj);
  $('inp:tipoDePessoa').val(dadosCooperado.tipoDePessoa);
  $('inp:profissaoRamo').val(dadosCooperado.profissaoRamo);
  $('inp:celular').val(
    `(${dadosCooperado.contato.ddd}) ${dadosCooperado.contato.numero}`
  );
};

/****************************************** FUNKYRADIO  */

const construirElementoInputCheckboxSelect = (texto, index, identificador) => {
  return `
      <div class="col-xs-12 col-md-2">
        <div class="form-group">
          <div class="funkyradio-success">
            <input
              class="input-${identificador}"
              type="checkbox"
              name="checkbox-${identificador}"
              id="checkbox-${identificador}-${index}"
            />
            <label
              class="label-group-${identificador}"
              for="checkbox-${identificador}-${index}"
            >
              ${texto}
            </label>
          </div>
        </div>
      </div>
    `;
};

const construirSectionContas = contas => {
  _.each(contas, (conta, index) => {
    const elemento = construirElementoInputCheckboxSelect(
      conta.numero,
      index,
      'conta'
    );

    $('.funkyradio.div-contas').append(elemento);
  });

  mostrarSections();
};

const obterContasSelecionadas = () => {
  const elemento = $('input[type="checkbox"]:checked.input-conta');

  return _.map(elemento, item => $(item).siblings()[0].innerText).join(', ');
};

const validarInput = elemento => {
  !elemento.value.length
    ? $(elemento)
        .addClass('is-invalid')
        .removeClass('is-valid')
    : $(elemento)
        .addClass('is-valid')
        .removeClass('is-invalid');
};

const validarCheckboxContas = () => {
  $('.funkyradio.div-contas input:checked').length
    ? $('.funkyradio.div-contas label')
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $('.funkyradio.div-contas label')
        .addClass('is-invalid')
        .removeClass('is-valid');
};

const validarEnvioSolicitacao = () => {
  validarCheckboxContas();

  const inputsObrigatorios = $('.form-integra .required');

  inputsObrigatorios.change(() => {
    _.each(inputsObrigatorios, elemento => validarInput(elemento));
  });

  inputsObrigatorios.trigger('change');

  if (!!$('.is-invalid').length || !!$('span.badge-secondary').length) {
    toastr.error('Campos ou documentos obrigatórios!', 'Atenção');
  } else {
    $('inp:contas').val(obterContasSelecionadas);
    $('inp:contasParaManipulacao').val(
      _.map(dadosCooperado.contas, e => e.numero).join(',')
    );

    $('.label-div-contas').remove();
    $('.div-contas')[0].innerHTML = '';

    doAction('Solicitação Encaminhada', false, false);

    $($(document.getElementsByClassName('cryo-confirm-dialog padded'))[0])
      .find('button.btn.btn-success')
      .click();
  }
};
