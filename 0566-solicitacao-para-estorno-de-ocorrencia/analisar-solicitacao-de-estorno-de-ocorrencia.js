$(document).ready(function() {
  exibirCampoColaborador();

  $(document.getElementById('formCamposAuxiliares')).hide();
  $(document.getElementById('btnCancel')).hide();

  if ($('div:estornoComInadimplencia').text() == 'Não') {
    $('#formInadimplencia').hide();
  }

  adicionarMobileSpan();
});

function exibirCampoColaborador() {
  var colaboradorOcasionou = document.querySelector(
    "[xname='inpcolaboradorOcasionouPerda']"
  ).value;

  if (colaboradorOcasionou == 'Sim') {
    $(document.getElementById('colaborador')).show();
    document.getElementById('colaboradorOcasionouPerda').colSpan = 1;
  } else {
    $(document.getElementById('colaborador')).hide();
    document.getElementById('colaboradorOcasionouPerda').colSpan = 2;
    document.querySelector("[xname='inpcolaborador']").value = '';
  }
}

//Oculta campo
function ocultarCampo(idCampo) {
  $(document.getElementById(idCampo)).hide();
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

const adicionarMobileSpan = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile =
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1;

  if (!isMobile) {
    $(
      `
        <span style='margin-left: 0.4em' class='label label-secondary'>
          <strong> Requisitante:${$('inp:nomeRequisitante').val()}</strong>
        </span>
      `
    ).insertAfter('.title span.label.label-info.flow-title');
  }
};
