$(document).ready(function(){
  
    verificarPosicaoIniciadora();
    
    if(!isMobile()){
    $(document.querySelector("#RequesterInfo")).hide();
      $('inp:nome').attr('style','width: 50%');
      $('inp:assunto').attr('style','width: 80%');
      $('inp:consideracoesDaNegociacao').attr('style','width: 80%; height:50px');
      $('inp:objetoDoContrato').attr('style','width: 80%; height:50px');
      $('inp:representacao').attr('style','width: 50%');
      $('inp:enderecoCompleto').attr('style','width: 50%');
      $('inp:possiveisPenalidades').attr('style','width: 80%; height:50px');
      $('inp:informacoesDePagamentorateioParaCooperativas').attr('style','width: 80%; height:50px');
    }else{
      adaptarMobile();
    }
  
    ajustarFormularioInicial();
  
    var validacaoAreaRequisitante = verificarAreaRequisitante(document.querySelector("[xname='inpareaRequisitante']").value);
    var tipoSolicitacao = document.querySelector("[xname='inptipoDeSolicitacao']");
    tipoSolicitacao.addEventListener('change', function(){
      ajustarFormulario(tipoSolicitacao,validacaoAreaRequisitante);
    });
  
    var tipoPessoa = document.querySelector("[xname='inptipoDePessoa']");
    tipoPessoa.addEventListener('change', function(){
      ajustarPorTipoPessoa(tipoPessoa);
    });
  
    var vigencia = document.querySelector("[xname='inpvigencia']");
    vigencia.addEventListener('change', function(){
      ajustarPorVigencia(vigencia);
    });
  
    $("[xname='inpobjetoDoContrato']").bind('paste', function(e) {
      e.preventDefault();
    });
  
  });
  
  function isMobile(){
    var userAgent = navigator.userAgent.toLowerCase();
    if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
          return true;
  }
  
  function adaptarMobile(){
    $(document.querySelector(".mobile-floating-controls")).hide();
  
    document.querySelector("[xname='inptipoDeSolicitacao']").setAttribute('style','width:85%!important');
    document.querySelector("[xname='inptipoDePessoa']").setAttribute('style','width:85%!important');
    document.querySelector("[xname='inpnome']").setAttribute('style','height: 20px!important;width:85%!important');
    document.querySelector("[xname='inpcpfcnpj']").setAttribute('style','height: 20px!important;width:85%!important');
    document.querySelector("[xname='inpassunto']").setAttribute('style','height: 20px!important;width:85%!important');
    document.querySelector("[xname='inpdocumento']").setAttribute('style','width:85%!important');
    document.querySelector("[xname='inpvigencia']").setAttribute('style','width:85%!important');
    document.querySelector("[xname='inpdata']").setAttribute('style','height: 20px!important;width:85%!important;background-color:white');
    document.querySelector("[xname='inpdata']").setAttribute('readonly','true');
    document.querySelector("[xname='inptipoDeContrato']").setAttribute('style','width:85%!important');
    document.querySelector("[xname='inpconsideracoesDaNegociacao']").setAttribute('style','height: 40px!important;width:85%!important');
    document.querySelector("[xname='inpobjetoDoContrato']").setAttribute('style','height: 40px!important;width:85%!important');
    document.querySelector("[xname='inprepresentacao']").setAttribute('style','height: 20px!important;width:85%!important');
    document.querySelector("[xname='inpenderecoCompleto']").setAttribute('style','height: 20px!important;width:85%!important');
    document.querySelector("[xname='inpvalor']").setAttribute('style','height: 20px!important;width:85%!important');
    document.querySelector("[xname='inpformaDePagamento']").setAttribute('style','width:85%!important');
    document.querySelector("[xname='inpprazoDePagamento']").setAttribute('style','height: 20px!important;width:85%!important;background-color:white');
    document.querySelector("[xname='inpprazoDePagamento']").setAttribute('readonly','true');
    document.querySelector("[xname='inpprazoAcordado']").setAttribute('style','height: 20px!important;width:85%!important;background-color:white');
    document.querySelector("[xname='inpprazoAcordado']").setAttribute('readonly','true');
    document.querySelector("[xname='inppossiveisPenalidades']").setAttribute('style','height: 40px!important;width:85%!important');
    document.querySelector("[xname='inpinformacoesDePagamentorateioParaCooperativas']").setAttribute('style','height: 40px!important;width:85%!important');
  
  
  }
  
  function ajustarFormularioInicial(){
    if ($('inp:cooperativaRequisitante').val() === '0515') {
        $($('inp:tipoDeSolicitacao').find('option')[1]).hide();
        $($('inp:tipoDeSolicitacao').find('option')[2]).hide();
    }

    if ($("inp:cooperativaRequisitante").val() !== "0566" || $("inp:cooperativaRequisitante").val() !== "0544" || $("inp:cooperativaRequisitante").val() !== "0590") {
    document.querySelector("#informacoes-contrato").setAttribute('class','box box-open-and-close');
    $(document.querySelector("#informacoes-auxiliares")).hide();
    $(document.querySelector("#informacoes-marcos-administrativos")).hide();
    $(document.querySelector("#mensagem-diretoria")).hide();
    $(document.querySelector("#tipo-pessoa")).hide();
    $(document.querySelector("#informacoes-pessoais")).hide();
    $(document.querySelector("#informacao-tipo-contrato")).hide();
    $(document.querySelector("#informacoes-categorizacao-contrato")).hide();
    $(document.querySelector("#informacoes-elaboracao")).hide();
    $(document.querySelector("#informacoes-administrativas")).hide();
    $(document.querySelector("#divStatisticsTable")).hide();
    $(document.querySelector("#btnCancel")).hide();
  
    document.querySelector("[xname='inptipoDeSolicitacao']").setAttribute('required','S');
    document.querySelector("[xname='inptipoDePessoa']").setAttribute('required','N');
    document.querySelector("[xname='inpnome']").setAttribute('required','N');
    document.querySelector("[xname='inpcpfcnpj']").setAttribute('required','N');
    document.querySelector("[xname='inpassunto']").setAttribute('required','N');
    document.querySelector("[xname='inpdocumento']").setAttribute('required','N');
    document.querySelector("[xname='inpvigencia']").setAttribute('required','N');
    document.querySelector("[xname='inpdata']").setAttribute('required','N');
    document.querySelector("[xname='inptipoDeContrato']").setAttribute('required','N');
    document.querySelector("[xname='inpconsideracoesDaNegociacao']").setAttribute('required','N');
    document.querySelector("[xname='inpobjetoDoContrato']").setAttribute('required','N');
    document.querySelector("[xname='inprepresentacao']").setAttribute('required','N');
    document.querySelector("[xname='inpenderecoCompleto']").setAttribute('required','N');
    document.querySelector("[xname='inpvalor']").setAttribute('required','N');
    document.querySelector("[xname='inpformaDePagamento']").setAttribute('required','N');
    document.querySelector("[xname='inpprazoDePagamento']").setAttribute('required','N');
    document.querySelector("[xname='inpprazoAcordado']").setAttribute('required','N');
    document.querySelector("[xname='inppossiveisPenalidades']").setAttribute('required','N');
    document.querySelector("[xname='inpinformacoesDePagamentorateioParaCooperativas']").setAttribute('required','N');
      
    } else if ($("inp:cooperativaRequisitante").val() == "0566" || $("inp:cooperativaRequisitante").val() == "0544" || $("inp:cooperativaRequisitante").val() == "0590") {
      $(document.querySelector("#tipoSolicitacao")).hide();
      $(document.querySelector("#mensagem-diretoria")).hide();
      $(document.querySelector("#mensagem-juridico")).hide();
      $(document.querySelector("#tipo-pessoa")).show();
      $(document.querySelector("#informacoes-pessoais")).hide();
      $(document.querySelector("#informacao-tipo-contrato")).show();
      $(document.querySelector("#informacoes-categorizacao-contrato")).show();
      $(document.querySelector("#vigencia")).show();
      $(document.querySelector("#data-vigencia")).hide();
      $(document.querySelector("#informacoes-elaboracao")).show();
      $(document.querySelector("#divStatisticsTable")).hide();
      $(document.querySelector("#btnCancel")).hide();
      $(document.querySelector("#informacoes-auxiliares")).hide();
      $(document.querySelector("#informacoes-administrativas")).show
        
      $(document.querySelector("#informacoes-marcos-administrativos")).hide();
      $(document.querySelector("#representacao")).hide();
      $(document.querySelector("#enderecoCompleto")).hide();
      $(document.querySelector("#prazoAcordado")).hide();
      $(document.querySelector("#possiveisPenalidades")).hide();
      
      document.querySelector("[xname='inptipoDeSolicitacao']").setAttribute('required','N');
      document.querySelector("[xname='inpdata']").setAttribute('required','N');
      document.querySelector("[xname='inprepresentacao']").setAttribute('required','N');
      document.querySelector("[xname='inpenderecoCompleto']").setAttribute('required','N');
      document.querySelector("[xname='inpprazoAcordado']").setAttribute('required','N');
      document.querySelector("[xname='inppossiveisPenalidades']").setAttribute('required','N');
      
    }
  }
  
  function ajustarFormulario(tipoSolicitacao,validacaoAreaRequisitante){
    if(tipoSolicitacao.selectedIndex == 1) {
      $(document.querySelector("#mensagem-diretoria")).hide();
      $(document.querySelector("#tipo-pessoa")).show();
      $(document.querySelector("#informacoes-pessoais")).hide();
      $(document.querySelector("#informacao-tipo-contrato")).hide();
      $(document.querySelector("#informacoes-categorizacao-contrato")).show();
      $(document.querySelector("#vigencia")).show();
      $(document.querySelector("#data-vigencia")).hide();
      $(document.querySelector("#informacoes-elaboracao")).hide();
  
      document.querySelector("[xname='inptipoDeSolicitacao']").setAttribute('required','S');
      document.querySelector("[xname='inptipoDePessoa']").setAttribute('required','S');
      document.querySelector("[xname='inpnome']").setAttribute('required','S');
      document.querySelector("[xname='inpcpfcnpj']").setAttribute('required','S');
      document.querySelector("[xname='inpassunto']").setAttribute('required','S');
      document.querySelector("[xname='inpdocumento']").setAttribute('required','S');
      document.querySelector("[xname='inpvigencia']").setAttribute('required','S');
      document.querySelector("[xname='inpdata']").setAttribute('required','N');
      document.querySelector("[xname='inptipoDeContrato']").setAttribute('required','N');
      document.querySelector("[xname='inpconsideracoesDaNegociacao']").setAttribute('required','S');
      document.querySelector("[xname='inpobjetoDoContrato']").setAttribute('required','S');
      document.querySelector("[xname='inprepresentacao']").setAttribute('required','N');
      document.querySelector("[xname='inpenderecoCompleto']").setAttribute('required','N');
      document.querySelector("[xname='inpvalor']").setAttribute('required','N');
      document.querySelector("[xname='inpformaDePagamento']").setAttribute('required','N');
      document.querySelector("[xname='inpprazoDePagamento']").setAttribute('required','N');
      document.querySelector("[xname='inpprazoAcordado']").setAttribute('required','N');
      document.querySelector("[xname='inppossiveisPenalidades']").setAttribute('required','N');
  
      if (validacaoAreaRequisitante) {
        $(document.querySelector("#informacoes-administrativas")).hide();
        document.querySelector("[xname='inpinformacoesDePagamentorateioParaCooperativas']").setAttribute('required','N');
      }else{
        $(document.querySelector("#informacoes-administrativas")).show();
        document.querySelector("[xname='inpinformacoesDePagamentorateioParaCooperativas']").setAttribute('required','S');
      }
  
    }else if (tipoSolicitacao.selectedIndex == 2) {
      $(document.querySelector("#mensagem-diretoria")).hide();
      $(document.querySelector("#tipo-pessoa")).show();
      $(document.querySelector("#informacoes-pessoais")).hide();
      $(document.querySelector("#informacao-tipo-contrato")).hide();
      $(document.querySelector("#informacoes-categorizacao-contrato")).show();
      $(document.querySelector("#vigencia")).show();
      $(document.querySelector("#data-vigencia")).hide();
      $(document.querySelector("#informacoes-elaboracao")).show();
  
      document.querySelector("[xname='inptipoDeSolicitacao']").setAttribute('required','S');
      document.querySelector("[xname='inptipoDePessoa']").setAttribute('required','S');
      document.querySelector("[xname='inpnome']").setAttribute('required','S');
      document.querySelector("[xname='inpcpfcnpj']").setAttribute('required','S');
      document.querySelector("[xname='inpassunto']").setAttribute('required','S');
      document.querySelector("[xname='inpdocumento']").setAttribute('required','S');
      document.querySelector("[xname='inpvigencia']").setAttribute('required','S');
      document.querySelector("[xname='inpdata']").setAttribute('required','N');
      document.querySelector("[xname='inptipoDeContrato']").setAttribute('required','N');
      document.querySelector("[xname='inpconsideracoesDaNegociacao']").setAttribute('required','S');
      document.querySelector("[xname='inpobjetoDoContrato']").setAttribute('required','S');
      document.querySelector("[xname='inprepresentacao']").setAttribute('required','S');
      document.querySelector("[xname='inpenderecoCompleto']").setAttribute('required','S');
      document.querySelector("[xname='inpvalor']").setAttribute('required','S');
      document.querySelector("[xname='inpformaDePagamento']").setAttribute('required','S');
      document.querySelector("[xname='inpprazoDePagamento']").setAttribute('required','S');
      document.querySelector("[xname='inpprazoAcordado']").setAttribute('required','S');
      document.querySelector("[xname='inppossiveisPenalidades']").setAttribute('required','S');
  
      if (validacaoAreaRequisitante) {
        $(document.querySelector("#informacoes-administrativas")).hide();
        document.querySelector("[xname='inpinformacoesDePagamentorateioParaCooperativas']").setAttribute('required','N');
      }else{
        $(document.querySelector("#informacoes-administrativas")).show();
        document.querySelector("[xname='inpinformacoesDePagamentorateioParaCooperativas']").setAttribute('required','S');
      }
  
    }else if (tipoSolicitacao.selectedIndex == 3) {
      $(document.querySelector("#mensagem-diretoria")).hide();
      $(document.querySelector("#tipo-pessoa")).hide();
      $(document.querySelector("#informacoes-pessoais")).hide();
      $(document.querySelector("#informacao-tipo-contrato")).hide();
      $(document.querySelector("#informacoes-categorizacao-contrato")).show();
      $(document.querySelector("#vigencia")).hide();
      $(document.querySelector("#informacoes-elaboracao")).hide();
      $(document.querySelector("#informacoes-administrativas")).hide();
  
      document.querySelector("[xname='inptipoDeSolicitacao']").setAttribute('required','S');
      document.querySelector("[xname='inptipoDePessoa']").setAttribute('required','N');
      document.querySelector("[xname='inpnome']").setAttribute('required','N');
      document.querySelector("[xname='inpcpfcnpj']").setAttribute('required','N');
      document.querySelector("[xname='inpassunto']").setAttribute('required','S');
      document.querySelector("[xname='inpdocumento']").setAttribute('required','S');
      document.querySelector("[xname='inpvigencia']").setAttribute('required','N');
      document.querySelector("[xname='inpdata']").setAttribute('required','N');
      document.querySelector("[xname='inptipoDeContrato']").setAttribute('required','N');
      document.querySelector("[xname='inpconsideracoesDaNegociacao']").setAttribute('required','N');
      document.querySelector("[xname='inpobjetoDoContrato']").setAttribute('required','S');
      document.querySelector("[xname='inprepresentacao']").setAttribute('required','N');
      document.querySelector("[xname='inpenderecoCompleto']").setAttribute('required','N');
      document.querySelector("[xname='inpvalor']").setAttribute('required','N');
      document.querySelector("[xname='inpformaDePagamento']").setAttribute('required','N');
      document.querySelector("[xname='inpprazoDePagamento']").setAttribute('required','N');
      document.querySelector("[xname='inpprazoAcordado']").setAttribute('required','N');
      document.querySelector("[xname='inppossiveisPenalidades']").setAttribute('required','N');
      document.querySelector("[xname='inpinformacoesDePagamentorateioParaCooperativas']").setAttribute('required','N');
  
    }else if (tipoSolicitacao.selectedIndex == 0) {
      ajustarFormularioInicial();
    }
  }
  
  function verificarAreaRequisitante(areaRequisitante){
    if ((areaRequisitante == "0507 - Administrativo Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Comercial Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Life Insurance Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Protocolista Endossos Auto Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Sinistro Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Unicred Corretora de Seguros Santa Catarina / Paraná")) {
      return true;
    }else{
      return false;
    }
  }
  
  function ajustarPorTipoPessoa(tipoPessoa){
    if(tipoPessoa.selectedIndex == 1) {
      ($(document.querySelector("#nome")).children())[0].textContent = "Nome Fornecedor: ";
      ($(document.querySelector("#cpfcnpj")).children())[0].textContent = "CPF: ";
      document.querySelector("[xname='inpcpfcnpj']").setAttribute('maxlength','11');
      $(document.querySelector("#informacoes-pessoais")).show();
      
    }else if(tipoPessoa.selectedIndex == 2) {
      ($(document.querySelector("#nome")).children())[0].textContent = "Razão Social / Nome Fantasia Fornecedor: ";
      ($(document.querySelector("#cpfcnpj")).children())[0].textContent = "CNPJ: ";
      document.querySelector("[xname='inpcpfcnpj']").setAttribute('maxlength','14');
      $(document.querySelector("#informacoes-pessoais")).show();
    }else if(tipoPessoa.selectedIndex == 0) {
      $(document.querySelector("#informacoes-pessoais")).hide();
    }
  }
  
  function ajustarPorVigencia(vigencia){
    if ((vigencia.selectedIndex == 1) || (vigencia.selectedIndex == 2)) {
      $(document.querySelector("#data-vigencia")).show();
      document.querySelector("[xname='inpdata']").setAttribute('required','S');
    }else{
      $(document.querySelector("#data-vigencia")).hide();
      document.querySelector("[xname='inpdata']").setAttribute('required','N');
    }    
  }
  
  function verificarPosicaoIniciadora(){
    if ($('select[name="inpCodPositionArea"]').val() != undefined) {
      var arrayAuxiliar = new Array;
      for (var i=0;i<document.querySelector("[name='inpCodPositionArea']").options.length;i++){
        arrayAuxiliar.push(document.querySelector("[name='inpCodPositionArea']").options[i].innerText);
      }
      arrayAuxiliar.shift();
      for (var i=arrayAuxiliar.length-1;i>=0;i--){
        if(arrayAuxiliar[i].substring(4,5) == "_"){
          arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]),1);
        }
      }
      $('select[name="inpCodPositionArea"]').val($("option:contains('" + arrayAuxiliar[0] + "')").val());
      $($('select[name="inpCodPositionArea"]').parent().parent()).hide();
    }
  }