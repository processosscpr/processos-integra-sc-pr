$(document).ready(function(){

    if(isMobile()){
      $(document.querySelector(".mobile-floating-controls")).hide();
    }
  
    var validacaoAreaRequisitante = verificarAreaRequisitante(document.querySelector("[xname='inpareaRequisitante']").value);
    ajustarFormularioInicial(validacaoAreaRequisitante);
  
  });
  
  function isMobile(){
    var userAgent = navigator.userAgent.toLowerCase();
    if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
          return true;
  }
  
  function ajustarFormularioInicial(validacaoAreaRequisitante){
    document.querySelector("#informacoes-contrato").setAttribute('class','box box-open-and-close');
    document.querySelector("#informacoes-administrativas").setAttribute('class','box box-open-and-close');
    $(document.querySelector("#informacoes-auxiliares")).hide();
    $(document.querySelector("#informacoes-marcos-administrativos")).hide();
    $(document.querySelector("#mensagem-juridico")).hide();
    $(document.querySelector("#mensagem-diretoria")).hide();
    $(document.querySelector("#mensagem-objeto")).hide();
  
    if(document.querySelector("[xname='inptipoDeSolicitacao']").value == "Análise de Contrato") {
      $(document.querySelector("#tipo-pessoa")).show();
      $(document.querySelector("#informacoes-pessoais")).show();
      $(document.querySelector("#informacao-tipo-contrato")).hide();
      $(document.querySelector("#informacoes-categorizacao-contrato")).show();
      $(document.querySelector("#informacoes-elaboracao")).hide();
      if((document.querySelector("[xname='inpvigencia']").value == "Determinado") || (document.querySelector("[xname='inpvigencia']").value == "Evento")){
        $(document.querySelector("#data-vigencia")).show();
      }else{
        $(document.querySelector("#data-vigencia")).hide();
      }
  
      if (validacaoAreaRequisitante) {
        $(document.querySelector("#informacoes-administrativas")).hide();
      }else{
        $(document.querySelector("#informacoes-administrativas")).show();
      }
      
    }else if (document.querySelector("[xname='inptipoDeSolicitacao']").value == "Elaboração de Contrato") {
      $(document.querySelector("#tipo-pessoa")).show();
      $(document.querySelector("#informacoes-pessoais")).show();
      $(document.querySelector("#informacao-tipo-contrato")).hide();
      $(document.querySelector("#informacoes-categorizacao-contrato")).show();
      $(document.querySelector("#informacoes-elaboracao")).show();
      if((document.querySelector("[xname='inpvigencia']").value == "Determinado") || (document.querySelector("[xname='inpvigencia']").value == "Evento")){
        $(document.querySelector("#data-vigencia")).show();
      }else{
        $(document.querySelector("#data-vigencia")).hide();
      }
  
      if (validacaoAreaRequisitante) {
        $(document.querySelector("#informacoes-administrativas")).hide();
      }else{
        $(document.querySelector("#informacoes-administrativas")).show();
      }
          
    }else if (document.querySelector("[xname='inptipoDeSolicitacao']").value == "Modelo de Contrato") {
      $(document.querySelector("#tipo-pessoa")).hide();
      $(document.querySelector("#informacoes-pessoais")).hide();
      $(document.querySelector("#informacao-tipo-contrato")).hide();
      $(document.querySelector("#informacoes-categorizacao-contrato")).show();
      $(document.querySelector("#informacoes-elaboracao")).hide();
      $(document.querySelector("#vigencia")).hide();
      $(document.querySelector("#informacoes-elaboracao")).hide();
      $(document.querySelector("#informacoes-administrativas")).hide();
      
    }
  }
  
  function verificarAreaRequisitante(areaRequisitante){
    if ((areaRequisitante == "0507 - Administrativo Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Comercial Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Life Insurance Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Protocolista Endossos Auto Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Sinistro Unicred Corretora de Seguros Santa Catarina / Paraná") || (areaRequisitante == "0507 - Unicred Corretora de Seguros Santa Catarina / Paraná")) {
      return true;
    }else{
      return false;
    }
  }