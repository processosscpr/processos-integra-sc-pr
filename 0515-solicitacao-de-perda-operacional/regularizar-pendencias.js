$(document).ready(function() {
  $(document.querySelector('#divStatisticsTable')).hide();
  $(document.getElementById('formCamposAuxiliares')).hide();

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
  } else {
    adaptarMobile();
  }
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();
  document
    .querySelector("[xname='inpagencia']")
    .setAttribute('style', 'width:80%!important');
  document
    .querySelector("[xname='inpCCNomeCompleto']")
    .setAttribute('style', 'height: 20px!important; width: 85%!important');
  document
    .querySelector("[xname='inpperdaDespesaAgencia']")
    .setAttribute('style', 'height: 20px!important; width: 85%!important');
  document
    .querySelector("[xname='inpnomeCooperado']")
    .setAttribute('style', 'height: 20px!important; width: 85%!important');
  document
    .querySelector("[xname='inpcontaCorrente']")
    .setAttribute('style', 'height: 20px!important; width: 85%!important');
  document
    .querySelector("[xname='inpcpf']")
    .setAttribute('style', 'height: 20px!important; width: 50%!important');
  document
    .querySelector("[xname='inpdataFato']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataLancamentoCC']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpvalorTotalPerda']")
    .setAttribute('style', 'height: 20px!important;width:45%!important');
  document
    .querySelector("[xname='inpdescricaoFato']")
    .setAttribute('style', 'height: 40px!important;width:85%!important');
  document
    .querySelector("[xname='inpacoesRealizadasSugestoes']")
    .setAttribute('style', 'height: 40px!important; width: 85%!important');
  document
    .querySelector("[xname='inpcolaboradoresEnvolvidos']")
    .setAttribute('style', 'height: 20px!important; width: 93%!important');
}
