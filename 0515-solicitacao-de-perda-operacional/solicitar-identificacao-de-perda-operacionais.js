$(document).ready(function() {
  verificarPosicaoIniciadora();

  $(document.querySelector('#btnCancel')).hide();
  $(document.querySelector('#divStatisticsTable')).hide();
  $(document.getElementById('formCamposAuxiliares')).hide();

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
  } else {
    adaptarMobile();
  }
});

function verificarPosicaoIniciadora() {
  if ($('select[name="inpCodPositionArea"]').val() != undefined) {
    var arrayAuxiliar = new Array();
    for (
      var i = 0;
      i < document.querySelector("[name='inpCodPositionArea']").options.length;
      i++
    ) {
      arrayAuxiliar.push(
        document.querySelector("[name='inpCodPositionArea']").options[i]
          .innerText
      );
    }
    arrayAuxiliar.shift();

    for (var i = arrayAuxiliar.length - 1; i >= 0; i--) {
      if (arrayAuxiliar[i].substring(4, 5) == '_') {
        arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]), 1);
      }
    }
    $('select[name="inpCodPositionArea"]').val(
      $("option:contains('" + arrayAuxiliar[0] + "')").val()
    );
    $(
      $('select[name="inpCodPositionArea"]')
        .parent()
        .parent()
    ).hide();
  }
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();
  document
    .querySelector("[xname='inpagencia']")
    .setAttribute('style', 'width:80%!important');
  document
    .querySelector("[xname='inpCCNomeCompleto']")
    .setAttribute('style', 'height: 20px!important; width: 85%!important');
  document
    .querySelector("[xname='inpperdaDespesaAgencia']")
    .setAttribute('style', 'height: 20px!important; width: 85%!important');
  document
    .querySelector("[xname='inpnomeCooperado']")
    .setAttribute('style', 'height: 20px!important; width: 85%!important');
  document
    .querySelector("[xname='inpcontaCorrente']")
    .setAttribute('style', 'height: 20px!important; width: 85%!important');
  document
    .querySelector("[xname='inpcpf']")
    .setAttribute('style', 'height: 20px!important; width: 50%!important');
  document
    .querySelector("[xname='inpdataFato']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataLancamentoCC']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpvalorTotalPerda']")
    .setAttribute('style', 'height: 20px!important;width:45%!important');
  document
    .querySelector("[xname='inpdescricaoFato']")
    .setAttribute('style', 'height: 40px!important;width:85%!important');
  document
    .querySelector("[xname='inpacoesRealizadasSugestoes']")
    .setAttribute('style', 'height: 40px!important; width: 85%!important');
  document
    .querySelector("[xname='inpcolaboradoresEnvolvidos']")
    .setAttribute('style', 'height: 20px!important; width: 93%!important');
}
