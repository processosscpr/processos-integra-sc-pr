$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();
});

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // configurações default do formulário
  ajustarCampoChecklistQuandoSomenteVisivel();

  // esconder elementos do formulário
  $('#coluna-documento-fo25').hide();
  $('#coluna-documento-credito-provisorio').hide();

  $('inp:tipoDePessoa').val() === 'Pessoa Física'
    ? $('#informacoes-cooperado-thirdRow').show()
    : $('#informacoes-cooperado-thirdRow').hide();

  $('inp:necessitaCreditoProvisorio').val() === 'Sim'
    ? $('#coluna-necessita-antecipar-credito-provisorio').show()
    : $('#coluna-necessita-antecipar-credito-provisorio').hide();

  $('inp:deferimento').val() === 'Deferido'
    ? ($('#customBtn_Solicitação\\ Deferida').show(),
      $('#customBtn_Solicitação\\ Indeferida').hide())
    : ($('#customBtn_Solicitação\\ Deferida').hide(),
      $('#customBtn_Solicitação\\ Indeferida').show());
};
