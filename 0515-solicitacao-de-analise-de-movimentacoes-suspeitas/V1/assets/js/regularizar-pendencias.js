$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();

  $('#customBtn_Novas\\ Informações\\ Encaminhadas').click(() => {
    validarInformacoesFormularioParaSubmeterSolicitacao();
  });
});

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // configurações default do formulário
  $('#customBtn_Novas\\ Informações\\ Encaminhadas').removeAttr('onclick');
  $('.form-group [XTYPE="FILE"]').css('display', 'block');
  $('#ContainerAttach h2').append(
    `<span><p> Documentos listados abaixo não são obrigatórios.</p></span>`
  );
  ajustarCampoChecklistQuandoSomenteVisivel();

  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();

  // esconder elementos do formulário
  $('#coluna-documento-fo25').hide();
  $('#section-informacoes-analise').hide();
  $('#section-informacoes-deferimento-reuniao').hide();

  $('inp:tipoDePessoa').val() === 'Pessoa Física'
    ? $('#informacoes-cooperado-thirdRow').show()
    : $('#informacoes-cooperado-thirdRow').hide();

  $('inp:necessitaCreditoProvisorio').val() === 'Sim'
    ? ($('#coluna-necessita-antecipar-credito-provisorio').show(),
      $('#coluna-documento-credito-provisorio').show(),
      captureModule.capture.addDoc('Documento Crédito Provisório Assinado'))
    : ($('#coluna-necessita-antecipar-credito-provisorio').hide(),
      $('#coluna-documento-credito-provisorio').hide());

  captureModule.capture.addDoc('Boletim de Ocorrência');
  captureModule.capture.addDoc('Parecer de Segurança da Informação - UBR');

  verificarNecessidadeFeedbackAnexoBoletimOcorrencia();
};

const verificarNecessidadeFeedbackAnexoBoletimOcorrencia = async () => {
  let documentosJaImportados = [];
  await _.forEach($('#tblFile td.docType'), tipoDocumentoImportado =>
    documentosJaImportados.push($(tipoDocumentoImportado).text())
  );

  if (documentosJaImportados.indexOf('Boletim de Ocorrência') == -1) {
    $('#customizedUpload').append(
      `<div id="invalid-feedback-anexo" style="width: 100%; margin-left: 5px; font-size: 80%; color: #138496;"><i class="fas fa-exclamation-triangle"></i> Boletim de Ocorrência não é obrigatório na solicitação. Porém, caso não anexado, após análise da UBR devolveremos a instância para que possas disponibilizar esse documento!</div>`
    );
    $('#invalid-feedback-anexo').show();
  }
};

const validarInformacoesFormularioParaSubmeterSolicitacao = () => {
  let docBoletimPendentes = $('#customizedUpload span.badge-secondary')
    .text()
    .match(/Boletim/gi);

  !docBoletimPendentes || !$('#invalid-feedback-anexo').length
    ? $('inp:boletimDeOcorrenciaAnexado').val('Sim')
    : $('inp:boletimDeOcorrenciaAnexado').val('Não');

  doAction('Novas Informações Encaminhadas', false, false);
};
