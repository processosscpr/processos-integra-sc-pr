$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();

  $(
    '#customBtn_Solicitação\\ Encaminhada\\ para\\ Aguardar\\ Reunião\\ Fundo'
  ).click(() => {
    validarInformacoesFormularioParaSubmeterAtividade();
  });
});

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // configurações default do formulário
  $(
    '#customBtn_Solicitação\\ Encaminhada\\ para\\ Aguardar\\ Reunião\\ Fundo'
  ).removeAttr('onclick');
  ajustarCampoChecklistQuandoSomenteVisivel();

  // esconder elementos do formulário
  $('#coluna-documento-credito-provisorio').hide();
  $('#coluna-documento-fo25').hide();
  $('#section-informacoes-deferimento-reuniao').hide();

  $('inp:tipoDePessoa').val() === 'Pessoa Física'
    ? $('#informacoes-cooperado-thirdRow').show()
    : $('#informacoes-cooperado-thirdRow').hide();

  $('inp:necessitaCreditoProvisorio').val() === 'Sim'
    ? $('#coluna-necessita-antecipar-credito-provisorio').show()
    : $('#coluna-necessita-antecipar-credito-provisorio').hide();
};

const validarInformacoesFormularioParaSubmeterAtividade = async () => {
  let documentosImportados = '';

  await _.forEach(
    $('#tblNewFile tbody tr'),
    linhaTabelaNewDocumentosImportados => {
      let nomeArquivoOriginal = $(linhaTabelaNewDocumentosImportados)
        .find('td.filename a')
        .text();
      let nomeArquivo = nomeArquivoOriginal.substr(
        0,
        nomeArquivoOriginal.lastIndexOf('_')
      );

      let nomeArquivoEstruturado = estruturarNomeArquivo(nomeArquivo);
      if (documentosImportados.indexOf(nomeArquivoEstruturado) == -1) {
        documentosImportados =
          documentosImportados + nomeArquivoEstruturado + '. ';
      }
    }
  );

  await _.forEach($('#tblFile tbody tr'), linhaTabelaDocumentosImportados => {
    let nomeArquivo = $(linhaTabelaDocumentosImportados)
      .find('td.filename a')
      .text();
    let tipoDocumento = $(linhaTabelaDocumentosImportados)
      .find('td.docType')
      .text();
    if (tipoDocumento === '') {
      let nomeArquivoEstruturado = estruturarNomeArquivo(nomeArquivo);
      if (documentosImportados.indexOf(nomeArquivoEstruturado) == -1) {
        documentosImportados =
          documentosImportados + nomeArquivoEstruturado + '. ';
      }
    } else {
      if (documentosImportados.indexOf(tipoDocumento) == -1) {
        documentosImportados = documentosImportados + tipoDocumento + '. ';
      }
    }
  });

  $('inp:documentosImportados').val(documentosImportados);

  let participantesDoProcesso =
    'Solicitante: ' +
    $('inp:nomeRequisitante').val() +
    ' Responsável Análise: ' +
    $('inp:responsavelAnalise').val();
  $('inp:participantesDoProcesso').val(participantesDoProcesso);

  doAction('Solicitação Encaminhada para Aguardar Reunião Fundo', false, false);
};

const estruturarNomeArquivo = arquivo => {
  let arquivoEstruturado = arquivo;
  for (let index = 0; index < arquivo.split('_').length; index++) {
    arquivoEstruturado = arquivoEstruturado.replace('_', ' ');
  }
  return arquivoEstruturado;
};
