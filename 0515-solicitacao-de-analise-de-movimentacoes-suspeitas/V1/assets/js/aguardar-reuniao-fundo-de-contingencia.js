$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();

  $('#customBtn_Parecer\\ da\\ Reunião\\ Encaminhado').click(() => {
    validarInformacoesFormularioParaSubmeterAtividade();
  });
});

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // configurações default do formulário
  $('#customBtn_Parecer\\ da\\ Reunião\\ Encaminhado').removeAttr('onclick');
  $('.form-group [XTYPE="FILE"]').css('display', 'block');
  ajustarCampoChecklistQuandoSomenteVisivel();

  // esconder elementos do formulário
  $('#coluna-documento-credito-provisorio').hide();

  $('inp:tipoDePessoa').val() === 'Pessoa Física'
    ? $('#informacoes-cooperado-thirdRow').show()
    : $('#informacoes-cooperado-thirdRow').hide();

  $('inp:necessitaCreditoProvisorio').val() === 'Sim'
    ? $('#coluna-necessita-antecipar-credito-provisorio').show()
    : $('#coluna-necessita-antecipar-credito-provisorio').hide();
};

const validarInformacoesFormularioParaSubmeterAtividade = async () => {
  await validarPreenchimentoFormulario();

  if ($('.is-invalid').length > 0) {
    toastr.error(
      'Há campos obrigatórios que não foram preenchidos. Favor verificar!'
    );
    return;
  }

  doAction('Parecer da Reunião Encaminhado', false, false);
};

const validarPreenchimentoFormulario = () => {
  let inputsFormulario, selectsFormulario;

  inputsFormulario = $(
    '.form-integra input[required="S"]:not([type="checkbox"]):not([readOnly="readOnly"])'
  );
  selectsFormulario = $(
    '.form-integra select[required="S"]:not([readOnly="readOnly"])'
  );

  _.forEach(inputsFormulario, input =>
    $(input).val() === ''
      ? $(input)
          .removeClass('is-valid')
          .addClass('is-invalid')
      : $(input)
          .removeClass('is-invalid')
          .addClass('is-valid')
  );

  _.forEach(selectsFormulario, select =>
    $(select).val() === ''
      ? $(select)
          .removeClass('is-valid')
          .addClass('is-invalid')
      : $(select)
          .removeClass('is-invalid')
          .addClass('is-valid')
  );
};
