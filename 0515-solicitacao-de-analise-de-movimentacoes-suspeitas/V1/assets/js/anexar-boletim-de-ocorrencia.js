$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();

  $('#customBtn_Boletim\\ de\\ Ocorrência\\ Anexado').click(() => {
    validarInformacoesFormularioParaSubmeterAtividade();
  });
});

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');
  $('#aActions').hide();

  // configurações default do formulário
  $('#customBtn_Boletim\\ de\\ Ocorrência\\ Anexado').removeAttr('onclick');
  ajustarCampoChecklistQuandoSomenteVisivel();

  // esconder elementos do formulário
  $('#coluna-documento-credito-provisorio').hide();
  $('#coluna-documento-fo25').hide();
  $('#section-informacoes-deferimento-reuniao').hide();

  $('inp:tipoDePessoa').val() === 'Pessoa Física'
    ? $('#informacoes-cooperado-thirdRow').show()
    : $('#informacoes-cooperado-thirdRow').hide();

  $('inp:necessitaCreditoProvisorio').val() === 'Sim'
    ? $('#coluna-necessita-antecipar-credito-provisorio').show()
    : $('#coluna-necessita-antecipar-credito-provisorio').hide();

  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();

  captureModule.capture.addDoc('Boletim de Ocorrência');
};

const validarInformacoesFormularioParaSubmeterAtividade = () => {
  if ($('span.badge.badge-secondary').length > 0) {
    toastr.error('Há documento obrigatório a ser anexado. Favor verificar!');
    return;
  } else {
    doAction('Boletim de Ocorrência Anexado', false, false);
  }
};
