$(document).ready(function() {});

/*--------------------------------------------------------*/
const url_token =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJg-CqsL3NypcCvWuQu01@2-7ejKkCN5V0IBK52p0D6LEWgV3g7RcWg6-TyDXTbVyzg__';
const url_conta = numeroConta =>
  `https://servicos.e-unicred.com.br/conta-corrente-us/conta-corrente/conta-corrente/v1/contas-correntes/${numeroConta}`;
const url_cadastro_pf = cpf =>
  `https://servicos.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-fisica/v1/pessoa-fisica/${cpf}`;
const url_cadastro_pj = cnpj =>
  `https://servicos.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-juridica/v1/pessoa-juridica/${cnpj}`;

//Fontes INTEGRA
//Fonte de dados: 0507 - Movimentações Suspeitas - Usuários 0515
const url_usuarios =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJsBk@nLc4IFenA60i-0pO@-k3Phfgh41ryk6vUDvXpAQpausKXZVVjEu@oVeG0eOIg__';
/*--------------------------------------------------------*/

//Função dinâmica utilizada em todas as atividades
const adicionarRequisitanteBadge = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile =
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1;

  if (!isMobile) {
    $(
      `
        <span style='margin-left: 0.4em' class='label label-secondary'>
          <strong> Requisitante: ${$('inp:nomeRequisitante').val()}</strong>
        </span>
      `
    ).insertAfter('.title span.label.label-info.flow-title');
  }
};

const ajustarBotoesTabelaMultivalorada = () => {
  $('#BtnInsertNewRow')
    .addClass('btn-primary btn-sm')
    .text('')
    .append(`<i class="fas fa-plus"></i>`);
  $('table.table.table-striped tbody button.btn-delete-mv')
    .addClass('btn-danger btn-sm')
    .text('')
    .append(`<i class="fas fa-trash-alt"></i>`);
};

const buscarToken = async () => {
  try {
    const { data } = await axios.get(url_token);
    if (data.success[0].fields.access_token) {
      return `Bearer ${data.success[0].fields.access_token}`;
    } else {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
    }
  } catch (error) {
    toggleSpinner();
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    console.log(error);
  }
};

const verificarTipoDePessoa = cpfCnpj => {
  if (cpfCnpj.length <= 11) {
    return 'Pessoa Física';
  } else {
    return 'Pessoa Jurídica';
  }
};

const formatarData = data => {
  const dataSplit = data.split('-');
  return dataSplit[2] + '/' + dataSplit[1] + '/' + dataSplit[0];
};

const ajustarCampoChecklistQuandoSomenteVisivel = () => {
  let valoresCheck = $('div[xid="divusuario"]')
    .text()
    .split(', ');
  $('#coluna-checkbox-usuario input').remove();

  _.forEach(valoresCheck, valor => {
    $('#coluna-checkbox-usuario div.form-group').append(
      `<input type="hidden" name="inp27148" xname="inpusuario" value="${valor}"></input>`
    );
  });
};

/*--------------------------------------------------------*/
//COMPONENTES
const inputSwitch = (idColuna, idInput, text) => `
    <div id="${idColuna}" class="col-xs-12 col-md-2">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="${idInput}">
            <label class="custom-control-label font-weight-bold pt-1" for="${idInput}">${text}</label>
        </div>
    </div>
`;

const checkboxUsuarios = (text, count) => `
  <label for="inpusuario-${count}" class="checkbox" style="display: block;">
    <input type="checkbox" name="inp27148" xname="inpusuario" id="inpusuario-${count}" label="Usuário" required="S" value="${text}" onchange="controlValueChange(this);">
    ${text}
  </label>
`;
