$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();

  $('#customBtn_Solicitação\\ Encaminhada').click(() => {
    validarInformacoesFormularioParaSubmeterSolicitacao();
  });
});

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');
  $('#aActions').hide();

  // configurações default do formulário
  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');
  $('.form-group [XTYPE="FILE"]').css('display', 'block');
  ajustarCampoChecklistQuandoSomenteVisivel();

  // esconder elementos do formulário
  $('#coluna-documento-fo25').hide();
  $('#section-informacoes-analise').hide();
  $('#section-informacoes-deferimento-reuniao').hide();

  $('inp:tipoDePessoa').val() === 'Pessoa Física'
    ? $('#informacoes-cooperado-thirdRow').show()
    : $('#informacoes-cooperado-thirdRow').hide();

  $('inp:necessitaCreditoProvisorio').val() === 'Sim'
    ? $('#coluna-necessita-antecipar-credito-provisorio').show()
    : $('#coluna-necessita-antecipar-credito-provisorio').hide();

  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();

  captureModule.capture.addDoc('Documento Crédito Provisório Assinado');
};

const validarInformacoesFormularioParaSubmeterSolicitacao = () => {
  if ($('span.badge.badge-secondary').length > 0) {
    toastr.error('Há documento obrigatório a ser anexado. Favor verificar!');
    return;
  } else {
    doAction('Solicitação Encaminhada', false, false);
  }
};
