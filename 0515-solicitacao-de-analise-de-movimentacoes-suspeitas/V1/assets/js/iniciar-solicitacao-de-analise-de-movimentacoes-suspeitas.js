$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();
  toggleSpinner();
  desativarInputsPopuladosPorAPI();
  ajustarBotoesTabelaMultivalorada();
  verificarPosicaoIniciadora();

  $('inp:conta').change(() => {
    limparFormulario();
  });

  $('#procurar-informacoes-conta').click(async () => {
    await limparFormulario();
    validarSubmissaoPesquisaDeConta();
  });

  $(document).on('change', '#switch-credito-provisorio', elemento => {
    verificarNecessidadePreenchimentoSwitchAnteciparCreditoProvisorio(
      elemento.target.checked
    );
  });

  $(document).on('change', '#switch-antecipar-credito-provisorio', elemento => {
    popularCampoTextoSuporteDeSwitch(elemento.target.checked);
  });

  $('#customBtn_Solicitação\\ Encaminhada').click(() => {
    validarInformacoesFormularioParaSubmeterSolicitacao(
      'Encaminhar Solicitação'
    );
  });

  $('#customBtn_Template\\ Gerado').click(() => {
    validarInformacoesFormularioParaSubmeterSolicitacao('Gerar Template');
  });
});

/*--------------------------------------------------------*/
let informacoesTitular = [];
/*--------------------------------------------------------*/

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#ContainerAttach').hide();
  $('#divStatisticsTable').hide();
  $('#customBtn_Solicitação\\ Encaminhada').hide();
  $('#customBtn_Template\\ Gerado').hide();
  $('#btnCancel').hide();

  // configurações default do formulário
  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');
  $('#customBtn_Template\\ Gerado').removeAttr('onclick');
  $('inp:demandaServicenow')[0].setAttribute('required', 'N');

  // esconder elementos do formulário
  $('#coluna-documento-credito-provisorio').hide();
  $('#coluna-documento-fo25').hide();
  $('#section-informacoes-cooperado').hide();
  $('#section-informacoes-ocorrido').hide();
  $('#section-informacoes-analise').hide();
  $('#section-informacoes-deferimento-reuniao').hide();

  // mostrar elementos do formulário
  $('#procurar-informacoes-conta').show();
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const desativarInputsPopuladosPorAPI = () => {
  let inputInfosConta = $('#section-informacoes-cooperado input');
  inputInfosConta.attr('readOnly', true);
};

const limparFormulario = () => {
  let inputInfosConta = $('#section-informacoes-cooperado input');
  inputInfosConta.val('');

  let selectInfosConta = $('#section-informacoes-cooperado select');
  selectInfosConta.val('');
  selectInfosConta.attr('readOnly', false);

  _.forEach($('#section-informacoes-cooperado select option'), option =>
    !option.value ? null : option.remove()
  );

  $('inp:demandaServicenow').val('');
  $('inp:dataDaOcorrencia').val('');
  $('inp:canalDigital').val('');
  $('#coluna-checkbox-usuario label.checkbox').remove();

  $('inp:dataDaTransacao').val('');
  $('inp:tipoDaTransacao').val('');
  $('inp:valorDaTransacao').val('');

  _.forEach($('div.table.table-striped tbody'), (tr, index) =>
    index === 0
      ? null
      : $(tr)
          .find('button.btn-delete-mv')
          .click()
  );

  $('inp:parecer').val('');
  $('inp:necessitaCreditoProvisorio').val('Não');
  $('inp:necessitaAntesDaAnalise').val('');

  $('#coluna-switch-credito-provisorio').remove();
  $('#coluna-switch-antecipar-credito-provisorio').remove();

  ajustarFormulario();
};

const validarSubmissaoPesquisaDeConta = () => {
  toggleSpinner();
  let numeroConta = $('inp:conta').val();

  if (!numeroConta) {
    toggleSpinner();
    toastr.error('Preencha um dado para que possamos iniciar a busca!');
    return;
  }

  buscarInformacoesDaConta(numeroConta);
};

const buscarInformacoesDaConta = async numeroConta => {
  const token = await buscarToken();
  const cooperativa = '0515'; //$('inp:cooperativaRequisitante').val();

  if (!token || !cooperativa) {
    toggleSpinner();
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );
    return;
  }

  await axios
    .get(url_conta(numeroConta), {
      headers: {
        Authorization: token,
        cooperativa: cooperativa,
      },
    })
    .then(response => {
      if (response.data.situacao !== 'ENCERRADA') {
        buscarInformacoesDoTitular(
          token,
          cooperativa,
          response.data.cpfCnpjPrimeiroTitular
        );
      } else {
        toggleSpinner();
        toastr.error(
          'Você pesquisou uma conta encerrada. Favor pesquisar uma conta com situação válida!'
        );
        return;
      }
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

const buscarInformacoesDoTitular = async (token, cooperativa, cpfCnpj) => {
  const tipoPessoa = await verificarTipoDePessoa(cpfCnpj);
  let urlDesseContexto;

  $('inp:tipoDePessoa').val(tipoPessoa);

  tipoPessoa === 'Pessoa Física'
    ? (urlDesseContexto = url_cadastro_pf(cpfCnpj))
    : (urlDesseContexto = url_cadastro_pj(cpfCnpj));

  await axios
    .get(urlDesseContexto, {
      headers: {
        Authorization: token,
        cooperativa: cooperativa,
      },
    })
    .then(response => {
      informacoesTitular.push(response.data);
      preencherDadosDoTitularNosInputs(tipoPessoa);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

const preencherDadosDoTitularNosInputs = tipoPessoa => {
  switch (tipoPessoa) {
    case 'Pessoa Física':
      $('inp:nomeRazaoSocial').val(informacoesTitular[0].nome);
      $('inp:cpfCnpj').val(informacoesTitular[0].cpf);
      $('inp:profissaoRamo').val(
        informacoesTitular[0].dadosProfissionais.profissao
      );
      $('inp:dataNascimento').val(
        formatarData(informacoesTitular[0].dataNascimento)
      );
      $('inp:estadoCivil').val(informacoesTitular[0].estadoCivil);

      break;
    case 'Pessoa Jurídica':
      $('inp:nomeRazaoSocial').val(informacoesTitular[0].nome);
      $('inp:cpfCnpj').val(informacoesTitular[0].cnpj);
      $('inp:profissaoRamo').val(informacoesTitular[0].cnae.finalidade);

      break;
  }
  verificarQuantidadeDeTelefones();
};

const verificarQuantidadeDeTelefones = () => {
  const quantidadeTelefones = informacoesTitular[0].contatos.telefones.length;

  if (quantidadeTelefones === 0) {
    $('inp:telefone').attr('readOnly', true);
  } else if (quantidadeTelefones === 1) {
    let numeroTelefone =
      informacoesTitular[0].contatos.telefones[0].ddd +
      informacoesTitular[0].contatos.telefones[0].numero;
    let numeroTelefoneFormatado =
      '(' +
      informacoesTitular[0].contatos.telefones[0].ddd +
      ') ' +
      informacoesTitular[0].contatos.telefones[0].numero;

    $('inp:telefone').append(
      $('<option>', {
        value: numeroTelefone,
        text: numeroTelefoneFormatado,
      })
    );

    $('inp:telefone').val(numeroTelefone);
    $('inp:telefone').attr('readOnly', true);
  } else {
    _.forEach(informacoesTitular[0].contatos.telefones, telefone => {
      let numeroTelefone = telefone.ddd + telefone.numero;
      let numeroTelefoneFormatado = '(' + telefone.ddd + ') ' + telefone.numero;
      $('inp:telefone').append(
        $('<option>', {
          value: numeroTelefone,
          text: numeroTelefoneFormatado,
        })
      );
    });
    $('inp:telefone')[0].setAttribute('required', 'S');
  }
  verificarQuantidadeDeEmails();
};

const verificarQuantidadeDeEmails = () => {
  const quantidadeEmails = informacoesTitular[0].contatos.emails.length;

  if (quantidadeEmails === 0) {
    $('inp:email').attr('readOnly', true);
  } else if (quantidadeEmails === 1) {
    let enderecoEmail = informacoesTitular[0].contatos.emails[0].endereco;
    $('inp:email').append(
      $('<option>', {
        value: enderecoEmail,
        text: enderecoEmail,
      })
    );

    $('inp:email').val(enderecoEmail);
    $('inp:email').attr('readOnly', true);
  } else {
    _.forEach(informacoesTitular[0].contatos.emails, email => {
      let enderecoEmail = email.endereco;

      $('inp:email').append(
        $('<option>', {
          value: enderecoEmail,
          text: enderecoEmail,
        })
      );
    });
    $('inp:email')[0].setAttribute('required', 'S');
  }
  verificarQuantidadeDeEnderecos();
};

const verificarQuantidadeDeEnderecos = () => {
  const quantidadeEnderecos = informacoesTitular[0].enderecos.length;

  if (quantidadeEnderecos === 0) {
    $('inp:endereco').attr('readOnly', true);
  } else if (quantidadeEnderecos === 1) {
    let enderecoPostal =
      informacoesTitular[0].enderecos[0].logradouro +
      ', ' +
      informacoesTitular[0].enderecos[0].numero +
      ', ' +
      informacoesTitular[0].enderecos[0].bairro +
      ', ' +
      informacoesTitular[0].enderecos[0].nomeCidade +
      '/' +
      informacoesTitular[0].enderecos[0].uf;
    $('inp:endereco').append(
      $('<option>', {
        value: enderecoPostal,
        text: enderecoPostal,
      })
    );

    $('inp:endereco').val(enderecoPostal);
    $('inp:endereco').attr('readOnly', true);
  } else {
    _.forEach(informacoesTitular[0].enderecos, endereco => {
      let enderecoPostal =
        endereco.logradouro +
        ', ' +
        endereco.numero +
        ', ' +
        endereco.bairro +
        ', ' +
        endereco.nomeCidade +
        '/' +
        endereco.uf;

      $('inp:endereco').append(
        $('<option>', {
          value: enderecoPostal,
          text: enderecoPostal,
        })
      );
    });
    $('inp:endereco')[0].setAttribute('required', 'S');
  }
  buscarUsuariosDaConta();
};

const buscarUsuariosDaConta = async () => {
  let numeroConta = $('inp:conta').val();
  $('#coluna-checkbox-usuario div input').remove();

  await axios
    .post(url_usuarios, {
      inpconta: numeroConta,
    })
    .then(async response => {
      if (response.data && response.data.success && response.data.success[0]) {
        await _.forEach(response.data.success, (result, index) => {
          $('#coluna-checkbox-usuario div').append(
            $(checkboxUsuarios(result.txt, index))
          );
        });
        apresentarInformacoesGeradasAoSolicitante();
      } else {
        //
      }
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

const apresentarInformacoesGeradasAoSolicitante = async () => {
  const tipoPessoa = $('inp:tipoDePessoa').val();

  $('#section-informacoes-cooperado').show();
  $('#section-informacoes-cooperado div.row').show();

  switch (tipoPessoa) {
    case 'Pessoa Física':
      $('#informacoes-cooperado-thirdRow').show();
      break;
    case 'Pessoa Jurídica':
      $('#informacoes-cooperado-thirdRow').hide();
      break;
  }

  $('#informacoes-ocorrido-forthRow').append(
    $(
      inputSwitch(
        'coluna-switch-credito-provisorio',
        'switch-credito-provisorio',
        'Necessita Crédito Provisório'
      )
    )
  );

  $('#informacoes-ocorrido-forthRow').append(
    $(
      inputSwitch(
        'coluna-switch-antecipar-credito-provisorio',
        'switch-antecipar-credito-provisorio',
        'Necessita Antecipar Crédito Provisório'
      )
    )
  );

  $('#section-informacoes-ocorrido').show();
  $('#coluna-credito-provisorio').hide();
  $('#coluna-switch-antecipar-credito-provisorio').hide();
  $('#coluna-necessita-antecipar-credito-provisorio').hide();

  let estruturaDocumentosJaMontada = $('table#customizedUpload span.badge');

  if (estruturaDocumentosJaMontada.length === 0) {
    captureModule.capture.buildDocsHtml();
    captureModule.capture.appendModule();
    captureModule.capture.uploadChange();

    captureModule.capture.addDoc('Boletim de Ocorrência');
    captureModule.capture.addDoc('Parecer de Segurança da Informação - UBR');

    $('#customizedUpload').append(
      `<div id="invalid-feedback-anexo" style="width: 100%; margin-left: 5px; font-size: 80%; color: #138496;"><i class="fas fa-exclamation-triangle"></i> Boletim de Ocorrência não é obrigatório na solicitação. Porém, caso não anexado, após análise da UBR devolveremos a instância para que possas disponibilizar esse documento!</div>`
    );
    $('#invalid-feedback-anexo').show();

    $('#ContainerAttach h2').append(
      `<span><p> Documentos listados abaixo não são obrigatórios.</p></span>`
    );
  }

  $('#annex').hide();
  $('#ContainerAttach').show();

  $('#customBtn_Solicitação\\ Encaminhada').show();

  toggleSpinner();
  toastr.success(`Informações da conta foram geradas com sucesso!`);
};

const verificarDataTransacaoComDataOcorrencia = elemento => {
  if ($('inp:dataDaOcorrencia').val() === '') {
    toastr.error('Preencha, primeiro, uma data de ocorrência!');
    $(elemento).val('');
    $('inp:dataDaOcorrencia').focus();
    return;
  }

  let dataOcorrencia = moment($('inp:dataDaOcorrencia').val(), 'DD/MM/AAAA');
  let dataTransacao = moment($(elemento).val(), 'DD/MM/AAAA');

  dataTransacao < dataOcorrencia
    ? ($(elemento).val(''),
      toastr.error(
        'Data de transação deve ser maior ou igual a data da ocorrência. Favor ajustar!'
      ))
    : null;
};

const verificarNecessidadePreenchimentoSwitchAnteciparCreditoProvisorio = check => {
  switch (check) {
    case true:
      $('inp:necessitaCreditoProvisorio').val('Sim');
      $('#coluna-switch-antecipar-credito-provisorio').show();
      $('inp:necessitaAnteciparCreditoProvisorio').val('Não');
      toastr.info('Deverás gerar template e resgatar assinatura do cooperado!');

      $('#customBtn_Solicitação\\ Encaminhada').hide();
      $('#customBtn_Template\\ Gerado').show();
      break;

    case false:
      $('inp:necessitaCreditoProvisorio').val('Não');
      $('#coluna-switch-antecipar-credito-provisorio').hide();
      $('#switch-antecipar-credito-provisorio').attr('checked', false);
      $('inp:necessitaAnteciparCreditoProvisorio').val('');

      $('#customBtn_Solicitação\\ Encaminhada').show();
      $('#customBtn_Template\\ Gerado').hide();
      break;
  }
};

const popularCampoTextoSuporteDeSwitch = check => {
  switch (check) {
    case true:
      $('inp:necessitaAnteciparCreditoProvisorio').val('Sim');
      toastr.info('Solicitação passará, então, pela validação da diretoria!');
      break;

    case false:
      $('inp:necessitaAnteciparCreditoProvisorio').val('Não');
      break;
  }
};

const validarInformacoesFormularioParaSubmeterSolicitacao = async action => {
  await validarPreenchimentoFormulario();

  if ($('.is-invalid').length > 0) {
    toastr.error(
      'Há campos obrigatórios que não foram preenchidos. Favor verificar!'
    );
    return;
  }

  let docBoletimPendentes = $('#customizedUpload span.badge-secondary')
    .text()
    .match(/Boletim/gi);
  !docBoletimPendentes
    ? $('inp:boletimDeOcorrenciaAnexado').val('Sim')
    : $('inp:boletimDeOcorrenciaAnexado').val('Não');

  const dataAtual = moment(new Date()).format('DD/MM/YYYY');
  $('inp:dataAtual').val(dataAtual);

  $('#coluna-switch-credito-provisorio').remove();
  $('#coluna-switch-antecipar-credito-provisorio').remove();

  action === 'Encaminhar Solicitação'
    ? doAction('Solicitação Encaminhada', false, false)
    : doAction('Template Gerado', false, false);

  $('.cryo-confirm-dialog .btn-success').click();
};

const validarPreenchimentoFormulario = () => {
  let inputsFormulario, selectsFormulario, checkFormulario;

  inputsFormulario = $(
    '.form-integra input[required="S"]:not([type="checkbox"]):not([readOnly="readOnly"])'
  );
  selectsFormulario = $(
    '.form-integra select[required="S"]:not([readOnly="readOnly"])'
  );
  checkFormulario = $(
    '.form-integra input[type="checkbox"]:checked:not(.custom-control-input)'
  );

  _.forEach(inputsFormulario, input =>
    $(input).val() === ''
      ? $(input)
          .removeClass('is-valid')
          .addClass('is-invalid')
      : $(input)
          .removeClass('is-invalid')
          .addClass('is-valid')
  );

  _.forEach(selectsFormulario, select =>
    $(select).val() === ''
      ? $(select)
          .removeClass('is-valid')
          .addClass('is-invalid')
      : $(select)
          .removeClass('is-invalid')
          .addClass('is-valid')
  );

  !checkFormulario.length
    ? $('inp:usuario')
        .parent()
        .removeClass('is-valid')
        .addClass('is-invalid')
    : $('inp:usuario')
        .parent()
        .removeClass('is-invalid')
        .addClass('is-valid');

  $('inp:parecer').val() === ''
    ? $('inp:parecer')
        .removeClass('is-valid')
        .addClass('is-invalid')
    : $('inp:parecer')
        .removeClass('is-invalid')
        .addClass('is-valid');
};

const verificarPosicaoIniciadora = () => {
  if ($('select[name="inpCodPositionArea"]').val() != undefined) {
    var arrayAuxiliar = new Array();
    for (
      var i = 0;
      i < document.querySelector("[name='inpCodPositionArea']").options.length;
      i++
    ) {
      arrayAuxiliar.push(
        document.querySelector("[name='inpCodPositionArea']").options[i]
          .innerText
      );
    }
    arrayAuxiliar.shift();

    for (var i = arrayAuxiliar.length - 1; i >= 0; i--) {
      if (arrayAuxiliar[i].substring(4, 5) == '_') {
        arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]), 1);
      }
    }
    $('select[name="inpCodPositionArea"]').val(
      $("option:contains('" + arrayAuxiliar[0] + "')").val()
    );
    $(
      $('select[name="inpCodPositionArea"]')
        .parent()
        .parent()
    ).hide();
  }
};
