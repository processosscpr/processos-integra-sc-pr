$(document).ready(function() {
  ajustarFormulario();
  adicionarRequisitanteBadge();

  $('inp:tipoDeOcorrenciaIdentificada').change(elemento => {
    validarSeFundoCobreMovimentacaoSuspeita(elemento.target.value);
  });
});

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // configurações default do formulário
  $('#customBtn_Fundo\\ Cobre\\ Movimentação\\ Suspeita').hide();
  $('#customBtn_Fundo\\ Não\\ Cobre\\ Movimentação\\ Suspeita').hide();
  $('inp:fundoCobre').attr('readOnly', true);
  ajustarCampoChecklistQuandoSomenteVisivel();

  // esconder elementos do formulário
  $('#coluna-documento-credito-provisorio').hide();
  $('#coluna-documento-fo25').hide();
  $('#section-informacoes-deferimento-reuniao').hide();

  $('inp:tipoDePessoa').val() === 'Pessoa Física'
    ? $('#informacoes-cooperado-thirdRow').show()
    : $('#informacoes-cooperado-thirdRow').hide();

  $('inp:necessitaCreditoProvisorio').val() === 'Sim'
    ? $('#coluna-necessita-antecipar-credito-provisorio').show()
    : $('#coluna-necessita-antecipar-credito-provisorio').hide();
};

const validarSeFundoCobreMovimentacaoSuspeita = valor => {
  switch (valor) {
    case 'Falha operacional Unicred Central SC/PR':
    case 'Falha operacional Unicred SC/PR Corretora de Seguros':
    case 'Falha de sistema proveniente de prestação de serviço Unicred Central SC/PR':
    case 'Falha de sistema proveniente de prestação de serviço Unicred  SC/PR Corretora':
    case 'Falha de sistema proveniente de prestação de serviço UBR':
    case 'Fraude na emissão de cheques COMPE 136':
    case 'Fraude eletrônica sem responsabilidade do cooperado ou outra IF':
    case 'Casos omissos, não previstos nos demais itens':
      $('inp:fundoCobre').val('Sim');
      $('#customBtn_Fundo\\ Cobre\\ Movimentação\\ Suspeita').show();
      $('#customBtn_Fundo\\ Não\\ Cobre\\ Movimentação\\ Suspeita').hide();
      break;

    case '':
      $('inp:fundoCobre').val('');
      $('#customBtn_Fundo\\ Cobre\\ Movimentação\\ Suspeita').hide();
      $('#customBtn_Fundo\\ Não\\ Cobre\\ Movimentação\\ Suspeita').hide();
      break;

    default:
      $('inp:fundoCobre').val('Não');
      $('#customBtn_Fundo\\ Cobre\\ Movimentação\\ Suspeita').hide();
      $('#customBtn_Fundo\\ Não\\ Cobre\\ Movimentação\\ Suspeita').show();
      break;
  }
};
