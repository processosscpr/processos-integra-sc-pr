Nome
PR_0515 - Alteração de Garantia / Aditamento de Contrato

Fluxograma
Colaboradores [Unicred Florianópolis] -> Iniciar Solicitação de Alteração de Garantia ou Aditamento de Contrato;
GA [Unicred Florianópolis] (Regra de Negócio) -> Analisar Solicitação;
UA [Unicred Florianópolis], especificamente Patrícia Barcelos -> Verificar Solicitação e Realizar Parecer;
Diretoria [Unicred Florianópolis], especificamente Luis Saldanha -> Analisar Parecer e Solicitação;
UA [Unicred Florianópolis], especificamente Patrícia Barcelos -> Realizar o Contrato da Solicitação;
Colaborador Requisitante -> Resgatar Assinatura do Cooperado para a Solicitação;
UA [Unicred Florianópolis], especificamente Patrícia Barcelos -> Realizar Alteração de Garantia ou Aditamento de Contrato;

Formulário
Informações da solicitação
Tipo da Solicitação -> Aditamento de Contrato ou Alteração de Garantia
Número do Contrato

Informações do Contrato
Matrícula
Conta
Associado
CPF / CNPJ
Profissão / Ramo
Renda / Faturamento
Score Serasa
Risco Serasa
Raiting
Valor Liberado
Saldo Devedor Atual

Informações Complementares
Descrição da Garantia Atual (Aparece apenas para Alteração de Garantia)
Descrição da Nova Garantia (Aparece apenas para Alteração de Garantia)
Justificativa

================================================================================

Informações da Solicitação

tipoDaSolicitacao
numeroDoContrato

Informações do Contrato

matricula
conta
associado
cpfCnpj
profissaoRamo
rendaFaturamento
scoreSerasa
riscoSerasa
raiting
valorLiberado
saldoDevedorAtual

Informações Complementares

descricaoDaGarantiaAtual
descricaoDaNovaGarantia
justificativa

================================================================================
