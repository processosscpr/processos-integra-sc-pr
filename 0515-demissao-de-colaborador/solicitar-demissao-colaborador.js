/**
DEMISSÃO
  - processo deve inciar pela marina ou pela fanny
  - indicar um gestor assim como contratação e movimentação
  - departamento pessoal - botar o nosso DP
  - atividade de resgatar a assinatura tem que ser pela marina

  TODO:
    Realizar Pagamento - botar procedure
*/

$(document).ready(function() {
  $(document.getElementById('btnCancel')).hide();
  $(document.getElementById('examesHomologacao')).hide();
  $(document.getElementById('formInformacoesPagamento')).hide();
  $(document.getElementById('formCamposAuxiliares')).hide();
  $(document.querySelector('#divStatisticsTable')).hide();
  $(document.getElementById('formChecklist')).hide();

  document.querySelector("[xname='inphoraAviso']").setAttribute('type', 'time');
  document
    .querySelector("[xname='inphoraDesligamento']")
    .setAttribute('type', 'time');

  var colspanAcessoColaborador = document.getElementById(
    'necessitaAcessoColaborador'
  );
  colspanAcessoColaborador.setAttribute('colspan', 2);

  verificarPosicaoIniciadora();
  ocultarCampos();

  $('inp:estagiario').change(function() {
    esconderLimparCampos();
  });

  $('inp:solicitacaoDemissao').change(function() {
    verificarColaborador();
    alterarTipoAviso();
  });

  $('inp:tipoAviso').change(function() {
    exibirTipoAviso();
  });

  $('inp:horaAviso').blur(function() {
    validarHoraAviso();
  });

  $('inp:horaDesligamento').blur(function() {
    validarHoraDesligamento();
  });

  $('inp:necessitaAcessoColaborador').change(function() {
    exibirCampoData();
  });

  $('inp:dataDesligamento')
    .datepicker()
    .on('changeDate', function(ev) {
      retornaDataDesligamento();
    });

  $('inp:data')
    .datepicker()
    .on('changeDate', function(ev) {
      retornaDataDesligamentoAcesso();
    });

  $('inp:dataAviso')
    .datepicker()
    .on('changeDate', function(ev) {
      retornaDataAviso();
    });

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
  } else {
    ajustarMobile();
  }
});

function verificarPosicaoIniciadora() {
  if ($('select[name="inpCodPositionArea"]').val() != undefined) {
    var arrayAuxiliar = new Array();
    for (
      var i = 0;
      i < document.querySelector("[name='inpCodPositionArea']").options.length;
      i++
    ) {
      arrayAuxiliar.push(
        document.querySelector("[name='inpCodPositionArea']").options[i]
          .innerText
      );
    }
    arrayAuxiliar.shift();

    for (var i = arrayAuxiliar.length - 1; i >= 0; i--) {
      if (arrayAuxiliar[i].substring(4, 5) == '_') {
        arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]), 1);
      }
    }
    $('select[name="inpCodPositionArea"]').val(
      $("option:contains('" + arrayAuxiliar[0] + "')").val()
    );
    $(
      $('select[name="inpCodPositionArea"]')
        .parent()
        .parent()
    ).hide();
  }
}

function ocultarCampos() {
  $(document.getElementById('colaborador')).hide();
  $(document.getElementById('causaDemissao')).hide();
  $(document.getElementById('tipoAviso')).hide();
  $(document.getElementById('dataAviso')).hide();
  $(document.getElementById('horaAviso')).hide();
  $(document.getElementById('dataDesligamento')).hide();
  $(document.getElementById('horaDesligamento')).hide();
  $(document.getElementById('necessitaAcessoColaborador')).hide();
  $(document.getElementById('data')).hide();
  $(document.getElementById('transferirAtividade')).hide();
}

function esconderLimparCampos() {
  var colaborador = document.getElementById('inpestagiario-0').checked;
  var estagiario = document.getElementById('inpestagiario-1').checked;

  if (estagiario || colaborador) {
    ocultarCampos();
    document.getElementById('inpsolicitacaoDemissao-0').checked = false;
    document.getElementById('inpsolicitacaoDemissao-1').checked = false;
    document.querySelector("[xname='inpcolaborador']").value = '';
    document.querySelector("[xname='inpcausaDemissao']").value = '';
    document.querySelector("[xname='inptipoAviso']").value = '';
    document.querySelector("[xname='inpdataAviso']").value = '';
    document.querySelector("[xname='inphoraAviso']").value = '';
    document.querySelector("[xname='inpdataDesligamento']").value = '';
    document.querySelector("[xname='inphoraDesligamento']").value = '';
    document.getElementById('inpnecessitaAcessoColaborador-0').checked = false;
    document.getElementById('inpnecessitaAcessoColaborador-1').checked = false;
    document.querySelector("[xname='inpdata']").value = '';
    document.querySelector("[xname='inpgestorInformado']").value = '';
  }
}

function verificarColaborador() {
  var colaborador = document.getElementById('inpestagiario-0').checked;
  var solicitacaoDemissao = document.getElementById('inpsolicitacaoDemissao-0')
    .checked;

  //Colaborador e Solicitação de Demissão = Oriunda da Organização
  if (colaborador == true && solicitacaoDemissao == true) {
    $(document.getElementById('colaborador')).show();
    $(document.getElementById('causaDemissao')).show();
    $(document.getElementById('tipoAviso')).show();
    cryo_alert(
      'TIPOS DE AVISO\nIniciativa da Cooperativa:\n • Indenizado: Empresa opta por indenizar o aviso ao trabalhador. \n • Trabalhado: Empregado trabalha por mais 30 dias após a data do aviso de desligamento.'
    );
    $(document.getElementById('dataAviso')).show();
    $(document.getElementById('horaAviso')).show();
    $(document.getElementById('necessitaAcessoColaborador')).show();
    $(document.getElementById('dataDesligamento')).hide();
    $(document.getElementById('horaDesligamento')).hide();
    $(document.getElementById('transferirAtividade')).show();
    //Colaborador e Solicitação de Demissão = Oriunda do Colaborador(a)
  } else if (colaborador == true && solicitacaoDemissao == false) {
    $(document.getElementById('colaborador')).show();
    $(document.getElementById('causaDemissao')).show();
    $(document.getElementById('tipoAviso')).show();
    cryo_alert(
      'TIPOS DE AVISO\nIniciativa do Colaborador:\n • Dispensado: Empresa dispensa o empregado do cumprimento do aviso como também do desconto dos dias não trabalhados. \n • Reavido: Empresa desconta o aviso do empregado quando este não o cumpre. \n • Trabalhado: Empregado cumpre o aviso (30 dias) após a data do aviso da demissão.'
    );
    $(document.getElementById('dataAviso')).show();
    $(document.getElementById('horaAviso')).show();
    $(document.getElementById('necessitaAcessoColaborador')).show();
    $(document.getElementById('dataDesligamento')).hide();
    $(document.getElementById('horaDesligamento')).hide();
    $(document.getElementById('transferirAtividade')).show();
    //Estagiário
  } else if (
    (colaborador == false && solicitacaoDemissao == true) ||
    solicitacaoDemissao == false
  ) {
    $(document.getElementById('colaborador')).show();
    $(document.getElementById('causaDemissao')).show();
    $(document.getElementById('dataDesligamento')).show();
    $(document.getElementById('horaDesligamento')).show();
    $(document.getElementById('necessitaAcessoColaborador')).show();
    $(document.getElementById('tipoAviso')).hide();
    $(document.getElementById('dataAviso')).hide();
    $(document.getElementById('horaAviso')).hide();
    $(document.getElementById('transferirAtividade')).show();

    document
      .querySelector("[xname='inptipoAviso']")
      .setAttribute('required', 'N');
    document
      .querySelector("[xname='inpdataAviso']")
      .setAttribute('required', 'N');
    document
      .querySelector("[xname='inphoraAviso']")
      .setAttribute('required', 'N');
  }
}

function exibirTipoAviso() {
  var tipoAviso = document.querySelector("[xname='inptipoAviso']").value;
  if (
    tipoAviso == 'Indenizado' ||
    tipoAviso == 'Dispensado' ||
    tipoAviso == 'Reavido' ||
    tipoAviso == ''
  ) {
    $(document.getElementById('dataDesligamento')).hide();
    $(document.getElementById('horaDesligamento')).hide();
    document
      .querySelector("[xname='inpdataDesligamento']")
      .setAttribute('required', 'N');
    document
      .querySelector("[xname='inphoraDesligamento']")
      .setAttribute('required', 'N');
    document.querySelector("[xname='inpdataAviso']").value = '';
    document.querySelector("[xname='inphoraAviso']").value = '';
  } else if (tipoAviso == 'Trabalhado') {
    $(document.getElementById('dataDesligamento')).show();
    $(document.getElementById('horaDesligamento')).show();
    document
      .querySelector("[xname='inpdataDesligamento']")
      .setAttribute('required', 'S');
    document
      .querySelector("[xname='inphoraDesligamento']")
      .setAttribute('required', 'S');
    document.querySelector("[xname='inpdataDesligamento']").value = '';
    document.querySelector("[xname='inphoraDesligamento']").value = '';
  }
}

function ResgatarLinkIntegracaoJSON() {
  var linkIntegracaoJSON;
  var solicitacaoDemissaoOrganizacao = document.getElementById(
    'inpsolicitacaoDemissao-0'
  ).checked;
  var solicitacaoDemissaoColaborador = document.getElementById(
    'inpsolicitacaoDemissao-1'
  ).checked;

  if (solicitacaoDemissaoOrganizacao) {
    linkIntegracaoJSON =
      'qw0Xk6xWKL563BI8VvBqJtB32OejKspouImmk5HBUvSVQO3O@rgdemrRgLOX5atqLp7S7J917eG72pdLU1i0qA__';
  } else if (solicitacaoDemissaoColaborador) {
    linkIntegracaoJSON =
      'qw0Xk6xWKL563BI8VvBqJo-heXiJygle7Q-KxSgnEyNeFOmUeJsHDJ4Fmrdc3SsuReVSCYYDQLnfwulxsnkT4Q__';
  }
  return linkIntegracaoJSON;
}

function alterarTipoAviso() {
  var c = ResgatarLinkIntegracaoJSON();

  var jqxhr = $.ajax({
    type: 'post',
    dataType: 'json',
    url: '../api/json/datasource/get/1.0/' + c,
    data: '',
  }).done(function(data) {
    if (data.success != null) {
      if (data.cache) {
        $(document.querySelector("[xname='inptipoAviso']")).attr(
          'data-from-cache',
          true
        );
      }
      if (data.runtime) {
        $(document.querySelector("[xname='inptipoAviso']")).attr(
          'data-runtime',
          data - runtime
        );
      }

      $(document.querySelector("[xname='inptipoAviso']"))
        .find('option')
        .remove();
      $(document.querySelector("[xname='inptipoAviso']")).append(
        '<option value="">Selecione</option>'
      );
      $.each(data.success, function(i, item) {
        $(document.querySelector("[xname='inptipoAviso']")).append(
          "<option value='" + item.cod + "'" + '>' + item.txt + '</option>'
        );
      });
    }
  });
}

function validarHoraAviso() {
  var horaAviso = document.querySelector("[xname='inphoraAviso']").value;
  if (horaAviso < '07:00' || horaAviso > '19:00') {
    alert('Horário não permitido para emissão do aviso!');
    document.querySelector("[xname='inphoraAviso']").value = '';
  }
}

function validarHoraDesligamento() {
  var horaDesligamento = document.querySelector("[xname='inphoraDesligamento']")
    .value;
  if (horaDesligamento < '07:00' || horaDesligamento > '19:00') {
    alert('Horário não permitido para efetuar o desligamento!');
    document.querySelector("[xname='inphoraDesligamento']").value = '';
  }
}

function exibirCampoData() {
  var campoData = document.getElementById('inpnecessitaAcessoColaborador-1')
    .checked;
  if (campoData) {
    var colspanAcessoColaborador = document.getElementById(
      'necessitaAcessoColaborador'
    );
    colspanAcessoColaborador.setAttribute('colspan', 1);
    $(document.getElementById('data')).show();
    document.querySelector("[xname='inpdata']").setAttribute('required', 'S');
    document.querySelector("[xname='inpdata']").value = '';
  } else {
    var colspanAcessoColaborador = document.getElementById(
      'necessitaAcessoColaborador'
    );
    colspanAcessoColaborador.setAttribute('colspan', 2);
    $(document.getElementById('data')).hide();
    document.querySelector("[xname='inpdata']").setAttribute('required', 'N');
    document.querySelector("[xname='inpdata']").value = '';
  }
}

function retornaDataDesligamento() {
  var dataAjusteDesligamento = diminuirDia(
    retornaDataXname("[xname='inpdataDesligamento']"),
    7
  );
  document.querySelector(
    "[xname='inpdataAjusteDesligamento']"
  ).value = retornaDataFormatada(dataAjusteDesligamento);
}

function retornaDataDesligamentoAcesso() {
  var dataDesligamentoAcesso = diminuirDia(
    retornaDataXname("[xname='inpdata']"),
    7
  );
  document.querySelector(
    "[xname='inpdataDesligamentoAcesso']"
  ).value = retornaDataFormatada(dataDesligamentoAcesso);
}

function retornaDataAviso() {
  var dataAviso = diminuirDia(retornaDataXname("[xname='inpdataAviso']"), 7);
  document.querySelector(
    "[xname='inpdataAjusteDesligamento']"
  ).value = retornaDataFormatada(dataAviso);
}

//subtrair apenas dias úteis de uma data
function diminuirDia(data, qtdDia) {
  var i = 0;
  while (i < qtdDia) {
    data.setDate(data.getDate() - 1);
    if (data.getDay() > 0 && data.getDay() < 6) {
      i++;
    }
  }
  return data;
}

//retorna data formatada dd/mm/yyyy
function retornaDataFormatada(data) {
  var dataToString = '';
  if (data != null) {
    if (data.getDate() < 10) {
      dataToString = '0' + data.getDate();
    } else {
      dataToString = data.getDate();
    }

    if (data.getMonth() < 9) {
      dataToString = dataToString + '/0' + (data.getMonth() + 1);
    } else {
      dataToString = dataToString + '/' + (data.getMonth() + 1);
    }

    dataToString = dataToString + '/' + data.getFullYear();
  }
  return dataToString;
}

function retornaDataXname(xname) {
  var data = document.querySelector(xname).value;
  if (data != '') {
    var dataSplit = data.split('/');
    var dataCompleta = new Date(dataSplit[2], dataSplit[1] - 1, dataSplit[0]);
    return dataCompleta;
  } else {
    return null;
  }
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function ajustarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();
  document.querySelector("[xname='inpestagiario']").setAttribute('style', '');
  document
    .querySelector("[xname='inpsolicitacaoDemissao']")
    .setAttribute('style', '');
  document
    .querySelector("[xname='inpcolaborador']")
    .setAttribute('style', 'width:90%!important');
  document
    .querySelector("[xname='inpcausaDemissao']")
    .setAttribute('style', 'width:90%!important');
  document
    .querySelector("[xname='inptipoAviso']")
    .setAttribute('style', 'width:60%!important');
  document
    .querySelector("[xname='inpdataAviso']")
    .setAttribute(
      'style',
      'height: 20px!important; width:80%!important; background-color:white'
    );
  document
    .querySelector("[xname='inpdataAviso']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inphoraAviso']")
    .setAttribute('style', 'height: 20px!important; width:50%!important;');
  document
    .querySelector("[xname='inpdataDesligamento']")
    .setAttribute(
      'style',
      'height: 20px!important; width:80%!important; background-color:white'
    );
  document
    .querySelector("[xname='inpdataDesligamento']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inphoraDesligamento']")
    .setAttribute('style', 'height: 20px!important; width:50%!important;');
  document
    .querySelector("[xname='inpnecessitaAcessoColaborador']")
    .setAttribute('style', '');
  document
    .querySelector("[xname='inpdata']")
    .setAttribute('style', 'height: 20px!important; width:85%!important');
  document.querySelector("[xname='inpdata']").setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpgestorInformado']")
    .setAttribute('style', 'width:95%!important');
}
