$(document).ready(function() {
  cryo_TaskClaimOwnershipWrapper();
  var AgrupadorFormulario = document.querySelector('#ContainerForm');
  var FilhoAgrupadorFormulario = $(AgrupadorFormulario).children();
  var DivFormulario = $(FilhoAgrupadorFormulario[0]).children();
  DivFormulario[0].innerHTML =
    'Formulário<button type="button" id="btnDesassociar" class="btn btn-primary;btn-group dropup right" style="background-color: rgb(0, 109, 204);color:white">Desassociar</button>';

  var clickButtonDesassociar = document.querySelector('#btnDesassociar');
  clickButtonDesassociar.addEventListener('click', function() {
    cryo_TaskUnclaimOwnershipWrapper();
    alert(
      'Alterações salvas!\nEsta atividade será reencaminhada para a fila de atendimento de sua área. \n\nVocê será redirecionado para a página inicial da Ferramenta Integra.'
    );
    save();
  });

  verificarCampoPreenchidoTipoAviso();
  verificarCampoDataAviso();
  verificarAcessoColaborador();

  $(document.getElementById('formCamposAuxiliares')).hide();
  $(document.getElementById('formChecklist')).hide();

  if (isMobile()) {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }
});

function verificarCampoPreenchidoTipoAviso() {
  var estagiario = document.querySelector("[xname='inpestagiario']").value;
  var solicitacaoDemissao = document.querySelector(
    "[xname='inpsolicitacaoDemissao']"
  ).value;
  var tipoAviso = document.querySelector("[xname='inptipoAviso']").value;

  if (
    (estagiario == 'Sim' && solicitacaoDemissao == 'Oriunda da Organização') ||
    (solicitacaoDemissao == 'Oriunda do(a) Colaborador(a)' && tipoAviso == '')
  ) {
    $(document.getElementById('tipoAviso')).hide();
  } else if (
    estagiario == 'Não' &&
    solicitacaoDemissao == 'Oriunda da Organização' &&
    tipoAviso == 'Trabalhado'
  ) {
    $(document.getElementById('tipoAviso')).show();
    $(document.getElementById('dataDesligamento')).show();
    $(document.getElementById('horaDesligamento')).show();
  } else if (
    estagiario == 'Não' &&
    solicitacaoDemissao == 'Oriunda da Organização' &&
    tipoAviso == 'Indenizado'
  ) {
    $(document.getElementById('tipoAviso')).show();
    $(document.getElementById('dataDesligamento')).hide();
    $(document.getElementById('horaDesligamento')).hide();
  } else if (
    (estagiario == 'Não' &&
      solicitacaoDemissao == 'Oriunda do(a) Colaborador(a)' &&
      tipoAviso == 'Dispensado') ||
    tipoAviso == 'Reavido'
  ) {
    $(document.getElementById('tipoAviso')).show();
    $(document.getElementById('dataDesligamento')).hide();
    $(document.getElementById('horaDesligamento')).hide();
  } else if (
    estagiario == 'Não' &&
    solicitacaoDemissao == 'Oriunda do(a) Colaborador(a)' &&
    tipoAviso == 'Trabalhado'
  ) {
    $(document.getElementById('tipoAviso')).show();
    $(document.getElementById('dataDesligamento')).show();
    $(document.getElementById('horaDesligamento')).show();
  }
}

function verificarCampoDataAviso() {
  var dataAviso = document.querySelector("[xname='inpdataAviso']").value;
  if (dataAviso == '') {
    $(document.getElementById('dataAviso')).hide();
    $(document.getElementById('horaAviso')).hide();
  } else {
    $(document.getElementById('dataAviso')).show();
    $(document.getElementById('horaAviso')).show();
  }
}

function verificarAcessoColaborador() {
  var necessitaAcessoColaborador = document.querySelector(
    "[xname='inpnecessitaAcessoColaborador']"
  ).value;
  if (necessitaAcessoColaborador == 'Não') {
    $(document.getElementById('data')).hide();
    var colspannecessitaAcessoColaborador = document.getElementById(
      'necessitaAcessoColaborador'
    );
    colspannecessitaAcessoColaborador.setAttribute('colspan', 2);
  } else {
    $(document.getElementById('data')).show();
    var colspannecessitaAcessoColaborador = document.getElementById(
      'necessitaAcessoColaborador'
    );
    colspannecessitaAcessoColaborador.setAttribute('colspan', 1);
  }
}

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}
