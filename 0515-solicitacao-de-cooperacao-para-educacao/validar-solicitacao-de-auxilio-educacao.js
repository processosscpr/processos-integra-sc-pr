$(document).ready(function() {
  cryo_TaskClaimOwnershipWrapper();

  var AgrupadorFormulario = document.querySelector('#ContainerForm');
  var FilhoAgrupadorFormulario = $(AgrupadorFormulario).children();
  var DivFormulario = $(FilhoAgrupadorFormulario[0]).children();
  DivFormulario[0].innerHTML =
    'Formulário<button type="button" id="btnDesassociar" class="btn btn-primary;btn-group dropup right" style="background-color: rgb(0, 109, 204);color:white">Desassociar</button>';

  var clickButtonDesassociar = document.querySelector('#btnDesassociar');
  clickButtonDesassociar.addEventListener('click', function() {
    cryo_TaskUnclaimOwnershipWrapper();
    alert(
      'Alterações salvas!\nEsta atividade será reencaminhada para a fila de atendimento de sua área. \n\nVocê será redirecionado para a página inicial da Ferramenta Integra. \n\n*Desconsiderar próxima mensagem, ao clicar em OK!'
    );
    save();
  });

  if (isMobile()) {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }

  document
    .querySelector('#informacoes-solicitacao')
    .setAttribute('class', 'box box-open-and-close');
  document
    .querySelector('#informacoes-auxilio-educacao')
    .setAttribute('class', 'box box-open-and-close');
  document
    .querySelector('#informacoes-curso')
    .setAttribute('class', 'box box-open-and-close');
  $(document.querySelector('#solicitacao-gp')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).hide();
  $(document.querySelector('#informacoes-auxiliares')).hide();
  $(document.querySelector('#informacoes-bd')).hide();
  resgatarInformacoesParaInsert();
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function resgatarInformacoesParaInsert() {
  var tipoAuxilio = document.querySelector("[xname='inptipoAuxilioEducacao']")
    .value;
  document.querySelector("[xid='divbdTipo']").textContent = tipoAuxilio;
  document.querySelector("[xname='inpbdTipo']").value = tipoAuxilio;

  var dataInicio = transformarDataPadraoBD(
    document.querySelector("[xname='inpdataInicioCurso']").value
  );
  document.querySelector("[xid='divbdDataInicio']").textContent = dataInicio;
  document.querySelector("[xname='inpbdDataInicio']").value = dataInicio;

  var dataTermino = transformarDataPadraoBD(
    document.querySelector("[xname='inpdataTerminoCurso']").value
  );
  document.querySelector("[xid='divbdDataTermino']").textContent = dataTermino;
  document.querySelector("[xname='inpbdDataTermino']").value = dataTermino;

  var valorMensal = transformarValorPadraoBD(
    document.querySelector("[xname='inpvalorMensal']").value
  );
  document.querySelector("[xid='divbdValorMensal']").textContent = valorMensal;
  document.querySelector("[xname='inpbdValorMensal']").value = valorMensal;

  var valorAuxilio = transformarValorPadraoBD(
    document.querySelector("[xname='inpvalorCalculadoParaAuxilio']").value
  );
  document.querySelector(
    "[xid='divbdValorReembolso']"
  ).textContent = valorAuxilio;
  document.querySelector("[xname='inpbdValorReembolso']").value = valorAuxilio;

  var inicioPagamento = transformarDataPadraoBD(
    document.querySelector("[xname='inpproximaParcela']").value
  );
  document.querySelector(
    "[xid='divbdInicioPagamento']"
  ).textContent = inicioPagamento;
  document.querySelector(
    "[xname='inpbdInicioPagamento']"
  ).value = inicioPagamento;

  var limiteReembolso = calcularLimiteReembolso(
    document.querySelector("[xname='inpproximaParcela']").value,
    document.querySelector("[xname='inpquantidadeParcelasRestantes']").value
  );
  document.querySelector(
    "[xid='divbdLimiteReembolso']"
  ).textContent = limiteReembolso;
  document.querySelector(
    "[xname='inpbdLimiteReembolso']"
  ).value = limiteReembolso;
}

function transformarDataPadraoBD(data) {
  var dataSplit = data.split('/');
  var dataPadronizada = dataSplit[2] + '-' + dataSplit[1] + '-' + dataSplit[0];
  return dataPadronizada;
}

function transformarValorPadraoBD(valor) {
  var auxiliarParaRemoverPontos;
  var valorAjustado;
  if (valor.length > 6) {
    auxiliarParaRemoverPontos = valor;
    for (var i = 0; i < valor.split('.').length - 1; i++) {
      auxiliarParaRemoverPontos = auxiliarParaRemoverPontos.replace('.', '');
    }
    valorAjustado = tirarVirgula(auxiliarParaRemoverPontos);
  } else {
    valorAjustado = tirarVirgula(valor);
  }
  return valorAjustado;
}

function tirarVirgula(valor) {
  var valorParonizado = valor.replace(',', '.');
  return valorParonizado;
}

function calcularLimiteReembolso(data, parcela) {
  var parcelaInt = parseInt(parcela);
  var dataSplit = data.split('/');
  var dataCompleta = new Date(dataSplit[2], dataSplit[1] - 1, dataSplit[0]);
  var dataLimiteReembolso = new Date(
    dataCompleta.getFullYear(),
    dataCompleta.getMonth() + parcelaInt,
    dataCompleta.getDate()
  );
  var lastDay = new Date(
    dataLimiteReembolso.getFullYear(),
    dataLimiteReembolso.getMonth() + 1,
    0
  );
  var dataFinal =
    lastDay.getFullYear() +
    '-' +
    (lastDay.getMonth() + 1) +
    '-' +
    lastDay.getDate();
  return dataFinal;
}
