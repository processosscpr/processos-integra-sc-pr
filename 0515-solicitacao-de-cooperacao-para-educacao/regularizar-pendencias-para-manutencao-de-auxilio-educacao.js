$(document).ready(function() {
  if (isMobile()) {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }

  document
    .querySelector('#informacoes-solicitacao')
    .setAttribute('class', 'box box-open-and-close');
  $(document.querySelector('#solicitacao-colaborador')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).hide();
  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacoes-auxiliares')).hide();
  $(document.querySelector('#informacoes-bd')).hide();
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}
