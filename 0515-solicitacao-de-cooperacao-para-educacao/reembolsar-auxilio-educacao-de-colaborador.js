$(document).ready(function() {
  cryo_TaskClaimOwnershipWrapper();

  var AgrupadorFormulario = document.querySelector('#ContainerForm');
  var FilhoAgrupadorFormulario = $(AgrupadorFormulario).children();
  var DivFormulario = $(FilhoAgrupadorFormulario[0]).children();
  DivFormulario[0].innerHTML =
    'Formulário<button type="button" id="btnDesassociar" class="btn btn-primary;btn-group dropup right" style="background-color: rgb(0, 109, 204);color:white">Desassociar</button>';

  var clickButtonDesassociar = document.querySelector('#btnDesassociar');
  clickButtonDesassociar.addEventListener('click', function() {
    cryo_TaskUnclaimOwnershipWrapper();
    alert(
      'Alterações salvas!\nEsta atividade será reencaminhada para a fila de atendimento de sua área. \n\nVocê será redirecionado para a página inicial da Ferramenta Integra. \n\n*Desconsiderar próxima mensagem, ao clicar em OK!'
    );
    save();
  });

  if (isMobile()) {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }

  document
    .querySelector('#informacoes-solicitacao')
    .setAttribute('class', 'box box-open-and-close');
  document
    .querySelector('#informacoes-tipos-solicitacao')
    .setAttribute('class', 'box box-open-and-close');
  $(document.querySelector('#informacoes-valor-reajustado')).hide();
  $(document.querySelector('#informacoes-bloqueio')).hide();
  $(document.querySelector('#informacoes-desbloqueio')).hide();
  $(document.querySelector('#informacoes-manutencao')).hide();
  $('#descricao-do-motivo').hide();
  $('#motivo-reajuste').hide();

  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacao-solicitar-reajuste')).hide();
  $(document.querySelector('#solicitacao-gp')).hide();
  $(document.querySelector('#informacoes-auxiliares')).hide();
  $(document.querySelector('#informacoes-bd')).hide();
  var elementoCodigoInstancia = document.getElementsByClassName(
    'label label-success'
  );
  $('#ContainerHistory')
    .find('div.box-header')[0]
    .append(elementoCodigoInstancia[0]);

  resgatarInformacoesParaUpdate();

  if ($('div:reajusteDeValor').text() == 'Sim') {
    $('#informacao-solicitar-reajuste').show();
    $('#motivo-reajuste').show();
    $('#informacoes-valor-reajustado').show();
  }

  if ($('div:motivoDoReajuste').text() == 'Outros') {
    $('#descricao-do-motivo').show();
  }
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function resgatarInformacoesParaUpdate() {
  var valorMensal = transformarValorPadraoBD(
    document.querySelector("[xname='inpvalorMensalReajustado']").value
  );
  $('div:bdValorMensal').text(valorMensal);
  $('inp:bdValorMensal').val(valorMensal);

  var valorAuxilio = transformarValorPadraoBD(
    document.querySelector("[xname='inpvalorCalculadoParaAuxilioReajustado']")
      .value
  );
  $('div:bdValorReembolso').text(valorAuxilio);
  $('inp:bdValorReembolso').val(valorAuxilio);
}

function transformarValorPadraoBD(valor) {
  var auxiliarParaRemoverPontos;
  var valorAjustado;
  if (valor.length > 6) {
    auxiliarParaRemoverPontos = valor;
    for (var i = 0; i < valor.split('.').length - 1; i++) {
      auxiliarParaRemoverPontos = auxiliarParaRemoverPontos.replace('.', '');
    }
    valorAjustado = tirarVirgula(auxiliarParaRemoverPontos);
  } else {
    valorAjustado = tirarVirgula(valor);
  }
  return valorAjustado;
}

function tirarVirgula(valor) {
  var valorParonizado = valor.replace(',', '.');
  return valorParonizado;
}
