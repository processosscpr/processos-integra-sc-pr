$(document).ready(function() {
  if (isMobile()) {
    $(document.querySelector('.mobile-floating-controls')).hide();
  }

  document
    .querySelector('#informacoes-solicitacao')
    .setAttribute('class', 'box box-open-and-close');
  document
    .querySelector('#informacoes-tipos-solicitacao')
    .setAttribute('class', 'box box-open-and-close');
  $(document.querySelector('#informacoes-valor-reajustado')).hide();
  $(document.querySelector('#informacoes-bloqueio')).hide();
  $(document.querySelector('#informacoes-desbloqueio')).hide();
  $(document.querySelector('#informacoes-manutencao')).hide();

  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacao-solicitar-reajuste')).hide();
  $(document.querySelector('#solicitacao-gp')).hide();
  $(document.querySelector('#informacoes-auxiliares')).hide();
  $(document.querySelector('#informacoes-bd')).hide();
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}
