$(document).ready(function() {
  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
    document
      .querySelector(
        "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
      )
      .setAttribute('style', 'width:95%');
  } else {
    adaptarMobile();
  }

  document
    .querySelector('#informacoes-solicitacao')
    .setAttribute('class', 'box box-open-and-close');
  $(document.querySelector('#solicitacao-gp')).hide();
  $(document.querySelector('#informacoes-auxiliares')).hide();
  $(document.querySelector('#informacoes-bd')).hide();
  $(document.querySelector('#btnCancel')).hide();
  $(document.querySelector('#divStatisticsTable')).hide();
  $('#descricao-do-motivo').hide();

  var solicitacao = $('div:solicitacao').text();
  if (solicitacao == 'Solicitar Auxílio Educação') {
    habilitarSolicitacaoAuxilioEducacao();
  } else {
    habilitarValorParaReembolsar();
  }

  var opcaoSimJaPossuiTitulo = document.getElementById(
    'inpjaPossuiTituloDeGraducaoEouPosgraduacao-1'
  ).checked;
  if (opcaoSimJaPossuiTitulo) {
    document
      .querySelector("[xname='inpquaisCursos']")
      .setAttribute('required', 'S');
    $(document.querySelector('#quais-cursos'))
      .children()
      .show();
  } else {
    document
      .querySelector("[xname='inpquaisCursos']")
      .setAttribute('required', 'N');
    $(document.querySelector('#quais-cursos'))
      .children()
      .hide();
  }

  $('inp:jaPossuiTituloDeGraducaoEouPosgraduacao').on('change', function() {
    var opcaoSim = document.getElementById(
      'inpjaPossuiTituloDeGraducaoEouPosgraduacao-1'
    ).checked;
    if (opcaoSim) {
      document
        .querySelector("[xname='inpquaisCursos']")
        .setAttribute('required', 'S');
      $(document.querySelector('#quais-cursos'))
        .children()
        .show();
    } else {
      document
        .querySelector("[xname='inpquaisCursos']")
        .setAttribute('required', 'N');
      $(document.querySelector('#quais-cursos'))
        .children()
        .hide();
    }
  });

  var opcaoSimRecebeuAuxilio = document.getElementById(
    'inpjaRecebeuAuxilioEducacaoDoSistemaUnicred-1'
  ).checked;
  if (opcaoSimRecebeuAuxilio) {
    document
      .querySelector("[xname='inphaQuantoTempo']")
      .setAttribute('required', 'S');
    $(document.querySelector('#quanto-tempo'))
      .children()
      .show();
  } else {
    document
      .querySelector("[xname='inphaQuantoTempo']")
      .setAttribute('required', 'N');
    $(document.querySelector('#quanto-tempo'))
      .children()
      .hide();
  }

  $('inp:jaRecebeuAuxilioEducacaoDoSistemaUnicred').on('change', function() {
    var opcaoSim = document.getElementById(
      'inpjaRecebeuAuxilioEducacaoDoSistemaUnicred-1'
    ).checked;
    if (opcaoSim) {
      document
        .querySelector("[xname='inphaQuantoTempo']")
        .setAttribute('required', 'S');
      $(document.querySelector('#quanto-tempo'))
        .children()
        .show();
    } else {
      document
        .querySelector("[xname='inphaQuantoTempo']")
        .setAttribute('required', 'N');
      $(document.querySelector('#quanto-tempo'))
        .children()
        .hide();
    }
  });

  var opcaoSimRecebeAuxilio = document.getElementById(
    'inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao-1'
  ).checked;
  if (opcaoSimRecebeAuxilio) {
    document
      .querySelector("[xname='inpemQuaisProgramasEstaInserido']")
      .setAttribute('required', 'S');
    $(document.querySelector('#quais-programas'))
      .children()
      .show();
  } else {
    document
      .querySelector("[xname='inpemQuaisProgramasEstaInserido']")
      .setAttribute('required', 'N');
    $(document.querySelector('#quais-programas'))
      .children()
      .hide();
  }

  $('inp:recebeAuxilioEducacaobolsadescontoDeOutraInstituicao').on(
    'change',
    function() {
      var opcaoSim = document.getElementById(
        'inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao-1'
      ).checked;
      if (opcaoSim) {
        document
          .querySelector("[xname='inpemQuaisProgramasEstaInserido']")
          .setAttribute('required', 'S');
        $(document.querySelector('#quais-programas'))
          .children()
          .show();
      } else {
        document
          .querySelector("[xname='inpemQuaisProgramasEstaInserido']")
          .setAttribute('required', 'N');
        $(document.querySelector('#quais-programas'))
          .children()
          .hide();
      }
    }
  );

  $('inp:valorMensal').blur(function() {
    var valorAuxilioEducacao = calcularAuxilioEducacao(
      document.querySelector("[xname='inpvalorMensal']").value
    );
    var valorAuxilioEducacaoToFixed = valorAuxilioEducacao.toFixed(2);
    var valoeAuxilioEducacaoReplace = valorAuxilioEducacaoToFixed.replace(
      '.',
      ','
    );
    document.querySelector(
      "[xname='inpvalorCalculadoParaAuxilio']"
    ).value = valoeAuxilioEducacaoReplace;
    document.querySelector(
      "[xid='divvalorCalculadoParaAuxilio']"
    ).textContent = valoeAuxilioEducacaoReplace;
  });

  $('inp:valorMensalReajustado').blur(function() {
    var valorAuxilioEducacao = calcularAuxilioEducacao(
      document.querySelector("[xname='inpvalorMensalReajustado']").value
    );
    var valorAuxilioEducacaoToFixed = valorAuxilioEducacao.toFixed(2);
    var valoeAuxilioEducacaoReplace = valorAuxilioEducacaoToFixed.replace(
      '.',
      ','
    );
    document.querySelector(
      "[xname='inpvalorCalculadoParaAuxilioReajustado']"
    ).value = valoeAuxilioEducacaoReplace;
    $('div:valorCalculadoParaAuxilioReajustado').text(
      valoeAuxilioEducacaoReplace
    );
  });

  if (document.getElementById('inpreajusteDeValor-1').checked) {
    ajustarCampos();
  }

  $('inp:reajusteDeValor').on('change', function() {
    var opcaoSim = document.getElementById('inpreajusteDeValor-1').checked;
    if (opcaoSim) {
      document
        .querySelector("[xname='inpvalorMensalReajustado']")
        .setAttribute('required', 'S');
      $(document.querySelector('#informacoes-valor-reajustado')).show();
      $('#motivo-reajuste').show();
      document
        .querySelector("[xname='inpmotivoDoReajuste']")
        .setAttribute('required', 'S');
    } else {
      document.querySelector('[xname="inpmotivoDoReajuste"]').selectedIndex = 0;
      document
        .querySelector("[xname='inpvalorMensalReajustado']")
        .setAttribute('required', 'N');
      $(document.querySelector('#informacoes-valor-reajustado')).hide();
      $('inp:descricaoDoMotivoDeReajuste').val('');
      $('#motivo-reajuste').hide();
      $('inp:valorMensalReajustado').val('');
      $('div:valorCalculadoParaAuxilioReajustado').text('');
      document
        .querySelector("[xname='inpmotivoDoReajuste']")
        .setAttribute('required', 'N');
    }
  });

  $('inp:motivoDoReajuste').change(habilitarCampoDescricao);
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();

  document
    .querySelector("[xname='inpsolicitacao']")
    .setAttribute('style', 'width:85%!important');

  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpquaisCursos']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inphaQuantoTempo']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpemQuaisProgramasEstaInserido']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');

  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpcurso']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('style', 'width:85%!important');
}

function habilitarSolicitacaoAuxilioEducacao() {
  $(document.querySelector('#informacoes-tipos-solicitacao')).hide();
  $(document.querySelector('#informacoes-auxilio-educacao')).show();
  $(document.querySelector('#quais-cursos'))
    .children()
    .hide();
  $(document.querySelector('#quanto-tempo'))
    .children()
    .hide();
  $(document.querySelector('#quais-programas'))
    .children()
    .hide();
  $(document.querySelector('#informacoes-curso')).show();
  obrigatoriedadeSolicitacaoEducacao();
}

function habilitarValorParaReembolsar() {
  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).show();
  $(document.querySelector('#informacoes-valor-reembolso')).show();
  $(document.querySelector('#informacao-solicitar-reajuste')).hide();
  $(document.querySelector('#informacoes-valor-reajustado')).hide();
  $(document.querySelector('#informacoes-bloqueio')).hide();
  $(document.querySelector('#informacoes-desbloqueio')).hide();
  $(document.querySelector('#informacoes-manutencao')).hide();
  obrigatoriedadeSolicitacaoReembolso();
}

function calcularAuxilioEducacao(valorMensal) {
  var auxiliarParaRemoverPontos;
  var valorAuxilioEducacao;
  if (valorMensal.length > 6) {
    auxiliarParaRemoverPontos = valorMensal;
    for (var i = 0; i < valorMensal.split('.').length - 1; i++) {
      auxiliarParaRemoverPontos = auxiliarParaRemoverPontos.replace('.', '');
    }
    valorAuxilioEducacao = dividirPorDois(auxiliarParaRemoverPontos);
  } else {
    valorAuxilioEducacao = dividirPorDois(valorMensal);
  }
  return valorAuxilioEducacao;
}

function dividirPorDois(valor) {
  var valorReplace = valor.replace(',', '.');
  var valorDividido = valorReplace / 2;

  return valorDividido;
}

function obrigatoriedadeSolicitacaoEducacao() {
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpjaPossuiTituloDeGraducaoEouPosgraduacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpjaRecebeuAuxilioEducacaoDoSistemaUnicred']")
    .setAttribute('required', 'S');
  document
    .querySelector(
      "[xname='inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao']"
    )
    .setAttribute('required', 'S');
  document
    .querySelector(
      "[xname='inpcriteriosParaProsseguirComSolicitacaoDeAuxilioEducacao']"
    )
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inptipoAuxilioEducacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('required', 'S');
  document.querySelector("[xname='inpcurso']").setAttribute('required', 'S');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('required', 'S');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaBloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmanutencaoPara']")
    .setAttribute('required', 'N');
}

function obrigatoriedadeSolicitacaoReembolso() {
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaPossuiTituloDeGraducaoEouPosgraduacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaRecebeuAuxilioEducacaoDoSistemaUnicred']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpcriteriosParaProsseguirComSolicitacaoDeAuxilioEducacao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptipoAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('required', 'N');
  document.querySelector("[xname='inpcurso']").setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaBloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmanutencaoPara']")
    .setAttribute('required', 'N');
}

function ajustarCampos() {
  $('#informacoes-valor-reajustado').show();
  $('#informacao-solicitar-reajuste').show();
  if (
    document.querySelector('[xname="inpmotivoDoReajuste"]').selectedIndex == 4
  ) {
    $('#descricao-do-motivo').show();
  }
}

function habilitarCampoDescricao() {
  if (
    document.querySelector('[xname="inpmotivoDoReajuste"]').selectedIndex == 4
  ) {
    $('#descricao-do-motivo').show();
    document
      .querySelector("[xname='inpdescricaoDoMotivoDeReajuste']")
      .setAttribute('required', 'S');
  } else {
    $('#descricao-do-motivo').hide();
    document
      .querySelector("[xname='inpdescricaoDoMotivoDeReajuste']")
      .setAttribute('required', 'N');
    $('inp:descricaoDoMotivoDeReajuste').val('');
  }
}
