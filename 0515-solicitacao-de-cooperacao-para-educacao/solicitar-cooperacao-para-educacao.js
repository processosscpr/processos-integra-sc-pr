$(document).ready(function() {
  verificarPosicaoIniciadora();
  preencherCampoParaSolicitacaoDeAuxilio(
    $('inp:colaboradorRequisitante').val()
  );

  if (!isMobile()) {
    $(document.querySelector('#RequesterInfo')).hide();
    document
      .querySelector(
        "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
      )
      .setAttribute('style', 'width:95%');
  } else {
    adaptarMobile();
  }

  document
    .querySelector('#informacoes-solicitacao')
    .setAttribute('class', 'box box-open-and-close');
  $(document.querySelector('#solicitacao-gp')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).hide();
  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacoes-auxiliares')).hide();
  $(document.querySelector('#informacoes-bd')).hide();
  $(document.querySelector('#btnCancel')).hide();
  $(document.querySelector('#divStatisticsTable')).hide();
  $('#descricao-do-motivo').hide();
  $('#motivo-reajuste').hide();

  var areaRequisitante = document.querySelector("[xname='inpareaRequisitante']")
    .value;
  ajustarCPF();

  setTimeout(function() {
    document
      .querySelector("[data-search-and-fill-for='inpcolaboradorRequisitante']")
      .click();
  }, 1);

  document
    .querySelector("[xname='inpsolicitacao']")
    .addEventListener('change', function() {
      if (
        document.querySelector("[xname='inpsolicitacao']").selectedIndex == 1
      ) {
        var dataAtual = new Date();
        if (
          dataAtual.getMonth() == 0 ||
          dataAtual.getMonth() == 3 ||
          dataAtual.getMonth() == 6 ||
          dataAtual.getMonth() == 9
        ) {
          habilitarSolicitacaoAuxilioEducacao();
        } else {
          alert(
            'Solicitação de Auxílio Educação apenas podem ser realizadas nos meses de janeiro, abril, julho e outubro, conforme política de auxílio educação.\nVolte a solicitar em um desses meses, estaremos te aguardando!\n\nEsta solicitação será cancelada automaticamente.'
          );
          $(document.getElementById('customBtn_Solicitação Cancelada')).click();
          var containerCancelar = $(
            document.querySelector('#cboxLoadedContent')
          )
            .children()
            .children();
          var botaoSim = $(containerCancelar[1]).children();
          $(botaoSim[0]).click();
        }
      } else if (
        document.querySelector("[xname='inpsolicitacao']").selectedIndex == 2
      ) {
        habilitarValorParaReembolsar();
      } else if (
        document.querySelector("[xname='inpsolicitacao']").selectedIndex == 0
      ) {
        habilitarSelecioneOpcao();
      }
    });

  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .addEventListener('change', function() {
      if (
        document.querySelector("[xname='inpajusteDeAuxilioEducacao']")
          .selectedIndex == 1
      ) {
        habilitarColaboradoresBloqueio();
      } else if (
        document.querySelector("[xname='inpajusteDeAuxilioEducacao']")
          .selectedIndex == 2
      ) {
        habilitarColaboradoresDesbloqueio();
      } else if (
        document.querySelector("[xname='inpajusteDeAuxilioEducacao']")
          .selectedIndex == 3
      ) {
        habilitarManutencaoColaboradores();
      } else if (
        document.querySelector("[xname='inpajusteDeAuxilioEducacao']")
          .selectedIndex == 0
      ) {
        habilitarSelecioneOpcao();
      }
    });

  $(document.querySelector("[xname='inpsolicitacao']")).focus(function() {
    if (areaRequisitante == '0515 - 90 - Administração de Pessoal') {
      $("[xname='inpajusteDeAuxilioEducacao']")
        .val('')
        .change();
    }
  });

  $(document.querySelector("[xname='inpajusteDeAuxilioEducacao']")).focus(
    function() {
      if (areaRequisitante == '0515 - 90 - Administração de Pessoal') {
        $("[xname='inpsolicitacao']")
          .val('')
          .change();
      }
    }
  );

  $('inp:reajusteDeValor').on('change', function() {
    var opcaoSim = document.getElementById('inpreajusteDeValor-1').checked;
    if (opcaoSim) {
      document
        .querySelector("[xname='inpvalorMensalReajustado']")
        .setAttribute('required', 'S');
      $(document.querySelector('#informacoes-valor-reajustado')).show();
      $('#motivo-reajuste').show();
      document
        .querySelector("[xname='inpmotivoDoReajuste']")
        .setAttribute('required', 'S');
    } else {
      document.querySelector('[xname="inpmotivoDoReajuste"]').selectedIndex = 0;
      document
        .querySelector("[xname='inpvalorMensalReajustado']")
        .setAttribute('required', 'N');
      $(document.querySelector('#informacoes-valor-reajustado')).hide();
      $('inp:descricaoDoMotivoDeReajuste').val('');
      $('#motivo-reajuste').hide();
      $('inp:valorMensalReajustado').val('');
      $('div:valorCalculadoParaAuxilioReajustado').text('');
      document
        .querySelector("[xname='inpmotivoDoReajuste']")
        .setAttribute('required', 'N');
    }
  });

  $('inp:motivoDoReajuste').change(habilitarCampoDescricao);

  $('inp:jaPossuiTituloDeGraducaoEouPosgraduacao').on('change', function() {
    var opcaoSim = document.getElementById(
      'inpjaPossuiTituloDeGraducaoEouPosgraduacao-1'
    ).checked;
    if (opcaoSim) {
      document
        .querySelector("[xname='inpquaisCursos']")
        .setAttribute('required', 'S');
      $(document.querySelector('#quais-cursos'))
        .children()
        .show();
    } else {
      document
        .querySelector("[xname='inpquaisCursos']")
        .setAttribute('required', 'N');
      $(document.querySelector('#quais-cursos'))
        .children()
        .hide();
    }
  });

  $('inp:jaRecebeuAuxilioEducacaoDoSistemaUnicred').on('change', function() {
    var opcaoSim = document.getElementById(
      'inpjaRecebeuAuxilioEducacaoDoSistemaUnicred-1'
    ).checked;
    if (opcaoSim) {
      document
        .querySelector("[xname='inphaQuantoTempo']")
        .setAttribute('required', 'S');
      $(document.querySelector('#quanto-tempo'))
        .children()
        .show();
    } else {
      document
        .querySelector("[xname='inphaQuantoTempo']")
        .setAttribute('required', 'N');
      $(document.querySelector('#quanto-tempo'))
        .children()
        .hide();
    }
  });

  $('inp:recebeAuxilioEducacaobolsadescontoDeOutraInstituicao').on(
    'change',
    function() {
      var opcaoSim = document.getElementById(
        'inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao-1'
      ).checked;
      if (opcaoSim) {
        document
          .querySelector("[xname='inpemQuaisProgramasEstaInserido']")
          .setAttribute('required', 'S');
        $(document.querySelector('#quais-programas'))
          .children()
          .show();
      } else {
        document
          .querySelector("[xname='inpemQuaisProgramasEstaInserido']")
          .setAttribute('required', 'N');
        $(document.querySelector('#quais-programas'))
          .children()
          .hide();
      }
    }
  );

  $('inp:valorMensalReajustado').blur(function() {
    var valorAuxilioEducacao = calcularAuxilioEducacao(
      document.querySelector("[xname='inpvalorMensalReajustado']").value
    );
    var valorAuxilioEducacaoToFixed = valorAuxilioEducacao.toFixed(2);
    var valoeAuxilioEducacaoReplace = valorAuxilioEducacaoToFixed.replace(
      '.',
      ','
    );
    document.querySelector(
      "[xname='inpvalorCalculadoParaAuxilioReajustado']"
    ).value = valoeAuxilioEducacaoReplace;
    $('div:valorCalculadoParaAuxilioReajustado').text(
      valoeAuxilioEducacaoReplace
    );
    // document.getElementById("div11665").textContent = valoeAuxilioEducacaoReplace;
  });

  $('inp:valorMensal').blur(function() {
    var valorAuxilioEducacao = calcularAuxilioEducacao(
      document.querySelector("[xname='inpvalorMensal']").value
    );
    var valorAuxilioEducacaoToFixed = valorAuxilioEducacao.toFixed(2);
    var valoeAuxilioEducacaoReplace = valorAuxilioEducacaoToFixed.replace(
      '.',
      ','
    );
    document.querySelector(
      "[xname='inpvalorCalculadoParaAuxilio']"
    ).value = valoeAuxilioEducacaoReplace;
    document.querySelector(
      "[xid='divvalorCalculadoParaAuxilio']"
    ).textContent = valoeAuxilioEducacaoReplace;
  });

  $('inp:colaboradoresParaBloqueio').change(function() {
    var elemento = document.querySelector(
      "[xname='inpcolaboradoresParaBloqueio']"
    );
    var itemSelecionado = elemento.options[elemento.selectedIndex].text;
    document.querySelector(
      "[xname='inpcolaboradorSelecionado']"
    ).value = itemSelecionado;
    document.querySelector(
      "[xid='divcolaboradorSelecionado']"
    ).textContent = itemSelecionado;
  });

  $('inp:colaboradoresParaDesbloqueio').change(function() {
    var elemento = document.querySelector(
      "[xname='inpcolaboradoresParaDesbloqueio']"
    );
    var itemSelecionado = elemento.options[elemento.selectedIndex].text;
    document.querySelector(
      "[xname='inpcolaboradorSelecionado']"
    ).value = itemSelecionado;
    document.querySelector(
      "[xid='divcolaboradorSelecionado']"
    ).textContent = itemSelecionado;
  });

  $('inp:manutencaoPara').change(function() {
    var elemento = document.querySelector("[xname='inpmanutencaoPara']");
    var itemSelecionado = elemento.options[elemento.selectedIndex].text;
    document.querySelector(
      "[xname='inpcolaboradorSelecionado']"
    ).value = itemSelecionado;
    document.querySelector(
      "[xid='divcolaboradorSelecionado']"
    ).textContent = itemSelecionado;
  });

  $('inp:proximaParcela')
    .datepicker()
    .on('changeDate', function() {
      validarDataProximaParcela();
    });

  $('inp:dataTerminoCurso')
    .datepicker()
    .on('changeDate', function() {
      validarDataTerminoCurso();
    });

  $('inp:valorMensalReajustado').change(verificarValorMaximo);
});

function isMobile() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1
  )
    return true;
}

function adaptarMobile() {
  $(document.querySelector('.mobile-floating-controls')).hide();

  document
    .querySelector("[xname='inpsolicitacao']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpvalorMensalReajustado']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpcolaboradoresParaBloqueio']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpmanutencaoPara']")
    .setAttribute('style', 'width:85%!important');

  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpquaisCursos']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inphaQuantoTempo']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inpemQuaisProgramasEstaInserido']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');

  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpcurso']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('style', 'width:85%!important');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute(
      'style',
      'height: 20px!important;width:85%!important;background-color:white'
    );
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('readonly', 'true');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('style', 'height: 20px!important;width:85%!important');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('style', 'width:85%!important');
}

function habilitarSolicitacaoAuxilioEducacao() {
  $(document.querySelector('#informacoes-tipos-solicitacao')).hide();
  $(document.querySelector('#informacoes-auxilio-educacao')).show();
  $(document.querySelector('#quais-cursos'))
    .children()
    .hide();
  $(document.querySelector('#quanto-tempo'))
    .children()
    .hide();
  $(document.querySelector('#quais-programas'))
    .children()
    .hide();
  $(document.querySelector('#informacoes-curso')).show();
  obrigatoriedadeSolicitacaoEducacao();
  alert(
    'Documentos obrigatórios para solicitação de auxílio educação:\n\n- Cópia do contrato firmado com a instituição de ensino, devidamente assinado pelas partes.\n-Descritivo do curso (registro do MEC, grade curricular com indicação dos professores).\n- Folha de registro do funcionário na cooperativa (Atualizada).\n- Atestado de matrícula.\n\nFavor anexá-los!'
  );
}

function habilitarValorParaReembolsar() {
  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).show();
  $(document.querySelector('#informacoes-valor-reembolso')).show();
  $(document.querySelector('#informacoes-valor-reajustado')).hide();
  $(document.querySelector('#informacoes-bloqueio')).hide();
  $(document.querySelector('#informacoes-desbloqueio')).hide();
  $(document.querySelector('#informacoes-manutencao')).hide();
  obrigatoriedadeSolicitacaoReembolso();
  alert(
    'Documentos obrigatórios para solicitação de reembolso de auxílio educação:\n\n- Boleto de pagamento do mês referente.\n- Comprovante de pagamento do boleto (não pode ser agendamento).\n\nFavor anexá-los!\nCaso faça reajuste de valor, comprovar o motivo do reajuste.'
  );
}

function habilitarColaboradoresBloqueio() {
  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).show();
  $(document.querySelector('#informacoes-valor-reembolso')).hide();
  $(document.querySelector('#informacoes-valor-reajustado')).hide();
  $(document.querySelector('#informacoes-bloqueio')).show();
  $(document.querySelector('#informacoes-desbloqueio')).hide();
  $(document.querySelector('#informacoes-manutencao')).hide();
  obrigatoriedadeBloquear();
  validarProprioColaboradorGPBloqueio();
}

function habilitarColaboradoresDesbloqueio() {
  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).show();
  $(document.querySelector('#informacoes-valor-reembolso')).hide();
  $(document.querySelector('#informacoes-valor-reajustado')).hide();
  $(document.querySelector('#informacoes-bloqueio')).hide();
  $(document.querySelector('#informacoes-desbloqueio')).show();
  $(document.querySelector('#informacoes-manutencao')).hide();
  obrigatoriedadeDesbloquear();
  validarProprioColaboradorGPDesbloqueio();
}

function habilitarManutencaoColaboradores() {
  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).show();
  $(document.querySelector('#informacoes-valor-reembolso')).hide();
  $(document.querySelector('#informacoes-valor-reajustado')).hide();
  $(document.querySelector('#informacoes-bloqueio')).hide();
  $(document.querySelector('#informacoes-desbloqueio')).hide();
  $(document.querySelector('#informacoes-manutencao')).show();
  obrigatoriedadeManutencao();
  validarProprioColaboradorGPManutencao();
  ajustarCoresBloqueioDesbloqueio();
}

function habilitarSelecioneOpcao() {
  $(document.querySelector('#informacoes-auxilio-educacao')).hide();
  $(document.querySelector('#informacoes-curso')).hide();
  $(document.querySelector('#informacoes-tipos-solicitacao')).hide();
  $(document.querySelector('#informacoes-valor-reembolso')).hide();
  $(document.querySelector('#informacoes-valor-reajustado')).hide();
  $(document.querySelector('#informacoes-bloqueio')).hide();
  $(document.querySelector('#informacoes-desbloqueio')).hide();
  $(document.querySelector('#informacoes-manutencao')).hide();
}

function calcularAuxilioEducacao(valorMensal) {
  var auxiliarParaRemoverPontos;
  var valorAuxilioEducacao;
  if (valorMensal.length > 6) {
    auxiliarParaRemoverPontos = valorMensal;
    for (var i = 0; i < valorMensal.split('.').length - 1; i++) {
      auxiliarParaRemoverPontos = auxiliarParaRemoverPontos.replace('.', '');
    }
    valorAuxilioEducacao = dividirPorDois(auxiliarParaRemoverPontos);
  } else {
    valorAuxilioEducacao = dividirPorDois(valorMensal);
  }
  return valorAuxilioEducacao > 1000 ? 1000 : valorAuxilioEducacao;
}

function dividirPorDois(valor) {
  var valorReplace = valor.replace(',', '.');
  var valorDividido = valorReplace / 2;

  return valorDividido;
}

function obrigatoriedadeSolicitacaoEducacao() {
  document
    .querySelector("[xname='inpsolicitacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpjaPossuiTituloDeGraducaoEouPosgraduacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpjaRecebeuAuxilioEducacaoDoSistemaUnicred']")
    .setAttribute('required', 'S');
  document
    .querySelector(
      "[xname='inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao']"
    )
    .setAttribute('required', 'S');
  document
    .querySelector(
      "[xname='inpcriteriosParaProsseguirComSolicitacaoDeAuxilioEducacao']"
    )
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inptipoAuxilioEducacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('required', 'S');
  document.querySelector("[xname='inpcurso']").setAttribute('required', 'S');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('required', 'S');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('required', 'S');

  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpreajusteDeValor']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensalReajustado']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaBloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmanutencaoPara']")
    .setAttribute('required', 'N');
}

function obrigatoriedadeSolicitacaoReembolso() {
  document
    .querySelector("[xname='inpsolicitacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaPossuiTituloDeGraducaoEouPosgraduacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaRecebeuAuxilioEducacaoDoSistemaUnicred']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpcriteriosParaProsseguirComSolicitacaoDeAuxilioEducacao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptipoAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('required', 'N');
  document.querySelector("[xname='inpcurso']").setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('required', 'N');

  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpreajusteDeValor']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpvalorMensalReajustado']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaBloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmanutencaoPara']")
    .setAttribute('required', 'N');
}

function obrigatoriedadeBloquear() {
  document
    .querySelector("[xname='inpsolicitacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaPossuiTituloDeGraducaoEouPosgraduacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaRecebeuAuxilioEducacaoDoSistemaUnicred']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpcriteriosParaProsseguirComSolicitacaoDeAuxilioEducacao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptipoAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('required', 'N');
  document.querySelector("[xname='inpcurso']").setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('required', 'N');

  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpreajusteDeValor']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensalReajustado']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaBloqueio']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmanutencaoPara']")
    .setAttribute('required', 'N');
}

function obrigatoriedadeDesbloquear() {
  document
    .querySelector("[xname='inpsolicitacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaPossuiTituloDeGraducaoEouPosgraduacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaRecebeuAuxilioEducacaoDoSistemaUnicred']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpcriteriosParaProsseguirComSolicitacaoDeAuxilioEducacao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptipoAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('required', 'N');
  document.querySelector("[xname='inpcurso']").setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('required', 'N');

  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpreajusteDeValor']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensalReajustado']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaBloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpmanutencaoPara']")
    .setAttribute('required', 'N');
}

function obrigatoriedadeManutencao() {
  document
    .querySelector("[xname='inpsolicitacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataAdmissao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaPossuiTituloDeGraducaoEouPosgraduacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpjaRecebeuAuxilioEducacaoDoSistemaUnicred']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inprecebeAuxilioEducacaobolsadescontoDeOutraInstituicao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpcriteriosParaProsseguirComSolicitacaoDeAuxilioEducacao']"
    )
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptipoAuxilioEducacao']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpinstituicaoDeEnsino']")
    .setAttribute('required', 'N');
  document.querySelector("[xname='inpcurso']").setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmodalidade']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inptotalDeHorasAula']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpproximaParcela']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpquantidadeParcelasRestantes']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataInicioCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpdataTerminoCurso']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensal']")
    .setAttribute('required', 'N');
  document
    .querySelector(
      "[xname='inpemQueEsteCursoContribuiraParaUnicredESeuDesenvolvimento']"
    )
    .setAttribute('required', 'N');

  document
    .querySelector("[xname='inpajusteDeAuxilioEducacao']")
    .setAttribute('required', 'S');
  document
    .querySelector("[xname='inpreajusteDeValor']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpvalorMensalReajustado']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaBloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
    .setAttribute('required', 'N');
  document
    .querySelector("[xname='inpmanutencaoPara']")
    .setAttribute('required', 'S');
}

function validarProprioColaboradorGPDesbloqueio() {
  var requisitante = document.querySelector(
    "[xname='inpcolaboradorRequisitante']"
  ).value;
  for (
    var i = 0;
    i <
    document.querySelector("[xname='inpcolaboradoresParaDesbloqueio']").options
      .length;
    i++
  ) {
    if (
      document.querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
        .options[i].innerText == requisitante
    ) {
      $(
        document.querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
          .options[i]
      ).prop('disabled', true);
    }
  }
}

function validarProprioColaboradorGPBloqueio() {
  var requisitante = document.querySelector(
    "[xname='inpcolaboradorRequisitante']"
  ).value;
  for (
    var i = 0;
    i <
    document.querySelector("[xname='inpcolaboradoresParaBloqueio']").options
      .length;
    i++
  ) {
    if (
      document.querySelector("[xname='inpcolaboradoresParaBloqueio']").options[
        i
      ].innerText == requisitante
    ) {
      $(
        document.querySelector("[xname='inpcolaboradoresParaBloqueio']")
          .options[i]
      ).prop('disabled', true);
    }
  }
}

function validarProprioColaboradorGPManutencao() {
  var requisitante = document.querySelector(
    "[xname='inpcolaboradorRequisitante']"
  ).value;
  for (
    var i = 0;
    i < document.querySelector("[xname='inpmanutencaoPara']").options.length;
    i++
  ) {
    if (
      rtrim(
        document.querySelector("[xname='inpmanutencaoPara']").options[i]
          .innerText
      ) == requisitante ||
      rtrim(
        document.querySelector("[xname='inpmanutencaoPara']").options[i]
          .innerText
      ) == requisitante
    ) {
      $(document.querySelector("[xname='inpmanutencaoPara']").options[i]).prop(
        'disabled',
        true
      );
    }
  }
}

function ajustarCPF() {
  var cpfPreenchido = document.querySelector("[xname='inpcpf']").value;
  for (var i = cpfPreenchido.length; i < 11; i++) {
    cpfPreenchido = '0'.concat(cpfPreenchido);
  }
  document.querySelector("[xid='divcpf']").textContent = cpfPreenchido;
  document.querySelector("[xname='inpcpf']").value = cpfPreenchido;
}

function validarDataProximaParcela() {
  var dataProximaParcela = document.querySelector("[xname='inpproximaParcela']")
    .value;
  var dataProximaParcelaSplit = dataProximaParcela.split('/');

  var dataProximaParcelaCompleta = new Date(
    dataProximaParcelaSplit[2],
    dataProximaParcelaSplit[1] - 1,
    dataProximaParcelaSplit[0]
  );
  var dataAtualCompleta = new Date();
  var dataAtualCompletaSemHorario = new Date(
    dataAtualCompleta.getFullYear(),
    dataAtualCompleta.getMonth(),
    dataAtualCompleta.getDate()
  );

  if (dataProximaParcelaCompleta < dataAtualCompletaSemHorario) {
    alert(
      'Data da próxima parcela deve ser maior ou igual que a data atual. Favor ajustar!'
    );
    document.querySelector("[xname='inpproximaParcela']").value = '';
  }
}

function validarDataTerminoCurso() {
  var dataInicioCurso = document.querySelector("[xname='inpdataInicioCurso']")
    .value;
  var dataTerminoCurso = document.querySelector("[xname='inpdataTerminoCurso']")
    .value;

  var dataInicioCursoSplit = dataInicioCurso.split('/');
  var dataTerminoCursoSplit = dataTerminoCurso.split('/');

  var dataInicioCursoCompleta = new Date(
    dataInicioCursoSplit[2],
    dataInicioCursoSplit[1] - 1,
    dataInicioCursoSplit[0]
  );
  var dataTerminoCursoCompleta = new Date(
    dataTerminoCursoSplit[2],
    dataTerminoCursoSplit[1] - 1,
    dataTerminoCursoSplit[0]
  );
  var dataAtualCompleta = new Date();

  if (dataTerminoCursoCompleta < dataInicioCursoCompleta) {
    alert(
      'Data término do curso deve ser maior ou igual que a data de ínicio do curso. Favor ajustar!'
    );
    document.querySelector("[xname='inpdataTerminoCurso']").value = '';
  }

  if (dataTerminoCursoCompleta <= dataAtualCompleta) {
    alert(
      'Data término do curso deve ser maior que a data atual. Favor ajustar!'
    );
    document.querySelector("[xname='inpdataTerminoCurso'']").value = '';
  }
}

function ajustarCoresBloqueioDesbloqueio() {
  for (
    var i = 1;
    i <
    document.querySelector("[xname='inpcolaboradoresParaBloqueio']").options
      .length;
    i++
  ) {
    for (
      var y = 1;
      y < document.querySelector("[xname='inpmanutencaoPara']").options.length;
      y++
    ) {
      if (
        document.querySelector("[xname='inpcolaboradoresParaBloqueio']")
          .options[i].innerText ==
        document.querySelector("[xname='inpmanutencaoPara']").options[y]
          .innerText
      ) {
        document
          .querySelector("[xname='inpmanutencaoPara']")
          .options[y].setAttribute('style', 'color:green');
      }
    }
  }

  for (
    var i = 1;
    i <
    document.querySelector("[xname='inpcolaboradoresParaDesbloqueio']").options
      .length;
    i++
  ) {
    for (
      var y = 1;
      y < document.querySelector("[xname='inpmanutencaoPara']").options.length;
      y++
    ) {
      if (
        document.querySelector("[xname='inpcolaboradoresParaDesbloqueio']")
          .options[i].innerText ==
        document.querySelector("[xname='inpmanutencaoPara']").options[y]
          .innerText
      ) {
        document
          .querySelector("[xname='inpmanutencaoPara']")
          .options[y].setAttribute('style', 'color:red');
      }
    }
  }
}

function preencherCampoParaSolicitacaoDeAuxilio(nomeDoColaborador) {
  $.ajax({
    //FONTE: 0515 - Verificar Auxílio Educação
    url:
      'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJgnPCMfKODzKP@SZObFcQsqJfLHQORx-NVmHEy2DoD85Jr3FHBIMPr8mVVdBxlOFJg__',
    type: 'POST',
    dataType: 'json',
    data: 'inpcolaboradorRequisitante=' + nomeDoColaborador,
    success: function(data) {
      if (data.success.length > 0) {
        $('inp:possibilidadeReembolsoAuxilio').val(
          data.success[0].fields.possibilidadeReembolsoAuxilio
        );
        $('inp:status').val(data.success[0].fields.status);
        $('div:valorParaReembolsar').text(
          data.success[0].fields.valorParaReembolsar
        );
        $('inp:valorParaReembolsar').val(
          data.success[0].fields.valorParaReembolsar
        );
        $('inp:possibilidadeSolicitacaoAuxilio').val(
          data.success[0].fields.possibilidadeSolicitacaoAuxilio
        );
      } else {
        $('inp:possibilidadeReembolsoAuxilio').val(
          'Impossibilidade de Solicitação'
        );
        $('inp:possibilidadeSolicitacaoAuxilio').val(
          'Possibilidade de Solicitação'
        );
      }
    },
    complete: function() {
      verificaPossibilidadeDeSolicitacaoDeAuxilio(
        $('inp:possibilidadeSolicitacaoAuxilio').val(),
        $('inp:possibilidadeReembolsoAuxilio').val(),
        $('inp:status').val()
      );
    },
  });
}

function verificaPossibilidadeDeSolicitacaoDeAuxilio(
  possibilidadeSolicitacao,
  possibilidadeReembolso,
  statusColaborador
) {
  var areaRequisitante = $('inp:areaRequisitante').val();

  if (
    (possibilidadeSolicitacao == 'Possibilidade de Solicitação' &&
      areaRequisitante != '0515 - 90 - Administração de Pessoal') ||
    (possibilidadeSolicitacao == '' &&
      areaRequisitante != '0515 - 90 - Administração de Pessoal')
  ) {
    $(document.querySelector('#solicitacao-colaborador')).show();
    $(document.querySelector('#solicitacao-gp')).hide();
    document
      .querySelector("[xname='inpajusteDeAuxilioEducacao']")
      .setAttribute('required', 'N');
    $(document.querySelector("[value='Solicitar Auxílio Educação']")).prop(
      'disabled',
      false
    );
    $(
      document.querySelector(
        "[value='Solicitar Reembolso de Auxílio Educação']"
      )
    ).prop('disabled', true);
  } else if (
    possibilidadeSolicitacao == 'Impossibilidade de Solicitação' &&
    possibilidadeReembolso == 'Possibilidade de Reembolso' &&
    statusColaborador == 'Desbloqueado' &&
    areaRequisitante != '0515 - 90 - Administração de Pessoal'
  ) {
    $(document.querySelector('#solicitacao-colaborador')).show();
    $(document.querySelector('#solicitacao-gp')).hide();
    document
      .querySelector("[xname='inpajusteDeAuxilioEducacao']")
      .setAttribute('required', 'N');
    $(document.querySelector("[value='Solicitar Auxílio Educação']")).prop(
      'disabled',
      true
    );
    $(
      document.querySelector(
        "[value='Solicitar Reembolso de Auxílio Educação']"
      )
    ).prop('disabled', false);
  } else if (
    possibilidadeSolicitacao == 'Impossibilidade de Solicitação' &&
    possibilidadeReembolso == 'Possibilidade de Reembolso' &&
    statusColaborador == 'Bloqueado' &&
    areaRequisitante != '0515 - 90 - Administração de Pessoal'
  ) {
    $(document.querySelector('#informacoes-solicitacao')).hide();
    alert(
      'Atualmente você precisa regularizar seu auxílio educação para solicitar reembolso. Favor contatar a área Gestao de Pessoas.\nApós regularização, voltar no processo para continuar sua solicitação.\n\nEsta solicitação em questão será cancelada automaticamente!'
    );
    $(document.getElementById('customBtn_Solicitação Cancelada')).click();
    var containerCancelar = $(document.querySelector('#cboxLoadedContent'))
      .children()
      .children();
    var botaoSim = $(containerCancelar[1]).children();
    $(botaoSim[0]).click();
  } else if (
    (possibilidadeSolicitacao == 'Possibilidade de Solicitação' &&
      areaRequisitante == '0515 - 90 - Administração de Pessoal') ||
    (possibilidadeSolicitacao == '' &&
      areaRequisitante == '0515 - 90 - Administração de Pessoal')
  ) {
    $(document.querySelector('#solicitacao-colaborador')).show();
    $(document.querySelector('#solicitacao-gp')).show();
    document
      .querySelector("[xname='inpsolicitacao']")
      .setAttribute('required', 'N');
    document
      .querySelector("[xname='inpajusteDeAuxilioEducacao']")
      .setAttribute('required', 'N');
    $(document.querySelector("[value='Solicitar Auxílio Educação']")).prop(
      'disabled',
      false
    );
    $(
      document.querySelector(
        "[value='Solicitar Reembolso de Auxílio Educação']"
      )
    ).prop('disabled', true);
  } else if (
    possibilidadeSolicitacao == 'Impossibilidade de Solicitação' &&
    possibilidadeReembolso == 'Possibilidade de Reembolso' &&
    statusColaborador == 'Desbloqueado' &&
    areaRequisitante == '0515 - 90 - Administração de Pessoal'
  ) {
    $(document.querySelector('#solicitacao-colaborador')).show();
    $(document.querySelector('#solicitacao-gp')).show();
    document
      .querySelector("[xname='inpsolicitacao']")
      .setAttribute('required', 'N');
    document
      .querySelector("[xname='inpajusteDeAuxilioEducacao']")
      .setAttribute('required', 'N');
    $(document.querySelector("[value='Solicitar Auxílio Educação']")).prop(
      'disabled',
      true
    );
    $(
      document.querySelector(
        "[value='Solicitar Reembolso de Auxílio Educação']"
      )
    ).prop('disabled', false);
  } else if (
    possibilidadeSolicitacao == 'Impossibilidade de Solicitação' &&
    possibilidadeReembolso == 'Possibilidade de Reembolso' &&
    statusColaborador == 'Bloqueado' &&
    areaRequisitante == '0515 - 90 - Administração de Pessoal'
  ) {
    $(document.querySelector("[value='Solicitar Auxílio Educação']")).prop(
      'disabled',
      true
    );
    $(
      document.querySelector(
        "[value='Solicitar Reembolso de Auxílio Educação']"
      )
    ).prop('disabled', true);
    $(document.querySelector('#solicitacao-colaborador')).hide();
    $(document.querySelector('#solicitacao-gp')).show();
    document
      .querySelector("[xname='inpsolicitacao']")
      .setAttribute('required', 'N');
    document
      .querySelector("[xname='inpajusteDeAuxilioEducacao']")
      .setAttribute('required', 'S');
    alert(
      "Atualmente você precisa regularizar seu auxílio educação para solicitar reembolso. Favor contatar a área Gestao de Pessoas.\nApós regularização, voltar no processo para continuar sua solicitação de reembolso.\n\nPode prosseguir solicitação com os demais itens liberados no campo 'Solicitação'!"
    );
  }
}

function habilitarCampoDescricao() {
  if (
    document.querySelector('[xname="inpmotivoDoReajuste"]').selectedIndex == 4
  ) {
    $('#descricao-do-motivo').show();
    document
      .querySelector("[xname='inpdescricaoDoMotivoDeReajuste']")
      .setAttribute('required', 'S');
  } else {
    $('#descricao-do-motivo').hide();
    document
      .querySelector("[xname='inpdescricaoDoMotivoDeReajuste']")
      .setAttribute('required', 'N');
    $('inp:descricaoDoMotivoDeReajuste').val('');
  }
}

function verificarPosicaoIniciadora() {
  if ($('select[name="inpCodPositionArea"]').val() != undefined) {
    var arrayAuxiliar = new Array();
    for (
      var i = 0;
      i < document.querySelector("[name='inpCodPositionArea']").options.length;
      i++
    ) {
      arrayAuxiliar.push(
        document.querySelector("[name='inpCodPositionArea']").options[i]
          .innerText
      );
    }
    arrayAuxiliar.shift();
    for (var i = arrayAuxiliar.length - 1; i >= 0; i--) {
      if (arrayAuxiliar[i].substring(4, 5) == '_') {
        arrayAuxiliar.splice(arrayAuxiliar.indexOf(arrayAuxiliar[i]), 1);
      }
    }
    $('select[name="inpCodPositionArea"]').val(
      $("option:contains('" + arrayAuxiliar[0] + "')").val()
    );
    $(
      $('select[name="inpCodPositionArea"]')
        .parent()
        .parent()
    ).hide();
  }
}

function verificarValorMaximo() {
  if (
    $('inp:valorMensalReajustado')
      .val()
      .replace(/\./g, '')
      .replace(/,/, '.') > 1999.99
  ) {
    cryo_alert('O valor do reembolso não pode ser maior que R$ 1.000,00!');
  }
}
