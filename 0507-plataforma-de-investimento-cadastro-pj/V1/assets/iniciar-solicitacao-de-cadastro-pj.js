const URL_TOKEN =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJg-CqsL3NypcCvWuQu01@2-7ejKkCN5V0IBK52p0D6LEWgV3g7RcWg6-TyDXTbVyzg__';
const URL_TOKEN_TST =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJvrH1QaVJrlzgWlabL@t4hFszHI6vLzXaLS9jlRj75U@ToRxL15Hh24treqU3l6log__';

const URL_CADASTRO_UFS_PJ =
  'https://servicos.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-juridica/v1/pessoa-juridica/';
const URL_CADASTRO_UFS_PJ_TST =
  'https://servicos-tst.e-unicred.com.br/cadastro-ufs/cadastro/pessoa-juridica/v1/pessoa-juridica/';

const URLS_DAS_FONTES_QUE_RETORNA_AS_CONTAS =
  'https://servicos.e-unicred.com.br/conta-corrente-us/conta-corrente/conta-corrente/v1/contas-correntes/';
const URLS_DAS_FONTES_QUE_RETORNA_AS_CONTAS_TST =
  'https://servicos-tst.e-unicred.com.br/conta-corrente-us/conta-corrente/conta-corrente/v1/contas-correntes/';

const urlsDasFontesQueRetornaAAgencia = [
  {
    id: '0515',
    nome: '0515 - Agência Bancária de uma Conta',
    url:
      'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJjjHsuLLfiKk1oHY8ar9PpJ09GhN-jx7ZuOJ2Yyc1brA7178QSps-IItsHERsWcG@A__',
  },
  {
    id: '0544',
    nome: '0544 - Agência Bancária de uma Conta',
    url:
      'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJjjF5hL7m6mY24W28rVa4uEkQNtvz6eSgX3BQmGfj36e-ldGjW6FJkE1AOzQP-1VVw__',
  },
  {
    id: '0566',
    nome: '0566 - Agência Bancária de uma Conta',
    url:
      'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJhJcmW5OqJdATdpBXLWyQsTilxnBqaQAmPBxrEMRD4AtPLF-r6X5aNxGfG91nJ1oeQ__',
  },
  {
    id: '0582',
    nome: '0582 - Agência Bancária de uma Conta',
    url:
      'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJkk5B-fjnaM498FOQ1PyGwTlJ6AfH5SiF7TFWeraTyHkXefGi9Vlwjs-6xzXEi0iAw__',
  },
  {
    id: '0590',
    nome: '0590 - Agência Bancária de uma Conta',
    url:
      'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJrXId9UfDSDHbBQs0lUABLQ8N7fY0@3ldIvDYKOBVneGxqb5gXAv-MwI0TqA64NhTw__',
  },
  {
    id: '9091',
    nome: '9091 - Agência Bancária de uma Conta',
    url:
      'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJvDbKTK2BrDpbvkaOO0wqNnifiYOgfEfutVo0YpxDdcKDLHvJ3GIgHl0rakuyxfceQ__',
  },
];

const tiposDeContrato = [
  {
    texto: 'Contrato Social',
    value: 'contratoSocial',
    xname: 'inp:tipoDeContrato',
  },
  {
    texto: 'Estatuto Social',
    value: 'estatutoSocial',
    xname: 'inp:tipoDeContrato',
  },
];

let dados,
  cooperativa,
  agencia,
  cnpj,
  cidade,
  token,
  objetoEnderecos,
  mensagemChecklist = [];

$(document).ready(function() {
  ajustarFormulario();
  adicionarMobileSpan();
  aplicarMascaras();
  desativarInputs();
  adicionarMensagensAuxiliares();
  mostrarOuEsconderSecaoPeloRadio();
  renderizarEventosPorClick();
  renderizarEventosPorMudancaNoSelect();
  renderizarEventosPorMudancaNoRadio();
  definirCamposObrigatorios();
  definirDocumentosObrigatorios();
});

const adicionarMobileSpan = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile =
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1;

  if (!isMobile) {
    $(
      `
        <span style='margin-left: 0.4em' class='label label-secondary'>
          <strong> Requisitante:${$('inp:nomeRequisitante').val()}</strong>
        </span>
      `
    ).insertAfter('.title span.label.label-info.flow-title');
  }
};

const ajustarFormulario = () => {
  // esconder elementos do INTEGRA
  $('#div-msg-attach-documents').hide();
  $('#RequesterInfo').hide();
  $('#btnCancel').hide();
  $('#divStatisticsTable').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  // funções de manipulação / ajustes em formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');

  // configurações default do formulário
  $('inp:operaPorContaPropria').val('Sim');
  $('inp:autorizaTransmissaoDeOrdens').val('Sim');
  $('#customBtn_Solicitação\\ Encaminhada')
    .removeAttr('onclick')
    .attr('disabled', true);
  $('.input-group-append').show();
  $('#inpoperaPorContaPropria-1').click();
  $('#inpautorizaTransmissaoDeOrdens-1').click();
  $('#inpautorizaTransmissaoDeOrdens-1').attr('disabled', true);
  $('#inpautorizaTransmissaoDeOrdens-0').attr('disabled', true);

  // adicionar Estrutura Formalization no Form
  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();

  esconderPrincipaisSections();
};

const esconderPrincipaisSections = () => {
  $('.div-cargo').hide();
  $('.div-descricao-noticia-desabonadora').hide();
  $('.div-descricao-informacao-adicional').hide();
  $('.div-descricao-faturamento').hide();
  $('.infos-relacionadas-a-conta-corrente').hide();
  $('.identificacao-do-cliente-conteudo').hide();
  $('.dados-bancarios').hide();
  $('.informacoes-de-endereco').hide();
  $('.informacoes-complementares').hide();
  $('.informacoes-formulario-kyc').hide();
  $('.checklist').hide();
  $('#ContainerAttach').hide();
};

const mostrarPrincipaisSections = () => {
  $('.identificacao-do-cliente-conteudo').show();
  $('.dados-bancarios').show();
  $('.informacoes-de-endereco').show();
  $('.informacoes-complementares').show();
  $('.informacoes-formulario-kyc').show();
  $('.checklist').show();
};

const definirCamposObrigatorios = () => {
  $('inp:cnpj').addClass('required');
  $('inp:formaDeConstituicao').addClass('required');
  $('inp:nire').addClass('required');
  $('inp:paisDeConstituicao').addClass('required');
  $('inp:paisDeDomicilio').addClass('required');
  $('inp:email').addClass('required');
  $('inp:site').addClass('required');

  $('inp:historicoDasAtividades').addClass('required');
  $('inp:expectativaDeInvestimentos').addClass('required');
  $('inp:momentoDaProspeccao').addClass('required');
  $('inp:descricaoDaProspeccao').addClass('required');

  // select
  $('inp:contaCorrente').addClass('required');
  $('inp:tipoDeContrato').addClass('required');

  // input radio
  $(
    $('inp:operaPorContaPropria')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:pessoaPoliticamenteExposta')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:pessoaPoliticamenteExpostaRelacionado')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:pessoaVinculadaAGenial')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:noticiaDesabonadora')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:informacaoAdicional')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:temPjComoSocio')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:possuiBalanco')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:possuiFaturamento')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:investimentosSuperior')
      .parent()
      .parent()[0]
  ).addClass('required-radio');

  $(
    $('inp:cadastroRealizadoPorProcuracao')
      .parent()
      .parent()[0]
  ).addClass('required-radio');
};

const definirDocumentosObrigatorios = () => {
  captureModule.capture.addDoc('Questionário Suitability');
  captureModule.capture.addDoc('Questionário Representantes');
  captureModule.capture.addDoc('Comprovante Residencial');
};

const formatarParaApenasNumeros = numero => {
  return numero.replace(/[^\d]/g, '');
};

const formatarCNPJ = numero => {
  numero = formatarParaApenasNumeros(numero);

  return numero.replace(
    /(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/,
    '$1.$2.$3/$4-$5'
  );
};

const formatarNumeroCelular = numero => {
  numero = formatarParaApenasNumeros(numero);

  numero =
    numero.length === 10
      ? numero.replace(/(\d{2})(\d{4})(\d{4})/, '($1) $2-$3')
      : numero.replace(/(\d{2})(\d{1})(\d{4})(\d{4})/, '($1) $2 $3-$4');

  return numero;
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const desativarInputs = () => {
  $('inp:endereco').prop('readonly', true);
  $('inp:numero').prop('readonly', true);
  $('inp:complemento').prop('readonly', true);
  $('inp:cep').prop('readonly', true);
  $('inp:bairro').prop('readonly', true);
  $('inp:cidade').prop('readonly', true);
  $('inp:uf').prop('readonly', true);
  $('inp:telefone').prop('readonly', true);
  $('inp:nomeDoBanco').prop('readonly', true);
  $('inp:numeroDoBanco').prop('readonly', true);
  $('inp:agencia').prop('readonly', true);

  $('inp:denominacaoSocial').prop('readonly', true);
  $('inp:cnae').prop('readonly', true);
  $('inp:dataDeConstituicao').prop('readonly', true);
};

const renderizarEventosPorMudancaNoRadio = () => {
  $('.funkyradio.div-endereco-cep').change('input[type="radio"]', () => {
    const inputRadio = $('.funkyradio.div-endereco-cep input:checked')[0]
      .labels[0].innerText;

    const valorChecked = formatarParaApenasNumeros(inputRadio);
    preencherSectionEndereco(objetoEnderecos.find(x => x.cep === valorChecked));
  });

  $('.funkyradio.div-celular').change('input[type="radio"]', () => {
    const inputRadio = $('.funkyradio.div-celular input:checked')[0].labels[0]
      .innerText;

    const valorChecked = formatarParaApenasNumeros(inputRadio);

    $('inp:telefone').val(valorChecked);
  });
};

const renderizarEventosPorMudancaNoSelect = () => {
  const selectContaCorrente = $('inp:contaCorrente');
  const selectTipoContrato = $('inp:tipoDeContrato');

  selectContaCorrente.change(function() {
    trazerDadosBancariosPeloNumeroDaConta(this.value);
  });

  selectTipoContrato.change(function() {
    validarTipoContrato(this.value);
  });
};

const adicionarLabelDocumentoObrigatorio = nomeDocumento => {
  captureModule.capture.addDoc(nomeDocumento);
};

const adicionarMensagemDeErro = (xname, mensagem) => {
  $(xname)
    .parent()
    .parent()
    .append(
      `<small class="form-text text-muted mensagem-de-erro-checklist">
        ${mensagem}
      </small>`
    );
};

const validarTipoContrato = () => {
  mensagemChecklist = mensagemChecklist.filter(
    x => x.campo !== 'inp:tipoDeContrato'
  );

  if ($('inp:tipoDeContrato').val() === 'estatutoSocial') {
    mensagemChecklist.push({
      campo: 'inp:tipoDeContrato',
      mensagem: 'Favor anexar "ATA de Eleição do Conselho e Diretoria"',
      anexado: false,
      nomeDocumento: 'ATA de Eleição do Conselho e Diretoria',
    });
  }

  validarMensagensChecklist(mensagemChecklist);
};

const validarTemPjComoSocio = () => {
  mensagemChecklist = mensagemChecklist.filter(
    x => x.campo !== 'inp:temPjComoSocio'
  );

  if (retornarValorDoInputRadioPeloXname('inp:temPjComoSocio') === 'Sim') {
    mensagemChecklist.push({
      campo: 'inp:temPjComoSocio',
      mensagem: 'Favor anexar "Contrato Social da PJ"',
      anexado: false,
      nomeDocumento: 'Contrato Social da PJ',
    });
  }

  validarMensagensChecklist(mensagemChecklist);
};

const validarPossuiBalanco = () => {
  mensagemChecklist = mensagemChecklist.filter(
    x => x.campo !== 'inp:possuiBalanco'
  );

  if (retornarValorDoInputRadioPeloXname('inp:possuiBalanco') === 'Sim') {
    mensagemChecklist.push({
      campo: 'inp:possuiBalanco',
      mensagem: 'Favor anexar "Balanço"',
      anexado: false,
      nomeDocumento: 'Balanço',
    });
  } else if (
    retornarValorDoInputRadioPeloXname('inp:possuiBalanco') === 'Não'
  ) {
    mensagemChecklist.push({
      campo: 'inp:possuiBalanco',
      mensagem:
        'Favor anexar "Declaração Patrimônio Líquido Assinada pelo Representante e Contador"',
      anexado: false,
      nomeDocumento: 'Declaração Patrimônio Líquido',
    });
  }

  validarMensagensChecklist(mensagemChecklist);
};

const validarPossuiFaturamento = () => {
  mensagemChecklist = mensagemChecklist.filter(
    x => x.campo !== 'inp:possuiFaturamento'
  );

  if (retornarValorDoInputRadioPeloXname('inp:possuiFaturamento') === 'Sim') {
    mensagemChecklist.push({
      campo: 'inp:possuiFaturamento',
      mensagem: 'Favor anexar "Faturamento Últimos 12 Meses"',
      anexado: false,
      nomeDocumento: 'Faturamento Últimos 12 Meses',
    });
  }

  validarMensagensChecklist(mensagemChecklist);
};

const validarInvestimentosSuperior = () => {
  mensagemChecklist = mensagemChecklist.filter(
    x => x.campo !== 'inp:investimentosSuperior'
  );

  if (
    retornarValorDoInputRadioPeloXname('inp:investimentosSuperior') === 'Sim'
  ) {
    mensagemChecklist.push({
      campo: 'inp:investimentosSuperior',
      mensagem:
        'Favor anexar "Declaração da Condição de Investidor Qualificado Assinado"',
      anexado: false,
      nomeDocumento: 'Declaração da Condição de Investidor Qualificado',
    });
  }

  validarMensagensChecklist(mensagemChecklist);
};

const validarCadastroRealizadoPorProcuracao = () => {
  mensagemChecklist = mensagemChecklist.filter(
    x => x.campo !== 'inp:cadastroRealizadoPorProcuracao'
  );

  if (
    retornarValorDoInputRadioPeloXname('inp:cadastroRealizadoPorProcuracao') ===
    'Sim'
  ) {
    mensagemChecklist.push({
      campo: 'inp:cadastroRealizadoPorProcuracao',
      mensagem: 'Favor anexar "Procuração"',
      anexado: false,
      nomeDocumento: 'Procuração',
    });
  }

  validarMensagensChecklist(mensagemChecklist);
};

const renderizarEventosPorClick = () => {
  const infosCooperado = $('.procurar-informacoes-do-cooperado');
  const btnSolicitacao = $('#customBtn_Solicitação\\ Encaminhada');
  const btnAnexarArquivos = $('.btn-anexar-arquivo');

  infosCooperado.click(() => validarSubmissaoCNPJ());
  btnSolicitacao.click(() => validarEnvioSolicitacao());
  btnAnexarArquivos.click(() => validarChecklist(mensagemChecklist));
};

const adicionarMensagensAuxiliares = () => {
  const autorizaTransmissaoDeOrdens =
    'AUTORIZA A TRANSMISSÃO DE ORDENS POR PROCURADOR OU REPRESENTANTE?';

  const pessoaPoliticamenteExposta =
    'EMPRESA, SÓCIOS, CONTROLADORES, ADMINISTRADORES OU PROCURADORES É CONSIDERADO PESSOA POLITICAMENTE EXPOSTA – PPE?';

  const pessoaPoliticamenteExpostaRelacionado =
    'TENHO RELACIONAMENTO COM UMA PESSOA POLITICAMENTE EXPOSTA – PPE RELACIONADO?';

  const pessoaVinculadaAGenial =
    'É PESSOA VINCULADA À GENIAL INVESTIMENTOS NOS TERMOS DO ART.1º, INCISO VI, DA ICVM 505/11?';

  const formularioKYC = 'Deve ser preenchido pelo gerente de relacionamento!';

  tippy(
    $('inp:autorizaTransmissaoDeOrdens')
      .parent()
      .parent()[0],
    {
      content: autorizaTransmissaoDeOrdens,
      theme: 'google',
    }
  );

  tippy(
    $('inp:pessoaPoliticamenteExposta')
      .parent()
      .parent()[0],
    {
      content: pessoaPoliticamenteExposta,
      theme: 'google',
    }
  );

  tippy(
    $('inp:pessoaPoliticamenteExpostaRelacionado')
      .parent()
      .parent()[0],
    {
      content: pessoaPoliticamenteExpostaRelacionado,
      theme: 'google',
    }
  );

  tippy(
    $('inp:pessoaVinculadaAGenial')
      .parent()
      .parent()[0],
    {
      content: pessoaVinculadaAGenial,
      theme: 'google',
    }
  );

  tippy($('section.informacoes-formulario-kyc')[0], {
    content: formularioKYC,
    theme: 'google',
  });
};

const retornarValorDoInputRadioPeloXname = xname => {
  const inputChecked = $(
    $(xname)
      .parent()
      .parent()[0]
  ).find('input:checked');

  return inputChecked.length ? inputChecked[0].value : null;
};

const mostrarOuEsconderSecaoPeloRadio = () => {
  $('inp:pessoaPoliticamenteExposta').on('change', function() {
    const valor = retornarValorDoInputRadioPeloXname(
      'inp:pessoaPoliticamenteExposta'
    );

    valor === 'Sim' ? $('.div-cargo').show() : $('.div-cargo').hide();
  });

  $('inp:noticiaDesabonadora').on('change', function() {
    const valor = retornarValorDoInputRadioPeloXname('inp:noticiaDesabonadora');

    valor === 'Sim'
      ? $('.div-descricao-noticia-desabonadora').show()
      : $('.div-descricao-noticia-desabonadora').hide();
  });

  $('inp:possuiFaturamento').on('change', function() {
    const valor = retornarValorDoInputRadioPeloXname('inp:possuiFaturamento');

    valor === 'Sim'
      ? $('.div-descricao-faturamento').show()
      : $('.div-descricao-faturamento').hide();
  });

  $('inp:informacaoAdicional').on('change', function() {
    const valor = retornarValorDoInputRadioPeloXname('inp:informacaoAdicional');

    valor === 'Sim'
      ? $('.div-descricao-informacao-adicional').show()
      : $('.div-descricao-informacao-adicional').hide();
  });

  $('inp:temPjComoSocio').on('change', function() {
    validarTemPjComoSocio(this.value);
  });

  $('inp:possuiBalanco').on('change', function() {
    validarPossuiBalanco(this.value);
  });

  $('inp:possuiFaturamento').on('change', function() {
    validarPossuiFaturamento(this.value);
  });

  $('inp:investimentosSuperior').on('change', function() {
    validarInvestimentosSuperior(this.value);
  });

  $('inp:cadastroRealizadoPorProcuracao').on('change', function() {
    validarCadastroRealizadoPorProcuracao(this.value);
  });
};

const buscarContasPeloCNPJ = async (cnpj, cooperativa) => {
  axios
    .get(`${URLS_DAS_FONTES_QUE_RETORNA_AS_CONTAS_TST}/?cpfCnpj=${cnpj}`, {
      headers: {
        Authorization: token,
        cooperativa: cooperativa,
      },
    })
    .then(success => {
      const contas = success.data.filter(
        x => x.situacao.toLocaleUpperCase() !== 'ENCERRADA'
      );
      contruirSelectContaCorrente(contas);
      if (contas.length === 1) {
        $('inp:contaCorrente')[0][1].selected = true;
        $('.infos-relacionadas-a-conta-corrente').show();
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      console.log(error);
    });
};

const contruirSelectContaCorrente = contas => {
  _.each(contas, conta => {
    adicionarOpcaoEmElemento(
      'inp:contaCorrente',
      `${conta.numero}`,
      conta.numero
    );
  });
};

const trazerDadosBancariosPeloNumeroDaConta = async conta => {
  const divRelacionadaAContaCorrete = $('.infos-relacionadas-a-conta-corrente');

  if (!conta.length) {
    divRelacionadaAContaCorrete.hide();
    $('inp:agencia').val('');

    return;
  }

  // filtra a fonte de dados baseado na cooperativa
  const fonteDeDados = urlsDasFontesQueRetornaAAgencia.find(
    x => x.id === cooperativa
  );

  await axios
    .post(fonteDeDados.url, { inpconta: conta })
    .then(success => {
      if (success.data.success.length) {
        agencia = success.data.success[0].txt;
      } else {
        toastr.error('Erro de comunicaçao com o serviço!');

        return;
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      console.log(error);
      return;
    });

  $('inp:agencia').val(agencia);

  divRelacionadaAContaCorrete.show();
};

const validarSubmissaoCNPJ = async () => {
  toggleSpinner();
  limparFormulario();

  // TODO: descomentar mais tarde!
  // cooperativa = $("inp:cooperativaRequisitante").val();

  cnpj = $('inp:cnpj').val();

  if (cnpj.length !== 18) {
    toastr.error('Digite um CNPJ com 14 dígitos!');
    toggleSpinner();

    return;
  }

  cnpj = formatarParaApenasNumeros(cnpj);

  await buscarDadosCooperado(cooperativa, cnpj);
  toggleSpinner();
};

const submeterFormulario = () => {
  toastr.success('Formulário submetido com sucesso!');

  return;
  doAction('Solicitação Encaminhada', false, false);
};

const aplicarMascaras = () => {
  aplicarMascaraCelular();
  aplicarMascaraCNPJ();
};

const aplicarMascaraCelular = () => {
  $('inp:telefone').mask('(99) 9 9999-9999');
};

const aplicarMascaraCNPJ = () => {
  $('inp:cnpj').mask('99.999.999/9999-99');
};

const validarInput = elemento => {
  !elemento.value.length
    ? $(elemento)
        .addClass('is-invalid')
        .removeClass('is-valid')
    : $(elemento)
        .addClass('is-valid')
        .removeClass('is-invalid');
};

const validarMensagensChecklist = checklist => {
  $('.mensagem-de-erro-checklist').remove();

  _.each(checklist, el => {
    adicionarMensagemDeErro(el.campo, el.mensagem);
  });
};

const validarCheckboxCelular = () => {
  $('.funkyradio.div-endereco-cep input:checked').length
    ? $('.funkyradio.div-endereco-cep label')
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $('.funkyradio.div-endereco-cep label')
        .addClass('is-invalid')
        .removeClass('is-valid');
};

const validarCheckboxEndereco = () => {
  $('.funkyradio.div-celular input:checked').length
    ? $('.funkyradio.div-celular label')
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $('.funkyradio.div-celular label')
        .addClass('is-invalid')
        .removeClass('is-valid');
};

validarCargo = () => {
  if ($('.div-cargo').is(':visible')) {
    !!$('inp:cargo').val().length
      ? $('inp:cargo')
          .addClass('is-valid')
          .removeClass('is-invalid')
      : $('inp:cargo')
          .addClass('is-invalid')
          .removeClass('is-valid');
  } else {
    $('inp:cargo').removeClass('is-invalid');
    $('inp:cargo').removeClass('is-valid');
  }
};

validarNoticiaDesabonadora = () => {
  if ($('.div-descricao-noticia-desabonadora').is(':visible')) {
    !!$('inp:descricaoNoticiaDesabonadora').val().length
      ? $('inp:descricaoNoticiaDesabonadora')
          .addClass('is-valid')
          .removeClass('is-invalid')
      : $('inp:descricaoNoticiaDesabonadora')
          .addClass('is-invalid')
          .removeClass('is-valid');
  } else {
    $('inp:descricaoNoticiaDesabonadora').removeClass('is-invalid');
    $('inp:descricaoNoticiaDesabonadora').removeClass('is-valid');
  }
};

validarFaturamento = () => {
  if ($('.div-descricao-faturamento').is(':visible')) {
    !!$('inp:descricaoFaturamento').val().length
      ? $('inp:descricaoFaturamento')
          .addClass('is-valid')
          .removeClass('is-invalid')
      : $('inp:descricaoFaturamento')
          .addClass('is-invalid')
          .removeClass('is-valid');
  } else {
    $('inp:descricaoFaturamento').removeClass('is-invalid');
    $('inp:descricaoFaturamento').removeClass('is-valid');
  }
};

validarInformacaoAdicional = () => {
  if ($('.div-descricao-informacao-adicional').is(':visible')) {
    !!$('inp:descricaoInformacaoAdicional').val().length
      ? $('inp:descricaoInformacaoAdicional')
          .addClass('is-valid')
          .removeClass('is-invalid')
      : $('inp:descricaoInformacaoAdicional')
          .addClass('is-invalid')
          .removeClass('is-valid');
  } else {
    $('inp:descricaoInformacaoAdicional').removeClass('is-invalid');
    $('inp:descricaoInformacaoAdicional').removeClass('is-valid');
  }
};

const validarInputRadio = group => {
  $(group).find('input:checked').length
    ? $(group)
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $(group)
        .addClass('is-invalid')
        .removeClass('is-valid');
};

const validarChecklist = checklist => {
  if (validarFormulario()) {
    $('.checklist input').attr('disabled', true);
    $('.checklist select').attr('disabled', true);

    $('.btn-anexar-arquivo').attr('disabled', true);
    $('#customBtn_Solicitação\\ Encaminhada').attr('disabled', false);

    _.each(checklist, elemento => {
      adicionarLabelDocumentoObrigatorio(elemento.nomeDocumento);
    });

    $('#ContainerAttach').show();
  }

  validarMensagensChecklist(checklist);
};

const validarEnvioSolicitacao = () => {
  if (!!validarFormulario() && !$('span.badge-secondary').length) {
    submeterFormulario();
  } else {
    toastr.error('Campos ou documentos obrigatórios, favor verificar!');
  }
};

const validarFormulario = () => {
  validarCheckboxEndereco();
  validarCheckboxCelular();

  // inputs que tem o comportamento de aparecer e
  // de acordo com o select
  validarCargo();
  validarNoticiaDesabonadora();
  validarInformacaoAdicional();
  validarFaturamento();

  const inputsObrigatorios = $('.form-integra .required');
  const inputsRadioObrigatorios = $('.form-integra .required-radio input');
  const divsRadioObrigatorios = $('.form-integra .required-radio');

  inputsRadioObrigatorios.change(() => {
    _.each(divsRadioObrigatorios, elemento => validarInputRadio(elemento));
  });

  inputsObrigatorios.change(() => {
    _.each(inputsObrigatorios, elemento => validarInput(elemento));
  });

  inputsRadioObrigatorios.trigger('change');
  inputsObrigatorios.trigger('change');

  if (!$('.is-invalid').length) {
    return true;
  } else {
    toastr.error('Campos obrigatórios, favor verificar!');

    return false;
  }
};

const buscarToken = async () => {
  await axios
    .get(URL_TOKEN_TST)
    .then(success => {
      if (success.data.success[0].fields.access_token) {
        token = `Bearer ${success.data.success[0].fields.access_token}`;
      } else {
        toastr.error('Erro de comunicaçao com o serviço!');
      }
    })
    .catch(error => {
      toastr.error('Erro de comunicaçao com o serviço!');

      console.log(error);
    });
};

const adicionarOpcaoEmElemento = (xname, texto, value) => {
  $(xname).append(
    $('<option>', {
      value: value,
      text: texto,
    })
  );
};

const buscarDadosCooperado = async (cooperativa, cnpj) => {
  await buscarToken();

  await axios
    .get(`${URL_CADASTRO_UFS_PJ_TST}${cnpj}`, {
      headers: {
        Authorization: token,
        cooperativa: cooperativa,
      },
    })
    .then(success => {
      toastr.success(
        `Informações referente ao CNPJ ${formatarCNPJ(cnpj)}
         carregadas com sucesso!`
      );

      dados = success.data;

      preencherFormulario(dados);
    })
    .catch(error => {
      if (error.response.status === 404) {
        toastr.error(
          `Pessoa não encontrada com CNPJ ${formatarCNPJ(cnpj)}`,
          'Ocorreu um erro!'
        );
      } else {
        toastr.error('Ocorreu algum erro inesperado!');
      }
    });
};

const limparFormulario = () => {
  if (!!$('.div-celular').length) $('.div-celular')[0].innerHTML = '';
  if (!!$('.div-endereco-cep').length) $('.div-endereco-cep')[0].innerHTML = '';

  $('inp:denominacaoSocial').val('');
  $('inp:cnae').val('');
  $('inp:dataDeConstituicao').val('');
  $('inp:formaDeConstituicao').val('');
  $('inp:nire').val('');

  $('inp:nomeDoBanco').val('');
  $('inp:numeroDoBanco').val('');
  $('inp:agencia').val('');
  $('inp:endereco').val('');
  $('inp:numero').val('');
  $('inp:complemento').val('');
  $('inp:cep').val('');
  $('inp:bairro').val('');
  $('inp:cidade').val('');
  $('inp:uf').val('');
  $('inp:paisDeConstituicao').val('');
  $('inp:paisDeDomicilio').val('');

  $('inp:telefone').val('');
  $('inp:email').val('');
  $('inp:site').val('');

  $('inp:historicoDasAtividades').val('');
  $('inp:expectativaDeInvestimentos').val('');
  $('inp:momentoDaProspeccao').val('');
  $('inp:descricaoDaProspeccao').val('');

  // limpar select
  $('inp:contaCorrente')[0].innerHTML = '<option value="">Selecione</option>';

  // esconder div conta corrente
  $('.infos-relacionadas-a-conta-corrente').hide();
};

const preencherFormulario = async data => {
  mostrarPrincipaisSections();

  // section identificacao-do-cliente-conteudo
  $('inp:denominacaoSocial').val(R.path(['nome'], data));
  $('inp:cnae').val(
    R.path(['cnae', 'atividadeEconomicaPrincipal', 'codigo'], data)
  );
  $('inp:dataDeConstituicao').val(
    R.path(['associacao', 'dataAssociacao'], data)
  );

  // section informacoes-de-endereco
  construirSectionEnderecos(data.enderecos);
  objetoEnderecos = data.enderecos;
  if (data.enderecos.length === 1) $('.input-cep')[0].click();

  // section dados-bancarios
  $('inp:nomeDoBanco').val('Unicred do Brasil');
  $('inp:numeroDoBanco').val('136');

  buscarContasPeloCNPJ(cnpj, cooperativa);

  // section informacoes-complementares
  construirSectionCelulares(data.contatos.telefones);
  if (data.contatos.telefones.length === 1) $('.input-celular')[0].click();

  construirSelectTipoContrato(tiposDeContrato);
};

const construirElementoInputRadioSelect = (texto, index, identificador) => {
  return `
      <div class="col-xs-12 col-md-2">
        <div class="form-group">
          <div class="funkyradio-success">
            <input
              class="input-${identificador}"
              type="radio"
              name="radio-${identificador}"
              id="radio-${identificador}-${index}"
            />
            <label
              class="label-group-${identificador}"
              for="radio-${identificador}-${index}"
            >
              ${texto}
            </label>
          </div>
        </div>
      </div>
    `;
};

const construirSectionEnderecos = enderecos => {
  _.each(enderecos, (endereco, index) => {
    const elemento = construirElementoInputRadioSelect(
      endereco.cep,
      index,
      'cep'
    );

    $('.funkyradio.div-endereco-cep').append(elemento);
  });
};

const construirSectionCelulares = celulares => {
  _.each(celulares, (celular, index) => {
    const elemento = construirElementoInputRadioSelect(
      `${formatarNumeroCelular(celular.ddd + celular.numero)}`,
      index,
      'celular'
    );

    $('.funkyradio.div-celular').append(elemento);
  });
};

const construirSelectTipoContrato = tiposDeContrato => {
  _.each(tiposDeContrato, tipo => {
    adicionarOpcaoEmElemento(tipo.xname, tipo.texto, tipo.value);
  });
};

const preencherSectionEndereco = endereco => {
  $('inp:cidade').val(endereco.nomeCidade);
  $('inp:endereco').val(endereco.logradouro);
  $('inp:numero').val(endereco.numero);
  $('inp:complemento').val(endereco.complemento);
  $('inp:cep').val(endereco.cep);
  $('inp:bairro').val(endereco.bairro);
  $('inp:uf').val(endereco.uf);
};
