const urlFonteDeDadosTelefones =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJjhIlRTDkdT@oWTWoCRmdOWIM5Ls1wTTTW-9vbT2x287tEL0-uo5ZVQXSD1CD0FJDA__';
const urlFonteDeDadosContas =
  'https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJsFpIF-BttHb3-2@yEGJxhOEBt-qmia9rFzlRXgKpQ0YmyIv4XbG81Zj6UnGPfp3oQ__';

$(document).ready(function() {
  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');

  toggleSpinner();
  desativarInputs();
  esconderSections();
  adicionarSpan();
  adicionarSectionNumeroCelular();
  esconderElementosDeafult();
  ajustarFormulario();
  aplicarMascaraCelular();

  $('.input-group-append').show();

  $('.funkyradio').on('change', 'input[type="radio"]', () =>
    toggleInputNovoNumero()
  );
  $('inp:linhaCredito').change(() => esconderMostrarLinhaCreditoSection());
  $('inp:seguroPrestamista').change(() => esconderMostrarMotivoSection());

  $('.search-proposta').click(() => validaInputConta());
  $('#customBtn_Solicitação\\ Encaminhada').click(() => validarFormulario());
});

limparForm = () => {
  const inputs = $('.form-integra input');

  $('inp:linhaCredito').val('');
  $('inp:linhaCredito').removeClass('is-invalid is-valid');

  $('inp:seguroPrestamista').val('');
  $('inp:seguroPrestamista').removeClass('is-invalid is-valid');

  $('inp:vencimento').val('');
  $('inp:vencimento').removeClass('is-invalid is-valid');

  $('.funkyradio')[0].innerHTML = '';
  $('.label-contato-numero').remove();
  adicionarSectionNumeroCelular();

  _.each(inputs, elemento => {
    $('.label-prestamista label').removeClass('is-invalid');
    $('.label-prestamista label').removeClass('is-valid');

    if ($(elemento).attr('xname') === 'inpconta') {
      console.log(`Eu sou inpconta: ${$(elemento).attr('xname')}`);
    } else {
      $(elemento).val('');
      $(elemento).removeClass('is-invalid is-valid');
    }
  });
};

aplicarMascaraCelular = () => {
  $('inp:telefone').mask('(99) 9 9999-9999');
};

ajustarFormulario = () => {
  //hide elements from INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col')
    .addClass('span12')
    .removeClass('span9');
  $('div.span9.buttons-col')
    .addClass('span12')
    .removeClass('span9');

  //funções de manipulação / ajustes em formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');
};

toggleInputNovoNumero = () => {
  if ($('#another').is(':checked')) {
    $('.adicionar-contato-numero').show();
  } else {
    $('.adicionar-contato-numero').hide();
  }
};

mostrarSections = () => {
  $('.informacoes-do-cooperado').show();
  $('.informacoes-linha-de-credito').show();
};

esconderElementosDeafult = () => {
  $('#btnCancel').hide();
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
};

esconderSections = () => {
  $('.informacoes-do-cooperado').hide();
  $('.informacoes-do-2-titular').hide();
  $('.informacoes-linha-de-credito').hide();
  $('.numero-proposta').hide();
  $('.section-relacionada-linha-de-credito').hide();
  $('.adicionar-contato-numero').hide();
  $('.div-motivo').hide();
};

desativarInputs = () => {
  $('inp:cooperado').prop('readonly', true);
  $('inp:dataAnaliseCadastral').prop('readonly', true);
  $('inp:gerenteConta').prop('readonly', true);
  $('inp:mesAnoRefRenda').prop('readonly', true);
  $('inp:agencia').prop('readonly', true);
  $('inp:segundoTitular').prop('readonly', true);
  $('inp:segundoTitularAnaliseCadastral').prop('readonly', true);
  $('inp:segundoTitularMesAnoRefDaRenda').prop('readonly', true);

  $('inp:sigla').prop('readonly', true);
  $('inp:descricao').prop('readonly', true);
  $('inp:tipoCredito').prop('readonly', true);
  $('inp:quantidadeMinimaParcela').prop('readonly', true);
  $('inp:quantidadeMaximaParcela').prop('readonly', true);
};

esconderMostrarLinhaCreditoSection = () => {
  const linhaSelecionada = $('inp:linhaCredito').val();

  linhaSelecionada.length
    ? $('.section-relacionada-linha-de-credito').show()
    : $('.section-relacionada-linha-de-credito').hide();
};

esconderMostrarMotivoSection = () => {
  const prestamistaSelecionado = $('inp:seguroPrestamista').val();

  if (prestamistaSelecionado === 'Não') {
    $('.div-motivo').show();
  } else {
    $('.div-motivo').hide();
  }
};

adicionarSpan = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile =
    userAgent.search(
      /(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i
    ) != -1;

  if (!isMobile) {
    $(
      `<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${$(
        'inp:nomeRequisitante'
      ).val()}</strong></span>`
    ).insertAfter('.title span.label.label-info.flow-title');
  }
  console.log('isMobile: ', isMobile);
};

adicionarSectionNumeroCelular = () => {
  adicionarLabelDoNovoNumero();
  adicionarRadioGrupo();
};

adicionarLabelDoNovoNumero = () => {
  const div = `
    <label class="label-contato-numero font-weight-bold">
      Selecione/preencha um celular para comunicarmos automaticamente o(a) Cooperado(a):
    </label>
  `;

  $('.contato-numero').prepend(div);
};

adicionarRadioGrupo = () => {
  const div = `
    <div class="col-xs-12 col-md-2">
      <div class="form-group">
        <div class="funkyradio-success">
          <input type="radio" name="radio" id="another" />
          <label class="label-group-cellphone" for="another">Outro</label>
        </div>
      </div>
    </div>
  `;

  $('.funkyradio').append(div);
};

toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

validaInputConta = () => {
  const inputNumeroDaConta = $('inp:conta');

  if (inputNumeroDaConta.val() <= 0) {
    toastr.error('Digite uma conta válida!');

    return;
  }

  esconderSections();
  limparForm();

  trazerDadosPeloNumeroDaConta(inputNumeroDaConta.val());
};

trazerTelefonesPeloNumeroDaConta = async numero => {
  toggleSpinner();

  try {
    const { data } = await axios.post(urlFonteDeDadosTelefones, {
      inpconta: numero,
    });

    if (data && data.success && data.success[0]) {
      toggleSpinner();

      console.log('data.success: ', data.success);

      data.success.forEach((cellphone, i) => {
        $('.funkyradio').prepend(cardTelefone(cellphone.txt, i));
      });

      toastr.success('Infomações carregadas com sucesso!');
    } else {
      toggleSpinner();

      toastr.error('Não existe números associados para esta conta!');
    }
  } catch (error) {
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );

    toggleSpinner();
  }
};

cardTelefone = (telefone, i) => {
  return `<div class="col-xs-12 col-md-2">
      <div class="form-group">
        <div class="funkyradio-success">
          <input class="input-cellphone" type="radio" name="radio" id="radio${i}" />
          <label class="label-group-cellphone" for="radio${i}">${formatarCelular(
    telefone
  )}</label>
        </div>
      </div>
  </div>`;
};

formatarParaApenasNumeros = numero => {
  return numero.replace(/[^\d]/g, '');
};

formatarCelular = numero => {
  numero = formatarParaApenasNumeros(numero);

  if (numero.length === 10) {
    return numero.replace(/(\d{2})(\d{4})(\d{4})/, '($1) $2-$3');
  } else if (numero.length === 11) {
    return numero.replace(/(\d{2})(\d{1})(\d{4})(\d{4})/, '($1) $2 $3-$4');
  }
};

trazerDadosPeloNumeroDaConta = async numero => {
  toggleSpinner();

  try {
    const { data } = await axios.post(urlFonteDeDadosContas, {
      inpconta: numero,
    });

    if (data && data.success && data.success[0]) {
      toggleSpinner();

      toastr.success('Infomações carregadas com sucesso!');
      trazerTelefonesPeloNumeroDaConta(numero);
      preencherFormulario(data.success[0]);
    } else {
      toggleSpinner();

      toastr.error(
        'Não encontramos esta informação. Ajuste e tente novamente!'
      );
    }
  } catch (error) {
    toastr.error(
      'Ocorreu um erro para buscar a informação! Contate o suporte.'
    );

    toggleSpinner();
  }
};

preencherFormulario = data => {
  $('inp:cooperado').val(data.fields.cooperado);
  $('inp:dataAnaliseCadastral').val(data.fields.dataAnaliseCadastral);
  $('inp:gerenteConta').val(data.fields.gerenteConta);
  $('inp:mesAnoRefRenda').val(data.fields.mesAnoRefRenda);
  $('inp:agencia').val(data.fields.agencia);

  $('inp:segundoTitular').val(data.fields.segundoTitular);
  $('inp:segundoTitularAnaliseCadastral').val(
    data.fields.segundoTitularAnaliseCadastral
  );
  $('inp:segundoTitularMesAnoRefDaRenda').val(
    data.fields.segundoTitularMesAnoRefDaRenda
  );

  mostrarSections();
  data.fields.segundoTitular
    ? $('.informacoes-do-2-titular').show()
    : $('.informacoes-do-2-titular').hide();
};

validarInput = elemento => {
  !elemento.value.length
    ? $(elemento)
        .addClass('is-invalid')
        .removeClass('is-valid')
    : $(elemento)
        .addClass('is-valid')
        .removeClass('is-invalid');

  if ($(elemento).attr('xname') === 'inpquantidadeParcelas') {
    const quantidadeParcelas = parseInt($('inp:quantidadeParcelas').val());

    if (!$('inp:quantidadeParcelas').val().length) return;

    const quantidadeMinimaParcela = parseInt(
      $('inp:quantidadeMinimaParcela').val()
    );
    const quantidadeMaximaParcela = parseInt(
      $('inp:quantidadeMaximaParcela').val()
    );

    if (
      quantidadeParcelas >= quantidadeMinimaParcela &&
      quantidadeParcelas <= quantidadeMaximaParcela
    ) {
      $(elemento)
        .addClass('is-valid')
        .removeClass('is-invalid');
    } else {
      $(elemento)
        .addClass('is-invalid')
        .removeClass('is-valid');
      toastr.error(
        '"Quantidade de Parcelas" não pode ser menor que "Quantidade Mínima Parcela" e não pode ser maior que "Quantidade Máxima Parcela"'
      );
    }
  }
};

validarTelefone = () => {
  if ($('.funkyradio input:checked').length) {
    const cellphoneValue = $('.funkyradio input:checked').siblings('label')[0]
      .innerHTML;

    if (cellphoneValue !== 'Outro') {
      $('inp:telefone').val(cellphoneValue);
    }
  }

  $('.funkyradio input:checked').length
    ? $('.label-group-cellphone')
        .addClass('is-valid')
        .removeClass('is-invalid')
    : $('.label-group-cellphone')
        .addClass('is-invalid')
        .removeClass('is-valid');
};

validarMotivo = () => {
  if ($('.div-motivo').is(':visible')) {
    !!$('inp:motivo').val().length
      ? $('inp:motivo')
          .addClass('is-valid')
          .removeClass('is-invalid')
      : $('inp:motivo')
          .addClass('is-invalid')
          .removeClass('is-valid');
  } else {
    $('inp:motivo').removeClass('is-invalid');
    $('inp:motivo').removeClass('is-valid');
  }
};

validarFormulario = () => {
  validarTelefone();
  validarMotivo();

  const inputsObrigatorios = $('.form-integra [required="S"]');

  inputsObrigatorios.change(() => {
    _.each(inputsObrigatorios, elemento => validarInput(elemento));
  });
  inputsObrigatorios.trigger('change');

  if (!$('.is-invalid').length && !$('span.badge-secondary').length) {
    submeterFormulario();
  } else {
    toastr.error('Campos ou documentos obrigatórios, favor verificar!');
  }
};

submeterFormulario = () => {
  const cellphone = formatarParaApenasNumeros($('inp:telefone').val());

  esconderSections();
  $('inp:telefone').val(cellphone);
  $('.funkyradio').innerHTML = '';
  $('.label-contato-numero').remove();

  toastr.success('Formulário submetido com sucesso!');
  doAction('Solicitação Encaminhada', false, false);
};
