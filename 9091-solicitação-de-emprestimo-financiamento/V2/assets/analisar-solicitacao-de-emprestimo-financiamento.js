$(document).ready(function () {
  $('#customBtn_Proposta\\ Criada').removeAttr('onclick');

  $('#customBtn_Proposta\\ Criada').click(() => validaForm());

  $('.div-motivo').hide();

  mostrarEsconderSegundoTitularSection();
  ajustarFormulario();
  adicionarSpan();
  esconderElementosDeafult();
});

esconderElementosDeafult = () => {
  $('#btnCancel').hide();
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
}

validaForm = () => {
  const inputsObrigatorios = $('.form-integra [required]');

  inputsObrigatorios.change(() => {
    _.each(inputsObrigatorios, (elemento) => validarInput(elemento));
  });
  inputsObrigatorios.trigger('change');

  $('.is-invalid').length ? toastr.error('Campos obrigatórios não preenchidos, favor verificar!') : submeterFormulario();
}

validarInput = (elemento) => {
  !elemento.value.length ?
    $(elemento).addClass('is-invalid').removeClass('is-valid') :
    $(elemento).addClass('is-valid').removeClass('is-invalid');
}

submeterFormulario = () => {
  toastr.success('Formulário submetido com sucesso!');

  doAction('Proposta Criada', false, false);
}

mostrarEsconderSegundoTitularSection = () => {
  !!$('.informacoes-do-2-titular .form-group>div')[0].innerHTML.length ?
    $('.informacoes-do-2-titular').show() :
    $('.informacoes-do-2-titular').hide();
}

ajustarFormulario = () => {
  //hide elements from INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col').addClass('span12').removeClass('span9');
  $('div.span9.buttons-col').addClass('span12').removeClass('span9');

  //funções de manipulação / ajustes em formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');
}

adicionarSpan = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile = userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1;

  if (!isMobile) {
    $(`<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${$('inp:nomeRequisitante').val()}</strong></span>`).insertAfter('.title span.label.label-info.flow-title');
  }
  console.log('isMobile: ', isMobile);
}
