$(document).ready(function () {
  ajustarFormulario();
  adicionarSpan();
  mostrarEsconderSegundoTitularSection();
  mostrarEsconderMotivoSection();
  esconderElementosDeafult();
});

esconderElementosDeafult = () => {
  $('#btnCancel').hide();
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('#divStatisticsTable').hide();
}

mostrarEsconderMotivoSection = () => {
  !!$('.div-motivo .form-group>div')[0].innerHTML.length ?
    $('.div-motivo').show() :
    $('.div-motivo').hide();
}

mostrarEsconderSegundoTitularSection = () => {
  !!$('.informacoes-do-2-titular .form-group>div')[0].innerHTML.length ?
    $('.informacoes-do-2-titular').show() :
    $('.informacoes-do-2-titular').hide();
}

ajustarFormulario = () => {
  //hide elements from INTEGRA
  $($('div.well')[0]).hide();
  $('#RequesterInfo').hide();
  $('div.span3.lateral-col').remove();
  $('div.span9.main-col').addClass('span12').removeClass('span9');
  $('div.span9.buttons-col').addClass('span12').removeClass('span9');

  //funções de manipulação / ajustes em formulário
  $('.form-group [XTYPE="TEXT"]').css('display', 'block');
  $('.form-group [XTYPE="SELECT"]').css('display', 'block');
  $('.form-group [XTYPE="DATA"]').css('display', 'block');
  $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');
}

adicionarSpan = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  const isMobile = userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1;

  if (!isMobile) {
    $(`<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${$('inp:nomeRequisitante').val()}</strong></span>`).insertAfter('.title span.label.label-info.flow-title');
  }
  console.log('isMobile: ', isMobile);
}
