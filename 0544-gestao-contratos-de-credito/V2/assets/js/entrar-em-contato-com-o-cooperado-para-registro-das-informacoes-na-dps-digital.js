$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();
  setAssociationFunctionality();
  ajustarFormulario();
  spinnerLoaderOnAnalyse();

  const tipoSolicitacao = $('inp:tipo').val();
  const numeroEmprestimo = $('inp:numero').val();

  tipoSolicitacao === 'Borderô de Desconto'
    ? buscarGarantiaCheque(numeroEmprestimo)
    : buscarGarantiasAvalista(numeroEmprestimo);

  $('#customBtn_DPS\\ Preenchida').click(elemento => {
    verificarObrigatoriedadesParaSubmeterAtividade();
  });
});

const ajustarFormulario = () => {
  // esconder elementos do formulário
  $('#coluna-input-arquivamento').hide();
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();

  $('#customBtn_DPS\\ Preenchida').removeAttr('onclick');

  //Adicionar Estrutura Formalization no Form
  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();

  captureModule.capture.addDoc('DPS Preenchida');
};

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  apresentarInformacoesGeradasAoUsuario();
};

const apresentarInformacoesGeradasAoUsuario = () => {
  const tipoSolicitacao = $('inp:tipo').val();
  const assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipoSolicitacao) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();
      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  $('inp:possuiPendenciaExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-eighthRow').hide()
    : $('#div-informacoes-emprestimo-eighthRow').show();

  assinaturaDigital === 'Sim'
    ? $('#div-informacoes-emprestimo-seventhRow').show()
    : $('#div-informacoes-emprestimo-seventhRow').hide();

  $('#section-informacoes-garantias').show();
  toastr.success(`Informações do(a) ${tipoSolicitacao} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const verificarObrigatoriedadesParaSubmeterAtividade = async () => {
  await _.forEach(
    $('div#invalid-feedback-envio-requisicao'),
    invalidFeedbackRequest => {
      $(invalidFeedbackRequest).remove();
    }
  );

  const elementoBadgeSecondaryListaDocumentos = $(
    '#customizedUpload span.badge-secondary'
  );
  let listaDocumentosParaAnexar = [];
  await _.forEach(elementoBadgeSecondaryListaDocumentos, badgeDocumento =>
    listaDocumentosParaAnexar.push(
      $(badgeDocumento)
        .text()
        .trim()
    )
  );

  if (listaDocumentosParaAnexar.length > 0) {
    $('#customizedUpload').append(
      `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> DPS Preenchida Deve ser Anexado!</div>`
    );
    toastr.error('Há Documentos e Informações Obrigatórias. Verifique!');
    $('#invalid-feedback-envio-requisicao').show();
  } else {
    doAction('DPS Preenchida', false, false);
  }
};
