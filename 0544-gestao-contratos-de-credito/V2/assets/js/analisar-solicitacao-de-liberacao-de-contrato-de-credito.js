$(document).ready(function() {
  hideIntegraElements();
  fullScreen();
  getInstanceHeader();
  ajustarFormulario();
  spinnerLoaderOnAnalyse();
  setAssociationFunctionality();

  const tipoSolicitacao = $('inp:tipo').val();
  const numeroEmprestimo = $('inp:numero').val();
  const contratoMae = $('inp:numeroContratoAnteriorMae').val();

  if (
    tipoSolicitacao !== 'Borderô de Desconto' ||
    (tipoSolicitacao === 'Borderô de Desconto' && contratoMae !== '')
  ) {
    $('inp:numero').attr('readOnly', true);
  }

  numeroEmprestimoGlobal.push(numeroEmprestimo);

  tipoSolicitacao === 'Borderô de Desconto'
    ? buscarGarantiaCheque(numeroEmprestimo)
    : buscarGarantiasAvalista(numeroEmprestimo);

  // adiciona painel atrás dos critérios de uma aba de formalização
  $('#tab1 p').click(e => {
    $('#tab1')
      .children()
      .children()
      .each(function(k, v) {
        if ($(v).find('[checkviewer] p').length === 0) return;
        $(v)
          .find('[checkviewer]')
          .addClass('conferencia-card');
      });
  });

  $('#claim-unclaim-task').mouseout(elemento => {
    const tipoSolicitacao = $('inp:tipo').val();
    const contratoMae = $('inp:numeroContratoAnteriorMae').val();

    if (
      tipoSolicitacao !== 'Borderô de Desconto' ||
      (tipoSolicitacao === 'Borderô de Desconto' && contratoMae !== '')
    ) {
      $('inp:numero').attr('readOnly', true);
    }
  });

  $('#procurar-informacoes-emprestimo').click(elemento => {
    validarSubmissaoBordero();
  });
});

const numeroEmprestimoGlobal = [];

function ajustarFormulario() {
  analisysModule.analisys.load();

  // esconder elementos do INTEGRA
  $($('#btnsFormalization').children()[2]).hide();

  // esconder elementos do formulário
  $('#coluna-input-arquivamento').hide();
  $('#section-informacoes-emprestimo div.row').hide();
  $('#section-informacoes-arquivamento').hide();

  // adicionar documento para apoiar na atividade
  captureModule.capture.buildDocsHtml();
  captureModule.capture.appendModule();
  captureModule.capture.uploadChange();

  captureModule.capture.addDoc('Gravame');
}

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  apresentarInformacoesGeradasAoUsuario();
};

const apresentarInformacoesGeradasAoUsuario = () => {
  const tipoSolicitacao = $('inp:tipo').val();
  const contratoMae = $('inp:numeroContratoAnteriorMae').val();
  const assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  switch (tipoSolicitacao) {
    case 'Proposta de Crédito':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').show();
      break;
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      $('#coluna-input-assinado-digitalmente').hide();
      break;
  }

  $('inp:possuiPendenciaExcecao').val() === 'Não'
    ? $('#div-informacoes-emprestimo-eighthRow').hide()
    : $('#div-informacoes-emprestimo-eighthRow').show();

  assinaturaDigital === 'Sim'
    ? $('#div-informacoes-emprestimo-seventhRow').show()
    : $('#div-informacoes-emprestimo-seventhRow').hide();

  if (tipoSolicitacao === 'Borderô de Desconto' && contratoMae === '') {
    $('.form-integra section')
      .eq(0)
      .prepend(
        `<div class="alert alert-info" role="alert">Este é o primeiro borderô da Proposta Mãe aprovada. Então, realize a primeira liberação e ajuste no fluxo alterando o número da proposta pelo número do título gerado correspondente ao borderô.</div>`
      );

    toggleSpinner();
    $('#procurar-informacoes-emprestimo')
      .text('Buscar Título Gerado')
      .show();

    $('.buttons-col').mouseenter(elemento => {
      $('#btnApprove').removeAttr('onclick');
    });

    $('#btnApprove').click(elemento => {
      verificarObrigatoriedadesParaSubmeterAtividade();
    });
  }

  $('#section-informacoes-garantias').show();
  toastr.success(`Informações do(a) ${tipoSolicitacao} geradas com sucesso!`);
  spinnerLoaderOffAnalyse();
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const validarSubmissaoBordero = async () => {
  toggleSpinner();
  await limparDadosBuscadosPeloBordero();
  let bordero = $('inp:numero').val();

  if (!bordero) {
    toggleSpinner();
    toastr.error('Preencha um dado para que possamos iniciar a busca!');
    $('inp:numero').val('');
    return;
  }

  if (bordero === numeroEmprestimoGlobal[0]) {
    toggleSpinner();
    toastr.error(
      'Esse é o número da proposta de crédito. Coloque o número do borderô gerado!'
    );
    return;
  }

  buscarBorderoGerado(bordero);
};

const limparDadosBuscadosPeloBordero = () => {
  $('inp:numeroContratoAnteriorMae').val('');
  $('div[xid="divnumeroContratoAnteriorMae"]').text('');
  $('inp:dataVencimentoTitulo').val('');
  $('div[xid="divdataVencimentoTitulo"]').text('');
  $('inp:totalParcelasTitulo').val('');
  $('div[xid="divtotalParcelasTitulo"]').text('');
  $('inp:valorTitulo').val('');
  $('div[xid="divvalorTitulo"]').text('');
};

const buscarBorderoGerado = async bordero => {
  await axios
    .post(url_titulos_primeiro_bordero, {
      inpnumero: bordero,
    })
    .then(response => {
      if (response.data && response.data.success && response.data.success[0]) {
        if (
          response.data.success[0].PropostaPai !== numeroEmprestimoGlobal[0]
        ) {
          toggleSpinner();
          toastr.error(
            `Esse título não corresponde a proposta mãe ${numeroEmprestimoGlobal[0]}. Ajuste e tente novamente!`
          );
          $('inp:numero').val('');
        } else {
          $('inp:numeroContratoAnteriorMae').val(
            response.data.success[0].PropostaPai
          );
          $('div[xid="divnumeroContratoAnteriorMae"]').text(
            response.data.success[0].PropostaPai
          );

          $('inp:dataVencimentoTitulo').val(
            response.data.success[0].DataVencimentoTitulo
          );
          $('div[xid="divdataVencimentoTitulo"]').text(
            response.data.success[0].DataVencimentoTitulo
          );

          $('inp:totalParcelasTitulo').val(
            response.data.success[0].ParcelasTitulo
          );
          $('div[xid="divtotalParcelasTitulo"]').text(
            response.data.success[0].ParcelasTitulo
          );

          $('inp:valorTitulo').val(
            transformarEmFormatoMoeda(response.data.success[0].ValorTitulo)
          );
          $('div[xid="divvalorTitulo"]').text(
            transformarEmFormatoMoeda(response.data.success[0].ValorTitulo)
          );

          toastr.success('Informações do título geradas com sucesso!');
        }
      } else {
        toggleSpinner();
        toastr.error(
          'Não encontramos esta informação. Ajuste e tente novamente!'
        );
        $('inp:numero').val('');
      }
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

const verificarObrigatoriedadesParaSubmeterAtividade = () => {
  const contratoMae = $('inp:numeroContratoAnteriorMae').val();

  if (contratoMae === '') {
    toastr.error(
      'Para prosseguir com essa opção, primeiro informe o título gerado, conforme demonstrado no alerta azul em sua tela.'
    );
    return;
  } else {
    doAction('Aprovado', true, false);
  }
};
