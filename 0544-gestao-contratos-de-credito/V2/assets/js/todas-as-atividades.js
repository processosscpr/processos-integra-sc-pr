$(document).ready(function() {});

/*--------------------------------------------------------*/
//FONTE DE DADOS: 0507 - Gestão Contratos - Contratos 0544
const url_proposta_credito = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJk-on2SMrY63ZwbuBiU44MOYF3TyTFRIyq--4qzkikFVH701aQ04kZaSrKCm1Zb8aQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Renegociações 0544
const url_renegociacao = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJmGmenH0D0HAtnft6eaDR2wJa3aMRZNUCE7nZYO0FCu-KwCcyIIhlWVUyiYA7EZNAA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Títulos 0544
const url_titulos = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJuYwiy9ozutn7LjorwHyI1Fal3dKBCzkbDf8QyVhsdoBqwG3pniEJ3l-VZiYQ4KcQA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Aditivo 0544
const url_aditivo = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJrr4YS@u4ZO8A49-our0S1IRvmcyxF2uQUVxccSkeEQk8us28EPjNmQSaPmqeLXGZA__`;

//FONTE DE DADOS: 0507 - Gestão Contratos - Títulos 1º Borderô 0544
const url_titulos_primeiro_bordero = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJiWA2Nrtcyyn3bc3ZsE4zVGCddnwX8U191Joe2zORHSvF4dDA0lEbI@rp2TJlfZHtg__`;

//FONTE DE DADOS: 0507 - Gestão Contratos - Contratos Acervo 0544
const url_proposta_credito_acervo = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJjFtvnAzfMQHRDrz@s-j2G5ZHoIiFbVaYa09Abi358yco5P-qW26YfUDsA@kUnSw5g__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Renegociações Acerv 0544
const url_renegociacao_acervo = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJhW3M@mfaQ4xUQr1FY7ju4@@@@1tCKc@CsTbAsjKH1peWo8HEvdENUkEuHe7HamHgA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Títulos Acervo 0544
const url_titulos_acervo = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJktF08q6KF5LaRul23-WGKVrpO713QalX41AT6BhJYhpvz-cnuBH6XN6A1bkgXqeiw__`;

//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Aval 0544
const url_garantia_aval = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJk42lAvQND1FGZ6ukvAAvl@aUPmysHe1mGVMNn3XFgiCz1N-1dEbkj6KqD6H7eJNsg__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Cheque 0544
const url_garantia_cheque = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJrFPqI0VaQtLJx32NpxMxdrg75iPNv@PNsULdlCgohGyC@Bhpjiultfr9MvEBAvYRw__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Cartão 0544
const url_garantia_cartao = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJg5vMWAPa1uOTLlJXZI9jdlWD6Sgz3S97a5f-3SZxE24xRRIsf5eGM06Ih2F7qqxpQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Capital 0544
const url_garantia_capital = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJlDJaJeH5nBfTw87QD5ggpXWGC-rRI1tMd3wV9kaG-E2GXGGR@URccvS3umSJn8Bbg__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Veículo 0544
const url_garantia_veiculos = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJmATgrgiqWxxNGf5kBOr2dSyhdUthfpWk6--jWMoiOB8zF1BaRPQrPqHWGNM3hCaIQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Imóvel 0544
const url_garantia_imovel = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJk-sOy4dBZfw0WDPm-QbEK7H06kqKj3ukJPTjbBBH6UpccwA2Kz0Pomyq1BVMQzaMg__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Aplicação 0544
const url_garantia_aplicacao_financeira = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJrwIKRoU8YXgB9l-jyByIFYDX4CCIG6tC3Rcm@wPXVXRW2Ll2dK@EqHK@DXmX8e6KA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Outras 0544
const url_garantia_outras = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJti-UNjbNWdAWnD6GcRNlo2-sfPXiXJuRSNvOsFiMd33q5k0OBHE221XM3b-cOs9ug__`;
//REGRA DE NEGÓCIO: 0507 - Listagem e Obrigatoriedade de Documentos - Gestão Contratos de Crédito
const url_regra_negocio = `https://bpm.e-unicred.com.br/api/1.0/businessrules/82/evaluate`;
/*--------------------------------------------------------*/

/*--------------------------------------------------------*/
let informacoesEmprestimo = [];
let garantias = [];
/*--------------------------------------------------------*/

const spinnerLoaderOnAnalyse = () =>
  $(
    '#section-informacoes-emprestimo h5.text-dark.border-bottom.border-gray.pb-2'
  ).append(
    ` <span id="spinner-loader-analyse" class="spinner-border spinner-border-sm" role="status"></span>`
  );
const spinnerLoaderOffAnalyse = () => $('#spinner-loader-analyse').remove();

const ajustarLayoutTabelaMultivalorada = () => {
  const botaoAddRow = $('#BtnInsertNewRow');
  const botaoDeleteRow = $(
    '#coluna-table-assinaturaDigital button.btn-delete-mv'
  );

  $(botaoAddRow)
    .addClass('btn-primary btn-sm')
    .text('')
    .append(`<i class="fas fa-plus"></i>`);
  $(botaoDeleteRow)
    .addClass('btn-danger btn-sm')
    .text('')
    .append(`<i class="fas fa-trash-alt"></i>`);
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCheque = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_cheque, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Cheque',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaCartao(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCartao = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_cartao, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Cartão',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiasAvalista(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiasAvalista = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_aval, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Avalista',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaCapital(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCapital = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_capital, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Capital',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaVeiculo(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaVeiculo = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_veiculos, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Veículo',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaImovel(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaImovel = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_imovel, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Imóvel',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaAplicacao(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaAplicacao = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_aplicacao_financeira, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Aplicação Financeira',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      buscarGarantiaOutras(numeroEmprestimo);
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaOutras = async numeroEmprestimo => {
  let dataAux = [];

  await axios
    .post(url_garantia_outras, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      $.each(response.data.success, (i, item) => {
        dataAux = {
          tipo: 'Outras',
          dados: item.fields,
        };
        garantias.push(dataAux);
      });
      montarEstruturaSectionGarantias();
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

//Função dinâmica utilizada em todas as atividades
const montarEstruturaSectionGarantias = async () => {
  //AVALISTA
  if (_.findIndex(garantias, { tipo: 'Avalista' }) != -1) {
    $('#div-informacoes-garantias-avalistas').prepend(
      $(sectionCollapse('far fa-id-badge', 'Avalistas'))
    );
    $('#div-informacoes-garantias-resumo-avalistas').append(
      $(
        cardResume(
          _.countBy(garantias, { tipo: 'Avalista' }).true,
          'Avalista(s)'
        )
      )
    );
  }
  //CHEQUE
  if (_.findIndex(garantias, { tipo: 'Cheque' }) != -1) {
    let totalChequeBruto = [],
      totalChequeLiquido = [];
    $('#div-informacoes-garantias-cheques').prepend(
      $(sectionCollapse('fas fa-money-check-alt', 'Cheques'))
    );
    $('#div-informacoes-garantias-resumo-cheques').append(
      $(cardResume(_.countBy(garantias, { tipo: 'Cheque' }).true, 'Cheque(s)'))
    );
    //Iniciar Cálculo Somatório Cheque
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Cheque'
        ? (totalChequeBruto.push(parseFloat(garantia.dados.ValorBruto)),
          totalChequeLiquido.push(parseFloat(garantia.dados.ValorLiquido)))
        : null;
    });
    $('#div-informacoes-garantias-resumo-cheques').append(
      $(
        cardResume(
          'Total Bruto:',
          transformarEmFormatoMoeda(_.sum(totalChequeBruto, Number).toFixed(2))
        )
      )
    );
    $('#div-informacoes-garantias-resumo-cheques').append(
      $(
        cardResume(
          'Total Líquido:',
          transformarEmFormatoMoeda(
            _.sum(totalChequeLiquido, Number).toFixed(2)
          )
        )
      )
    );
  }
  //CARTÃO
  if (_.findIndex(garantias, { tipo: 'Cartão' }) != -1) {
    let totalCartaoCredito = [],
      totalCartaoAntecipar = [],
      totalCartaoLiquido = [];
    $('#div-informacoes-garantias-cartoes').prepend(
      $(sectionCollapse('far fa-credit-card', 'Créditos de Cartões'))
    );
    $('#div-informacoes-garantias-resumo-cartoes').append(
      $(
        cardResume(
          _.countBy(garantias, { tipo: 'Cartão' }).true,
          'Crédito(s) de Cartão(ões)'
        )
      )
    );
    //Iniciar Cálculo Somatório Cartão
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Cartão'
        ? (totalCartaoCredito.push(parseFloat(garantia.dados.ValorCredito)),
          totalCartaoAntecipar.push(
            parseFloat(garantia.dados.ValorAntecipacao)
          ),
          totalCartaoLiquido.push(parseFloat(garantia.dados.ValorLiquido)))
        : null;
    });
    $('#div-informacoes-garantias-resumo-cartoes').append(
      $(
        cardResume(
          'Total Crédito:',
          transformarEmFormatoMoeda(
            _.sum(totalCartaoCredito, Number).toFixed(2)
          )
        )
      )
    );
    $('#div-informacoes-garantias-resumo-cartoes').append(
      $(
        cardResume(
          'Total Antecipar:',
          transformarEmFormatoMoeda(
            _.sum(totalCartaoAntecipar, Number).toFixed(2)
          )
        )
      )
    );
    $('#div-informacoes-garantias-resumo-cartoes').append(
      $(
        cardResume(
          'Total Líquido:',
          transformarEmFormatoMoeda(
            _.sum(totalCartaoLiquido, Number).toFixed(2)
          )
        )
      )
    );
  }
  //CAPITAL
  if (_.findIndex(garantias, { tipo: 'Capital' }) != -1) {
    $('#div-informacoes-garantias-cotas').prepend(
      $(sectionCollapse('fas fa-coins', 'Cota Capital'))
    );
    $('#div-informacoes-garantias-resumo-cotas').append(
      $(
        cardResume(
          _.countBy(garantias, { tipo: 'Capital' }).true,
          'Cota Capital'
        )
      )
    );
  }
  //VEÍCULO
  if (_.findIndex(garantias, { tipo: 'Veículo' }) != -1) {
    let totalValorVeiculo = [];
    $('#div-informacoes-garantias-veiculos').prepend(
      $(sectionCollapse('fas fa-car', 'Veículos'))
    );
    $('#div-informacoes-garantias-resumo-veiculos').append(
      $(
        cardResume(_.countBy(garantias, { tipo: 'Veículo' }).true, 'Veículo(s)')
      )
    );
    //Iniciar Cálculo Somatório Veículo
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Veículo'
        ? totalValorVeiculo.push(parseFloat(garantia.dados.ValorVeiculo))
        : null;
    });
    $('#div-informacoes-garantias-resumo-veiculos').append(
      $(
        cardResume(
          'Total:',
          transformarEmFormatoMoeda(_.sum(totalValorVeiculo, Number).toFixed(2))
        )
      )
    );
  }
  //IMÓVEL
  if (_.findIndex(garantias, { tipo: 'Imóvel' }) != -1) {
    let totalValorImovel = [];
    $('#div-informacoes-garantias-imoveis').prepend(
      $(sectionCollapse('fas fa-home', 'Imóveis'))
    );
    $('#div-informacoes-garantias-resumo-imoveis').append(
      $(cardResume(_.countBy(garantias, { tipo: 'Imóvel' }).true, 'Imóvel(is)'))
    );
    //Iniciar Cálculo Somatório Imóvel
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Imóvel'
        ? totalValorImovel.push(parseFloat(garantia.dados.ValorImovel))
        : null;
    });
    $('#div-informacoes-garantias-resumo-imoveis').append(
      $(
        cardResume(
          'Total:',
          transformarEmFormatoMoeda(_.sum(totalValorImovel, Number).toFixed(2))
        )
      )
    );
  }
  //APLICAÇÃO FINANCEIRA
  if (_.findIndex(garantias, { tipo: 'Aplicação Financeira' }) != -1) {
    let totalValorAplicacaoFinanceira = [];
    $('#div-informacoes-garantias-aplicacoes').prepend(
      $(sectionCollapse('fas fa-hand-holding-usd', 'Aplicações Financeiras'))
    );
    $('#div-informacoes-garantias-resumo-aplicacoes').append(
      $(
        cardResume(
          _.countBy(garantias, { tipo: 'Aplicação Financeira' }).true,
          'Aplicação(ões)'
        )
      )
    );
    //Iniciar Cálculo Somatório Aplicação Fincanceira
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Aplicação Financeira'
        ? totalValorAplicacaoFinanceira.push(
            parseFloat(garantia.dados.ValorGarantia)
          )
        : null;
    });
    $('#div-informacoes-garantias-resumo-aplicacoes').append(
      $(
        cardResume(
          'Total:',
          transformarEmFormatoMoeda(
            _.sum(totalValorAplicacaoFinanceira, Number).toFixed(2)
          )
        )
      )
    );
  }
  //OUTRAS
  if (_.findIndex(garantias, { tipo: 'Outras' }) != -1) {
    let totalValorOutras = [];
    $('#div-informacoes-garantias-outras').prepend(
      $(sectionCollapse('fas fa-box-open', 'Outras'))
    );
    $('#div-informacoes-garantias-resumo-outras').append(
      $(
        cardResume(
          'Quantidade Outras:',
          _.countBy(garantias, { tipo: 'Outras' }).true
        )
      )
    );
    //Iniciar Cálculo Somatório Outras
    _.forEach(garantias, garantia => {
      garantia.tipo == 'Outras'
        ? totalValorOutras.push(parseFloat(garantia.dados.ValorGarantia))
        : null;
    });
    $('#div-informacoes-garantias-resumo-outras').append(
      $(
        cardResume(
          'Total:',
          transformarEmFormatoMoeda(_.sum(totalValorOutras, Number).toFixed(2))
        )
      )
    );
  }
  montarEstruturaCardsDeGarantias();
};

//Função dinâmica utilizada em todas as atividades
const montarEstruturaCardsDeGarantias = async () => {
  await _.forEach(garantias, garantia => {
    switch (garantia.tipo) {
      case 'Avalista':
        $('#div-informacoes-garantias-cards-avalistas').append(
          $(cardGarantiaAvalista(garantia.dados))
        );
        break;
      case 'Cheque':
        $('#div-informacoes-garantias-cards-cheques').append(
          $(cardGarantiaCheque(garantia.dados))
        );
        break;
      case 'Cartão':
        $('#div-informacoes-garantias-cards-cartoes').append(
          $(cardGarantiaCartao(garantia.dados))
        );
        break;
      case 'Capital':
        $('#div-informacoes-garantias-cards-cotas').append(
          $(cardGarantiaCota(garantia.dados))
        );
        break;
      case 'Veículo':
        $('#div-informacoes-garantias-cards-veiculos').append(
          $(cardGarantiaVeiculo(garantia.dados))
        );
        break;
      case 'Imóvel':
        $('#div-informacoes-garantias-cards-imoveis').append(
          $(cardGarantiaImovel(garantia.dados))
        );
        break;
      case 'Aplicação Financeira':
        $('#div-informacoes-garantias-cards-aplicacoes').append(
          $(cardGarantiaAplicacao(garantia.dados))
        );
        break;
      case 'Outras':
        $('#div-informacoes-garantias-cards-outras').append(
          $(cardGarantiaOutras(garantia.dados))
        );
        break;
    }
  });
  submeterProximaValidacaoAposRetornoDasGarantias();
};

/*--------------------------------------------------------*/
//COMPONENTES - CARDS
const cardResume = (label, value) => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card text-white bg-primary">
            <div class="card-body">
                <strong class="text-gray-dark">${label} ${value}</strong>
            </div>
        </div>
    </div>
`;

const cardGarantiaAvalista = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${
                  dados.Pessoa
                }</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(
                  dados.IdentificacaoPessoal
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaCheque = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Banco:</b> ${dados.Banco} <b>Ag:</b> ${
  dados.Agencia
} <b>Conta:</b> ${dados.Conta}</p>
                <p class="card-text"><b>Número Cheque:</b> ${dados.Cheque}</p>
                <p class="card-text"><b>Data Vencimento:</b> ${
                  dados.DataVencimentoCheque
                }</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Bruto:</b> ${transformarEmFormatoMoeda(
                  dados.ValorBruto
                )}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Líquido:</b> ${transformarEmFormatoMoeda(
                  dados.ValorLiquido
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaCartao = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Adquirente:</b> ${
                  dados.Adquirente
                } <b>Antecip:</b> ${dados.PercentualAntecipacao}%</p>
                <p class="card-text"><b>Data Crédito:</b> ${
                  dados.DataCredito
                }</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Crédito:</b> ${transformarEmFormatoMoeda(
                  dados.ValorCredito
                )}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Antecipar:</b> ${transformarEmFormatoMoeda(
                  dados.ValorAntecipacao
                )}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Líquido:</b> ${transformarEmFormatoMoeda(
                  dados.ValorLiquido
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaCota = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorCapital
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaVeiculo = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${
                  dados.Pessoa
                }</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(
                  dados.IdentificacaoPessoal
                )}</p>
                <p class="card-text"><span><i class="fas fa-car-side"></i></span> ${
                  dados.Veiculo
                } - ${dados.Marca} | ${dados.Placa}</p>
                <p class="card-text"><b>Renavam:</b> ${
                  dados.Renavam
                } <b>Chassi:</b> ${dados.Chassi}</p>
                <p class="card-text"><span><i class="far fa-calendar-alt"></i></span> <b>Fabr:</b> ${
                  dados.AnoFabricacao
                } <span><i class="far fa-calendar-alt"></i> <b>Modelo:</b> ${
  dados.AnoModelo
}</span></p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorVeiculo
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaImovel = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${
                  dados.Pessoa
                }</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(
                  dados.IdentificacaoPessoal
                )}</p>
                <p class="card-text"><span><i class="fas fa-map-marker-alt"></i></span> [${
                  dados.TipoImovel
                }] ${dados.DsEndereco}, ${dados.DsNumero} - ${
  dados.DsComplemento
}, ${dados.DsBairro}, ${dados.DsCidade} - ${dados.DsUF} - ${dados.DsCEP}</p>
                <p class="card-text"><b>Matrícula:</b> ${
                  dados.MatriculaImovel
                } <b>Registro:</b> ${dados.RegistroImovel}</p>
                <p class="card-text"><b>Descrição:</b> ${dados.DsImovel}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorImovel
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaAplicacao = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Conta:</b> ${
                  dados.Conta
                } <b>Produto:</b> ${dados.Produto}</p>
                <p class="card-text"><b>Título:</b> ${dados.Titulo}</p>
                <p class="card-text"><span><i class="far fa-calendar-alt"></i></span> <b>Aplicação:</b> ${
                  dados.DataAplicacao
                } <span><i class="far fa-calendar-alt"></i> <b>Vencimento:</b> ${
  dados.DataVencimento
}</span></p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorGarantia
                )}</p>
            </div>
        </div>
    </div>
`;

const cardGarantiaOutras = dados => `
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Descrição Garantia:</b> ${
                  dados.DsGarantia
                }</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(
                  dados.ValorGarantia
                )}</p>
            </div>
        </div>
    </div>
`;

//COMPONENTES - SECTION COLLAPSE
const sectionCollapse = (classe, tipo) => `
    <h6 class="text-gray-dark border-bottom border-gray pb-2"><span><i class="${classe}"></i></span> ${tipo}<a class="text-dark float-right" onclick="collapseGarantia(this)" target="waiting-collapse-integra"><i class="fas fa-chevron-down"></i></a></h6>
`;

//COMPONENTES - ALERT
const alert = text => `
    <div class="col-xs-12 col-md-12" style="margin-top: 10px; margin-bottom: 10px">
        <div class="alert alert-info" role="alert">
            ${text}
        </div>
    </div>
`;

//COMPONENTES - SWITCH
const inputSwitchAssinaturaDigital = () => `
    <div id="switchAssinaturaDigital" class="col-xs-12 col-md-2">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="customSwitchAssinaturaDigital">
            <label class="custom-control-label font-weight-bold pt-1" for="customSwitchAssinaturaDigital">Contrato Será Assinado Digitalmente</label>
        </div>
    </div>
`;

//COMPONENTES - SWITCH
const inputSwitchPendenciaExcecao = () => `
<div id="switchPendenciaExcecao" class="col-xs-12 col-md-2">
    <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="customSwitchPendenciaExcecao">
        <label class="custom-control-label font-weight-bold pt-1" for="customSwitchPendenciaExcecao">Possui Pendência / Exceção</label>
    </div>
</div>
`;

//COMPONENTES - SWITCH
const inputSwitchAprovacaoGA = () => `
<div id="switchAprovacaoGA" class="col-xs-12 col-md-2">
    <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="customSwitchAprovacaoGA">
        <label class="custom-control-label font-weight-bold pt-1" for="customSwitchAprovacaoGA">Necessita OK da Diretoria</label>
    </div>
</div>
`;
/*--------------------------------------------------------*/

//Função utilizada em todas as atividades
const transformarEmFormatoMoeda = valor => {
  if (valor === '') {
    return '';
  } else {
    return parseFloat(valor).toLocaleString('pt-br', {
      minimumFractionDigits: 2,
    });
  }
};

//Função utilizada em todas as atividades
const mascaraIdentificador = valor => {
  if (valor.length <= 11) {
    return valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4');
  } else {
    return valor.replace(
      /(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,
      '$1.$2.$3/$4-$5'
    );
  }
};

//Função utilizada em todas as atividades
const collapseGarantia = elemento => {
  $(elemento).attr('target') === 'waiting-collapse-integra'
    ? ($(
        $(
          $(elemento)
            .parent()
            .parent()
        ).find('div.row')[0]
      ).show(),
      $(
        $(
          $(elemento)
            .parent()
            .parent()
        ).find('div.row')[1]
      ).show(),
      $(elemento).attr('target', 'in-collapse-integra'),
      $(elemento)
        .find('i')
        .removeClass('fa-chevron-down')
        .addClass('fa-chevron-up'))
    : ($(
        $(
          $(elemento)
            .parent()
            .parent()
        ).find('div.row')[0]
      ).hide(),
      $(
        $(
          $(elemento)
            .parent()
            .parent()
        ).find('div.row')[1]
      ).hide(),
      $(elemento).attr('target', 'waiting-collapse-integra'),
      $(elemento)
        .find('i')
        .removeClass('fa-chevron-up')
        .addClass('fa-chevron-down'));
};
