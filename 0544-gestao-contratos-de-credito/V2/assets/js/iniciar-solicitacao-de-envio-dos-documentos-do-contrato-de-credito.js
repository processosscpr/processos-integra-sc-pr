$(document).ready(function() {
  hideIntegraElements();
  hideAttachments();
  getInstanceHeader();
  ajustarFormulario();
  ajustarLayoutTabelaMultivalorada();
  toggleSpinner();
  desativarInputsPopuladosPorAPI();

  $('inp:tipo').change(async elemento => {
    await limparInformcoesFormulario();
    $('inp:numero').val('');
    $('inp:acervo').val('');
    $('inp:necessitaDeArquivamento').val('');
    $('#coluna-input-acervo').show();
    $('#coluna-numero-emprestimo').show();
    $('#procurar-informacoes-emprestimo').show();
  });

  $('inp:acervo').change(async elemento => {
    await limparInformcoesFormulario();
    $('inp:necessitaDeArquivamento').attr('readOnly', false);
    $('inp:necessitaDeArquivamento').val('');
    $('inp:numero').val('');

    if (elemento.target.value === 'Sim') {
      $('#coluna-input-arquivamento').show();
      toastr.info(
        'Esta solicitação não envolve conferência dos documentos, apenas os arquivará no ECM da ferramenta INTEGRA'
      );

      if ($('inp:tipo').val() === 'Borderô de Desconto') {
        $('inp:necessitaDeArquivamento').val('Sim');
        $('inp:necessitaDeArquivamento').attr('readOnly', true);
        toastr.info(
          'Encaminhe os documentos físicos para a UA realizar o arquivamento!'
        );
      }
    } else {
      $('#coluna-input-arquivamento').hide();
      $('inp:necessitaDeArquivamento').val('Não');
    }
  });

  $('inp:necessitaDeArquivamento').change(async elemento => {
    if (elemento.target.value === 'Sim') {
      toastr.info(
        'Encaminhe os documentos físicos para a UA realizar o arquivamento!'
      );
    }
  });

  $('inp:numero').change(elemento => {
    limparInformcoesFormulario();
  });

  $('#procurar-informacoes-emprestimo').click(async elemento => {
    await limparInformcoesFormulario();
    validarSubmissaoEmprestimo();
  });

  $(document).on('change', '#customSwitchAssinaturaDigital', e => {
    alterarValorInputAssinaturaDigital(e.target.checked);
  });

  $(document).on('change', '#customSwitchPendenciaExcecao', e => {
    verificarNecessidadePreenchimentoExcecao(e.target.checked);
  });

  $(document).on('change', '#customSwitchAprovacaoGA', e => {
    alterarValorInputGANecessitaAprovacaoDiretoria(e.target.checked);
  });

  $('#customBtn_Solicitação\\ Encaminhada').click(elemento => {
    verificarObrigatoriedadesParaSubmeterSolicitacao();
  });
});

const ajustarFormulario = () => {
  // configurações default do formulário
  $('inp:acervo').val('Não');
  $('#customBtn_Solicitação\\ Encaminhada').removeAttr('onclick');
  $('#ContainerAttach .box-header').append(`
      <div id="aviso-nao-unificacao-anexo">
        <div class="col">
          <div class="alert alert-info my-2">
            Somente é possível um arquivo por tipo de documento. Caso possua vários arquivos
            separados, deve-se, por enquanto, agrupá-los pelo scanner da impressora.
          </div>
        </div>
      </div>
    `);

  // esconder elementos do formulário
  $('#coluna-input-acervo').hide();
  $('#coluna-input-arquivamento').hide();
  $('#coluna-numero-emprestimo').hide();
  $('#section-informacoes-emprestimo').hide();
  $('#section-informacoes-arquivamento').hide();
  $('#customBtn_Solicitação\\ Encaminhada').hide();
};

const toggleSpinner = () => {
  $('.spinner-loader').toggle();
};

const desativarInputsPopuladosPorAPI = () => {
  let inputInfosEmprestimo = $('#section-informacoes-emprestimo').find('input');
  inputInfosEmprestimo.attr('readOnly', true);
};

const limparInformcoesFormulario = async () => {
  $('#section-informacoes-emprestimo').hide();
  $('#section-informacoes-garantias').hide();
  $('#ContainerAttach').hide();

  let inputInfosEmprestimo = $('#section-informacoes-emprestimo').find('input');
  await _.forEach(inputInfosEmprestimo, input => {
    $(input).val('');
  });

  $('inp:contratoSeraAssinadoDigitalmente').val('');
  $('inp:possuiPendenciaExcecao').val('');
  $('inp:dadosDaPendenciaExcecao').val('');

  const rowsFromTheTable = $(
    '#div-informacoes-emprestimo-seventhRow table'
  ).find('tbody tr');
  await _.forEach(rowsFromTheTable, row => {
    $(row)
      .find('button.btn-danger')
      .click();
  });
  $('#div-informacoes-emprestimo-seventhRow')
    .find('input')
    .val('');
  $('#div-informacoes-emprestimo-seventhRow')
    .find('input')[0]
    .setAttribute('required', 'N');
  $('#div-informacoes-emprestimo-seventhRow')
    .find('input')[1]
    .setAttribute('required', 'N');

  await _.forEach($('#div-informacoes-garantias').children(), elemento => {
    $(elemento)
      .find('div.col-xs-12')
      .remove(),
      $(elemento)
        .find('h6.text-gray-dark.border-bottom.border-gray.pb-2')
        .remove();
  });

  informacoesEmprestimo = [];
  garantias = [];

  $('#switchAssinaturaDigital').remove();
  $('#switchPendenciaExcecao').remove();
  $('#switchAprovacaoGA').remove();
};

const validarSubmissaoEmprestimo = () => {
  toggleSpinner();
  let numeroEmprestimo = $('inp:numero').val();

  if (!numeroEmprestimo) {
    toggleSpinner();
    toastr.error('Preencha um dado para que possamos iniciar a busca!');
    return;
  }

  verificarQualRequisicaoRealizar();
};

const verificarQualRequisicaoRealizar = () => {
  let tipo = $('inp:tipo').val();
  let acervo = $('inp:acervo').val();

  switch (tipo) {
    case 'Proposta de Crédito':
      acervo === 'Não'
        ? buscarinformacoesEmprestimo(url_proposta_credito, tipo)
        : buscarinformacoesEmprestimo(url_proposta_credito_acervo, tipo);
      break;
    case 'Termo Aditivo':
      buscarinformacoesEmprestimo(url_aditivo, tipo);
      break;
    case 'Renegociação':
      acervo === 'Não'
        ? buscarinformacoesEmprestimo(url_renegociacao, tipo)
        : buscarinformacoesEmprestimo(url_renegociacao_acervo, tipo);
      break;
    case 'Borderô de Desconto':
      acervo === 'Não'
        ? buscarinformacoesEmprestimo(url_titulos, tipo)
        : buscarinformacoesEmprestimo(url_titulos_acervo, tipo);
      break;
  }
};

const buscarinformacoesEmprestimo = async (url, tipoSolicitacao) => {
  let numeroEmprestimo = $('inp:numero').val();

  await axios
    .post(url, {
      inpnumero: numeroEmprestimo,
    })
    .then(response => {
      if (response.data && response.data.success && response.data.success[0]) {
        informacoesEmprestimo = response.data.success[0];
        tipoSolicitacao === 'Borderô de Desconto'
          ? buscarGarantiaCheque(numeroEmprestimo)
          : buscarGarantiasAvalista(numeroEmprestimo);
      } else {
        toggleSpinner();
        toastr.error(
          'Não encontramos esta informação. Ajuste e tente novamente!'
        );
      }
    })
    .catch(error => {
      toggleSpinner();
      toastr.error(
        'Ocorreu um erro para buscar a informação! Contate o suporte.'
      );
      console.log(error);
    });
};

const submeterProximaValidacaoAposRetornoDasGarantias = () => {
  preencherDadosDoEmprestimoNosInputs();
};

const preencherDadosDoEmprestimoNosInputs = async () => {
  let tipo = $('inp:tipo').val();
  let valorEmMoeda = await transformarEmFormatoMoeda(
    informacoesEmprestimo.fields.Valor
  );

  $('inp:conta').val(informacoesEmprestimo.txt);
  $('inp:associado').val(informacoesEmprestimo.fields.Associado);
  $('inp:cpfCnpj').val(informacoesEmprestimo.fields.Identificacao);
  $('inp:linhaDeCredito').val(
    informacoesEmprestimo.fields.DescricaoLinhaCredito
  );
  $('inp:valor').val(valorEmMoeda);
  $('inp:finalidade').val(informacoesEmprestimo.fields.Finalidade);
  $('inp:dataSolicitacao').val(informacoesEmprestimo.fields.DataSolicitacao);
  $('inp:parcelas').val(informacoesEmprestimo.fields.Parcelas);
  $('inp:seguroPrestamista').val(informacoesEmprestimo.fields.Prestamista);
  $('inp:tipoVencimento').val(informacoesEmprestimo.fields.TipoVencimento);
  $('inp:diaVencimento').val(informacoesEmprestimo.fields.DiaVencimento);
  $('inp:carenciaPrimParc').val(informacoesEmprestimo.fields.Carencia);
  $('inp:jurosNaCarencia').val(informacoesEmprestimo.fields.JurosNaCarencia);
  $('inp:financiarIof').val(informacoesEmprestimo.fields.FinanciarIOF);

  switch (tipo) {
    case 'Proposta de Crédito':
      $('inp:contratoSeraAssinadoDigitalmente').val('Não');
      break;
    case 'Renegociação':
      $('inp:numeroContratoAnteriorMae').val(
        informacoesEmprestimo.fields.PropostaAnterior
      );
      break;
    case 'Borderô de Desconto':
      let valorTituloEmMoeda = await transformarEmFormatoMoeda(
        informacoesEmprestimo.fields.ValorTitulo
      );

      $('inp:numeroContratoAnteriorMae').val(
        informacoesEmprestimo.fields.PropostaPai
      );
      $('inp:valorTitulo').val(valorTituloEmMoeda);
      $('inp:dataVencimentoTitulo').val(
        informacoesEmprestimo.fields.DataVencimentoTitulo
      );
      $('inp:totalParcelasTitulo').val(
        informacoesEmprestimo.fields.ParcelasTitulo
      );
      break;
  }
  $('inp:possuiPendenciaExcecao').val('Não');

  apresentarInformacoesGeradasAoSolicitante();
};

const apresentarInformacoesGeradasAoSolicitante = () => {
  let tipo = $('inp:tipo').val();
  let acervo = $('inp:acervo').val();

  $('#section-informacoes-emprestimo').show();
  $('#section-informacoes-emprestimo div.row').show();

  if (acervo === 'Não') {
    $('#div-informacoes-emprestimo-sixthRow').prepend(
      $(inputSwitchPendenciaExcecao())
    );
  }

  switch (tipo) {
    case 'Proposta de Crédito':
      if (acervo === 'Não') {
        $('#div-informacoes-emprestimo-sixthRow').prepend(
          $(inputSwitchAssinaturaDigital())
        );
      }
    case 'Termo Aditivo':
      $('#coluna-contrato-anterior-mae').hide();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Renegociação':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').hide();
      break;
    case 'Borderô de Desconto':
      $('#coluna-contrato-anterior-mae').show();
      $('#div-informacoes-emprestimo-fifthRow').show();
      break;
  }

  $('#coluna-input-assinado-digitalmente').hide();
  $('#coluna-input-excecao').hide();
  $('#div-informacoes-emprestimo-seventhRow').hide();
  $('#div-informacoes-emprestimo-eighthRow').hide();

  $('#section-informacoes-garantias').show();

  $('#annex').hide();
  $('#ContainerAttach').show();
  $('#customBtn_Solicitação\\ Encaminhada').show();

  toggleSpinner();
  toastr.success(`Informações do(a) ${tipo} geradas com sucesso!`);
};

const alterarValorInputAssinaturaDigital = async check => {
  await _.forEach(
    $('div#invalid-feedback-envio-requisicao'),
    invalidFeedbackRequest => {
      $(invalidFeedbackRequest).remove();
    }
  );

  switch (check) {
    case true:
      if (
        $('span.badge-success')
          .text()
          .match(/Contrato/gi)
      ) {
        toastr.error(
          'Primeiro remova o contrato anexado, pois será gerado automaticamente ao cooperado na ferramenta DocuSign!'
        );
        $('#customSwitchAssinaturaDigital').attr('checked', false);
        return;
      }

      if (
        $('span.badge-success')
          .text()
          .match(/Prestamista/gi)
      ) {
        toastr.error(
          'Primeiro remova o prestamista anexado, pois será gerado automaticamente ao cooperado na ferramenta DocuSign!'
        );
        $('#customSwitchAssinaturaDigital').attr('checked', false);
        return;
      }

      await _.forEach($('#customizedUpload span.badge'), docList => {
        $(docList)
          .text()
          .match(/Contrato/gi) ||
        $(docList)
          .text()
          .match(/Prestamista/gi)
          ? captureModule.capture.removeDoc($(docList).attr('cod'))
          : null;
      });
      $('inp:contratoSeraAssinadoDigitalmente').val('Sim');
      toastr.info(
        'Atividade será encaminhada para a área BackOffice gerar o contrato e/ou prestamista para assinatura digital no DocuSign'
      );

      $('#div-informacoes-emprestimo-seventhRow')
        .find('input')
        .attr('readOnly', false);
      $('#div-informacoes-emprestimo-seventhRow')
        .find('input')[0]
        .setAttribute('required', 'S');
      $('#div-informacoes-emprestimo-seventhRow')
        .find('input')[1]
        .setAttribute('required', 'S');
      $('#div-informacoes-emprestimo-seventhRow').show();

      break;

    case false:
      $('inp:contratoSeraAssinadoDigitalmente').val('Não');

      const rowsFromTheTable = $(
        '#div-informacoes-emprestimo-seventhRow table'
      ).find('tbody tr');
      const lengthDocs = $('span.badge:not(.badge-important)').length;
      const ultimoCodDocs = $(
        $('span.badge:not(.badge-important)')[lengthDocs - 1]
      ).attr('cod');

      captureModule.capture.addDoc('Contrato (Obrigatório)');
      captureModule.capture.addDoc('Prestamista');

      const lengthDocsAfterInsert = $('span.badge:not(.badge-important)')
        .length;
      $($('span.badge:not(.badge-important)')[lengthDocsAfterInsert - 2]).attr(
        'cod',
        parseInt(ultimoCodDocs) + 1
      );
      $($('span.badge:not(.badge-important)')[lengthDocsAfterInsert - 1]).attr(
        'cod',
        parseInt(ultimoCodDocs) + 2
      );

      await _.forEach(rowsFromTheTable, row => {
        $(row)
          .find('button.btn-danger')
          .click();
      });

      $('#div-informacoes-emprestimo-seventhRow')
        .find('input')
        .val('');
      $('#div-informacoes-emprestimo-seventhRow')
        .find('input')[0]
        .setAttribute('required', 'N');
      $('#div-informacoes-emprestimo-seventhRow')
        .find('input')[1]
        .setAttribute('required', 'N');
      $('#div-informacoes-emprestimo-seventhRow').hide();

      break;
  }
};

const verificarNecessidadePreenchimentoExcecao = async check => {
  const tipoCadastrado = $('inp:tipoCadastrado').val();

  switch (check) {
    case true:
      $('inp:possuiPendenciaExcecao').val('Sim');
      $('inp:dadosDaPendenciaExcecao').val('');
      $('inp:dadosDaPendenciaExcecao')[0].setAttribute('required', 'S');
      $('#div-informacoes-emprestimo-eighthRow').show();
      $($('#coluna-textarea-excecao').children()[0]).append(
        `<div id="invalid-feedback-excecao" class="invalid-feedback">Retiramos a obrigatoriedade de todos os documentos. Sua solicitação passará por validação da Diretoria da Cooperativa! Disponibilizamos a possibilidade de anexar o Pedido de Exceção.</div>`
      );
      $('#invalid-feedback-excecao').show();
      await _.forEach(
        $('div#invalid-feedback-envio-requisicao'),
        invalidFeedbackRequest => {
          $(invalidFeedbackRequest).remove();
        }
      );

      if (tipoCadastrado === 'Gerência de Agência') {
        $('#div-informacoes-emprestimo-sixthRow').append(
          $(inputSwitchAprovacaoGA())
        );
      }
      break;
    case false:
      $('inp:possuiPendenciaExcecao').val('Não');
      $('inp:dadosDaPendenciaExcecao').val('');
      $('inp:dadosDaPendenciaExcecao')[0].setAttribute('required', 'N');
      $('#div-informacoes-emprestimo-eighthRow').hide();
      $('#invalid-feedback-excecao').remove();

      if (tipoCadastrado === 'Gerência de Agência') {
        $('#switchAprovacaoGA').remove();
        $('inp:gaNecessitaAprovacaoDiretoria').val('Não');
      }
      break;
  }
};

const alterarValorInputGANecessitaAprovacaoDiretoria = check => {
  switch (check) {
    case true:
      $('inp:gaNecessitaAprovacaoDiretoria').val('Sim');
      break;
    case false:
      $('inp:gaNecessitaAprovacaoDiretoria').val('Não');
      break;
  }
};

const verificarObrigatoriedadesParaSubmeterSolicitacao = async () => {
  _.forEach(
    $('div#invalid-feedback-envio-requisicao'),
    invalidFeedbackRequest => {
      $(invalidFeedbackRequest).remove();
    }
  );
  let possuiExcecao = $('inp:possuiPendenciaExcecao').val();
  let tipo = $('inp:tipo').val();
  let acervo = $('inp:acervo').val();
  let assinaturaDigital = $('inp:contratoSeraAssinadoDigitalmente').val();

  let camposAssinaturaPendentes = [];
  await _.forEach(
    $('#div-informacoes-emprestimo-seventhRow').find('input'),
    input => {
      $(input).val() === '' ? camposAssinaturaPendentes.push(input) : null;
    }
  );

  let docsPendentes = $('#customizedUpload span.badge-secondary').map(
    (index, element) =>
      $(element)
        .text()
        .match(/Obrigat/gi)
  );

  if (assinaturaDigital === 'Sim' && camposAssinaturaPendentes.length > 0) {
    toastr.error(
      'Há campos obrigatórios a serem preenchidos. Favor verificar!'
    );
    return;
  }

  if (acervo === 'Sim') {
    if ($('#customizedUpload span.badge-success').length < 1) {
      toastr.error(
        'Para prosseguir a solicitação você deve ao menos anexar um dos arquivos listados na seção Anexos!'
      );
      return;
    } else {
      $('#switchAssinaturaDigital').remove();
      $('#switchPendenciaExcecao').remove();
      $('#switchAprovacaoGA').remove();
      doAction('Solicitação Encaminhada', false, false);
    }
  } else {
    if (possuiExcecao === 'Não') {
      switch (tipo) {
        case 'Proposta de Crédito':
        case 'Renegociação':
          if (docsPendentes.length > 0) {
            if (assinaturaDigital === 'Sim') {
              $('#customizedUpload').append(
                `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Documento Parecer Deve ser Anexado!</div>`
              );
            } else {
              $('#customizedUpload').append(
                `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Documento Contrato e Parecer Devem ser Anexados!</div>`
              );
            }

            toastr.error(
              'Há Documentos e Informações Obrigatórias. Verifique!'
            );
            $('#invalid-feedback-envio-requisicao').show();
          } else {
            $('#switchAssinaturaDigital').remove();
            $('#switchPendenciaExcecao').remove();
            $('#switchAprovacaoGA').remove();
            doAction('Solicitação Encaminhada', false, false);
          }
          break;
        case 'Borderô de Desconto':
          if (docsPendentes.length > 0) {
            $('#customizedUpload').append(
              `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`
            );
            toastr.error(
              'Há Documentos e Informações Obrigatórias. Verifique!'
            );
            $('#invalid-feedback-envio-requisicao').show();
          } else {
            $('#switchAssinaturaDigital').remove();
            $('#switchPendenciaExcecao').remove();
            $('#switchAprovacaoGA').remove();
            doAction('Solicitação Encaminhada', false, false);
          }
          break;
        case 'Termo Aditivo':
          if (docsPendentes.length > 0) {
            $('#customizedUpload').append(
              `<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`
            );
            toastr.error(
              'Há Documentos e Informações Obrigatórias. Verifique!'
            );
            $('#invalid-feedback-envio-requisicao').show();
          } else {
            $('#switchAssinaturaDigital').remove();
            $('#switchPendenciaExcecao').remove();
            $('#switchAprovacaoGA').remove();
            doAction('Solicitação Encaminhada', false, false);
          }
          break;
      }
    } else if (possuiExcecao === 'Sim') {
      $('#switchAssinaturaDigital').remove();
      $('#switchPendenciaExcecao').remove();
      $('#switchAprovacaoGA').remove();
      doAction('Solicitação Encaminhada', false, false);
    }
  }
};
