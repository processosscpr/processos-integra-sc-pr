$(document).ready(function() {
    ajustarFormulario();

    $('inp:tipo').change(elemento => {
        $('#search-emprestimo').attr('style','display: block;');
        $('#div-search-emprestimo').show();
        limparInformcoesFormulario();
    });

    $('inp:numero').change(e => {
        limparInformcoesFormulario();
    });

    $('#search-emprestimo').click(async (e) => {
        await limparInformcoesFormulario();
        verificarQualRequisicaoRealizar();
    });

    $(document).on('change', '#customSwitchPendenciaExcecao', e => {
        verificarNecessidadePreenchimentoPendenciaExcecao(e.target.checked);
    });

    $(document.getElementById('customBtn_Solicitação Encaminhada'))[0].onclick = null;
    $(document.getElementById('customBtn_Solicitação Encaminhada')).click(function() {
        verificarTratamentoObrigacoesDasInformacoesParaEnvioDaSolicitacao();
    });
});

function ajustarFormulario() {
    $('#spinner-loader').hide();
    let nomeRequisitante = $('inp:nomeRequisitante').val();
    let inputInfosEmprestimo = $('#section-informacoes-emprestimo').find('input');

    //hide elements from INTEGRA
    $($('div.well')[0]).hide();
    $('#RequesterInfo').hide();
    $('#ContainerAttach').hide();
    $('#divStatisticsTable').hide(); 
    $(document.getElementById('customBtn_Solicitação Encaminhada')).hide();
    $('#btnCancel').hide();

    //hide elements from the form
    $('#div-search-emprestimo').hide();
    $('#section-informacoes-emprestimo').hide();
    $('#section-informacoes-arquivamento').hide();

    //show elements from the from
    if (!isMobile()) {
        $(`<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${nomeRequisitante}</strong></span>`).insertAfter('.title span.label.label-info.flow-title');
    }
    $('#search-Contrato').attr('style','display: block');

    //funções de manipulação / ajustes em formulário
    inputInfosEmprestimo.attr('readOnly',true);
    _.forEach(inputInfosEmprestimo, (input) => { input.setAttribute('required','N') });
}

/*--------------------------------------------------------*/
//FONTE DE DADOS: 0507 - Gestão Contratos - Contratos 0544
const url_proposta_credito = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJk-on2SMrY63ZwbuBiU44MOYF3TyTFRIyq--4qzkikFVH701aQ04kZaSrKCm1Zb8aQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Renegociações 0544
const url_renegociacao = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJmGmenH0D0HAtnft6eaDR2wJa3aMRZNUCE7nZYO0FCu-KwCcyIIhlWVUyiYA7EZNAA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Títulos 0544
const url_titulos = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJuYwiy9ozutn7LjorwHyI1Fal3dKBCzkbDf8QyVhsdoBqwG3pniEJ3l-VZiYQ4KcQA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Aditivo 0544
const url_aditivo = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJrr4YS@u4ZO8A49-our0S1IRvmcyxF2uQUVxccSkeEQk8us28EPjNmQSaPmqeLXGZA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Checklist 0544
const url_itens_checklist = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJl2fGXxzr6udHQyN8h5D9mi-9ZDi1caRmc1tDwhDlhKjH2NiL7ZB@dh7dt3x8-8kfg__`;
/*--------------------------------------------------------*/

/*--------------------------------------------------------*/
let informacoesEmprestimo = [];
let textosItensChecklist = [];
let checklistDaSolicitacao = [];

const linhasVinculadasChecklist = [{"Tipo": "Todas as Linhas","ListaLinhas":{"Linhas": [11,30,33,37,38,40,41,66,67,82,83,88,92,94,100,106,107,112,119,145,186,195,221,222,223,226,233,234,238,239,253,261,263,270,278,281,282,283,287,298,299,501,802,809,810,811,813,814,815,826,840,845,846,852,853,854,859,860,863,864,865,866,867,868,1502,1503,1504,1505,1506,1507,1603,1604,1605,1606,1607,1608,1609,1610,1611,1612,1615,1616,1617,1618,1619,1620,1622,1623,1624,1625,1626,1627,1628,1629,1632,1633,1634,1635,1636,1637,1638,1639,1900,1901,1903,1904,2102,10001,10002,10003,10004,10005,10009,10010,10015,10016,10017,10019,10020,10021,10022,10023,10024,10025,10028,10030,10031,10032,10033,10037,10038,10039,10040,10041,10042,10043,10044,10045,10046,10047,10048,10049,10050,10051,10054,10056,10057,10058,10061,10062,10063,10064,10065,10066,10067,10068,10069,10070,10071,10072,10073,10074,10075,10076,10077,10078,10079,10080,10081,10082,10083,10084,10085,10086,10089,10090,10091,10092,10093,10094,10095,10098,10099,10101,10102,10103,10104,10105,11000,11002,11003,11006,11007,11012,11013,11014,11016,11017,11018,11019,11020,11021,11022,11023,11026,11027,11028,11033,11034,11035,11036,11037,11038,11040,11041,11042,11045,11046,11047,11048,11050,11051,11052,11053,11056,11701,11702,11703,11704,11710,11711,11712,11713,11718,11720,11724,11725,11730,11731,11735,11736,11737,11740,11741,11742,11747,11748,11749,11751,11753,11756,11757,11758,11760,11761,11762,11763,11764,11765,11769,11774,11777,11778,11780,11781,11782,11783,11784,11785,11786,11787,11814,11815,11816,11819,11823,11824,11825,11827,10087,10088,11737,11788,11789,11727,11768,11795,11796,11797,11798,11799,11800,11801,11802,874,877,878,1800,11767]}},{"Tipo": "Acordo Judicial","ListaLinhas":{"Linhas": [211,267,299,830,850,870,871]}},{"Tipo": "Adiantamento de Produção","ListaLinhas":{"Linhas": [11,222,238,239,10004,10062]}},    {"Tipo": "Agropecuário, Custeio e Investimento","ListaLinhas":{"Linhas": [811,815,1616,1622,10084,10085,10086,11823,11824]}},{"Tipo": "Antecipação Performada / Mista de Rece,veis","ListaLinhas":{"Linhas": [840,845,846,11701,11702,11751]}},{"Tipo": "Antecipação de IR","ListaLinhas":{"Linhas": [1900,11000]}},{"Tipo": "Aquisição de Imóvel","ListaLinhas":{"Linhas": [40,41,106,112,195,221,226,270,287,1502,1503,1610,1612,1624,1632,1633,1636,11051]}},{"Tipo": "Auto Crédito CP","ListaLinhas":{"Linhas": [1611,10033]}},{"Tipo": "Capital Social CP e CG","ListaLinhas":{"Linhas": [107,263,802,826,859,860,1606,1607,1628,10028,10082,10095,10104]}},{"Tipo": "Construção e Reforma","ListaLinhas":{"Linhas": [30,37,38,66,67,186,261,278,281,282,283,299,1615,1623,10009,10010,10063,10064,11012,11013,11014,11026,11027,11028,11769]}},{"Tipo": "Consórcio","ListaLinhas":{"Linhas": [10005,10072]}},{"Tipo": "Conta Garantida","ListaLinhas":{"Linhas": [223,2102,10001,10089,10090,10091,11045,11046,11052,11718,11737]}},{"Tipo": "Conta Garantida e Crédito Pessoal - Imóvel","ListaLinhas":{"Linhas": [11,30,33,37,38,66,67,82,83,88,92,94,100,107,119,145,186,222,223,233,234,238,239,253,261,263,278,281,282,283,298,299,501,802,809,810,811,813,814,815,826,840,845,846,852,853,854,859,860,863,864,865,866,867,868,1504,1505,1506,1507,1603,1604,1605,1606,1607,1608,1609,1611,1615,1616,1617,1618,1619,1620,1622,1623,1625,1626,1627,1628,1629,1634,1635,1637,1638,1639,1900,1901,1903,1904,2102,10001,10002,10003,10004,10005,10009,10010,10015,10016,10017,10019,10020,10021,10022,10023,10024,10025,10028,10030,10031,10032,10033,10037,10038,10039,10040,10041,10042,10043,10044,10045,10046,10047,10048,10049,10050,10051,10054,10056,10057,10058,10061,10062,10063,10064,10065,10066,10067,10068,10069,10070,10071,10072,10073,10074,10075,10076,10077,10078,10079,10080,10081,10082,10083,10084,10085,10086,10089,10090,10091,10092,10093,10094,10095,10098,10099,10101,10102,10103,10104,10105,11000,11002,11003,11006,11007,11012,11013,11014,11016,11017,11018,11019,11020,11021,11022,11023,11026,11027,11028,11033,11034,11035,11036,11037,11038,11040,11041,11042,11045,11046,11047,11048,11050,11051,11052,11053,11056,11701,11702,11703,11704,11710,11711,11712,11713,11718,11720,11724,11725,11730,11731,11735,11736,11737,11740,11741,11742,11747,11748,11749,11751,11753,11756,11757,11758,11760,11761,11762,11763,11764,11765,11769,11774,11777,11778,11780,11781,11782,11783,11784,11785,11786,11787,11814,11815,11816,11819,11823,11824,11825,11827]}},{"Tipo": "Conta Garantida e Crédito Pessoal - Veículo","ListaLinhas":{"Linhas": [11,30,33,37,38,40,41,66,67,82,83,88,92,94,100,106,107,112,119,145,186,195,221,222,223,226,233,234,238,239,253,261,263,270,278,281,282,283,287,298,299,501,802,809,810,811,813,814,815,826,840,845,846,852,853,854,859,860,863,864,865,866,867,868,1502,1503,1504,1505,1506,1507,1603,1604,1605,1606,1607,1608,1609,1610,1611,1612,1615,1616,1617,1618,1619,1620,1622,1623,1624,1625,1626,1627,1628,1629,1632,1633,1634,1635,1636,1637,1638,1639,1900,1901,1903,1904,2102,10001,10002,10003,10004,10005,10009,10010,10015,10016,10017,10019,10020,10021,10022,10023,10024,10025,10028,10030,10031,10032,10033,10037,10038,10039,10040,10041,10042,10043,10044,10045,10046,10047,10048,10049,10050,10051,10054,10056,10057,10058,10061,10062,10063,10064,10065,10066,10067,10068,10069,10070,10071,10072,10073,10074,10075,10076,10077,10078,10079,10080,10081,10082,10083,10084,10085,10086,10089,10090,10091,10092,10093,10094,10095,10098,10099,10101,10102,10103,10104,10105,11000,11002,11003,11006,11007,11012,11013,11014,11016,11017,11018,11019,11020,11021,11022,11023,11026,11027,11028,11033,11034,11035,11036,11037,11038,11040,11041,11042,11045,11046,11047,11048,11050,11051,11052,11053,11056,11701,11702,11703,11704,11710,11711,11712,11713,11718,11720,11724,11725,11730,11731,11735,11736,11737,11740,11741,11742,11747,11748,11749,11751,11753,11756,11757,11758,11760,11761,11762,11763,11764,11765,11769,11774,11777,11778,11780,11781,11782,11783,11784,11785,11786,11787,11814,11815,11816,11819,11823,11824,11825,11827]}},{"Tipo": "Desconto Cheques Conta Vinculada","ListaLinhas":{"Linhas": [1902,11726,11727,11768,11795,11796,11797,11798,11799,11800,11801,11802,11820]}},{"Tipo": "Empreendedor CP CG","ListaLinhas":{"Linhas": [82,88,92,863,864,1634,1635,10025,10094,10101,10102]}},{"Tipo": "Eventos","ListaLinhas":{"Linhas": [1603, 1627, 10030]}},{"Tipo": "Imposto","ListaLinhas":{"Linhas": [33,83,119,145,233,234,501,866,867,1619,1620,10015,10016,10073]}},{"Tipo": "Investidor","ListaLinhas":{"Linhas": [10017,10074,10075,10076,10077,10092,10093,11019,11020,11021,11035,11036,11037,11038,11056]}},{"Tipo": "Mais","ListaLinhas":{"Linhas": [1604,1605,1626,10031,10032]}},{"Tipo": "Máquinas e Equipamentos Novos","ListaLinhas":{"Linhas": [1618,1637,10050,10051,10057,11047,11048]}},{"Tipo": "Máquinas e Equipamentos Usados","ListaLinhas":{"Linhas": [854,865,1617,10048,10049,10054,10056,10058,11006,11007,11735,11778,11782,11783,11786,11787,11825]}},{"Tipo": "Prospecção","ListaLinhas":{"Linhas": [809,810,868,1608,1609,1625,1901,10023,10024,10078,10079,11022,11023,11040,11041,11042]}},{"Tipo": "Veículo Novo","ListaLinhas":{"Linhas": [100,298,813,10037,10038,10045,10046,10047,10098,11002]}},{"Tipo": "Veículo Usado","ListaLinhas":{"Linhas": [94,814,10039,10040,10041,10042,10043,10044,10099,11003]}}];
/*--------------------------------------------------------*/

const limparInformcoesFormulario = async () => {
    $('#section-informacoes-emprestimo').hide();
    $('#section-informacoes-validacoes').hide();
    $('#section-informacoes-garantias').hide();
    $('#ContainerAttach').hide();

    let inputInfosEmprestimo = $('#section-informacoes-emprestimo').find('input');
    await _.forEach(inputInfosEmprestimo, (input) => { $(input).val('') });

    $('#div-informacoes-emprestimo-sixthRow').find('#switchPendenciaExcecao').remove();
    $('inp:possuiPendenciaExcecao').val('');
    $('inp:dadosDaPendenciaExcecao').val('');

    await _.forEach($('#div-informacoes-verificacoes').children(), (elemento) => { $(elemento).remove() });
    
    await _.forEach($('#div-informacoes-garantias').children(), (elemento) => { $(elemento).find('div.col-xs-12').remove(), $(elemento).find('h6.text-gray-dark.border-bottom.border-gray.pb-2').remove() });

    informacoesEmprestimo = [];
    textosItensChecklist = [];
    checklistDaSolicitacao = [];
    garantias = [];
}

const verificarQualRequisicaoRealizar = () => {
    spinnerLoaderOn();
    feedbackInput('remove','numero',false);
    
    let tipo = $('inp:tipo').val();
    switch (tipo) {
        case 'Proposta de Crédito':
            buscarinformacoesEmprestimo(url_proposta_credito,'solicitação');
            break;
        case 'Termo Aditivo':
            buscarinformacoesEmprestimo(url_aditivo,'solicitação');
            break;
        case 'Renegociação':
            buscarinformacoesEmprestimo(url_renegociacao,'solicitação');
            break;
        case 'Borderô de Desconto':
            buscarinformacoesEmprestimo(url_titulos,'solicitação');
            break;
    }
}

const buscarinformacoesEmprestimo = async (url,info) => {
    let numeroEmprestimo = $('inp:numero').val();

    if(!numeroEmprestimo) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Preencha um dado para que possamos iniciar a busca!');
        return;
    }

    try {
        const {
            data
        } = await axios.post(url, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            informacoesEmprestimo = data.success[0];
            buscarGarantiasAvalista(numeroEmprestimo,info);
        } else {
            spinnerLoaderOff();
            feedbackInput('add','numero',true,'invalid','Não encontramos esta informação. Ajuste e tente novamente!');
        }
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

const buscarchecklistDaSolicitacao = async () => {
    try {
        const {
            data
        } = await axios.post(url_itens_checklist);
        if (data && data.success && data.success[0]) {
                textosItensChecklist = data.success;
        }
        gerarDocumentosReferenteSolicitacao();
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

const gerarDocumentosReferenteSolicitacao = async () => {
    //Remover Documentos Atuais
    await _.forEach($('#customizedUpload span.badge'), (docList) => { captureModule.capture.removeDoc($(docList).attr('cod')) });
    //Buscar Documentos e Tratar Obrigatoriedade
    let token = $('#inpToken').val(), tipo = $('inp:tipo').val(), prestamista = informacoesEmprestimo.fields.Prestamista;
    const requestListOfDocuments = {
        url: url_regra_negocio, method: 'POST', headers: { 'Authorization': token, 'content-type': 'application/json' },
        data: [{
          "DsFieldName": "tipo","DsValue": tipo
        },{
          "DsFieldName": "seguroPrestamista","DsValue": prestamista
        }]
    }
  
    try {
        let { data } = await axios(requestListOfDocuments);
        const documentos = JSON.parse(data[0].DsReturn);
        
        await $.each(documentos, (key, value) => {
            captureModule.capture.addDoc(value.Document);
        });
        inputarInformacoesGeradas();
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

const inputarInformacoesGeradas = async () => {
    let tipo = $('inp:tipo').val();
    let valorEmMoeda = await transformarEmFormatoMoeda(informacoesEmprestimo.fields.Valor);

    $('inp:conta').val(informacoesEmprestimo.txt);
    $('inp:associado').val(informacoesEmprestimo.fields.Associado);
    $('inp:linhaDeCredito').val(informacoesEmprestimo.fields.DescricaoLinhaCredito);
    $('inp:valor').val(valorEmMoeda);
    $('inp:finalidade').val(informacoesEmprestimo.fields.Finalidade);
    $('inp:dataSolicitacao').val(informacoesEmprestimo.fields.DataSolicitacao);
    $('inp:dataAprovacao').val(informacoesEmprestimo.fields.DataAprovacao);
    $('inp:parcelas').val(informacoesEmprestimo.fields.Parcelas);
    $('inp:seguroPrestamista').val(informacoesEmprestimo.fields.Prestamista);
    $('inp:tipoVencimento').val(informacoesEmprestimo.fields.TipoVencimento);
    $('inp:diaVencimento').val(informacoesEmprestimo.fields.DiaVencimento);
    $('inp:carenciaPrimParc').val(informacoesEmprestimo.fields.Carencia);
    $('inp:jurosNaCarencia').val(informacoesEmprestimo.fields.JurosNaCarencia);
    $('inp:financiarIof').val(informacoesEmprestimo.fields.FinanciarIOF);

    tipo === 'Renegociação' ? $('inp:numeroContratoAnteriorMae').val(informacoesEmprestimo.fields.PropostaAnterior) : tipo === 'Borderô de Desconto' ? $('inp:numeroContratoAnteriorMae').val(informacoesEmprestimo.fields.PropostaPai) : null;

    if (tipo === 'Borderô de Desconto') {
        let valorTituloEmMoeda = await transformarEmFormatoMoeda(informacoesEmprestimo.fields.ValorTitulo);
        $('inp:valorTitulo').val(valorTituloEmMoeda);
        $('inp:dataVencimentoTitulo').val(informacoesEmprestimo.fields.DataVencimentoTitulo);
        $('inp:totalParcelasTitulo').val(informacoesEmprestimo.fields.ParcelasTitulo);
    }

    $('inp:possuiPendenciaExcecao').val('Não');
    
    _.isEmpty(garantias) ? gerarCheckListDeValidacao() /*Estruturar*/ : montarEstruturaSectionGarantias('solicitação');
}

const gerarCheckListDeValidacao = async () => {
    let linhaDeCredito = informacoesEmprestimo.fields.LinhaCredito;
    let agrupamentosDaLinhaDeCredito = [];

    await _.forEach(linhasVinculadasChecklist, (agrupamento) => { _.indexOf(agrupamento.ListaLinhas.Linhas,parseInt(linhaDeCredito)) != -1 ? agrupamentosDaLinhaDeCredito.push(agrupamento.Tipo) : null });
    await _.forEach(textosItensChecklist, (agrupamento) => { _.forEach(agrupamentosDaLinhaDeCredito, (item) => { item == agrupamento.cod ? _.indexOf(checklistDaSolicitacao, agrupamento.txt) == -1 ? checklistDaSolicitacao.push(agrupamento.txt) : null : null }) });
        
    await _.isEmpty(checklistDaSolicitacao) ? $('#div-informacoes-verificacoes').append($(alert('Não há checklist de verificação para essa solicitação. Caso queira cadastrar um checklist, entrar em contato com a Unidade Administrativa!'))) : _.forEach(checklistDaSolicitacao, (item) => { $('#div-informacoes-verificacoes').append($(divFlexWithCheck(item))) });
    apresentarInformacoesGeradas();
}

const apresentarInformacoesGeradas = () => {
    let tipo = $('inp:tipo').val();

    switch (tipo) {
        case 'Proposta de Crédito':
        case 'Termo Aditivo':
            $('#div-numero-contrato-anterior-pai').hide();
            $('#div-informacoes-emprestimo-fifthRow').hide();
            break;
        case 'Renegociação':
            $('#div-numero-contrato-anterior-pai').show();
            $('#div-informacoes-emprestimo-fifthRow').hide();
            break;
        case 'Borderô de Desconto':
            $('#div-numero-contrato-anterior-pai').show();
            $('#div-informacoes-emprestimo-fifthRow').show();
            break;
    }

    $('#div-informacoes-emprestimo-sixthRow').prepend($(inputSwitchPendenciaExcecao()));
    $('#inputPendenciaExcecao').hide();
    $('#div-informacoes-emprestimo-seventhRow').hide();

    $('#section-informacoes-emprestimo').show();
    $('#section-informacoes-garantias').show();
    $('#div-informacoes-verificacoes').hide();
    $('#section-informacoes-validacoes').show();
    $('#collapse-verificacao').show();

    $('#annex').hide();
    $('#ContainerAttach').show();

    $(document.getElementById('customBtn_Solicitação Encaminhada')).show();

    spinnerLoaderOff();
}

const verificarNecessidadePreenchimentoPendenciaExcecao = (check) => {
    switch (check) {
        case true:
            $('inp:possuiPendenciaExcecao').val('Sim');
            $('inp:dadosDaPendenciaExcecao').val('');
            $('inp:dadosDaPendenciaExcecao')[0].setAttribute('required','S');
            $('#div-informacoes-emprestimo-seventhRow').show();
            $($('#areaPendenciaExcecao').children()[0]).append(`<div id="invalid-feedback-pendencia-excecao" class="invalid-feedback">Retiramos a obrigatoriedade de todos os documentos. Sua solicitação passará por validação do seu Gerente de Agência!</div>`);
            $('#invalid-feedback-pendencia-excecao').show();
            _.forEach($('div#invalid-feedback-envio-requisicao'), (invalidFeedbackRequest) => { $(invalidFeedbackRequest).remove() });
            break;    
        case false:
            $('inp:possuiPendenciaExcecao').val('Não');
            $('inp:dadosDaPendenciaExcecao').val('');
            $('inp:dadosDaPendenciaExcecao')[0].setAttribute('required','N');
            $('#div-informacoes-emprestimo-seventhRow').hide();
            $('#invalid-feedback-pendencia-excecao').remove();
            break;
    }
}

const verificarTratamentoObrigacoesDasInformacoesParaEnvioDaSolicitacao = () => {
    _.forEach($('div#invalid-feedback-envio-requisicao'), (invalidFeedbackRequest) => { $(invalidFeedbackRequest).remove() });
    let possuiPendenciaExcecao = $('inp:possuiPendenciaExcecao').val();
    let tipo = $('inp:tipo').val();
    let docsPendentes = $('#customizedUpload span.badge-secondary').map((index, element) => $(element).text().match(/Obrigat/gi));

    if (possuiPendenciaExcecao === 'Não') {
        switch (tipo) {
            case 'Proposta de Crédito':
            case 'Renegociação':
                if (docsPendentes.length > 0) {
                    $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Contrato, Parecer e Prestamista (Caso tenha seguro prestamista) Devem ser Anexados!</div>`);
                    $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                    $('#invalid-feedback-envio-requisicao').show();
                } else {
                    $('#switchPendenciaExcecao').remove();
                    doAction('Solicitação Encaminhada',false,false);
                }
                break;
            case 'Borderô de Desconto':
                if (docsPendentes.length > 0) {
                    $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`);
                    $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                    $('#invalid-feedback-envio-requisicao').show();
                } else {
                    $('#switchPendenciaExcecao').remove();
                    doAction('Solicitação Encaminhada',false,false);
                }
                break;
            case 'Termo Aditivo':
                if (docsPendentes.length > 0) {
                    $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`);
                    $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                    $('#invalid-feedback-envio-requisicao').show();
                } else {
                    $('#switchPendenciaExcecao').remove();
                    doAction('Solicitação Encaminhada',false,false);
                }
                break;
        }
    } else if (possuiPendenciaExcecao === 'Sim') {
        $('#switchPendenciaExcecao').remove();
        doAction('Solicitação Encaminhada',false,false);
    }
}