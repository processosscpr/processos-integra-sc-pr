$(document).ready(function() {
    ajustarFormulario();
});

function ajustarFormulario() {
    analisysModule.regularization.load();


    let nomeRequisitante = $('inp:nomeRequisitante').val();
    let numeroEmprestimo = $('inp:numero').val();

    //hide elements from INTEGRA
    $($('div.well')[0]).hide();
    $('#RequesterInfo').hide();
    $('div.span3.lateral-col').remove();
    $('div.span9.main-col').addClass('span12').removeClass('span9');
    $('div.span9.buttons-col').addClass('span12').removeClass('span9');
    $($('#btnsFormalization').children()[2]).hide();
    $('#annex button.btn.btn-primary.btn-small').hide();

    //hide elements from the form
    $('#section-informacoes-emprestimo div.row').hide();
    $('#section-informacoes-arquivamento').hide();

    //show elements from the from
    if (!isMobile()) {
        $(`<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${nomeRequisitante}</strong></span>`).insertAfter('.title span.label.label-info.flow-title');
    }

    //funções de manipulação / ajustes em formulário
    $('.form-group [XTYPE="TEXT"]').css('display', 'block');
    $('.form-group [XTYPE="SELECT"]').css('display', 'block');
    $('.form-group [XTYPE="DATA"]').css('display', 'block');
    $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');

    $('#tab1').prepend('<div class="alert alert-info" style="margin-right: 5px; margin-left: 5px"><small>Para regularizar os documentos reprovados clique no ícone: <span class="badge badge-secondary" style="padding: 2px 4px 2px 4px;"><i class="icon-white icon-upload"></i></span></small></div>');
    _.forEach($('#tab1').children().children(), (itensAbaConferencia) => $($(itensAbaConferencia).children()[2]).addClass('criterios'));
    
    $('inp:possuiPendenciaExcecao').val() === 'Não' ? $('#areaPendenciaExcecao').hide() : $('#areaPendenciaExcecao').show();

    spinnerLoaderOnAnalyse();
    buscarGarantiasAvalista(numeroEmprestimo,'análise');
}