$(document).ready(function() {
    $('.form-integra input[type="text"]').addClass('form-control');
    $('.form-integra input[type="text"]').width('');
    $('.form-integra table input[type="text"]').css('width', 'auto');
    $('.form-integra select').addClass('form-control');
    $('.form-integra select').width('100%');
    $('.form-integra select').width('');
    $('.form-integra table select').css('width', 'auto');
    $('.form-integra textarea').addClass('form-control');
    $('.form-integra textarea').width('');
    $('.form-integra table textarea').css('width', 'auto');
});

/*--------------------------------------------------------*/
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Aval 0544
const url_garantia_aval = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJk42lAvQND1FGZ6ukvAAvl@aUPmysHe1mGVMNn3XFgiCz1N-1dEbkj6KqD6H7eJNsg__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Cheque 0544
const url_garantia_cheque = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJrFPqI0VaQtLJx32NpxMxdrg75iPNv@PNsULdlCgohGyC@Bhpjiultfr9MvEBAvYRw__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Cartão 0544
const url_garantia_cartao = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJg5vMWAPa1uOTLlJXZI9jdlWD6Sgz3S97a5f-3SZxE24xRRIsf5eGM06Ih2F7qqxpQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Capital 0544
const url_garantia_capital = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJlDJaJeH5nBfTw87QD5ggpXWGC-rRI1tMd3wV9kaG-E2GXGGR@URccvS3umSJn8Bbg__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Veículo 0544
const url_garantia_veiculos = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJmATgrgiqWxxNGf5kBOr2dSyhdUthfpWk6--jWMoiOB8zF1BaRPQrPqHWGNM3hCaIQ__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Imóvel 0544
const url_garantia_imovel = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJk-sOy4dBZfw0WDPm-QbEK7H06kqKj3ukJPTjbBBH6UpccwA2Kz0Pomyq1BVMQzaMg__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Aplicação 0544
const url_garantia_aplicacao_financeira = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJrwIKRoU8YXgB9l-jyByIFYDX4CCIG6tC3Rcm@wPXVXRW2Ll2dK@EqHK@DXmX8e6KA__`;
//FONTE DE DADOS: 0507 - Gestão Contratos - Garantia Outras 0544
const url_garantia_outras = `https://bpm.e-unicred.com.br/api/json/datasource/get/1.0/qw0Xk6xWKL563BI8VvBqJti-UNjbNWdAWnD6GcRNlo2-sfPXiXJuRSNvOsFiMd33q5k0OBHE221XM3b-cOs9ug__`;
//REGRA DE NEGÓCIO: 0507 - Listagem e Obrigatoriedade de Documentos - Gestão Contratos de Crédito
const url_regra_negocio = `https://bpm.e-unicred.com.br/api/1.0/businessrules/82/evaluate`;
/*--------------------------------------------------------*/

/*--------------------------------------------------------*/
let garantias = [];

const spinnerLoaderOn = () => $('#spinner-loader').show();
const spinnerLoaderOff = () => $('#spinner-loader').hide();

const spinnerLoaderOnAnalyse = () => $('#section-informacoes-emprestimo h5.text-dark.border-bottom.border-gray.pb-2').append(` <span id="spinner-loader-analyse" class="spinner-border spinner-border-sm" role="status"></span>`);
const spinnerLoaderOffAnalyse = () => $('#spinner-loader-analyse').remove();
/*--------------------------------------------------------*/

const isMobile = () => {
    let userAgent = navigator.userAgent.toLowerCase();
    if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
        return true; 
}

//STATUS: remove ou add | IDENTIFICADOR: identificador do input | CLASSE: invalid ou valid | FEEDBACK: mensagem de feedback | EMPTYINPUT: true ou false
const feedbackInput = (status,identificador,emptyInput,classe,feedback) => {
    status === 'remove' ? $(`inp:${identificador}`).removeClass('is-invalid').removeClass('is-valid') : classe === 'valid' ? $(`inp:${identificador}`).addClass('is-valid') : $(`inp:${identificador}`).addClass('is-invalid');
    status === 'remove' ? $($(`inp:${identificador}`).parent()).find('#feedback-input').remove() : classe === 'valid' ? $($(`inp:${identificador}`).parent()).append(`<div class="valid-feedback" id="feedback-input">${feedback}</div>`) : $($(`inp:${identificador}`).parent()).append(`<div class="invalid-feedback" id="feedback-input">${feedback}</div>`);
    $('#feedback-input').show();
    emptyInput ? $(`inp:${identificador}`).val('') : null;
}

//Função dinâmica utilizada em todas as atividades
const buscarGarantiasAvalista = async (numeroEmprestimo,info) => {
    let dataAux = [];
    
    try {
        const {
            data
        } = await axios.post(url_garantia_aval, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            $.each(data.success, (i, item) => {
                dataAux = {
                    'tipo': 'Avalista',
                    'dados': item.fields
                }
                garantias.push(dataAux);
            });
        }
        buscarGarantiaCheque(numeroEmprestimo,info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCheque = async (numeroEmprestimo,info) => {
    let dataAux = [];
    
    try {
        const {
            data
        } = await axios.post(url_garantia_cheque, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            $.each(data.success, (i, item) => {
                dataAux = {
                    'tipo': 'Cheque',
                    'dados': item.fields
                }
                garantias.push(dataAux);
            });
        }
        buscarGarantiaCartao(numeroEmprestimo,info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCartao = async (numeroEmprestimo,info) => {
    let dataAux = [];
    
    try {
        const {
            data
        } = await axios.post(url_garantia_cartao, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            $.each(data.success, (i, item) => {
                dataAux = {
                    'tipo': 'Cartão',
                    'dados': item.fields
                }
                garantias.push(dataAux);
            });
        }
        buscarGarantiaCapital(numeroEmprestimo,info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaCapital = async (numeroEmprestimo,info) => {
    let dataAux = [];
    
    try {
        const {
            data
        } = await axios.post(url_garantia_capital, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            $.each(data.success, (i, item) => {
                dataAux = {
                    'tipo': 'Capital',
                    'dados': item.fields
                }
                garantias.push(dataAux);
            });
        }
        buscarGarantiaVeiculo(numeroEmprestimo,info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaVeiculo = async (numeroEmprestimo,info) => {
    let dataAux = [];
    
    try {
        const {
            data
        } = await axios.post(url_garantia_veiculos, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            $.each(data.success, (i, item) => {
                dataAux = {
                    'tipo': 'Veículo',
                    'dados': item.fields
                }
                garantias.push(dataAux);
            });
        }
        buscarGarantiaImovel(numeroEmprestimo,info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaImovel = async (numeroEmprestimo,info) => {
    let dataAux = [];
    
    try {
        const {
            data
        } = await axios.post(url_garantia_imovel, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            $.each(data.success, (i, item) => {
                dataAux = {
                    'tipo': 'Imóvel',
                    'dados': item.fields
                }
                garantias.push(dataAux);
            });
        }
        buscarGarantiaAplicacao(numeroEmprestimo,info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaAplicacao = async (numeroEmprestimo,info) => {
    let dataAux = [];
    
    try {
        const {
            data
        } = await axios.post(url_garantia_aplicacao_financeira, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            $.each(data.success, (i, item) => {
                dataAux = {
                    'tipo': 'Aplicação Financeira',
                    'dados': item.fields
                }
                garantias.push(dataAux);
            });
        }
        buscarGarantiaOutras(numeroEmprestimo,info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

//Função dinâmica utilizada em todas as atividades
const buscarGarantiaOutras = async (numeroEmprestimo,info) => {
    let dataAux = [];
    
    try {
        const {
            data
        } = await axios.post(url_garantia_outras, {
            'inpnumero': numeroEmprestimo
        });
        if (data && data.success && data.success[0]) {
            $.each(data.success, (i, item) => {
                dataAux = {
                    'tipo': 'Outras',
                    'dados': item.fields
                }
                garantias.push(dataAux);
            });
        }
        info === 'solicitação' ? buscarchecklistDaSolicitacao() : info === 'regularização exceção' || info === 'verificar exceção' ? gerarDocumentosReferenteRegularizacaoExcecao(info) : montarEstruturaSectionGarantias(info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

const gerarDocumentosReferenteRegularizacaoExcecao = async (info) => {
    let documentosJaImportados = [];
    _.forEach($('#tblFile td.docType'), (tipoDocumentoImportado) => documentosJaImportados.push($(tipoDocumentoImportado).text()));

    //Adicionar Estrutura Formalization no Form
    captureModule.capture.buildDocsHtml();
    captureModule.capture.appendModule();
    captureModule.capture.uploadChange();
    //Buscar Documentos e Tratar Obrigatoriedade
    let token = $('#inpToken').val(), tipo = $('inp:tipo').val(), prestamista = $('inp:seguroPrestamista').val();
    const requestListOfDocuments = {
        url: url_regra_negocio, method: 'POST', headers: { 'Authorization': token, 'content-type': 'application/json' },
        data: [{
          "DsFieldName": "tipo","DsValue": tipo
        },{
          "DsFieldName": "seguroPrestamista","DsValue": prestamista
        }]
    }
  
    try {
        let { data } = await axios(requestListOfDocuments);
        const documentos = JSON.parse(data[0].DsReturn);
        
        await $.each(documentos, (key, value) => {
            documentosJaImportados.indexOf(value.Document) == -1 ? captureModule.capture.addDoc(value.Document) : null;
        });
        montarEstruturaSectionGarantias(info);
    } catch (error) {
        spinnerLoaderOff();
        feedbackInput('add','numero',true,'invalid','Ocorreu um erro para buscar a informação! Contate o suporte.');
        console.log(error);
    }
}

//Função dinâmica utilizada em todas as atividades
const montarEstruturaSectionGarantias = async (info) => {
    //AVALISTA
    if (_.findIndex(garantias, { 'tipo': 'Avalista' }) != -1) {
        $('#div-informacoes-garantias-avalistas').prepend($(sectionCollapse('far fa-id-badge','Avalistas')));
        $('#div-informacoes-garantias-resumo-avalistas').append($(cardResume(_.countBy(garantias, { 'tipo': 'Avalista' }).true,'Avalista(s)')));
    }
    //CHEQUE
    if (_.findIndex(garantias, { 'tipo': 'Cheque' }) != -1) {
        let totalChequeBruto = [], totalChequeLiquido = [];
        $('#div-informacoes-garantias-cheques').prepend($(sectionCollapse('fas fa-money-check-alt','Cheques')));
        $('#div-informacoes-garantias-resumo-cheques').append($(cardResume(_.countBy(garantias, { 'tipo': 'Cheque' }).true,'Cheque(s)')));
        //Iniciar Cálculo Somatório Cheque
        _.forEach(garantias, (garantia) => { garantia.tipo == 'Cheque' ? (totalChequeBruto.push(parseFloat(garantia.dados.ValorBruto)), totalChequeLiquido.push(parseFloat(garantia.dados.ValorLiquido))) : null } );
        $('#div-informacoes-garantias-resumo-cheques').append($(cardResume('Total Bruto:',transformarEmFormatoMoeda(_.sum(totalChequeBruto, Number).toFixed(2)))));
        $('#div-informacoes-garantias-resumo-cheques').append($(cardResume('Total Líquido:',transformarEmFormatoMoeda(_.sum(totalChequeLiquido, Number).toFixed(2)))));
    } 
    //CARTÃO
    if (_.findIndex(garantias, { 'tipo': 'Cartão' }) != -1) {
        let totalCartaoCredito = [], totalCartaoAntecipar = [], totalCartaoLiquido = [];
        $('#div-informacoes-garantias-cartoes').prepend($(sectionCollapse('far fa-credit-card','Créditos de Cartões')));
        $('#div-informacoes-garantias-resumo-cartoes').append($(cardResume(_.countBy(garantias, { 'tipo': 'Cartão' }).true,'Crédito(s) de Cartão(ões)')));
        //Iniciar Cálculo Somatório Cartão
        _.forEach(garantias, (garantia) => { garantia.tipo == 'Cartão' ? (totalCartaoCredito.push(parseFloat(garantia.dados.ValorCredito)), totalCartaoAntecipar.push(parseFloat(garantia.dados.ValorAntecipacao)), totalCartaoLiquido.push(parseFloat(garantia.dados.ValorLiquido))) : null } );
        $('#div-informacoes-garantias-resumo-cartoes').append($(cardResume('Total Crédito:',transformarEmFormatoMoeda(_.sum(totalCartaoCredito, Number).toFixed(2)))));
        $('#div-informacoes-garantias-resumo-cartoes').append($(cardResume('Total Antecipar:',transformarEmFormatoMoeda(_.sum(totalCartaoAntecipar, Number).toFixed(2)))));
        $('#div-informacoes-garantias-resumo-cartoes').append($(cardResume('Total Líquido:',transformarEmFormatoMoeda(_.sum(totalCartaoLiquido, Number).toFixed(2)))));
    }
    //CAPITAL
    if (_.findIndex(garantias, { 'tipo': 'Capital' }) != -1) {
        $('#div-informacoes-garantias-cotas').prepend($(sectionCollapse('fas fa-coins','Cota Capital')));
        $('#div-informacoes-garantias-resumo-cotas').append($(cardResume(_.countBy(garantias, { 'tipo': 'Capital' }).true,'Cota Capital')));
    }
    //VEÍCULO
    if (_.findIndex(garantias, { 'tipo': 'Veículo' }) != -1) {
        let totalValorVeiculo = [];
        $('#div-informacoes-garantias-veiculos').prepend($(sectionCollapse('fas fa-car','Veículos')));
        $('#div-informacoes-garantias-resumo-veiculos').append($(cardResume(_.countBy(garantias, { 'tipo': 'Veículo' }).true,'Veículo(s)')));
        //Iniciar Cálculo Somatório Veículo
        _.forEach(garantias, (garantia) => { garantia.tipo == 'Veículo' ? totalValorVeiculo.push(parseFloat(garantia.dados.ValorVeiculo)) : null } );
        $('#div-informacoes-garantias-resumo-veiculos').append($(cardResume('Total:',transformarEmFormatoMoeda(_.sum(totalValorVeiculo, Number).toFixed(2)))));
    }
    //IMÓVEL
    if (_.findIndex(garantias, { 'tipo': 'Imóvel' }) != -1) {
        let totalValorImovel = [];
        $('#div-informacoes-garantias-imoveis').prepend($(sectionCollapse('fas fa-home','Imóveis')));
        $('#div-informacoes-garantias-resumo-imoveis').append($(cardResume(_.countBy(garantias, { 'tipo': 'Imóvel' }).true,'Imóvel(is)')));
        //Iniciar Cálculo Somatório Imóvel
        _.forEach(garantias, (garantia) => { garantia.tipo == 'Imóvel' ? totalValorImovel.push(parseFloat(garantia.dados.ValorImovel)) : null } );
        $('#div-informacoes-garantias-resumo-imoveis').append($(cardResume('Total:',transformarEmFormatoMoeda(_.sum(totalValorImovel, Number).toFixed(2)))));
    }
    //APLICAÇÃO FINANCEIRA
    if (_.findIndex(garantias, { 'tipo': 'Aplicação Financeira' }) != -1) {
        let totalValorAplicacaoFinanceira = [];
        $('#div-informacoes-garantias-aplicacoes').prepend($(sectionCollapse('fas fa-hand-holding-usd','Aplicações Financeiras')));
        $('#div-informacoes-garantias-resumo-aplicacoes').append($(cardResume(_.countBy(garantias, { 'tipo': 'Aplicação Financeira' }).true,'Aplicação(ões)')));
        //Iniciar Cálculo Somatório Aplicação Fincanceira
        _.forEach(garantias, (garantia) => { garantia.tipo == 'Aplicação Financeira' ? totalValorAplicacaoFinanceira.push(parseFloat(garantia.dados.ValorGarantia)) : null } );
        $('#div-informacoes-garantias-resumo-aplicacoes').append($(cardResume('Total:',transformarEmFormatoMoeda(_.sum(totalValorAplicacaoFinanceira, Number).toFixed(2)))));
    }
    //OUTRAS
    if (_.findIndex(garantias, { 'tipo': 'Outras' }) != -1) {
        let totalValorOutras = [];
        $('#div-informacoes-garantias-outras').prepend($(sectionCollapse('fas fa-box-open','Outras')));
        $('#div-informacoes-garantias-resumo-outras').append($(cardResume('Quantidade Outras:',_.countBy(garantias, { 'tipo': 'Outras' }).true)));
        //Iniciar Cálculo Somatório Outras
        _.forEach(garantias, (garantia) => { garantia.tipo == 'Outras' ? totalValorOutras.push(parseFloat(garantia.dados.ValorGarantia)) : null } );
        $('#div-informacoes-garantias-resumo-outras').append($(cardResume('Total:',transformarEmFormatoMoeda(_.sum(totalValorOutras, Number).toFixed(2)))))
    }
     montarEstruturaCardsDeGarantias(info);
}

//Função dinâmica utilizada em todas as atividades
const montarEstruturaCardsDeGarantias = async (info) => {
    await _.forEach(garantias, (garantia) => {
        switch (garantia.tipo) {
            case 'Avalista':
                $('#div-informacoes-garantias-cards-avalistas').append($(cardGarantiaAvalista(garantia.dados)));
                break;
            case 'Cheque':
                $('#div-informacoes-garantias-cards-cheques').append($(cardGarantiaCheque(garantia.dados)));
                break;
            case 'Cartão':
                $('#div-informacoes-garantias-cards-cartoes').append($(cardGarantiaCartao(garantia.dados)));
                break;
            case 'Capital':
                $('#div-informacoes-garantias-cards-cotas').append($(cardGarantiaCota(garantia.dados)));
                break;
            case 'Veículo':
                $('#div-informacoes-garantias-cards-veiculos').append($(cardGarantiaVeiculo(garantia.dados)));
                break;
            case 'Imóvel':
                $('#div-informacoes-garantias-cards-imoveis').append($(cardGarantiaImovel(garantia.dados)));
                break;
            case 'Aplicação Financeira':
                $('#div-informacoes-garantias-cards-aplicacoes').append($(cardGarantiaAplicacao(garantia.dados)));
                break;
            case 'Outras':
                $('#div-informacoes-garantias-cards-outras').append($(cardGarantiaOutras(garantia.dados)));
                break;
        }
    });
    info === 'solicitação' ? gerarCheckListDeValidacao() : restringirInformacoesBaseadasNoTipo(info);
}

//Função dinâmica utilizada em atividades de análise / regularização
const restringirInformacoesBaseadasNoTipo = (info) => {
    let tipo = $('inp:tipo').val();

    $('#section-informacoes-emprestimo div.row').show();

    switch (tipo) {
        case 'Proposta de Crédito':
        case 'Termo Aditivo':
            $('#div-numero-contrato-anterior-pai').hide();
            $('#div-informacoes-emprestimo-fifthRow').hide();
            break;
        case 'Renegociação':
            $('#div-numero-contrato-anterior-pai').show();
            $('#div-informacoes-emprestimo-fifthRow').hide();
            break;
        case 'Borderô de Desconto':
            $('#div-numero-contrato-anterior-pai').show();
            $('#div-informacoes-emprestimo-fifthRow').show();
            break;
    }

    $('#section-informacoes-garantias').show();

    info === 'regularização exceção' ? ( $('#div-informacoes-emprestimo-sixthRow').prepend($(inputSwitchPendenciaExcecao())), $('#inputPendenciaExcecao').hide(), $('#customSwitchPendenciaExcecao').attr('checked',true) ) : null;

    spinnerLoaderOffAnalyse();
}

//Função utilizada somente na atividade de solicitação
const collapseVerificacao = (elemento) => {
    $(elemento).attr('target') === 'waiting-collapse-integra' ? (
        $('#div-informacoes-verificacoes').show(),
        $(elemento).attr('target','in-collapse-integra'),
        $(elemento).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
    ) : (
        $('#div-informacoes-verificacoes').hide(),
        $(elemento).attr('target','waiting-collapse-integra'),
        $(elemento).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
    )
}

//Função utilizada em todas as atividades
const collapseGarantia = (elemento) => {
    $(elemento).attr('target') === 'waiting-collapse-integra' ? (
        $($($(elemento).parent().parent()).find('div.row')[0]).show(),
        $($($(elemento).parent().parent()).find('div.row')[1]).show(),
        $(elemento).attr('target','in-collapse-integra'),
        $(elemento).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
    ) : (
        $($($(elemento).parent().parent()).find('div.row')[0]).hide(),
        $($($(elemento).parent().parent()).find('div.row')[1]).hide(),
        $(elemento).attr('target','waiting-collapse-integra'),
        $(elemento).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
    )
}

//Função utilizada somente na atividade de solicitação
const verifyCheck = (elemento) => {
    let elementoClass = $(elemento).find('i').attr('class');
    $($(elemento).parent().parent()).removeClass('list-group-item-success').removeClass('list-group-item-danger').removeClass('list-group-item-info');

    if (elementoClass.indexOf('fa-check') != -1) {
        $($(elemento).parent().parent()).addClass('list-group-item-success');
        $(elemento).addClass('btn-primary');
        $($($(elemento).parent()).find('button')[1]).removeClass('btn-danger');
        $($($(elemento).parent()).find('button')[2]).removeClass('btn-info');
    } else if (elementoClass.indexOf('fa-times') != -1) {
        $($(elemento).parent().parent()).addClass('list-group-item-danger');
        $(elemento).addClass('btn-danger');
        $($($(elemento).parent()).find('button')[0]).removeClass('btn-primary');
        $($($(elemento).parent()).find('button')[2]).removeClass('btn-info');
    } else if (elementoClass.indexOf('fa-ban') != -1) {
        $($(elemento).parent().parent()).addClass('list-group-item-info');
        $(elemento).addClass('btn-info');
        $($($(elemento).parent()).find('button')[0]).removeClass('btn-primary');
        $($($(elemento).parent()).find('button')[1]).removeClass('btn-danger');
    }
}

//Função utilizada em todas as atividades
const transformarEmFormatoMoeda = (valor) => {
    return parseFloat(valor).toLocaleString('pt-br', {minimumFractionDigits: 2});
}

//Função utilizada em todas as atividades
const mascaraIdentificador = (valor) => {
    if (valor.length <= 11) {
        return valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g,"\$1.\$2.\$3\-\$4");    
    } else {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");
    }   
}

/*--------------------------------------------------------*/
//COMPONENTES - CARDS
const cardResume = (label,value) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card text-white bg-primary">
            <div class="card-body">
                <strong class="text-gray-dark">${label} ${value}</strong>
            </div>
        </div>
    </div>
`)

const cardGarantiaAvalista = (dados) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${dados.Pessoa}</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(dados.IdentificacaoPessoal)}</p>
            </div>
        </div>
    </div>
`)

const cardGarantiaCheque = (dados) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Banco:</b> ${dados.Banco} <b>Ag:</b> ${dados.Agencia} <b>Conta:</b> ${dados.Conta}</p>
                <p class="card-text"><b>Número Cheque:</b> ${dados.Cheque}</p>
                <p class="card-text"><b>Data Vencimento:</b> ${dados.DataVencimentoCheque}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Bruto:</b> ${transformarEmFormatoMoeda(dados.ValorBruto)}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Líquido:</b> ${transformarEmFormatoMoeda(dados.ValorLiquido)}</p>
            </div>
        </div>
    </div>
`)

const cardGarantiaCartao = (dados) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Adquirente:</b> ${dados.Adquirente} <b>Antecip:</b> ${dados.PercentualAntecipacao}%</p>
                <p class="card-text"><b>Data Crédito:</b> ${dados.DataCredito}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Crédito:</b> ${transformarEmFormatoMoeda(dados.ValorCredito)}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Antecipar:</b> ${transformarEmFormatoMoeda(dados.ValorAntecipacao)}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> <b>Líquido:</b> ${transformarEmFormatoMoeda(dados.ValorLiquido)}</p>
            </div>
        </div>
    </div>
`)

const cardGarantiaCota = (dados) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(dados.ValorCapital)}</p>
            </div>
        </div>
    </div>
`)

const cardGarantiaVeiculo = (dados) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${dados.Pessoa}</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(dados.IdentificacaoPessoal)}</p>
                <p class="card-text"><span><i class="fas fa-car-side"></i></span> ${dados.Veiculo} - ${dados.Marca} | ${dados.Placa}</p>
                <p class="card-text"><b>Renavam:</b> ${dados.Renavam} <b>Chassi:</b> ${dados.Chassi}</p>
                <p class="card-text"><span><i class="far fa-calendar-alt"></i></span> <b>Fabr:</b> ${dados.AnoFabricacao} <span><i class="far fa-calendar-alt"></i> <b>Modelo:</b> ${dados.AnoModelo}</span></p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(dados.ValorVeiculo)}</p>
            </div>
        </div>
    </div>
`)

const cardGarantiaImovel = (dados) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text text-truncate"><span><i class="fas fa-user"></i></span> ${dados.Pessoa}</p>
                <p class="card-text"><span><i class="far fa-address-card"></i></span> ${mascaraIdentificador(dados.IdentificacaoPessoal)}</p>
                <p class="card-text"><span><i class="fas fa-map-marker-alt"></i></span> [${dados.TipoImovel}] ${dados.DsEndereco}, ${dados.DsNumero} - ${dados.DsComplemento}, ${dados.DsBairro}, ${dados.DsCidade} - ${dados.DsUF} - ${dados.DsCEP}</p>
                <p class="card-text"><b>Matrícula:</b> ${dados.MatriculaImovel} <b>Registro:</b> ${dados.RegistroImovel}</p>
                <p class="card-text"><b>Descrição:</b> ${dados.DsImovel}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(dados.ValorImovel)}</p>
            </div>
        </div>
    </div>
`)

const cardGarantiaAplicacao = (dados) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Conta:</b> ${dados.Conta} <b>Produto:</b> ${dados.Produto}</p>
                <p class="card-text"><b>Título:</b> ${dados.Titulo}</p>
                <p class="card-text"><span><i class="far fa-calendar-alt"></i></span> <b>Aplicação:</b> ${dados.DataAplicacao} <span><i class="far fa-calendar-alt"></i> <b>Vencimento:</b> ${dados.DataVencimento}</span></p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(dados.ValorGarantia)}</p>
            </div>
        </div>
    </div>
`)

const cardGarantiaOutras = (dados) => (`
    <div class="col-xs-12 col-md-3" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card border-primary">
            <div class="card-body">
                <p class="card-text"><b>Descrição Garantia:</b> ${dados.DsGarantia}</p>
                <p class="card-text"><span><i class="fas fa-dollar-sign"></i></span> ${transformarEmFormatoMoeda(dados.ValorGarantia)}</p>
            </div>
        </div>
    </div>
`)

//COMPONENTES - SECTION COLLAPSE
const sectionCollapse = (classe,tipo) => (`
    <h6 class="text-gray-dark border-bottom border-gray pb-2"><span><i class="${classe}"></i></span> ${tipo}<a class="text-dark float-right" onclick="collapseGarantia(this)" target="waiting-collapse-integra"><i class="fas fa-chevron-down"></i></a></h6>
`)

//COMPONENTES - DIV FLEX WITH CHECK
const divFlexWithCheck = (text) => (`
    <div class="d-flex flex-row bd-highlight mb-2">                          
        <div class="p-2 bd-highlight">
            <button onclick="verifyCheck(this)" type="button" class="btn"><i class="fas fa-check"></i></button>
            <button onclick="verifyCheck(this)" type="button" class="btn"><i class="fas fa-times"></i></button>
            <button onclick="verifyCheck(this)" type="button" class="btn"><i class="fas fa-ban"></i></button>
        </div>
        <div class="p-2 bd-highlight"><h6 class="pt-2">${text}</h6></div>
    </div>
`)

//COMPONENTES - ALERT
const alert = (text) => (`
    <div class="col-xs-12 col-md-12" style="margin-top: 10px; margin-bottom: 10px">
        <div class="alert alert-info" role="alert">
            ${text}
        </div>
    </div>
`)

//COMPONENTES - SWITCH
const inputSwitchPendenciaExcecao = () => (`
    <div id="switchPendenciaExcecao" class="col-xs-12 col-md-2">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="customSwitchPendenciaExcecao">
            <label class="custom-control-label font-weight-bold pt-1" for="customSwitchPendenciaExcecao">Possui Pendência / Exceção</label>
        </div>
    </div>
`)
/*--------------------------------------------------------*/