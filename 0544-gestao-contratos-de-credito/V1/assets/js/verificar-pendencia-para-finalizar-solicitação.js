$(document).ready(function() {
    ajustarFormulario();

    $(document.getElementById('customBtn_Encaminhar Pendências para Finalização da Solicitação'))[0].onclick = null;
    $(document.getElementById('customBtn_Encaminhar Pendências para Finalização da Solicitação')).click(function() {
        verificarTratamentoObrigacoesDasInformacoesParaEnvioDaSolicitacao();
    });
});

function ajustarFormulario() {
    let nomeRequisitante = $('inp:nomeRequisitante').val();
    let numeroEmprestimo = $('inp:numero').val();

    //hide elements from INTEGRA
    $($('div.well')[0]).hide();
    $('#RequesterInfo').hide();
    $('div.span3.lateral-col').remove();
    $('div.span9.main-col').addClass('span12').removeClass('span9');
    $('div.span9.buttons-col').addClass('span12').removeClass('span9');

    //hide elements from the form
    $('#section-informacoes-emprestimo div.row').hide();
    $('#section-informacoes-arquivamento').hide();

    //show elements from the from
    if (!isMobile()) {
        $(`<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${nomeRequisitante}</strong></span>`).insertAfter('.title span.label.label-info.flow-title');
    }

    //funções de manipulação / ajustes em formulário
    $('.form-group [XTYPE="TEXT"]').css('display', 'block');
    $('.form-group [XTYPE="SELECT"]').css('display', 'block');
    $('.form-group [XTYPE="DATA"]').css('display', 'block');
    $('.form-group [XTYPE="TEXTAREA"]').css('display', 'block');

    spinnerLoaderOnAnalyse();
    buscarGarantiasAvalista(numeroEmprestimo,'verificar exceção');
}

const verificarTratamentoObrigacoesDasInformacoesParaEnvioDaSolicitacao = () => {
    let tipo = $('inp:tipo').val();
    let docsPendentes = $('#customizedUpload span.badge-secondary').map((index, element) => $(element).text().match(/Obrigat/gi));
    
    switch (tipo) {
        case 'Proposta de Crédito':
        case 'Renegociação':
            if (docsPendentes.length > 0) {
                $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Contrato, Parecer e Prestamista (Caso tenha seguro prestamista) Devem ser Anexados!</div>`);
                $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                $('#invalid-feedback-envio-requisicao').show();
            } else {
                doAction('Encaminhar Pendências para Finalização da Solicitação',false,false);
            }
            break;
        case 'Borderô de Desconto':
            if (docsPendentes.length > 0) {
                $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`);
                $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                $('#invalid-feedback-envio-requisicao').show();
            } else {
                doAction('Encaminhar Pendências para Finalização da Solicitação',false,false);
            }
            break;
        case 'Termo Aditivo':
            if (docsPendentes.length > 0) {
                $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`);
                $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                $('#invalid-feedback-envio-requisicao').show();
            } else {
                doAction('Encaminhar Pendências para Finalização da Solicitação',false,false);
            }
            break;
    }
}