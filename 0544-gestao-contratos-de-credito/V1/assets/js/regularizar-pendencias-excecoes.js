$(document).ready(function() {
    ajustarFormulario();

    $(document).on('change', '#customSwitchPendenciaExcecao', e => {
        verificarNecessidadePreenchimentoPendenciaExcecao(e.target.checked);
    });

    $(document.getElementById('customBtn_Novas Informações Encaminhadas'))[0].onclick = null;
    $(document.getElementById('customBtn_Novas Informações Encaminhadas')).click(function() {
        verificarTratamentoObrigacoesDasInformacoesParaEnvioDaSolicitacao();
    });

    $('#inpDsReasonInputReason').focusout(element => {
        $('#switchPendenciaExcecao').remove();
    });
});

function ajustarFormulario() {
    let nomeRequisitante = $('inp:nomeRequisitante').val();
    let numeroEmprestimo = $('inp:numero').val();

    //hide elements from INTEGRA
    $($('div.well')[0]).hide();
    $('#RequesterInfo').hide();
    $('div.span3.lateral-col').remove();
    $('div.span9.main-col').addClass('span12').removeClass('span9');
    $('div.span9.buttons-col').addClass('span12').removeClass('span9');

    //hide elements from the form
    $('#section-informacoes-emprestimo div.row').hide();
    $('#section-informacoes-arquivamento').hide();

    //show elements from the from
    if (!isMobile()) {
        $(`<span style="margin-left: 0.4em" class="label label-secondary"><strong> Requisitante: ${nomeRequisitante}</strong></span>`).insertAfter('.title span.label.label-info.flow-title');
    }

    //funções de manipulação / ajustes em formulário
    $('.form-group [XTYPE="TEXT"]').css('display', 'block');
    $('.form-group [XTYPE="SELECT"]').css('display', 'block');
    $('.form-group [XTYPE="DATA"]').css('display', 'block');

    $($('#areaPendenciaExcecao').children()[0]).append(`<div id="invalid-feedback-regularizar-excecao" class="invalid-feedback">Como a soliictação está selecionada como pendência / exceção, os documentos não são obrigatórios, porém, a requisição necessita de aprovação do GA ou Diretoria. Retiramos da lista dos documentos o que você já importou!</div>`);
    $('#invalid-feedback-regularizar-excecao').show();

    spinnerLoaderOnAnalyse();
    buscarGarantiasAvalista(numeroEmprestimo,'regularização exceção');
}

const verificarNecessidadePreenchimentoPendenciaExcecao = (check) => {
    $('#switchPendenciaExcecao').addClass('col-md-9').removeClass('col-md-2');
    $('#invalid-feedback-regularizar-excecao').remove();
    $('#valid-feedback-regularizar-excecao').remove();
    switch (check) {
        case true:
            $('inp:possuiPendenciaExcecao').val('Sim');
            $('inp:dadosDaPendenciaExcecao').val('');
            $('inp:dadosDaPendenciaExcecao')[0].setAttribute('required','S');
            $('#div-informacoes-emprestimo-seventhRow').show();
            $($('#areaPendenciaExcecao').children()[0]).append(`<div id="invalid-feedback-pendencia-excecao" class="invalid-feedback">Retiramos a obrigatoriedade de todos os documentos. Sua solicitação passará por validação do seu Gerente de Agência!</div>`);
            $('#invalid-feedback-pendencia-excecao').show();
            _.forEach($('div#invalid-feedback-envio-requisicao'), (invalidFeedbackRequest) => { $(invalidFeedbackRequest).remove() });
            break;    
        case false:
            $('inp:possuiPendenciaExcecao').val('Não');
            $('inp:dadosDaPendenciaExcecao').val('');
            $('inp:dadosDaPendenciaExcecao')[0].setAttribute('required','N');
            $('#div-informacoes-emprestimo-seventhRow').hide();
            $('#invalid-feedback-pendencia-excecao').remove();
            $($('#switchPendenciaExcecao').children()[0]).append(`<div id="valid-feedback-regularizar-excecao" class="valid-feedback">Como você selecionou que não há pendência / exceção, deve-se anexar os documentos obrigatórios para encaminhar à área BackOffice realizar a liberação!</div>`);
            $('#valid-feedback-regularizar-excecao').show();
            break;
    }
}

const verificarTratamentoObrigacoesDasInformacoesParaEnvioDaSolicitacao = () => {
    _.forEach($('div#invalid-feedback-envio-requisicao'), (invalidFeedbackRequest) => { $(invalidFeedbackRequest).remove() });
    let possuiPendenciaExcecao = $('inp:possuiPendenciaExcecao').val();
    let tipo = $('inp:tipo').val();
    let docsPendentes = $('#customizedUpload span.badge-secondary').map((index, element) => $(element).text().match(/Obrigat/gi));

    if (possuiPendenciaExcecao === 'Não') {
        switch (tipo) {
            case 'Proposta de Crédito':
            case 'Renegociação':
                if (docsPendentes.length > 0) {
                    $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Contrato, Parecer e Prestamista (Caso tenha seguro prestamista) Devem ser Anexados!</div>`);
                    $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                    $('#invalid-feedback-envio-requisicao').show();
                } else {
                    doAction('Novas Informações Encaminhadas',false,true)
                }
                break;
            case 'Borderô de Desconto':
                if (docsPendentes.length > 0) {
                    $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Borderô e Formalização de Garantia Devem ser Anexados!</div>`);
                    $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                    $('#invalid-feedback-envio-requisicao').show();
                } else {
                    doAction('Novas Informações Encaminhadas',false,true)
                }
                break;
            case 'Termo Aditivo':
                if (docsPendentes.length > 0) {
                    $('#customizedUpload').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-left: 5px; font-size: 80%; color: #dc3545;"><i class="fas fa-exclamation-triangle"></i> Termo Garantia Deve se Anexado!</div>`);
                    $('#buttons').append(`<div id="invalid-feedback-envio-requisicao" style="width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;">Há Documentos e Informações Obrigatórias. Verifique!</div>`);
                    $('#invalid-feedback-envio-requisicao').show();
                } else {
                    doAction('Novas Informações Encaminhadas',false,true)
                }
                break;
        }
    } else if (possuiPendenciaExcecao === 'Sim') {
        doAction('Novas Informações Encaminhadas',false,true)
    }
}